package org.cocos2dx.javascript.Extension.Data;

import org.json.JSONArray;

public class ClickData {
    public int movecount = 0;
    public boolean onClick;
    public int clickCount = 0;
    public long startTime = 0;
    public long duration = 0;
    public long MAX_DURATION = 400;
    public  int mode;
}
