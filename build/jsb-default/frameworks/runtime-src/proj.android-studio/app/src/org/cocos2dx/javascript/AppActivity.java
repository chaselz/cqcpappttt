/****************************************************************************
Copyright (c) 2015-2016 Chukong Technologies Inc.
Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.javascript;

import org.cocos2dx.javascript.Extension.AnalyticsHelper;
import org.cocos2dx.javascript.Extension.Data.ClickData;
import org.cocos2dx.javascript.Extension.WindowPermissionCheck;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;
import org.cocos2dx.lib.Cocos2dxWebViewHelper;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;

import android.util.Log;
import android.view.MotionEvent;

import android.view.Surface;
import android.view.View;
import android.widget.FrameLayout;


import com.azhon.appupdate.config.UpdateConfiguration;
import com.azhon.appupdate.listener.OnDownloadListener;
import com.azhon.appupdate.manager.DownloadManager;
import com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton;
import com.flurry.android.FlurryAgent;
import com.igexin.sdk.IUserLoggerInterface;
import com.igexin.sdk.PushManager;
import com.jumbo.cqcp.BuildConfig;
import com.jumbo.cqcp.R;

import com.vanniktech.rxpermission.RealRxPermission;

import java.io.File;


public class AppActivity extends Cocos2dxActivity implements WindowPermissionCheck.OnWindowPermissionListener,OnDownloadListener {

    private static final String TAG = "AppActivity";
    private static Activity app = null;

    private ClickData data = null;


    private static DragFloatActionButton btn = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Workaround in
        // https://stackoverflow.com/questions/16283079/re-launch-of-activity-on-home-button-but-only-the-first-time/16447508
        if (!isTaskRoot()) {
            // Android launched another instance of the root activity into an existing task
            // so just quietly finish and go away, dropping the user back into the activity
            // at the top of the stack (ie: the last state of this task)
            // Don't need to finish it again since it's finished in super.onCreate .
            return;
        }
//        UXCam.startWithKey("k8yywa5g3j9gz5p");
        if(BuildConfig.BUILD_TYPE.equals("release") ) {
            new FlurryAgent.Builder()
                    .withLogEnabled(true)
                    .build(this, "NQVNMXSD6XRCRR6DGQVF");
        }
        // DO OTHER INITIALIZATION BELOW
        SDKWrapper.getInstance().init(this);
        app = this;
        data = new ClickData();
        RealRxPermission.getInstance(app)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe();
        InitFloatBtn();
        initSdk();
    }

    private void initSdk() {
        Log.d(TAG, "initializing sdk...");
        PushManager.getInstance().initialize(this);
        if (BuildConfig.DEBUG) {
            //切勿在 release 版本上开启调试日志
            PushManager.getInstance().setDebugLogger(this, new IUserLoggerInterface() {

                @Override
                public void log(String s) {
                    Log.i("PUSH_LOG", s);
                }
            });
        }
    }

    @Override
    public Cocos2dxGLSurfaceView onCreateView() {
        Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
        // TestCpp should create stencil buffer
        glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
        SDKWrapper.getInstance().setGLSurfaceView(glSurfaceView, this);

        return glSurfaceView;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SDKWrapper.getInstance().onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        SDKWrapper.getInstance().onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SDKWrapper.getInstance().onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SDKWrapper.getInstance().onActivityResult(requestCode, resultCode, data);
        WindowPermissionCheck.onActivityResult(this,requestCode,resultCode,data,this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        SDKWrapper.getInstance().onNewIntent(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SDKWrapper.getInstance().onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SDKWrapper.getInstance().onStop();
    }

    @Override
    public void onBackPressed() {
        SDKWrapper.getInstance().onBackPressed();
        super.onBackPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        SDKWrapper.getInstance().onConfigurationChanged(newConfig);
        if(app == null)
            return;

        switch (app.getWindowManager().getDefaultDisplay().getRotation()){
            case Surface.ROTATION_270:
            case Surface.ROTATION_90:
                sentToCocosJS("cc.game.emit('DeviceOrientation','L')");
                break;
            case Surface.ROTATION_0:
                sentToCocosJS("cc.game.emit('DeviceOrientation','V')");
                break;
        }

        super.onConfigurationChanged(newConfig);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        SDKWrapper.getInstance().onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        SDKWrapper.getInstance().onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        SDKWrapper.getInstance().onStart();
        super.onStart();
    }

    /** 请求文件读写权限。*/
    private void requestStoragePermission() {
        String[] permissionsGroup=new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,Manifest.permission.READ_SMS,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

//        RxPermissions rxPermissions = new RxPermissions(app);
//        rxPermissions
//                .request(Manifest.permission.CAMERA)
//                .subscribe(granted -> {
//                    if (granted) { // Always true pre-M
//                        // I can control the camera now
//                    } else {
//                        // Oups permission denied
//                    }
//                });

    }

    /***
     * 文字複製
     * @param str
     */
    public static void copy(final String str){
        Runnable runnable = new Runnable() {
            public void run() {
                ClipboardManager mClipboardManager = (ClipboardManager)app.getSystemService(CLIPBOARD_SERVICE);
                mClipboardManager.setText(str);
            }
        };
        app.runOnUiThread(runnable);
    }

    /***
     * 取的App版本號
     * @return
     */
    public static String GetVersion(){
        return BuildConfig.VERSION_NAME.split("-")[0]+"."+BuildConfig.VERSION_CODE;
    }

    /***
     * 取得遊戲平台
     * @return
     */
    public static int GetPlatform(){
        return BuildConfig.platform;
    }

    public static void setOrientation(String dir){
        if(dir.equals("V"))
            ((AppActivity)(SDKWrapper.getInstance().getContext())).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        else
            ((AppActivity)(SDKWrapper.getInstance().getContext())).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public static void setTouchBtnActive(String active){
        if(active.equals("show"))
            showTouchBtn();
        else
            hideTouchBtn();
    }

    public static void reloadWebView(int index){
        Cocos2dxWebViewHelper.reload(index);
    }

    public static void showTouchBtn(){
        Log.e("showTouchBtn","我有被打開ㄛ");
        app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.VISIBLE);
                btn.bringToFront();
            }
        });

    }

    public static void hideTouchBtn(){
        app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.GONE);
                btn.bringToFront();
            }
        });
    }

    public static int GetNetType() {
        int netType = 0;
        ConnectivityManager connMgr = (ConnectivityManager) getContext()
                .getSystemService(getContext().CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo == null) {
            netType = -1;
        } else {
            int nType = networkInfo.getType();
            if (nType == ConnectivityManager.TYPE_MOBILE) {
                if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
                    netType = 3;
                } else {
                    netType = 2;
                }
            } else if (nType == ConnectivityManager.TYPE_WIFI) {
                netType = 1;
            }
        }
        return netType;
    }

    public static void sentToCocosJS(final String value){
        Cocos2dxGLSurfaceView.getInstance().queueEvent(new Runnable() {
            @Override
            public void run() {
                Log.w("sentToCocosJS",value);
                Cocos2dxJavascriptJavaBridge.evalString(value);
            }
        });
    }

    private void InitFloatBtn(){
        btn = new DragFloatActionButton(app.getApplicationContext());

        btn.setClickable(true);
        btn.setImageResource(R.drawable.home);
        btn.setMaxHeight(12);
        btn.setMaxWidth(12);
        btn.setMinimumHeight(12);
        btn.setMinimumWidth(12);
        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {//当前状态
                    case MotionEvent.ACTION_DOWN:
                        data.startTime = System.currentTimeMillis();
                        data.clickCount++;
                        data.movecount = 0;
                        data.onClick=true;
                        break;
                    case MotionEvent.ACTION_MOVE:

                        data.movecount++;
                        if(data.movecount > 6)
                            data.onClick = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        if(data.onClick) {
                            long time = System.currentTimeMillis() - data.startTime;
                            data.duration += time;

                            if(data.clickCount == 3){
                                if(data.duration <= data.MAX_DURATION){
                                    data.clickCount = 0;
                                    data.duration = 0;
                                }
                            }else if(data.clickCount == 1){
                                Handler handler = new Handler();

                                Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        if(data.clickCount ==1) {
                                            ShowBackAlert();
                                        }
                                        data.clickCount = 0;
                                        data.duration = 0;
                                    }
                                };
                                handler.postDelayed(runnable,data.MAX_DURATION);

                            }
                        }else{
                            data.clickCount = 0;
                            data.duration = 0;
                        }
                    default:
                        break;
                }
                return false;//还回为true,说明事件已经完成了，不会再被其他事件监听器调用
            }
        });
        btn.bringToFront();

        FrameLayout.LayoutParams lParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);

        mFrameLayout.addView(btn,lParams);

        btn.setVisibility(View.GONE);
    }

    public static void appupdata(String url){
        /*
         * 整个库允许配置的内容
         * 非必选
         */
        UpdateConfiguration configuration = (UpdateConfiguration) new UpdateConfiguration()
                //输出错误日志
                .setEnableLog(true)
                //设置自定义的下载
                //.setHttpManager()
                //下载完成自动跳动安装页面
                .setJumpInstallPage(true)
                //设置对话框背景图片 (图片规范参照demo中的示例图)
                //.setDialogImage(R.drawable.ic_dialog)
                //设置按钮的颜色
                //.setDialogButtonColor(Color.parseColor("#E743DA"))
                //设置对话框强制更新时进度条和文字的颜色
                //.setDialogProgressBarColor(Color.parseColor("#E743DA"))
                //设置按钮的文字颜色
//                .setDialogButtonTextColor(Color.WHITE)
                //设置是否显示通知栏进度
                .setShowNotification(true)
                //设置是否提示后台下载toast
                .setShowBgdToast(true)
                //设置强制更新
                .setForcedUpgrade(true)
                //设置对话框按钮的点击监听
//                .setButtonClickListener(this)
                //设置下载过程的监听
                .setOnDownloadListener((OnDownloadListener) AppActivity.app);
        DownloadManager mamger = DownloadManager.getInstance(AppActivity.getContext());
        mamger.setApkName("jumboapp.apk")
                .setApkUrl(url)
                .setConfiguration(configuration)
                .setSmallIcon(R.mipmap.ic_launcher)
                .download();
    }

    /**
     * 顯示返回大廳的Alert
     */
    void ShowBackAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(this.getText(R.string.backhallstring))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // close app
                        sentToCocosJS("cc.game.emit('closeTouchBtn')");
                    }
                });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                }
        });

        // Create the AlertDialog object and return it
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }


    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailure() {
        sentToCocosJS(String.format("cc.game.emit('appdownloadcancel')"));
    }

    @Override
    public void start() {

    }

    @Override
    public void downloading(int max, int progress) {
        int curr = (int) (progress / (double) max * 100.0);
        sentToCocosJS(String.format("cc.game.emit('appdownload',%d)",curr));
    }

    @Override
    public void done(File apk) {
        sentToCocosJS(String.format("cc.game.emit('appdownloadcancel')"));
    }

    @Override
    public void cancel() {
        sentToCocosJS(String.format("cc.game.emit('appdownloadcancel')"));
    }

    @Override
    public void error(Exception e) {

    }
}
