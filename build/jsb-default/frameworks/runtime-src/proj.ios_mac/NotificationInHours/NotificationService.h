//
//  NotificationService.h
//  NotificationInHours
//
//  Created by Mark Hong on 2020/4/28.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
