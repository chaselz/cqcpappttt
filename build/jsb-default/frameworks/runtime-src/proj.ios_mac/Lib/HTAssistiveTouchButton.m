//
//  HTAssistiveTouchButton.m
//  HTAssistiveTouchButton
//
//  Created by Mark Hong on 2019/8/7.
//  Copyright © 2019 Mark Hong. All rights reserved.
//

#import "HTAssistiveTouchButton.h"
#import <objc/runtime.h>
@interface HTAssistiveTouchButton ()
    
    
@property (nonatomic, assign) CGPoint beginPosition;  // button触摸开始时的触摸点坐标
@property (nonatomic, assign) float offsetX;  // x坐标偏移量
@property (nonatomic, assign) float offsetY;  // y坐标偏移量
@property (nonatomic, assign) float kScreenHeight;  // 螢幕高度
@property (nonatomic, assign) float kScreenWidth;  // 螢幕寬度
@property (nonatomic, assign) float Insetsleft;  // 螢幕高度
@property (nonatomic, assign) float Insetsright;  // 螢幕寬度
@property (nonatomic, assign) float Insetstop;  // 螢幕高度
@property (nonatomic, assign) float Insetsdown;  // 螢幕寬度
@property (nonatomic, assign) Boolean isMoreClick;  // 是否多連續點擊
@property (nonatomic, assign) Boolean isClick;  // 是否多連續點擊
@end

@implementation HTAssistiveTouchButton
    

static void *clickKey = &clickKey;
    
- (void)setClickBlock:(void (^)(void))clickBlock{
    objc_setAssociatedObject(self, & clickKey, clickBlock, OBJC_ASSOCIATION_COPY);
}
    
- (void (^)(void))clickBlock{
    return objc_getAssociatedObject(self, &clickKey);
}
    
-(void)setOnclick:(void (^)(void))block{
    self.clickBlock = block;
    [self addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setMooreClick:(void (^)(void))block{
    self.moreclickBlock = block;
    [self addTarget:self action:@selector(moreclickBtn:withEvent:) forControlEvents:UIControlEventTouchDownRepeat];
}
    
- (void) clickBtn : (UIButton*) sender{
    if(!self.isMoving){
        if(!self.isClick){
            self.isClick = true;
            [self performSelector:@selector(ontClick) withObject:nil afterDelay:0.5];

        }
    }
}

- (void) ontClick{
    self.isClick = false;
    if(self.isMoreClick){
        self.isMoreClick = false;
        return;
    }
    
    self.clickBlock();
}

- (void) moreclickBtn : (UIButton*) sender withEvent:(UIEvent*)event{
    if(!self.isMoving){

        UITouch* touch = [[event allTouches] anyObject];
        if (touch.tapCount == 3) {
            self.isMoreClick = true;
            self.moreclickBlock();
        }
        
    }
}
    
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self initWHValue];
        self.childButtons = [[NSMutableArray alloc] init];
        
        [self addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(buttonDrag) forControlEvents:UIControlEventTouchDragInside];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
    }
    
    return self;
}
    
- (void)dealloc{
    NSLog(@"deallor me");
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[UIDevice currentDevice]endGeneratingDeviceOrientationNotifications];
}
    
- (void)initWHValue{
    if (@available(iOS 11.0, *)) {
        self.kScreenHeight = UIApplication.sharedApplication.keyWindow.safeAreaLayoutGuide.layoutFrame.size.height;
        self.kScreenWidth = UIApplication.sharedApplication.keyWindow.safeAreaLayoutGuide.layoutFrame.size.width;
        
        self.Insetsleft = UIApplication.sharedApplication.keyWindow.safeAreaInsets.left;
        self.Insetsright = UIApplication.sharedApplication.keyWindow.safeAreaInsets.right;
        self.Insetstop = UIApplication.sharedApplication.keyWindow.safeAreaInsets.top;
        self.Insetsdown = UIApplication.sharedApplication.keyWindow.safeAreaInsets.bottom;
    } else {
        self.kScreenHeight = [UIScreen mainScreen].bounds.size.height;
        self.kScreenWidth = [UIScreen mainScreen].bounds.size.width;
    }
}
    
- (void)orientationDidChange:(NSNotification *)notification{
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
    double rotation = 0;
    UIInterfaceOrientation statusBarOrientation;
    switch (currentOrientation) {
        case UIDeviceOrientationFaceDown:
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationUnknown:
        return;
        case UIDeviceOrientationPortrait:
        rotation = 0;
        statusBarOrientation = UIInterfaceOrientationPortrait;
        break;
        case UIDeviceOrientationPortraitUpsideDown:
        rotation = -M_PI;
        statusBarOrientation = UIInterfaceOrientationPortraitUpsideDown;
        break;
        case UIDeviceOrientationLandscapeLeft:
        rotation = M_PI_2;
        statusBarOrientation = UIInterfaceOrientationLandscapeRight;
        break;
        case UIDeviceOrientationLandscapeRight:
        rotation = -M_PI_2;
        statusBarOrientation = UIInterfaceOrientationLandscapeLeft;
        break;
    }
    //    CGAffineTransform transform = CGAffineTransformMakeRotation(rotation);
    
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        //要旋轉的物件
        //        [self setTransform:transform];
        [self initWHValue];
        [self backScreenEdge];
        
    } completion:nil];
}
    
- (void)addChild:(UIButton *)btn{
    [btn addTarget:self action:@selector(hideSubBtns) forControlEvents:(UIControlEvents)UIControlEventTouchUpInside];
    [self.childButtons addObject:btn];
}
    
    // 点击按钮
- (void)buttonClicked {
    
    if(self.childButtons.count == 0){
        [self hideSubBtns];
        return;
    }
    
    if (!self.isMoving) {
        
        if ([self.childButtons[0] isHidden]) {
            // 弹出子按钮
            for (UIButton *btn in self.childButtons) {
                [btn setCenter:self.center];
                btn.hidden = NO;
            }
            
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 [self setChildButtonsFrame];
                             }
                             completion:nil];
        } else {
            // 收起子按钮
            [self hideSubBtns];
        }
    }
}
    
    /// 收起子按钮
- (void)hideSubBtns{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         for (UIButton *btn in self.childButtons) {
                             [btn setCenter:self.center];
                         }
                     }
                     completion:^(BOOL finished) {
                         for (UIButton *btn in self.childButtons) {
                             btn.hidden = YES;
                         }
                     }];
}
    
    /// 拖拽按钮
- (void)buttonDrag {
    if (self.isMoving) {
        for (UIButton *btn in self.childButtons) {
            btn.hidden = YES;
        }
    }
}
    
    // 确定弹出后子按钮的位置
- (void)setChildButtonsFrame {
    if (self.frame.origin.x < self.kScreenWidth/2)
    {
        // 悬浮按钮位于屏幕左侧中部
        if (self.frame.origin.y >= 100 && self.frame.origin.y + self.frame.size.height <= self.kScreenHeight - 80)
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x + 80, self.center.y - 80)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x + 90, self.center.y)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x + 80, self.center.y + 80)];
                }
            }
        }
        // 悬浮按钮位于屏幕左上角
        else if (self.frame.origin.y < 100)
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x + 80, self.center.y)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x + 70, self.center.y + 70)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x, self.center.y + 80)];
                }
            }
        }
        // 悬浮按钮位于屏幕左下角
        else
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x, self.center.y - 80)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x + 70, self.center.y - 70)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x + 80, self.center.y)];
                }
            }
        }
    }
    else
    {
        // 悬浮按钮位于屏幕右侧中部
        if (self.frame.origin.y >= 100 && self.frame.origin.y + self.frame.size.height <= self.kScreenHeight - 80)
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x - 80, self.center.y - 80)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x - 90, self.center.y)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x - 80, self.center.y + 80)];
                }
            }
        }
        // 悬浮按钮位于屏幕右上角
        else if (self.frame.origin.y < 100)
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x - 80, self.center.y)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x - 70, self.center.y + 70)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x, self.center.y + 80)];
                }
            }
        }
        // 悬浮按钮位于屏幕右下角
        else
        {
            for (int i = 0; i < self.childButtons.count; i++) {
                if (0 == i) {
                    [self.childButtons[0] setCenter:CGPointMake(self.center.x, self.center.y - 80)];
                } else if (1 == i) {
                    [self.childButtons[1] setCenter:CGPointMake(self.center.x - 70, self.center.y - 70)];
                } else {
                    [self.childButtons[2] setCenter:CGPointMake(self.center.x - 80, self.center.y)];
                }
            }
        }
    }
}
    
    // 开始触摸按钮
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    self.isMoving = NO;  // 开始触摸时为非移动状态
    
    UITouch *touch = [touches anyObject];
    self.beginPosition = [touch locationInView:self];  // 触摸开始点的坐标
}
    
    // 按钮拖动事件
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    //    NSLog(@"按钮拖动事件");
    UITouch *touch = [touches anyObject];
    CGPoint currentPosition = [touch locationInView:self];  // 当前触摸点的坐标
    // 位置偏移量
    self.offsetX = currentPosition.x - self.beginPosition.x;
    self.offsetY = currentPosition.y - self.beginPosition.y;
    
    // 如果按钮偏移量很微小，视为没有移动
    if (self.offsetX > 1 || self.offsetX < -1 || self.offsetY > 1 || self.offsetY < -1) {
        self.isMoving = YES;
    }
    
    [self setCenter:CGPointMake(self.center.x + self.offsetX, self.center.y + self.offsetY)];  // 移动后的中心坐标
    // 按钮左右方向极限坐标
    if (self.center.x > self.kScreenWidth) {
        
        NSLog(@">");
        [self setCenter:CGPointMake(self.kScreenWidth, self.center.y)];
    }
    else if (self.center.x < self.Insetsleft +self.frame.size.width/2) {
        NSLog(@"<=");
        [self setCenter:CGPointMake(self.Insetsleft+self.frame.size.width/2, self.center.y)];
    }
    
    
    if (self.center.y > self.kScreenHeight) {// 按钮上下方向极限坐标
        NSLog(@"h >");
        [self setCenter:CGPointMake(self.center.x, self.kScreenHeight)];
    }
    else if (self.center.y < (self.frame.size.height/2 + 10)) {
        NSLog(@"h <=");
        [self setCenter:CGPointMake(self.center.x, self.frame.size.height/2 + 10)];
    }
    
}
    
    // 结束触摸按钮
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self backScreenEdge];
}
    
    //回到螢幕邊緣
- (void)backScreenEdge{
    // 悬浮按钮始终贴在屏幕的一侧
    if (self.center.x > self.kScreenWidth/2) {
        NSLog(@"吸附在右侧");
        // 吸附在右侧
        if (self.frame.origin.y + 40 + self.frame.size.height <= self.kScreenHeight) {
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 if (@available(iOS 11.0, *)) {
                                     [self setCenter:CGPointMake(self.kScreenWidth + self.Insetsleft - 15, self.center.y)];
                                 }else{
                                     [self setCenter:CGPointMake(self.kScreenWidth - 15, self.center.y)];
                                 }
                             }
                             completion:nil];
        }
        // 吸附在左侧
        else {
            NSLog(@"吸附在右下角");
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 if (@available(iOS 11.0, *)) {
                                     [self setCenter:CGPointMake(self.kScreenWidth + self.Insetsleft - 15, self.kScreenHeight - 15)];
                                 }else{
                                     [self setCenter:CGPointMake(self.kScreenWidth - 15, self.kScreenHeight - 15)];
                                 }
                                 
                             }
                             completion:nil];
            
        }
    }
    else {
        // 吸附在左侧
        NSLog(@"吸附在左侧2");
        if (self.frame.origin.y + 40 + self.frame.size.height <= self.kScreenHeight) {
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 [self setCenter:CGPointMake(self.Insetsleft, self.center.y)];
                             }
                             completion:nil];
            
        }
        // 吸附在左下角
        else {
            NSLog(@"吸附在左下角");
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^
             {
                 [self setCenter:CGPointMake(self.Insetsleft, self.kScreenHeight - 15)];
             }
                             completion:nil];
        }
    }
}
    
    
@end
