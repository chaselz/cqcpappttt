//
//  NotificationService.h
//  NotificationService
//
//  Created by Mark Hong on 2020/4/23.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
