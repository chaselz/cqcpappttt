/****************************************************************************
 Copyright (c) 2010-2013 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "SDKWrapper.h"
#import "platform/ios/CCEAGLView-ios.h"
#include "cocos/scripting/js-bindings/jswrapper/SeApi.h"
#import "Flurry.h"
#import <UserNotifications/UserNotifications.h>
#import <AVFoundation/AVFoundation.h>
#import "Utils.h"
// GTSDK 配置信息
#if Jumbo
#define kGtAppId @"tO8SR64ZPhAhvMmRhEXIR2"
#define kGtAppKey @"ccAsTFmhOb9KtGY4BVfru6"
#define kGtAppSecret @"quHlTYvInA9jtTK1y6Rt52"
#else
#define kGtAppId @"jdgls013GhAxxbWaBzuQO3"
#define kGtAppKey @"Lh0ZmwnGZO6Az42UQQpdZ3"
#define kGtAppSecret @"4CYdWcviV86F7r4XZ0OsD2"
#endif
using namespace cocos2d;

@implementation AppController

Application* app = nullptr;
static BOOL isALL;
BOOL isOPen = false;
NSString *dataStr = @"";

@synthesize window;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
#if !ForJumboa && !( Jumbo && !NDEBUG )
    // [ GTSDK ]：使用APPID/APPKEY/APPSECRENT启动个推
    NSLog(@"[ PushSDK ]  %@ - %@ - %@",kGtAppId,kGtAppKey,kGtAppSecret);
    [GeTuiSdk startSdkWithAppId:kGtAppId appKey:kGtAppKey appSecret:kGtAppSecret delegate:self];
    
    [self requestPushNotificationPermissions];
    
    // [ 参考代码，开发者注意根据实际需求自行修改 ] 注册VOIP
    [self voipRegistration];
#endif
    ///監聽推播事件
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    //Flurry 的 sdk 初始化
#if NDEBUG
    [Flurry startSession:@"JRMW96Q453GV4ZS5PV25"
    withSessionBuilder:[[[FlurrySessionBuilder new]
       withCrashReporting:YES]
       withLogLevel:FlurryLogLevelAll]];
#endif
    [[SDKWrapper getInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    // Add the view controller's view to the window and display.
    float scale = [[UIScreen mainScreen] scale];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    window = [[UIWindow alloc] initWithFrame: bounds];
    
    // cocos2d application instance
    app = new AppDelegate(bounds.size.width * scale, bounds.size.height * scale);
    app->setMultitouch(true);
    
    // Use RootViewController to manage CCEAGLView
    _viewController = [[RootViewController alloc]init];
#ifdef NSFoundationVersionNumber_iOS_7_0
    _viewController.automaticallyAdjustsScrollViewInsets = NO;
    _viewController.extendedLayoutIncludesOpaqueBars = NO;
    _viewController.edgesForExtendedLayout = UIRectEdgeAll;
#else
    _viewController.wantsFullScreenLayout = YES;
#endif
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
        selector:@selector(statusBarOrientationChanged:)
        name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    
    //run the cocos2d-x game scene
    app->start();
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleDeviceOrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    ///檢查是否有點擊推播訊息
    NSDictionary *remoteNotif = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    // Accept push notification when app is not open
    if (remoteNotif) {
       // [self handleRemoteNotification:application didReceiveRemoteNotification:remoteNotif];
    }
    
    return YES;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    // could not register a Push Notification token at this time.
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:-1];
    [[SDKWrapper getInstance] applicationWillResignActive:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:-1];
    [[SDKWrapper getInstance] applicationDidBecomeActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    [[SDKWrapper getInstance] applicationDidEnterBackground:application];
    app->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    [[SDKWrapper getInstance] applicationWillEnterForeground:application];
   
    app->applicationWillEnterForeground();
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SDKWrapper getInstance] applicationWillTerminate:application];
    delete app;
    app = nil;
}

#pragma mark - 远程通知(推送)回调

/// [ 系统回调 ] 远程通知注册成功回调，获取DeviceToken成功，同步给个推服务器
- (void)application:(UIApplication*)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)devToken
{
    [GeTuiSdk registerDeviceTokenData:devToken];
//     parse token bytes to string
    NSString *token = [Utils getHexStringForData:devToken];
    NSLog(@"[ PushSDK ] [ DeviceToken(NSString) ]: %@\n\n", token);
}

// MARK: - iOS 10+中收到推送消息
// iOS 10收到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {

    NSDictionary * userInfo = notification.request.content.userInfo;
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    NSString *key = [userInfo valueForKey:@"yourCustomKey"];
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        NSLog(@"iOS10 前台收到远程通知:{body:%@，key:%@}", body,key);

    } else {
        // 判断为本地通知
        NSLog(@"iOS10 前台收到本地通知:{\\\\nbody:%@，\\\\ntitle:%@,\\\\nsubtitle:%@,\\\\nbadge：%@，\\\\nsound：%@，\\\\nuserInfo：%@\\\\n}",body,title,subtitle,badge,sound,userInfo);
    }
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
}


/// [ 系统回调 ] iOS 10及以上 收到APNs推送并点击时触发
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    
    [self handleRemoteNotification:UIApplication.sharedApplication didReceiveRemoteNotification:response.notification.request.content.userInfo];
    NSLog(@"[ PushSDK ] [APNs] %@ \nTime:%@ \n%@",
          NSStringFromSelector(_cmd),
          response.notification.date,
          response.notification.request.content.userInfo);
    
    // [ GTSDK ]：将收到的APNs信息同步给个推统计
    [GeTuiSdk handleRemoteNotification:response.notification.request.content.userInfo];
    completionHandler();
}
#pragma mark - APP运行中接收到通知(推送)处理 - iOS 10以下版本收到推送
///點選推播選項的委派（當app有被開啟時）
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // app has received a push notification
    [self handleRemoteNotification:application didReceiveRemoteNotification:userInfo];
    NSLog(@"[ PushSDK ] [APNs] %@：%@", NSStringFromSelector(_cmd), userInfo);
    // [ GTSDK ]：将收到的APNs信息同步给个推统计
    [GeTuiSdk handleRemoteNotification:userInfo];
    
    // [ 参考代码，开发者注意根据实际需求自行修改 ] 根据APP需要自行修改参数值
    completionHandler(UIBackgroundFetchResultNewData);
}


#pragma mark - 用户通知(推送) _ 自定义方法
/// [ 系统回调 ] 收到静默通知。iOS 10以下收到APNs推送并点击时触发、APP在前台时收到APNs推送
///選擇推播選項的資料
-(void)handleRemoteNotification:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
 
// Do whatever you want here
    if([userInfo  objectForKey: @"mode"]){
         //创建词典对象，初始化长度为10
        NSMutableDictionary *pushDictionary = [NSMutableDictionary dictionaryWithCapacity:2];
         //向词典中动态添加数据
        [pushDictionary setObject:[userInfo valueForKey:@"mode"] forKey:@"mode"];
        [pushDictionary setObject:[userInfo valueForKey:@"value"] forKey:@"value"];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:pushDictionary options:0 error:0];
        dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //推送到cocos creator 裡
        std::string jsCallStr = cocos2d::StringUtils::format("cc.game.emit('%s','%s')" , "pushnotification",[dataStr UTF8String]);
        if(isOPen){
            se::ScriptEngine::getInstance()->evalString(jsCallStr.c_str());
        }
    }
    NSLog(@"%@",[userInfo descriptionInStringsFileFormat]);
}

///要通知需求的權限方法
- (void)requestPushNotificationPermissions
{
    // iOS 10+
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        
        switch (settings.authorizationStatus)
        {
            // User hasn't accepted or rejected permissions yet. This block shows the allow/deny dialog
            case UNAuthorizationStatusNotDetermined:
            {
                center.delegate = self;
                [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
                 {
                     if(granted)
                     {
                        dispatch_async(dispatch_get_main_queue(), ^{
                                     [[UIApplication sharedApplication] registerForRemoteNotifications];
                        });
                     }
                     else
                     {
                         // notify user to enable push notification permission in settings
                     }
                 }];
                break;
            }
            // the user has denied the permission
            case UNAuthorizationStatusDenied:
            {
                // notify user to enable push notification permission in settings
                break;
            }
            // the user has accepted; Register a PN token
            case UNAuthorizationStatusAuthorized:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                          [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                
                break;
            }
            default:
                break;
        }
    }];
}


#pragma mark - Cocos Creator 對接方法

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if(isALL)
        return UIInterfaceOrientationMaskAll;
    else
        return UIInterfaceOrientationMaskPortrait;
    
}

+ (void)backToPortrait{
    isALL = NO;
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

+ (void)backToAll{
    isALL = YES;
}

+(void)isOpenCocosApp{
    isOPen = true;
}

+ (NSString *)getPushNotificationData{
    return dataStr;
}

//螢幕轉動的委派事件
- (void)statusBarOrientationChanged:(NSNotification *)notification {
    CGRect bounds = [UIScreen mainScreen].bounds;
    float scale = [[UIScreen mainScreen] scale];
    float width = bounds.size.width * scale;
    float height = bounds.size.height * scale;
    Application::getInstance()->updateViewSize(width, height);
}

//裝置方向改變的處理
- (void)handleDeviceOrientationChange:(NSNotification *)notification{ UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
        std::string jsCallStr = cocos2d::StringUtils::format("");
    switch (deviceOrientation) {
        case UIDeviceOrientationFaceUp:
            NSLog(@"螢幕朝上平躺");
            break;
        case UIDeviceOrientationFaceDown:
            NSLog(@"螢幕朝下平躺");
            break;
        case UIDeviceOrientationUnknown:
            NSLog(@"未知方向");
            break;
        case UIDeviceOrientationLandscapeLeft:
            jsCallStr = cocos2d::StringUtils::format("cc.game.emit('%s','%s')" , "DeviceOrientation","L");
            NSLog(@"螢幕向左橫置");
            break;
        case UIDeviceOrientationLandscapeRight:
            jsCallStr = cocos2d::StringUtils::format("cc.game.emit('%s','%s')" , "DeviceOrientation","L");
            NSLog(@"螢幕向右橫置");
            break;
        case UIDeviceOrientationPortrait:
            NSLog(@"螢幕直立");
            jsCallStr = cocos2d::StringUtils::format("cc.game.emit('%s','%s')" , "DeviceOrientation","V");
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"螢幕直立，上下顛倒");
            break;
        default:
            NSLog(@"無法辨識");
            break;
            
    }
    if(jsCallStr.length() > 0 && isOPen)
        se::ScriptEngine::getInstance()->evalString(jsCallStr.c_str());
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


#pragma mark - VOIP 接入

/**
 * [ 参考代码，开发者注意根据实际需求自行修改 ] 注册VOIP服务
 *
 * 警告：以下为参考代码, 注意根据实际需要修改.
 *
 */
- (void)voipRegistration {
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    PKPushRegistry *voipRegistry = [[PKPushRegistry alloc] initWithQueue:mainQueue];
    voipRegistry.delegate = self;
    // Set the push type to VoIP
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

#pragma mark PKPushRegistryDelegate 协议方法

/// [ 系统回调 ] 系统返回VOIPToken，并提交个推服务器
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
    // [ GTSDK ]：（新版）向个推服务器注册 VoipToken
    [GeTuiSdk registerVoipTokenCredentials:credentials.token];
    NSLog(@"[ PushSDK ] [ VoipToken(NSData) ]: %@\n\n", credentials.token);
}

/**
 * [ 系统回调 ] 收到voip推送信息
 * 接收VOIP推送中的payload进行业务逻辑处理（一般在这里调起本地通知实现连续响铃、接收视频呼叫请求等操作），并执行个推VOIP回执统计
 */
- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    //  [ GTSDK ]：个推VOIP回执统计
    [GeTuiSdk handleVoipNotification:payload.dictionaryPayload];
    
    // [ 测试代码 ] 接受VOIP推送中的payload内容进行具体业务逻辑处理
    NSLog(@"[ PushSDK ] [ Voip Payload ]: %@, %@", payload, payload.dictionaryPayload);
    
    // 语音播报
    NSString* payloadStr = [payload.dictionaryPayload objectForKey:@"payload"];
    [self playAudioTitle:payloadStr];
}

// 语音播报
- (void)playAudioTitle:(NSString*) aText {
    // 后台语音播报需要设置
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];
    
    AVSpeechSynthesizer * av = [[AVSpeechSynthesizer alloc] init];
    AVSpeechUtterance * utterance = [[AVSpeechUtterance alloc]initWithString:aText];
    AVSpeechSynthesisVoice * voiceType = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];
    utterance.voice = voiceType;
    utterance.rate = 0.5;
    [av speakUtterance:utterance];
}

#pragma mark - GeTuiSdkDelegate

/// [ GTSDK回调 ] SDK启动成功返回cid
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId {
    NSLog(@"[ PushSDK ] [GTSdk RegisterClient]:%@", clientId);
}

/// [ GTSDK回调 ] SDK收到透传消息回调
- (void)GeTuiSdkDidReceivePayloadData:(NSData *)payloadData andTaskId:(NSString *)taskId andMsgId:(NSString *)msgId andOffLine:(BOOL)offLine fromGtAppId:(NSString *)appId {
    // [ GTSDK ]：汇报个推自定义事件(反馈透传消息)
    [GeTuiSdk sendFeedbackMessage:90001 andTaskId:taskId andMsgId:msgId];
    NSString *payloadMsg = [[NSString alloc] initWithBytes:payloadData.bytes length:payloadData.length encoding:NSUTF8StringEncoding];
    NSString *msg = [NSString stringWithFormat:@"Receive Payload: %@, taskId: %@, messageId: %@ %@", payloadMsg, taskId, msgId, offLine ? @"<离线消息>" : @""];
    NSLog(@"[ PushSDK ] [GTSdk ReceivePayload]:%@", msg);
}

/// [ GTSDK回调 ] SDK收到sendMessage消息回调
- (void)GeTuiSdkDidSendMessage:(NSString *)messageId result:(int)result {
    NSString *msg = [NSString stringWithFormat:@"Received sendmessage:%@ result:%d", messageId, result];
    NSLog(@"[ PushSDK ] [GeTuiSdk DidSendMessage]:%@\n\n",msg);

}

/// [ GTSDK回调 ] SDK运行状态通知
- (void)GeTuiSDkDidNotifySdkState:(SdkStatus)aStatus {
   
}

/// [ GTSDK回调 ] SDK设置推送模式回调
- (void)GeTuiSdkDidSetPushMode:(BOOL)isModeOff error:(NSError *)error {

}

- (void)GeTuiSdkDidOccurError:(NSError *)error {
    NSLog(@"[ PushSDK ] [GeTuiSdk GeTuiSdkDidOccurError]:%@\n\n",error.localizedDescription);
}

- (void)GeTuiSdkDidAliasAction:(NSString *)action result:(BOOL)isSuccess sequenceNum:(NSString *)aSn error:(NSError *)aError {
    /*
     参数说明
     isSuccess: YES: 操作成功 NO: 操作失败
     aError.code:
     30001：绑定别名失败，频率过快，两次调用的间隔需大于 5s
     30002：绑定别名失败，参数错误
     30003：绑定别名请求被过滤
     30004：绑定别名失败，未知异常
     30005：绑定别名时，cid 未获取到
     30006：绑定别名时，发生网络错误
     30007：别名无效
     30008：sn 无效 */
    if([action isEqual:kGtResponseBindType]) {
        NSLog(@"[ PushSDK ] bind alias result sn = %@, code = %@", aSn, @(aError.code));
    }
    if([action isEqual:kGtResponseUnBindType]) {
        NSLog(@"[ PushSDK ] unbind alias result sn = %@, code = %@", aSn, @(aError.code));
    }
}

- (void)GeTuiSdkDidSetTagsAction:(NSString *)sequenceNum result:(BOOL)isSuccess error:(NSError *)aError {
    /*
     参数说明
     sequenceNum: 请求的序列码
     isSuccess: 操作成功 YES, 操作失败 NO
     aError.code:
     20001：tag 数量过大（单次设置的 tag 数量不超过 100)
     20002：调用次数超限（默认一天只能成功设置一次）
     20003：标签重复
     20004：服务初始化失败
     20005：setTag 异常
     20006：tag 为空
     20007：sn 为空
     20008：离线，还未登陆成功
     20009：该 appid 已经在黑名单列表（请联系技术支持处理）
     20010：已存 tag 数目超限
     20011：tag 内容格式不正确
     */
    NSLog(@"[ PushSDK ] GeTuiSdkDidSetTagAction sequenceNum:%@ isSuccess:%@ error: %@", sequenceNum, @(isSuccess), aError);
}
@end
