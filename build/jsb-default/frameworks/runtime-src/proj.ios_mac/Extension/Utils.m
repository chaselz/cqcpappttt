//
//  Utils.m
//  GtSdkDemo
//
//  Created by ak on 2020/03/20.
//  Copyright © 2019 Gexin Interactive (Beijing) Network Technology Co.,LTD. All rights reserved.
//

#import "Utils.h"

@implementation Utils


+ (BOOL)PushModel {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"OffPushMode"];
}
+ (void)SetPushModel:(BOOL)mode {
    [[NSUserDefaults standardUserDefaults] setBool:mode forKey:@"OffPushMode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (NSString *)getHexStringForData:(NSData *)data {
    NSUInteger len = [data length];
    char *chars = (char *) [data bytes];
    NSMutableString *hexString = [[NSMutableString alloc] init];
    for (NSUInteger i = 0; i < len; i++) {
        [hexString appendString:[NSString stringWithFormat:@"%0.2hhx", chars[i]]];
    }
    return hexString;
}

/// 时间转字符串
+ (NSString *)formateTime:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:date];
    return dateTime;
}

@end
