//
//  AnalyticsHelper.m
//  JumBoTest
//
//  Created by Huang YangChing on 2020/4/12.
//

#import "AnalyticsHelper.h"
#import "Flurry.h"

@implementation AnalyticsHelper

+ (void)logEventName: (NSString *)name eventParams: (NSString *)params{
        NSError *jsonError;
        NSData *objectData = [params dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                              options:NSJSONReadingMutableContainers
                                                error:&jsonError];
    
        [Flurry logEvent:name withParameters:json];
}

+ (void)endTimedEventName: (NSString *)name eventParams: (NSString *)params{
        NSError *jsonError;
        NSData *objectData = [params dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                              options:NSJSONReadingMutableContainers
                                                error:&jsonError];
    
        [Flurry endTimedEvent:name withParameters:json];
}

+ (void)logErrorID: (NSString *)_id eventParams: (NSString *)params{
//        NSException *exception = [NSException exceptionWithName:@"testerr" reason:@"sample reason" userInfo:nil];
        [Flurry logError:_id message:params exception:nil];
}

+ (void)setUserID: (NSString *)name{
        [Flurry setUserID:name];
}
@end
