//
//  AppExtension.h
//  JumBoTest-mobile
//
//  Created by Mark Hong on 2020/2/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppExtension : NSObject
+ (void)copy: (NSString *)str;
+ (void)updata:(NSString *)str;
+ (NSNumber *)GetPlatform;
+ (NSString *)GetAppUpdataUrl;
+ (NSString *)GetVersion;
+ (int) getNetworkTyp;
@end
NS_ASSUME_NONNULL_END
