//
//  AnalyticsHelper.h
//  JumBoTest
//
//  Created by Huang YangChing on 2020/4/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnalyticsHelper : NSObject
+ (void)setUserID: (NSString *)name;
+ (void)logErrorID: (NSString *)_id eventParams: (NSString *)params;
+ (void)endTimedEventName: (NSString *)name eventParams: (NSString *)params;
+ (void)logEventName: (NSString *)name eventParams: (NSString *)params;
@end

NS_ASSUME_NONNULL_END
