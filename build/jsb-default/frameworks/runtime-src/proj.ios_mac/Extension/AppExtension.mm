//
//  AppExtension.m
//  JumBoTest-mobile
//
//  Created by Mark Hong on 2020/2/11.
//

#import "AppExtension.h"
#import "Reachability.h"

@implementation AppExtension
+ (void)copy: (NSString *)str{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = str;
}

+ (void)updata:(NSString *)str{
    NSString *appurl =[NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@",str];
    NSURL *URL = [NSURL URLWithString:appurl];
    [[UIApplication sharedApplication]openURL:URL options:@{} completionHandler:nil];
}

+ (NSNumber *)GetPlatform{
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSNumber *platform = [infoDict objectForKey:@"platform"];
    return platform;
}

+ (NSString *)GetAppUpdataUrl{
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *url = [infoDict objectForKey:@"appupdataurl"];
    return url;
}

+ (NSString *)GetVersion{
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *buildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%@.%@",appVersion,buildVersion];
}

+ (int) getNetworkTyp{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    int iType = 0;
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:// 没有网络
            iType = -1;
            break;
        case ReachableViaWiFi:// Wifi
            iType = 1;
            break;
        case ReachableViaWWAN:// 手机自带网络
            iType = 2;
            break;
    }
    return iType;
}
@end
