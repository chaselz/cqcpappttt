//
//  HTAssistiveTouchButton.h
//  HTAssistiveTouchButton
//
//  Created by Mark Hong on 2019/8/7.
//  Copyright © 2019 Mark Hong. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HTAssistiveTouchButton.
FOUNDATION_EXPORT double HTAssistiveTouchButtonVersionNumber;

//! Project version string for HTAssistiveTouchButton.
FOUNDATION_EXPORT const unsigned char HTAssistiveTouchButtonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HTAssistiveTouchButton/PublicHeader.h>

@interface HTAssistiveTouchButton : UIButton
    
@property (nonatomic, assign) BOOL isMoving;  // button是否处于移动状态
@property (nonatomic, strong) NSMutableArray *childButtons;  // 可弹出的子按钮
    
-(void) addChild:(UIButton *)btn;
    
@property (nonatomic, strong) void (^clickBlock) (void);
@property (nonatomic, strong) void (^moreclickBlock) (void);
- (void) setOnclick : (void (^)(void))block;
- (void)setMooreClick:(void (^)(void))block;
- (void) clickBtn : (UIButton*) sender;
    //- (void) setTarget : action:(SEL)action;
@end
