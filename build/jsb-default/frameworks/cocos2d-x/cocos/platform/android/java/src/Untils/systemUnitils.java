package Untils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

public class systemUnitils {

    /**
     * 是否為小米Ｇ
     * @return
     */
    public static Boolean isMUUI(){

        String manufacturer = Build.MANUFACTURER;
        //这个字符串可以自己定义,例如判断华为就填写huawei,魅族就填写meizu
        return  "xiaomi".equalsIgnoreCase(manufacturer);
    }

    /**
     * 是否為第一次開啟或者新版本第一次開啟
     * @param context
     * @return
     */
    public static boolean isFirstStartorNewVersion(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                "SHARE_APP_TAG", 0);
        Boolean isFirst = preferences.getBoolean("FIRSTStart", true);
        String appversion = preferences.getString("VERSION", "");
        String version = getLocalVersionName(context) +"."+ getLocalVersion(context);
        return isFirst || !version.equals(appversion);
    }

    /**
     * 儲存第一次開啟跟版本號
     * @param context
     */
    public static void saveFirstandVersion(Context context){
        SharedPreferences preferences = context.getSharedPreferences(
                "SHARE_APP_TAG", 0);

        String version = getLocalVersionName(context) +"."+ getLocalVersion(context);
        preferences.edit().putBoolean("FIRSTStart", false).commit();
        preferences.edit().putString("VERSION", version).commit();
    }

    /**
     * 获取本地软件版本号
     */
    public static int getLocalVersion(Context ctx) {
        int localVersion = 0;
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

}
