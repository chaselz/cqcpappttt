
const {ccclass, property} = cc._decorator;

@ccclass
export default class nYear extends cc.Component {

    @property(cc.RichText)
    yearlabel: cc.RichText = null;

    private callback:void = null;
    private key:number = 0;
    public get Key(){
        return this.key;
    }
    protected onLoad(): void {
        this.node.on("click",function () {
            this.callback(this.key);
        },this);
    }

    setData(y:number,otherstr:string = "",cb:void){
        this.callback = cb;
        this.key = y;
        this.drawStroke();
        this.yearlabel.string = "<b>{0}{1}</b>".format(y,otherstr);
    }

    setState(state){
        if(state)
            this.drawFill();
        else
            this.drawStroke();
    }

    private drawStroke(){
        let gp = this.node.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0-gp.node.width /2 ,0-gp.node.height /2 ,gp.node.width,gp.node.height,16);
        gp.stroke();
    }

    private drawFill(){
        let gp = this.node.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0-gp.node.width /2 ,0-gp.node.height /2 ,gp.node.width,gp.node.height,16);
        gp.fill();
    }
    // update (dt) {}
}
