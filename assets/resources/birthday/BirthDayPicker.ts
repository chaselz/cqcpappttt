import nYear from "./nYear";
import newUIItemDay from "../UIDatePicker/newUIItemDay";
import {DateType} from "../UIDatePicker/newUIDatePicker";
import nDay from "./nDay";
import DayInputBox from "../../scripts/UI/DayInputBox";
import {VM} from "../../scripts/modelView/ViewModel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BirthDayPicker extends cc.Component {

    @property(cc.Node)
    Main:cc.Node = null;

    @property(cc.RichText)
    titleTime:cc.RichText = null;

    @property(cc.Node)
    YearNode: cc.Node = null;

    @property(cc.Node)
    MonthNode: cc.Node = null;

    @property(cc.Node)
    DayNode:cc.Node = null;

    @property(cc.Node)
    WeekNode:cc.Node = null;

    @property(cc.Prefab)
    pfbYear: cc.Prefab = null;

    @property(cc.Prefab)
    pfbday: cc.Prefab = null;

    @property(cc.Node)
    OKBtn: cc.Node = null;

    @property((cc.RichText))
    chooseAlertLabel:cc.RichText = null;

    public static main : BirthDayPicker = null;

    private state : BirthDayState = 0;
    private YData: number = 0;
    private MData: number = 0;
    private DData: number = 0;
    private weekname = ["日","一","二","三","四","五","六"];
    private callback : void = null;
    private tager :DayInputBox = null;
    protected onEnable(): void {

    }

    protected onLoad(): void {
        BirthDayPicker.main = this;
        this.InitYear();
        this.InitMonth();
        this.InitDay();
        this.setOKEvent();
        this.initWeekLabel();
        this.setBackChooseViewBtn();
        this.node.active = false;
        //
    }

    public show(_tager){
        this.tager = _tager;
        this.node.active = true;
        this.YearNode.getComponent(cc.ScrollView).scrollToBottomLeft(0);
        this.YearNode.getComponent(cc.ScrollView).scrollToBottom(0);
        this.InitValue();
        this.setMode(BirthDayState.Y);
        this.updateYearNodeState();
        this.updateMonthNodeState();
    }

    public setCallBack(cb){
        this.callback = cb;
    }

    public close(){
        this.node.active = false;
    }

    private InitValue(){
        this.state  = BirthDayState.Y;
        this.YData = 0;
        this.MData = 0;
        this.DData = 0;
    }

    private setBackChooseViewBtn(){
        this.titleTime.node.on("click",function () {
            if(this.state == BirthDayState.D){
                this.DData = 0;
                this.MData = 0;
                this.setMode(BirthDayState.M);
            }else if(this.state == BirthDayState.M){
                this.MData = 0;
                this.YData = 0;
                this.setMode(BirthDayState.Y);
            }
        },this);
    }
    /***
     * 更新week列表文字
     */
    private initWeekLabel(){
        let labels = this.WeekNode.getComponentsInChildren(cc.Label);
        for(var i = 0;i<labels.length;i++){
            labels[i].string = this.weekname[i];
        }
    }

    private DrawBG(){
        this.Main.getComponentInChildren(cc.Layout).updateLayout();
        this.Main.getComponent(cc.Layout).updateLayout();
        let gp = this.Main.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0-gp.node.width /2 ,0-gp.node.height ,gp.node.width,gp.node.height,16);
        gp.fill();
    }

    private InitYear(){
        var content = this.YearNode.getComponent(cc.ScrollView).content;
        var year = new Date().getFullYear();

        for(var i=year -100 ;i<=year;i++){
            let node = cc.instantiate(this.pfbYear);
            node.parent = content;
            node.getComponent(nYear).setData(i,"",this.YMDCallBack.bind(this));
            // this.pfgListDay.push(node);
        }
        this.YearNode.getComponent(cc.ScrollView).scrollToBottomLeft(0);
    }

    private InitMonth(){
        var content = this.MonthNode.getComponent(cc.ScrollView).content;
        for(var i=1 ;i<13;i++){
            let node = cc.instantiate(this.pfbYear);
            node.parent = content;
            node.getComponent(nYear).setData(i,"月",this.YMDCallBack.bind(this));
            // this.pfgListDay.push(node);
        }
        this.MonthNode.getComponent(cc.ScrollView).scrollToBottomLeft(0);
    }

    private InitDay(){
        for(var i=0 ;i<32;i++){
            let node = cc.instantiate(this.pfbday);
            node.parent = this.DayNode;
            node.getComponent(nDay).setCallBack(this.YMDCallBack.bind(this));
            // this.pfgListDay.push(node);
        }
    }

    private setOKEvent(){
        this.OKBtn.on("click",function () {
            if(this.state == BirthDayState.D){
                if(this.DData != 0) {
                    if(this.callback)
                        this.callback(this.tager,this.YData, this.MData, this.DData);
                    this.close();
                }
            }
        },this);
    }

    private YMDCallBack(value){
        if(this.state == BirthDayState.Y){
            if(this.YData == value)
                this.YData = 0;
            else
                this.YData = value;
            this.updateYearNodeState();
            this.setMode(BirthDayState.M);
        }else if(this.state == BirthDayState.M){
            if(this.MData == value)
                this.MData = 0;
            else
                this.MData = value;
            this.updateMonthNodeState();
            this.updataDate();
            this.setMode(BirthDayState.D);
        }else{
            this.OKBtn.getComponent(cc.Button).interactable = this.DData != value;
            if(this.DData == value)
                this.DData = 0;
            else
                this.DData = value;
            this.updateTimeLabel();
            this.updateDayNodeState();
        }
    }
    // update (dt) {}
    private updateYearNodeState(){
        this.YearNode.getComponent(cc.ScrollView).content.getComponentsInChildren(nYear).forEach(x=>{
            x.setState(x.Key == this.YData);
        });
    }

    private updateMonthNodeState(){
        this.MonthNode.getComponent(cc.ScrollView).content.getComponentsInChildren(nYear).forEach(x=>{
            x.setState(x.Key == this.MData);
        });
    }

    private updateDayNodeState(){
        this.DayNode.getComponentsInChildren(nDay).forEach(x=>{
            x.setState(x.Day == this.DData);
        });
    }

    private updateTimeLabel(){
        var y = this.YData == 0 ? "-":this.YData;
        var m = this.MData == 0 ? "-":this.MData;
        var d = this.DData == 0 ? "-":this.DData;

        this.titleTime.string = "<b>{0} 年 {1} 月 {2} 日</b>".format(y,m,d);
    }

    private setMode(type){
        this.state = type;
        this.YearNode.active = type == BirthDayState.Y;
        this.MonthNode.active = type == BirthDayState.M;
        this.DayNode.active = type == BirthDayState.D;
        this.WeekNode.active = type == BirthDayState.D;
        this.updateTimeLabel();
        this.DrawBG();
        this.OKBtn.getComponent(cc.Button).interactable = false;
        this.updateChooseAlertLable();
    }

    private updateChooseAlertLable(){
        var key = "";
        switch (this.state) {
            case BirthDayState.D:
                key = "chooseday";
                break;
            case BirthDayState.M:
                key = "choosemonth";
                break;
            default:
                key = "chooseyear";
                break;
        }
        this.chooseAlertLabel.string = VM.getValue("lang."+key);
    }


    private updataDate(){
        let totalDays = new Date(this.YData, this.MData, 0).getDate();
        let fromWeek = new Date(this.YData, this.MData, 1).getDay();
        //先調整高度
        this.DayNode.height = (Math.floor((fromWeek+totalDays - 1) / 7) + 1) * this.DayNode.children[0].height;
        for (let i = 0; i < this.DayNode.childrenCount; ++i) {
            let node = this.DayNode.children[i];
            if (i < totalDays) {
                node.active = true;
                let index = fromWeek + i;
                let row = Math.floor(index / 7);
                let col = index % 7;
                let x = -(this.DayNode.width - node.width) * 0.5 + col * node.width;
                let y = (this.DayNode.height - node.height) * 0.5 - row * node.height;
                node.setPosition(x, y);
                let script = node.getComponent(nDay);
                script.setDay(i);
            } else {
                node.active = false;
            }
        }
    }
}

enum BirthDayState {
    Y = 0,
    M = 1,
    D = 2,
}