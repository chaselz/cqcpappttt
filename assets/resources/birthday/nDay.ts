
const {ccclass, property} = cc._decorator;

@ccclass
export default class nDay extends cc.Component {

    @property(cc.Label)
    lbDay: cc.Label = null;

    @property(cc.Sprite)
    spSel: cc.Sprite = null;

    private day:Number = 0;
    private cb:void = null;

    public get Day(){
        return this.day;
    }

    protected onLoad(): void {
        this.node.on("click",this.onClickItem,this);
    }

    public setDay(day:number, ) {
        this.day = day + 1;
        this.lbDay.string = this.day.toString();
        this.spSel.enabled = false;

    }

    public setCallBack(_cb:void){
        this.cb = _cb;
    }

    public onClickItem() {
        if (this.cb)
            this.cb(this.day);
    }

    public setState(state){
        this.spSel.enabled = state;
    }
}
