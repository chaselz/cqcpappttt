cc.Class({
    extends: cc.Component,

    properties: {
        lbDay: cc.Label,
        spSel: cc.Sprite,
    },

    setDay(index, day, sel, over, cb) {
        this.index = index;
        this.day = day;
        this.cb = cb;
        this.lbDay.node.color = over ? cc.Color.GRAY : cc.Color.BLACK;
        this.lbDay.string = day;
        this.spSel.enabled = sel && !over;
    },

    onClickItem() {
        if (this.cb) {
            this.cb(this.index, this.day);
        }
    },
});
