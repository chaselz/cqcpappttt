
import Color = cc.Color;

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewUIItemDay extends cc.Component {

    @property(cc.RichText)
    lbDay: cc.RichText = null;

    @property(cc.Sprite)
    spSel: cc.Sprite = null;

    private index :Number = 0;
    private day:Number = 0;
    private cb:void = null;

    public get Day(){
        return this.day;
    }

    public setDay(index, day, over, cb,sel=false) {
        this.index = index;
        this.day = day;
        this.cb = cb;
        this.lbDay.node.color = over ? cc.Color.GRAY : new Color(211,211,211);
        this.lbDay.string = "<b>{0}</b>".format(day);
        this.spSel.enabled = sel;//sel && !over;

    }

    public onClickItem() {
        if (this.cb) {
            this.cb(this.index, this.day);
        }
    }

    public OriginView(over:boolean){
        if(over)
            return;
        this.spSel.enabled = false;
    }

    public ChooseView(over:boolean){
        if(over)
            return;
        this.spSel.enabled = true;
    }
}
