/** 
 * 日期组件
 */

var UIDatePicker = cc.Class({
    extends: cc.Component,

    ctor(){
        this.target_node = null;
    },
    
    properties: {
        lbYearMonth: cc.Label,
        ndDays: cc.Node,
        pfbDay: cc.Prefab,
    },

    statics :{
        main : null,
    },

    onLoad () {
        UIDatePicker.main = this;
        this.initData();
        this.initTodayData();
        this.initchooseDate();
        this.updateDate();
        this.node.active = false;
    },

    initData() {
        this.date = this.date ? this.date : new Date();
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();

        this.pfgListDay = [];
        for (let i = 0; i < 31; ++i) {
            let node = cc.instantiate(this.pfbDay);
            node.parent = this.ndDays;
            this.pfgListDay.push(node);
        }
    },
    //初始化今天日期
    initTodayData(){
        this.today = this.day;
        this.tomonth = this.month;
        this.toyear = this.year;
    },
    //初始化選擇的日期
    initchooseDate(){
        this.chooseY = this.year;
        this.chooseM = this.month;
        this.chooseD = this.day;
    },

    //
    /*
        * 设置显示的日志，默认为当前日期
        * @param year 年
        * @param month 月
        * @param day 日
        * @param IsJSDate 是否為 JS的 new Date()的資料日期
     */
    setDate(year, month, day, IsJSDate) {
        this.date = new Date(year, month - (IsJSDate?0:1), day);
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();
        this.initchooseDate();
        this.updateDate();
    },

    updateDate () {
        this.lbYearMonth.string = cc.js.formatStr("%s年%s月", this.year, this.month + 1);

        let totalDays = new Date(this.year, this.month + 1, 0).getDate();
        let fromWeek = new Date(this.year, this.month, 1).getDay();

        for (let i = 0; i < this.pfgListDay.length; ++i) {
            let node = this.pfgListDay[i];
            if (i < totalDays) {
                node.active = true;
                let index = fromWeek + i;
                let row = Math.floor(index / 7);
                let col = index % 7;
                let x = -(this.ndDays.width - node.width) * 0.5 + col * node.width;
                let y = (this.ndDays.height - node.height) * 0.5 - row * node.height;
                node.setPosition(x, y);
                let script = node.getComponent("UIItemDay");
                script.setDay(i, i + 1, (this.chooseD == i + 1 && this.month == this.chooseM && this.year == this.chooseY),this.checkIsOverToDay(this.year,this.month,i+1),(selIndex, selDay)=>{
                    this.chooseD = selDay;
                    this.chooseM = this.month;
                    this.chooseY = this.year;
                    this.updateDate();
                    this.onClickClose();
                });
            } else {
                node.active = false;
            }
        }
    },
    /*
        上個月
     */
    onClickLeft () {
        if (this.month > 0) {
            this.month -= 1;
        } else {
            this.month = 11;
            this.year -= 1;
        }
        this.date.setFullYear(this.year);
        this.date.setMonth(this.month);
        this.updateDate();
    },
    /*
        下個月
     */
    onClickRight () {
        if (this.month < 11) {
            this.month += 1;
        } else {
            this.month = 0;
            this.year += 1;
        }
        this.date.setFullYear(this.year);
        this.date.setMonth(this.month);
        this.updateDate();
    },

    // 设置选中日期之后的回调
    setPickDateCallback(cb) {
        this.cb = cb;
    },
    /*
        關閉日曆
     */
    onClickClose () {
        if(!this.checkIsOverToDay(this.chooseY,this.chooseM,this.chooseD)){
            if (this.cb) {
                this.cb(this.target_node,this.chooseY, this.chooseM + 1, this.chooseD);
            }
            //this.node.parent = null;
            this.hide();
        }
    },
    //檢查是否超過今天日期
    checkIsOverToDay(y,m,d){
        return (y > this.toyear || (y == this.toyear && ((m > this.tomonth) || ( m == this.tomonth && d > this.today))));
    },
    show(_node){
        this.target_node = _node;
        this.node.active = true;
    },
    hide(){
        this.node.active = false;
    }
});
