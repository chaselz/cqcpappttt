import array = cc.js.array;
import newUIItemDay from "./newUIItemDay";
import TimeSlider from "../../scripts/UI/TimeSlider";


const {ccclass, property} = cc._decorator;

@ccclass
export default class newUIDatePicker extends cc.Component {

    @property(cc.RichText)
    titleDateLabel: cc.RichText = null;
    @property(cc.RichText)
    lbYearMonth: cc.RichText = null;
    @property(cc.Node)
    ndDays: cc.Node = null;
    @property(cc.Prefab)
    pfbDay: cc.Prefab = null;
    @property(cc.Button)
    saveBtn: cc.Button = null;
    @property(cc.Button)
    clearBtn: cc.Button = null;
    @property(cc.Button)
    closeBtn: cc.Button = null;
    @property(cc.Node)
    bottomNode: cc.Node = null;
    @property(cc.Node)
    mdWeekNode: cc.Node = null;

    @property(TimeSlider)
    startTimeSlider: TimeSlider = null;
    @property(TimeSlider)
    endTimeSlider: TimeSlider = null;

    // LIFE-CYCLE CALLBACKS:
    private date: Date = null;
    private nowdate: Date = null;
    private intervalDaydate : Date = null;
    private year: any = null;
    private month: any = null;
    private day: any = null;
    private hour: any = null;
    private tohour : any = null;
    private today : any = null;
    private tomonth : any = null;
    private toyear : any = null;

    private pfgListDay : any = [];

    private chooseY : any = null;
    private chooseM : any = null;
    private chooseD : any = null;
    private chooseH : any = null;
    private chooseS : any = null;

    private cb : void = null;
    private target_node : cc.Node = null;
    private mode : DateType = 4;
    private weekname = ["日","一","二","三","四","五","六"];
    private targer : any = null;

    public static get main(){
        return newUIDatePicker._instant;
    }
    private static _instant : newUIDatePicker = null;
    onLoad () {
        newUIDatePicker._instant = this;
        this.initBtn();
        this.initWeekLabel();
        this.initData();
        this.setTimeSilderCallBack();
        this.node.active = false;
    }

    start () {

    }

    /***
     * 更新week列表文字
     */
    private initWeekLabel(){
        let labels = this.mdWeekNode.getComponentsInChildren(cc.Label);
        for(var i = 0;i<labels.length;i++){
            labels[i].string = this.weekname[i];
        }
    }

    /***
     * 初始化Btns
     */
    private initBtn(){
        this.saveBtn.node.on("click",function () {
            this.onClickClose();
        },this);
        this.clearBtn.node.on("click",this.clearDate,this);
        this.closeBtn.node.on("click",function () {
            this.hide();
        },this);
    }

    private clearDate(){
        this.chooseY = [0,0];
        this.chooseM = [0,0];
        this.chooseD = [0,0];
        this.chooseH = [0,23];
        this.chooseS = [0,0];
        this.initTimeSlider();
        this.updateItemDate(32);
        this.clearIntervalDayline();
        this.bottomAreaActive(false);
        this.updateTitleDateLabel();
    }

    private initTimeSlider(){
        this.startTimeSlider.value = this.chooseH[0];
        this.endTimeSlider.value = this.chooseH[1];
    }
    /***
     * 初始化背景
     */
    private initColorAndBg(){
        let gp = this.getComponentInChildren(cc.Graphics);
        gp.node.getComponent(cc.Layout).updateLayout();
        gp.clear();
        gp.roundRect(0 - gp.node.width / 2,0 - gp.node.height ,gp.node.width,gp.node.height,15);
        gp.fill();
    }

    /***
     * 初始化時間
     */
    private initData() {
        this.date = this.date ? this.date : new Date();
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();

        this.pfgListDay = [];
        for (let i = 0; i < 31; ++i) {
            let node = cc.instantiate(this.pfbDay);
            node.parent = this.ndDays;
            this.pfgListDay.push(node);
        }
    }
    //初始化今天日期
    private initTodayData(){
        this.date = new Date();
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();
        this.hour = this.date.getHours();
    }
    //初始化選擇的日期
    private initchooseDate(){
        switch (this.mode) {
            case DateType.OnlyDay:
            case DateType.WithTime:
                this.chooseY = 0;
                this.chooseM = 0;
                this.chooseD = 0;
                this.chooseH = 0;
                break;
            default:
                this.chooseY = [0,0];
                this.chooseM = [0,0];
                this.chooseD = [0,0];
                this.chooseH = [0,23];
                break;
        }

    }

    /*
        * 设置显示的日志，默认为当前日期
        * @param year 年
        * @param month 月
        * @param day 日
        * @param IsJSDate 是否為 JS的 new Date()的資料日期
     */
    private setDate(year, month, day, IsJSDate) {
        this.date = new Date(year, month - (IsJSDate?0:1), day);
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();
        this.initchooseDate();
        this.updateDate();
    }

    /***
     * 更新日曆
     */
    private updateDate () {
        this.lbYearMonth.string = cc.js.formatStr("<b>%s 年 %s 月</b>", this.year, this.month + 1);
        let totalDays = new Date(this.year, this.month + 1, 0).getDate();
        let fromWeek = new Date(this.year, this.month, 1).getDay();
        //先調整高度
        this.ndDays.height = (Math.floor((fromWeek+totalDays - 1) / 7) + 1) * this.pfgListDay[0].height;
        for (let i = 0; i < this.pfgListDay.length; ++i) {
            let node = this.pfgListDay[i];
            if (i < totalDays) {
                node.active = true;
                let index = fromWeek + i;
                let row = Math.floor(index / 7);
                let col = index % 7;
                let x = -(this.ndDays.width - node.width) * 0.5 + col * node.width;
                let y = (this.ndDays.height - node.height) * 0.5 - row * node.height;
                node.setPosition(x, y);
                let script = node.getComponent(newUIItemDay);
                script.setDay(i, i + 1,this.checkIsOverToDay(this.year,this.month,i+1),(selIndex, selDay)=>{
                    switch (this.mode) {
                        case DateType.OnlyDay:
                        case DateType.WithTime:
                            this.onlyDayModeFunc(selIndex,selDay);
                            break;
                        case DateType.IntervalDay:
                        case DateType.IntervalTime:
                            this.intervalDayModeFunc(selIndex,selDay);
                                break;
                    }
                },this.IsCheckDay(this.year,this.month,i+1));
            } else {
                node.active = false;
            }
        }

        this.ndDays.parent.getComponent(cc.Layout).updateLayout();
        if((this.mode == DateType.IntervalDay ||  this.mode == DateType.IntervalTime) && !this.chooseY.includes(0))
            this.drawIntervalDayline();
    }
    private IsCheckDay(y,m,d){
        if(this.mode == DateType.OnlyDay || this.mode == DateType.WithTime){
            return this.chooseY == y && this.chooseM == m +1 && this.chooseD == d;
        }else{
            return this.chooseY[0] == y && this.chooseM[0] == m+1 && this.chooseD[0] == d ||
                this.chooseY[1] == y && this.chooseM[1] == m+1 && this.chooseD[1] == d ;
        }

    }
    /***
     * 劃日期區間的線
     */
    private drawIntervalDayline(){
        let fromWeek = new Date(this.year, this.month , 1).getDay();
        let gp = this.ndDays.getComponent(cc.Graphics);
        let point = [null,null];
        var curMonthDays = new Date(this.year,this.month +1,0).getDate();
        let max_x =  -(this.ndDays.width - this.pfgListDay[0].width) * 0.5 + 6 * this.pfgListDay[0].width;
        for (let i = 0; i < curMonthDays; ++i) {
            let day = i+1;
            if(this.checkDayInterval(day)) {
                let node = this.pfgListDay[i];
                let index = fromWeek + i;
                let row = Math.floor(index / 7);
                let col = index % 7;
                let x = -(this.ndDays.width - node.width) * 0.5 + col * node.width;
                let y = (this.ndDays.height - node.height) * 0.5 - row * node.height;
                //當紀錄點沒有資料時取剛開始的點位
                if(point[0] == null && point[1] == null) {
                    point = [x, y];
                }
                if(point[1] != y){///到該換行時
                        gp.moveTo(point[0],point[1]);
                        gp.lineTo(point[0] + node.width / 2,point[1]);
                        gp.lineCap = cc.Graphics.LineCap.ROUND;
                        gp.stroke();
                        if(day == this.chooseD[1])///排除當選擇的最後時間在新的一行的開始日期禁止畫線
                            continue;
                        point = [x + node.width / 2,y];
                        gp.moveTo(point[0],point[1]);
                        gp.lineTo(point[0] - node.width / 2,point[1]);
                }else {
                    if(x!=max_x){//不是最左邊的時間
                        gp.moveTo(point[0], point[1]);
                        gp.lineTo(x , y);
                        point = [x , y];
                        
                    }else{//為最右邊時間
                        if(day == this.chooseD[0]){//當為第一個選擇時間要把記錄下來的位置清除以免會畫到那條線
                            point = [null,null];
                            continue;
                        }
                        gp.moveTo(point[0], point[1]);
                        gp.lineTo(x - node.width / 2, y);
                        point = [x - node.width / 2, y];
                    }
                    gp.lineCap = cc.Graphics.LineCap.ROUND;
                }
                gp.stroke();
                
            }
        }
    }

    private checkDayInterval(day){
        let date = new Date(this.year,this.month+1,day).getTime();
        let min_date = new Date(this.chooseY[0],this.chooseM[0],this.chooseD[0]).getTime();
        let max_date = new Date(this.chooseY[1],this.chooseM[1],this.chooseD[1]).getTime();
        return date >= min_date && date <= max_date;
    }

    /***
     * 清除區間繪畫的線
     */
    private clearIntervalDayline(){
        this.ndDays.getComponent(cc.Graphics).clear();
    }

    /***
     * 單選日期模式
     * @param selIndex
     * @param selDay
     */
    private onlyDayModeFunc(selIndex:number,selDay:number){
        this.chooseD = selDay;
        this.chooseM = this.month +1;
        this.chooseY = this.year;
        this.updateItemDate(selIndex);
        this.bottomAreaActive(true);
        this.updateTitleDateLabel();
    }

    /***
     * 選擇日曆與時間模式
     */
    private withTimeModeFunc(selIndex:number,selDay:number){

    }

    /***
     * 選擇兩個時間區間
     * @param selIndex
     * @param selDay
     */
    private intervalDayModeFunc(selIndex:number,selDay:number){
        if(this.checkIsOverToDay(this.year,this.month,selIndex+1))
            return;;
        if(this.chooseY[0] == 0 ){
            this.setChooseDate(selIndex,0,true,this.year,this.month + 1,selDay);
            this.checkIsReachTwoChooseDay();
            return;
        }else if(this.chooseY[1] == 0 ){
            this.setChooseDate(selIndex,1,true,this.year,this.month + 1,selDay);
            this.checkIsReachTwoChooseDay();
            return;
        }else{ //調整成再點選初始化
            this.clearDate();
            this.setChooseDate(selIndex,0,true,this.year,this.month + 1,selDay);
            this.updateTitleDateLabel();
        }
    }

    /***
     * 選擇時間與日期區間
     */
    private intervalTimeModeFunc(){

    }

    /***
     * 設定選擇時間資料
     * @param selIndex
     * @param dateIndex
     * @param IsSelect
     * @param year
     * @param month
     * @param day
     */
    private setChooseDate(selIndex:number,dateIndex:number,IsSelect:boolean,year:number,month:number = 0, day:number = 0){
        this.chooseD[dateIndex] = day;
        this.chooseM[dateIndex] = month;
        this.chooseY[dateIndex] = year;
        this.updateItemDateInWithIndex(selIndex,IsSelect);
    }

    /***
     * 設定選擇時間資料
     * @param selIndex
     * @param dateIndex
     * @param IsSelect
     * @param year
     * @param month
     * @param day
     */
    private setChooseDateWithDay(dayIndex:number,dateIndex:number,year:number,month:number = 0, day:number = 0){
        this.updateItemDateInWithDay(dayIndex,day);
        this.chooseD[dateIndex] = day;
        this.chooseM[dateIndex] = month;
        this.chooseY[dateIndex] = year;
    }

    /***
     * 檢查是否可以畫線
     */
    private checkIsReachTwoChooseDay(){
        this.updateTitleDateLabel();
        if(this.chooseY[0] == 0 || this.chooseY[1] == 0)
            return;
        this.sortChooseDate();
        this.updateTitleDateLabel();
        this.bottomAreaActive(true);
        this.drawIntervalDayline();
    }

    /***
     * 排序時間
     */
    private sortChooseDate(){
        if(new Date(this.chooseY[0],this.chooseM[0],this.chooseD[0]).getTime() > new Date(this.chooseY[1],this.chooseM[1],this.chooseD[1]).getTime()){
            this.dateSizeWwap(this.chooseY);
            this.dateSizeWwap(this.chooseM);
            this.dateSizeWwap(this.chooseD);
        }
    }

    /***
     * 時間資料交換
     * @param value
     */
    private dateSizeWwap(value:array){
        let temp = value[0];
        value[0] = value[1];
        value[1] = temp;
    }

    private updateTitleDateLabel(){
        if(this.chooseY instanceof Array){

            if(this.chooseY[0] == 0 && this.chooseY[1] == 0){
                this.titleDateLabel.string = "";
            }
            else if(this.chooseY[0]==0 && this.chooseY[1] != 0){
                if(this.chooseY[1] != this.year)
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s年%s月%s日</b>",this.chooseY[1], this.chooseM[1],this.chooseD[1]);
                else
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s月%s日</b>", this.chooseM[1],this.chooseD[1]);
            }else if(this.chooseY[1]==0 && this.chooseY[0] != 0){
                if(this.chooseY[0] != this.year)
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s年%s月%s日</b>",this.chooseY[0], this.chooseM[0],this.chooseD[0]);
                else
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s月%s日</b>", this.chooseM[0],this.chooseD[0]);
            }else{
                if(this.chooseY[0] != this.chooseY[1])
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s年%s月%s日 - %s年%s月%s日</b>",this.chooseY[0], this.chooseM[0],this.chooseD[0],this.chooseY[1], this.chooseM[1],this.chooseD[1]);
                else
                    this.titleDateLabel.string = cc.js.formatStr("<b>%s月%s日 - %s月%s日</b>", this.chooseM[0],this.chooseD[0], this.chooseM[1],this.chooseD[1]);
            }
        }else{
            if(this.chooseY != 0)
                this.titleDateLabel.string = cc.js.formatStr("<b>%s月%s日</b>", this.chooseM,this.chooseD);
        }
    }

    /***
     * 更新每個日期的物件狀態
     * @param index
     */
    private updateItemDate(index){
        for (let i = 0; i < this.pfgListDay.length; ++i) {
            let node = this.pfgListDay[i].getComponent(newUIItemDay);
            if(i == index)
                node.ChooseView(this.checkIsOverToDay(this.year,this.month,i+1));
            else
                node.OriginView(this.checkIsOverToDay(this.year,this.month,i+1));
        }
    }

    /***
     * 更新每個日期的物件狀態(當是選擇時間區間)
     * @param index
     * @param firstorsencond
     * @param IsSelect
     */
    private updateItemDateInWithIndex(index:number,IsSelect:boolean){
        let node = this.pfgListDay[index].getComponent(newUIItemDay);
        if(IsSelect)
            node.ChooseView(false);
        else
            node.OriginView(false);

    }


    /***
     * 更新每個日期的物件狀態(當是選擇時間區間)
     * @param index
     * @param firstorsencond
     * @param IsSelect
     */
    private updateItemDateInWithDay(befor_day:number,now_day:number){
        let t1= this.pfgListDay.find(day=> day.getComponent(newUIItemDay).Day == befor_day);
        if(this.chooseY[0] != this.chooseY[1] || this.chooseM[0] != this.chooseM[1] || this.chooseD[0] != this.chooseD[1])
            t1.getComponent(newUIItemDay).OriginView(false);

        let t2= this.pfgListDay.find(day=> day.getComponent(newUIItemDay).Day == now_day);
        t2.getComponent(newUIItemDay).ChooseView(false);
    }

    private setTitleDateInIntervalMode(){

    }

    /***
     * 下方物件Active
     * @param active
     */
    private bottomAreaActive(active:boolean){
        this.startTimeSlider.node.active = this.mode == DateType.IntervalTime || this.mode == DateType.WithTime;
        this.endTimeSlider.node.active = this.mode == DateType.IntervalTime;
        if(this.bottomNode.active!=active) {
            this.bottomNode.active = active;
            if(active) {
                this.startTimeSlider.node.parent.getComponent(cc.Layout).updateLayout();
                this.bottomNode.getComponent(cc.Layout).updateLayout();
            }
            this.initColorAndBg();
        }
    }

    /***
     * 設定小時選擇器回傳
     */
    private setTimeSilderCallBack(){
        this.startTimeSlider.setTimeSilderCB((hour)=>{
            if(this.mode = DateType.IntervalTime)
                this.chooseH[0] = hour;
            else
                this.chooseH = hour;
        });
        this.endTimeSlider.setTimeSilderCB((hour)=>{
            this.chooseH[1] = hour;
        })
    }

    /*
        上個月
     */
    public onClickLeft () {
        this.clearIntervalDayline();
        if (this.month > 0) {
            this.month -= 1;
        } else {
            this.month = 11;
            this.year -= 1;
        }
        this.date.setFullYear(this.year);
        this.date.setMonth(this.month);
        this.updateDate();
    }
    /*
        下個月
     */
    public onClickRight () {
        this.clearIntervalDayline();
        if (this.month < 11) {
            this.month += 1;
        } else {
            this.month = 0;
            this.year += 1;
        }
        this.date.setFullYear(this.year);
        this.date.setMonth(this.month);
        this.updateDate();
    }

    // 设置选中日期之后的回调
    public setPickDateCallback(cb) {
        this.cb = cb;
    }
    /*
        關閉日曆
     */
    private onClickClose () {
        if(!this.checkIsOverToDay(this.chooseY,this.chooseM - 1,this.chooseD)){
            if (this.cb) {
                if(this.mode == DateType.IntervalDay || this.mode == DateType.OnlyDay)
                    this.cb(this.targer,this.chooseY, this.chooseM, this.chooseD);
                else
                    this.cb(this.targer,this.chooseY, this.chooseM, this.chooseD, this.chooseH);
            }
            this.hide();
        }
    }
    //檢查是否超過今天日期
    private checkIsOverToDay(y,m,d,h=0){
        let date = new Date(y,m,d,h).getTime();
        return date > this.nowdate.getTime() || date < this.intervalDaydate.getTime();
        // return (y > this.toyear || (y == this.toyear && ((m > this.tomonth) || ( m == this.tomonth && d > this.today))));
    }
    public show(_target:any,type:DateType = DateType.IntervalDay,intervaldateString:string = ""){
        this.targer = _target;
        this.mode = type;
        this.nowdate = new Date();
        this.node.active = true;
        this.intervalDaydate = new Date(intervaldateString.isNull() ? "1910-01-01":intervaldateString);
        this.initTodayData();
        this.bottomAreaActive(false);
        this.initchooseDate();
        this.initTimeSlider();
        this.updateDate();
        this.clearIntervalDayline();
        this.initColorAndBg();
    }
    public showIntervalDay(_target:any,type:DateType = DateType.IntervalDay,IntervalDay:number = 0){

        this.initTodayData();
        this.targer = _target;
        this.mode = type;
        this.nowdate = new Date();
        this.node.active = true;
        let interval = new Date();
        this.intervalDaydate = new Date(interval.setDate(interval.getDate()-IntervalDay));
        this.bottomAreaActive(false);
        this.initchooseDate();
        this.initTimeSlider();
        this.updateDate();
        this.clearIntervalDayline();
        this.initColorAndBg();
    }
    public hide(){
        this.node.active = false;
    }

    protected onEnable(): void {
        this.titleDateLabel.string = "";
    }
}

export const enum DateType {
    OnlyDay = 1,
    WithTime = 2,
    IntervalDay = 3,
    IntervalTime = 4
}