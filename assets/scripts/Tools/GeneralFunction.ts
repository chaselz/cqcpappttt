import {VM} from "../modelView/ViewModel";
import AnalyticsHelper from "../Extension/Nactive/AnalyticsHelper";

var ApiManager = require("ApiManager");

export default class GeneralFunction {

    public static LoginMustFunc(userid:string = cc.sys.localStorage.getItem("account")){
        AnalyticsHelper.setUserId(userid);
        VM.setValue("core.islogin",true);
        ApiManager.User.get.UserGameBalanceProgress();
        ApiManager.Game.get.CompanyList();
        ApiManager.User.get.UserData();
        ApiManager.User.get.UserMsg();
        ApiManager.Voucher.GetLuckyMoneyList(2,1,20);
    }

}
