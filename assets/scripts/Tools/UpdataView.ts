import {VM} from "../modelView/ViewModel";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import {PlatFormType} from "../Enum/PlatFormType";

const {ccclass, property} = cc._decorator;

@ccclass
export default class UpdataView extends cc.Component {

    @property(cc.Label)
    contentlabel: cc.Label = null;

    @property(cc.Label)
    sizelabel: cc.Label = null;

    @property(cc.Button)
    btn: cc.Button = null;

    @property(cc.ProgressBar)
    PBar: cc.ProgressBar = null;

    @property(cc.Node)
    VipNode: cc.Node = null;

    private ok_click:void;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.btn.node.on("click",function () {
            if(this.ok_click)
                this.ok_click();
        },this);
        this.OnDownloadListener();
        this.OnInstallCancel();
    }

    updateInfo(content:string,okfunc:void){
        this.ok_click = okfunc;
        this.contentlabel.string = content;
        let str = VM.getValue("lang.newappsize");
        this.sizelabel.string = str.format(this.getRandom(20.0,5.0));
        if(MobileExtension.GetPlatform() == PlatFormType.VIP){
            this.VipNode.active = true;
            this.btn.node.active = false;
        }

    }
    // update (dt) {}
    //以下function多了一個參數min代替了原本區間內的最小值1，而原本的最大值x則是改成了參數max
    getRandom(min,max){
        return Math.floor(Math.random()*max)+min;
    };

    OnDownloadListener(){
        cc.game.on("appdownload",(curr)=>{
            this.btn.node.active = false;
            this.PBar.node.active = true;
            this.PBar.progress = curr / 100;
        });
    }

    OnInstallCancel(){
        cc.game.on("appdownloadcancel",()=>{
            this.btn.node.active = true;
            this.PBar.node.active = false;
        });
    }

}
