import {VM} from "../modelView/ViewModel";
import Alert from "../UI/Alert";
import {UpdataStatus} from "../Enum/UpdataStatus";
import {PlatFormType} from "../Enum/PlatFormType";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import UpdataView from "./UpdataView";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

export default class UpdataApp  {

    private static updata_url : string = "";
    private static updata_content : string = "";
    static async start(){
        if(!cc.sys.isNative)
            return UpdataStatus.NoUpdata;

        let updatajson = await this.getAppUpdataJson();

        // if(MobileExtension.GetPlatform() == PlatFormType.VIP)
        //     return UpdataStatus.NoUpdata;

        console.log("platform ="+MobileExtension.GetPlatform());
        console.log("GetVersion ="+MobileExtension.GetVersion());
        console.log("GetUpdataUrl ="+MobileExtension.GetUpdataUrl());
        if(updatajson){
            if(this.checkVersion(updatajson.update_ver_code,MobileExtension.GetVersion())){
                this.updata_content = updatajson.update_content;
                if(cc.sys.os == cc.sys.OS_IOS){
                    this.updata_url = updatajson.iosurl;
                }else{
                    this.updata_url = updatajson.androidurl;
                }
                this.showAlert();
                return UpdataStatus.NeedUpdata;
            }
        }
        return UpdataStatus.NoUpdata;
    }

    private static checkVersion(updata:string,machine:string){
        var tempupdate = updata.split(".");
        var tempmachine = machine.split(".");
        tempupdate = tempupdate[0]+tempupdate[1]+tempupdate[2];
        tempmachine = tempmachine[0]+tempmachine[1]+tempmachine[2];
        if(tempmachine.length > tempupdate.length){
            tempupdate = this.zerofull(tempupdate,tempmachine.length-tempupdate.length);
        }else if(tempmachine.length < tempupdate.length){
            tempmachine = this.zerofull(tempmachine,tempupdate.length-tempmachine.length);_
        }
        return parseInt(tempmachine) < parseInt(tempupdate);
    }

    private static zerofull(string,count){
        for(var i =0;i<count;i++){
            string += "0";
        }
        return string;
    }

    private static async showAlert(){
         let alert = await UI_manager.create_prefab_atSync(cc.find("Canvas"),"UpdataView");
         alert.getComponent(UpdataView).updateInfo(this.updata_content,this.runUpdata.bind(this));
        // Alert.main.Show("lang.appupdata",this.updata_content,this.runUpdata.bind(this),this.cancelUpdata.bind(this));
    }

    private static cancelUpdata(){
        setTimeout(()=>{
            this.showAlert();
        },1000);
    }

    private static runUpdata(){
        if(cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "appupdata", "(Ljava/lang/String;)V", this.updata_url);
        }else if(cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppExtension", "updata:", this.updata_url);
        }
        VM.setValue("core.lock",false);
    }

    private static getAppUpdataJson(){
        return new Promise((resolve,reject)=> {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", MobileExtension.GetUpdataUrl(), true);

            xhr.onload = function () {
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var json = JSON.parse(xhr.responseText);
                    console.log("app json =", xhr.responseText);
                    resolve(json);
                }else{
                    resolve(null);
                }
            };

            xhr.onerror = function(){
                resolve(null);
            };

            xhr.onabort = function(){
                resolve(null);
            };

            xhr.ontimeout = function(){
                resolve(null);
            };

            xhr.send();
        });
    }

}
