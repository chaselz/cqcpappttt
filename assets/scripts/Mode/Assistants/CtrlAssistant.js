
var CtrlAssistant = cc.Class({

    properties: {
        _list : null,
    },

    ctor(){
        if(CtrlAssistant.main != null)
            return;

        this._list = new Map();
        CtrlAssistant.main = this;
    },

    statics(){
        main:null;
    },

    saveCtrl(_ctrl,name){
        this._list.set(name,_ctrl);
    },

    getCtrl(name){
        if(this._list.has(name))
            return this._list.get(name)

        return null;
    }
    // update (dt) {},
});
if(!CC_EDITOR) {
    let temp = new CtrlAssistant();
}

module.exports = CtrlAssistant;