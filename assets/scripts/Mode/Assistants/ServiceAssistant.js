import {VM} from "../../modelView/ViewModel";
import HomeStatus from "../Status/HomeStatus";
import AddBankStatus from "../Status/AddBankStatus";
import BankListStatus from "../Status/BankListStatus";
import ModifyPWStatus from "../Status/ModifyPWStatus";
import DWRecordsStatus from "../Status/DWRecordsStatus";
import BetRecordStatus from "../Status/BetRecordStatus";
import BetRecordDateStatus from "../Status/BetRecordDateStatus";
import BetRecordMakerStatus from "../Status/BetRecordMakerStatus";
import BetRecordGameStatus from "../Status/BetRecordGameStatus";
import BetRecordNumStatus from "../Status/BetRecordNumStatus";
import {PageViewType} from "../../Enum/PageViewType";
import GameHallStatus from "../Status/GameHallStatus";
import IndoorTransferStatus from "../Status/IndoorTransferStatus";
import VoucherStatus from "../Status/VoucherStatus";
import newDetailsStatus from "../Status/newDetailsStatus";
import RechargeV2Status from "../Status/RechargeV2Status";

var HierarchicalStateController = require("HierarchicalStateController");
var UIAssistant = require("UIAssistant");
var GameStatus = require("GameStatus");
var LoginStatus = require("LoginStatus");
var BoundStatus = require("BoundStatus");
var InitStatus = require("InitStatus");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var NewInitStatus = require("NewInitStatus");
var MemberStatus = require("MemberStatus");
var RegisterStatus = require("RegisterStatus");
var BoundDetailStatus = require("BoundDetailStatus");
var RechargeStatus = require("RechargeStatus");
var PayWayStatus = require("PayWayStatus");
var HallStatus = require("HallStatus");
var GameScreenStatus = require("GameScreenStatus");
var GameListStatus = require("GameListStatus");
var WithDrawStatus = require("WithDrawStatus");
var MyMessageStatus = require("MyMessageStatus");
var MsgInfoStatus = require("MsgInfoStatus");
var HelpCenterStatus = require("HelpCenterStatus");
var HelpListStatus = require("HelpListStatus");
var HelpInfoStatus = require("HelpInfoStatus");
var AgentJoinStatus = require("AgentJoinStatus");

var ServiceAssistant = cc.Class({
    extends: cc.Component,

    properties: {
    },
    editor:{
        executionOrder: -10
    },
    ctor(){
        if(ServiceAssistant.main == null)
            ServiceAssistant.main = this;

        this.StateController = null;
        this.rate_it_showed = false;
        this.daily_reward_showed = false;
    },

    statics:{
        main : null,
    },
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.StateController = new HierarchicalStateController(null);
        this.StateController.setDelegate(this.StatusResult);
        UIAssistant.main.AddLister(this.showPage);
        this.StateController.Start(new NewInitStatus.default(this.node));       //繼承

    },
    showPage(page_name) {
        if(VM.getValue("core.previousPage") == page_name)
            return;

        switch (page_name) {
            case PageViewType.Home:
                ServiceAssistant.main.StateController.TransTo(2,new HomeStatus());
                break;
            case PageViewType.SignIn:
                ServiceAssistant.main.StateController.TransTo(2,new LoginStatus());
                break;
            case PageViewType.SignUp:
                if(VM.getValue("core.previousPage") == PageViewType.HelpList)
                    VM.setValue("login.isToTermsView",true);
                ServiceAssistant.main.StateController.TransTo(2,new RegisterStatus.default());
                break;
            case PageViewType.Games:
                ServiceAssistant.main.StateController.TransTo(2,new HallStatus());
                break;
            case PageViewType.GameScreen:
                ServiceAssistant.main.StateController.TransTo(2,new GameScreenStatus());
                break;
            case PageViewType.GameList:
                ServiceAssistant.main.StateController.TransTo(2,new GameListStatus());
                break;
            case PageViewType.Bound:
                ServiceAssistant.main.StateController.TransTo(2,new BoundStatus());
                break;
            case PageViewType.BoundDetail:
                ServiceAssistant.main.StateController.TransTo(2,new BoundDetailStatus());
                break;
            case PageViewType.Member:
            case PageViewType.newMember:
                ServiceAssistant.main.StateController.TransTo(2,new MemberStatus());
                break;
            case PageViewType.Recharge:
                ServiceAssistant.main.StateController.TransTo(2,new RechargeStatus.default());
                break;
            case PageViewType.PayView:
                if(VM.getValue("core.previousPage") == PageViewType.InputDeposit)
                    break;
                ServiceAssistant.main.StateController.TransTo(2,new PayWayStatus());
                break;
            case PageViewType.WithDraw:
                ServiceAssistant.main.StateController.TransTo(2,new WithDrawStatus.default());
                break;
            case PageViewType.AddBank:
                ServiceAssistant.main.StateController.TransTo(2,new AddBankStatus());
                break;
            case PageViewType.Banklist:
                ServiceAssistant.main.StateController.TransTo(2,new BankListStatus());
                break;
            case PageViewType.ModifyPW:
                ServiceAssistant.main.StateController.TransTo(2,new ModifyPWStatus());
                break;
            case PageViewType.MyMessage:
                ServiceAssistant.main.StateController.TransTo(2,new MyMessageStatus());
                break;
            case PageViewType.MsgInfo:
                ServiceAssistant.main.StateController.TransTo(2,new MsgInfoStatus());
                break;
            case PageViewType.DWRecords:
                ServiceAssistant.main.StateController.TransTo(2,new DWRecordsStatus());
                break;
            case PageViewType.BetRecord:
                ServiceAssistant.main.StateController.TransTo(2,new BetRecordStatus());
                break;
            case PageViewType.BetRecordDate:
                ServiceAssistant.main.StateController.TransTo(3,new BetRecordDateStatus());
                break;
            case PageViewType.BetRecordMaker:
                ServiceAssistant.main.StateController.TransTo(3,new BetRecordMakerStatus());
                break;
            case PageViewType.BetRecordGame:
                ServiceAssistant.main.StateController.TransTo(3,new BetRecordGameStatus());
                break;
            case PageViewType.BetRecordNum:
                ServiceAssistant.main.StateController.TransTo(3,new BetRecordNumStatus());
                break;
            case PageViewType.GameHall:
                ServiceAssistant.main.StateController.TransTo(2,new GameHallStatus());
                break;
            case PageViewType.IndoorTransfer:
                ServiceAssistant.main.StateController.TransTo(2,new IndoorTransferStatus());
                break;
            case PageViewType.Voucher:
                ServiceAssistant.main.StateController.TransTo(2,new VoucherStatus());
                break;
            case PageViewType.HelpCenter:
                ServiceAssistant.main.StateController.TransTo(2,new HelpCenterStatus());
                break;
            case PageViewType.HelpList:
                ServiceAssistant.main.StateController.TransTo(2,new HelpListStatus());
                break;
            case PageViewType.HelpInfo:
                ServiceAssistant.main.StateController.TransTo(2,new HelpInfoStatus());
                break;
            case PageViewType.newDetails:
                if(VM.getValue("core.previousPage") == PageViewType.MenuView || VM.getValue("core.previousPage") == PageViewType.BetRecordMaker || VM.getValue("core.previousPage") == PageViewType.CheckMenuView)
                    break;
                ServiceAssistant.main.StateController.TransTo(2,new newDetailsStatus());
                break;
            case PageViewType.AgentJoin:
                // ServiceAssistant.main.StateController.TransTo(2,new AgentJoinStatus());
                break;
            case PageViewType.RechargeV2:
                ServiceAssistant.main.StateController.TransTo(2,new RechargeV2Status());
                break;
            default:
                break;
        }
        VM.setValue("core.previousPage",page_name);
    },

    StatusResult(statetype,date){
        console.log("Type is "+statetype);
    },

    update (dt) {
        this.StateController.StateUpdate();
    },
    
    
});
