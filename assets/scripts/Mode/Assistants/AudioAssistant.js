var Sound = require("Sound");
var AudioAssistant = cc.Class({
    extends: cc.Component,

    properties: {
        musicVolume:{
            type: cc.Float,
            default: 0.5,
            slide:true,
            min:0,
            max:1,
            step:0.01
        },
        tracks :{
            type: [Sound],
            default: []
        },
        sounds :{
            type: [Sound],
            default: []
        }

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    ctor(){
        AudioAssistant.main = this;
        this.currentTrack = "";
        this.GetSoundByName = function (name) {
            return this.sounds.find(x=>x.NAME == name);
        }
    },

    statics:{
        main:null,
    },

    start () {

    },

    PlayMusic(trackName){
        if(trackName != null && trackName != "")
            this.currentTrack = trackName;
        var to = null;
        this.tracks.forEach(function (x) {
            if(trackName == x.NAME){
                to = x.clips.randomElemt();
            }
        });
        this.CrossFade(to);
    },

    CrossFade(to){
        if(to == null)
            return;
        if(cc.audioEngine.AudioState == cc.audioEngine.AudioState.PLAYING)
            cc.audioEngine.stopMusic();

        cc.audioEngine.setMusicVolume(this.musicVolume);
        cc.audioEngine.playMusic(to,true);
    },

    Shot(clip){
        var sound = this.GetSoundByName(clip);

        if(sound != null){
            if(sound.clips.length == 0) return;
            cc.audioEngine.setEffectsVolume(this.musicVolume);
            cc.audioEngine.playEffect(sound.clips.randomElemt(),false);
        }
    },
    // update (dt) {},
});
