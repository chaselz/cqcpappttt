
var CPanel = require("CPanel");
var Page = require("Page");
var AudioAssistant = require('AudioAssistant');


var UIAssistant = cc.Class({
    extends: cc.Component,

    properties: {
        UImodules :{
            default:[],
            type:[cc.Node],
        },
        panels : {
            default: [],
            type: [CPanel],
            notify: function () {
                this.panels.sort(function (a,b) {
                    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                });
            }
        },
        pages : {
            default: [],
            type: [Page],
        },
    },
    editor:CC_EDITOR && {
        executeInEditMode: true,
    },

    ctor(){
        this.onShowPage = [];
        this.history = [];
        UIAssistant.main = this;
        this.Notify = function (pagename) {
            this.onShowPage.forEach(x=>{
                x(pagename);
            })
        }
    },

    statics :{
        main : null,
    },
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    start () {
        console.log("UIAssistant Start");
        if(!CC_EDITOR) {
            this.ArraysConvertation();
            let defaultPage = this.GetDefaultPage();
            if (defaultPage != null && defaultPage != undefined)
                this.ShowPage(defaultPage, true);
        }
    },

    ArraysConvertation(){
        this.panels = [];
        var self = this;
        this.UImodules.forEach(function (module) {
            if(module == null)
                return;

            module.getComponentsInChildren("CPanel").forEach(function (cpanel) {
                // console.log("CPanel Name = "+cpanel.name);
                if(!self.panels.includes(cpanel))
                    self.panels.push(cpanel);
            })
        });
    },

    ShowPage(page,immediate = false,backMode = false){
        if(page.constructor === String){
            var temp = this.pages.find(x=>x.NAME == page);
            if(temp != null){
                page = null;
                page = temp;
            }else {
                console.error("無此Page Name: "+page+"，請重新檢查");
                return;
            }
        }

        if(CPanel.uiAnimation > 0)
            return;

        if(this.history.length > 0 && this.history[this.history.length - 1] == page.NAME)
            return;

        if(this.pages == null)
            return;

        if(!backMode && this.history.length > 0){//比較下一頁page level 比較低會使用返回的特效
            var his_name = this.history[this.history.length -1];
            var his_page = this.pages.find(x=>x.NAME == his_name);
            backMode = his_page.pageLevel > page.pageLevel;
        }

        this.history.push(page.NAME);
        if(this.history.length > 100)
            this.history.shift();
        this.panels.forEach(function(panel) {
            if (page.panels.includes(panel.name)){
                panel.SetVisible(true, immediate,backMode);
            }else
                if(!page.ignoring_panels.includes(panel.name) && !panel.freez) {
                    panel.SetVisible(false, immediate)
                }
        });

        if(page.soundtrck !== "-" && page.soundtrck != "")
            if(page.soundtrck != AudioAssistant.main.currentTrack)
                AudioAssistant.main.PlayMusic(page.soundtrck);

        // if(page.setTimeScale)

        // if(UIAssistant.onShowPage != null) {
        //     UIAssistant.onShowPage(page.NAME);
        // }
        this.Notify(page.NAME);
    },

    FreezPanel(panel_name,value = true){
        let panel = this.panels.find(x=>x.name == panel_name);
        if(panel != null)
            panel.freez = value;
    },

    SetPanelVisible(panel_name,visible,immediate = false){
        //BalanceArea,false
        //let panel = this.panels.find(x=>x.name == (panel_name+"<CPanel>"));
        let panel = this.panels.find(
            function(x) {
            if(x.name ==(panel_name+"<CPanel>"))    
            //console.log(x);
            //console.log(x.name);
            //console.log(panel_name);
            return x;
            });
            //console.log(panel)
        
        if(panel){          //判斷有沒有panel    //null
            if(immediate)
                panel.SetVisible(visible,true);
            else
                panel.SetVisible(visible);
        }
    },

    HideAll(){
        for(let panel in this.panels)
            panel.SetVisible(false);
    },

    ShowPreviousPage(){
        if(this.history.length < 2){
            console.error("沒有上一頁");
            return;
        }
        let index = this.history.length;
        this.ShowPage(this.history[index - 2],false,true);
        this.history.pop();
        this.history.pop();
    },

    GetPreviousPage(){
        return this.history.length > 1 ? this.history[this.history.length -2] : "";
    },

    GetCurrentPage(){
        return this.history.length > 0 ? this.history[this.history.length -1] : "";
    },

    GetDefaultPage(){
        return this.pages.find(x=>x.default_page);
    },

    GetHistorylength(){
        return this.history.length;
    },

    GetPageActive(name){
        let cpanel = this.panels.find(x=>x.node.name == name);
        if(cpanel)
            return cpanel.node.active;
        else
            return false;

    },

    AddPage(m_page){
        for(let i =0;i<m_page.panels.length;i++){
            m_page.panels[i] += "<CPanel>";
        }
        for(let i =0;i<m_page.ignoring_panels.length;i++){
            m_page.ignoring_panels[i] += "<CPanel>";
        }
        this.pages.push(m_page);
        this.ArraysConvertation();
    },

    AddLister(func){
        this.onShowPage.push(func)
    },
    // update (dt) {},
});
