
var HierarchicalStateController = cc.Class({

    ctor(oStartState){
        this.m_oCurrentState = [];
        this.m_bTerminated = false;
        this.m_bStarted = false;
        this.m_bAtStateBegin = false;
        this.stateFinishEvent = null;

        if(oStartState != null)
            this.Start(oStartState);

        console.log("HierarchicalStateController Init");
    },

    get FinishEvent(){
        return this.stateFinishEvent;
    },

    Start(oState){
        if(this.m_bTerminated){
            console.error("[HStateController] has been terminated");
            return;
        }
        if(this.m_bStarted){
            console.error("[HStateController] hs been started");
            return;
        }

        this.m_bStarted = true;
        this.m_oCurrentState.push(oState); // m_oCurrentState = [] , [oState] , [oState,oState]     //new MemberStatus
        this.m_oCurrentState[0].SetProperty(this,0);
    },

    Terminate(){
        for(var i = this.m_oCurrentState.length -1 ; i > -1;i--){
            this.m_oCurrentState[i].StateEnd();
        }
        this.m_oCurrentState.length = 0;
        this.m_bTerminated = true;
    },

    TransTo(iLevel,oState){
        //2,new MemberStatus
        if(this.m_bTerminated){
            console.error("[StateController] has been terminated");
            return;
        }

        if(!this.m_bStarted){
            console.error("[StateController] need to be started first");
            return;
        }

        if(iLevel > this.m_oCurrentState.length){
            console.error("[StateController] Level is too big, Level = "+iLevel);
            return;
        }

        console.log("[StateController] Level "+iLevel+ " transTo: " +oState.constructor.name);
        if(iLevel == this.m_oCurrentState.length){
            this.m_oCurrentState.push(oState);                             //
            this.m_oCurrentState[iLevel].SetProperty(this,iLevel);          //這個東西到第幾層
        }else{
        
            for(var i = this.m_oCurrentState.length -1; i >= iLevel;i--){
                this.m_oCurrentState[i].StateEnd();
                this.m_oCurrentState.splice(i,1); //delte 1 item at m_oCurrentState[i] , i => offset, 1=> length
                //this.m_oCurrentState.splice(i,1, new Array); 
            }
            this.m_oCurrentState.push(oState);
            this.m_oCurrentState[iLevel].SetProperty(this,iLevel);
        }
    },

    StateUpdate(){
        if(this.m_bTerminated || !this.m_bStarted)
            return;
        let self = this;
        this.m_oCurrentState.forEach(function(istate) {            
            if(istate.AtStateBegin){
                istate.TouchStateBegin();
                istate.StateBegin();
                if(self.m_oCurrentState == null)
                    return;
            }
            istate.StateUpdate();
        });
    },

    StateOnFocus(focus){
        if(this.m_bTerminated || !this.m_bStarted)
            return;
        this.m_oCurrentState.forEach(function(istate) {
            istate.OnFocus(focus);
        });
    },

    /*
     * 設定委派  delegate-function(state,date)
     */
    setDelegate(delegate){
        this.stateFinishEvent = delegate;
    }
});