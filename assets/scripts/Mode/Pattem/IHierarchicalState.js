var IHierarchicalState = cc.Class({

    ctor() {
        this.test = 123;
        this.m_StateController = null;
        this.m_iLevel = 0;
        this.m_bAtStateBegin = true;
    },

    properties: {
        Controller:{
            get: function () {
                return this.m_StateController;
            },
        },
        StateLevel:{
            get: function () {
                return this.m_iLevel;
            },
        },
        AtStateBegin:{
            get: function () {
                return this.m_bAtStateBegin;
            },
        },

    },

    SetProperty(oController,iLevel){
        this.m_StateController = oController;
        this.m_iLevel = iLevel;
    },

    TouchStateBegin(){
        this.m_bAtStateBegin = false;
    },

    TransTo(oState){
        this.m_StateController.TransTo(this.m_iLevel,oState);
    },

    TransToLevel(iLevel,oState){
        this.m_StateController.TransTo(iLevel,oState);
    },

    AddSubState(oState){
        this.m_StateController.TransTo(this.m_iLevel + 1,oState);
    },

    ToString(){
        return "<IHState>"+this.name;
    },

    CallBackEvent(_date){
        this.Controller.FinishEvent(this.name,_date);
    },

    StateBegin(){
    },

    StateUpdate(){
    },
    StateEnd(){

    },
    OnFocus(focus){

    }
});
module.exports = IHierarchicalState;