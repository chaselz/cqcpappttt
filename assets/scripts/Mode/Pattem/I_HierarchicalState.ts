
const {ccclass, property} = cc._decorator;
import HierarchicalStateController from './HierarchicalStateController';

export default class I_HierarchicalState {

    protected name  = "I_HierarchicalState";
    private m_iLevel = 0;
    private m_bAtStateBegin = true;
    private m_StateController:HierarchicalStateController = null;

    get Controller (){
        return this.m_StateController;
    }
    get StateLevel (){
        return this.m_iLevel;
    }
    get AtStateBegin (){
        return this.m_bAtStateBegin;
    }
    SetProperty(oController,iLevel){
        this.m_StateController = oController;
        this.m_iLevel = iLevel;
    }

    TouchStateBegin(){
        this.m_bAtStateBegin = false;
    }

    TransTo(oState){
        this.m_StateController.TransTo(this.m_iLevel,oState);
    }

    TransToLevel(iLevel,oState){
        this.m_StateController.TransTo(iLevel,oState);
    }

    AddSubState(oState){
        this.m_StateController.TransTo(this.m_iLevel + 1,oState);
    }

    ToString(){
        return "<IHState>"+this.name;
    }

    CallBackEvent(_date){
        this.Controller.FinishEvent(this.name,_date);
    }

    async StateBegin(){

    }

    async StateUpdate(){

    }
    async StateEnd(){

    }
    OnFocus(focus){

    }
}

