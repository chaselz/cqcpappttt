var CPanel = require("CPanel");
var Page = cc.Class({
    name:"Page",
    properties: {
        NAME: '',
        panels:{
            default:[],
            type:[cc.String]
        },
        ignoring_panels:{
            default:[],
            type:[cc.String]
        },
        soundtrck: "",
        default_page: false,
        setTimeScale: true,
        timeScale: 1,
        pageLevel: 10
    },
});