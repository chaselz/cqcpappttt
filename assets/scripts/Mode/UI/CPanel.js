
var CPanel = cc.Class({
    extends: cc.Component,

    properties: {
        freez : false,
        hide : "",
        show : "",
        isChangeViewAnim: false,
        isPlaying: {
            get(){
                return this._isPlaying;
            },
            set(value){
                if(this._isPlaying != value){
                    this._isPlaying = value;
                    CPanel.uiAnimation += this._isPlaying ? 1 : -1;
                }
            }
        }
    },
    ctor(){
        this._isPlaying = false;
        this.currentClip = "";
        this.anim = null;
    },

    // LIFE-CYCLE CALLBACKS:
    statics: {
        uiAnimation : 0,
    },
    // onLoad () {},

    start () {
        this.anim = this.getComponent(cc.Animation);
    },

    // update (dt) {},

    onEnable(){
        this.freez = false;
    },

    SetVisible(visible,immediate = false,isback = false){
        this.currentClip = "";
        if(!visible){
            if(this.hide != "")
                this.currentClip =this.hide;
            else{
                this.node.active = false;
                return;
            }
        }else{
            this.node.active = true;

            if(this.isChangeViewAnim){
                let widget = this.node.getComponent(cc.Widget)
                if(widget.enabled){ //關閉 widget , 因為它 updateAlignment 比較晚這樣會導致我們調整位置會被重置 
                    widget.enabled = false;
                    //確定CPanel 一定會被 自適應
                    widget.updateAlignment();
                }
                this.node.setPosition(cc.v2(isback ? -640 : 640, 0));
                this.node.opacity = 128; 
                var finished = cc.callFunc(function () {
                }, this);
                var moveto = cc.moveTo(0.08,cc.v2(0, 0));
                var fadeIn = cc.fadeIn(1);
                var spawn = cc.spawn(moveto,fadeIn,finished);
                this.node.runAction(spawn);
            }
            if(this.show != "")
                this.currentClip = this.show;
            else
                return;
        }

        if(this.currentClip == "")
            return;

        if(immediate){

        }else
            this.Play(this.currentClip);
    },

    Play(clip){
        console.log("paly anim");
        this.isPlaying = true;

        let self = this;
        this.anim.on("finished",()=>{
            self.isPlaying = false;

            this.node.active = !(clip == self.hide);
        },this);

        this.anim.play(clip);
    },
    PlayClip(clip){
        if(!this.isPlaying)
            this.Play(clip);
    },
});
