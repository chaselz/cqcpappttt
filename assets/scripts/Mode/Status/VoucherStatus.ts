import {VoucherType} from "../../Enum/VoucherType";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");


@ccclass
export default class VoucherStatus extends IHierarchicalState {

    private apiindex :Number = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private apilistente(type:any,data:any){
        switch (type) {
            case ApiType.verifycodeType:
                this.AnaylizeVerifyCode(data);
                break;
            case ApiType.receivevoucherType:
                this.AnaylizeReceiveVoucher(data);
                break;
            case ApiType.luckymoneyType:
                this.AnaylizeLuckyMoney(data);
                break;
            case ApiType.receiveluckymoneyType:
                this.AnaylizeReceiveLuckyMoney(data);
                break;
        }
    }

    private AnaylizeReceiveLuckyMoney(data:any){
        let vc = CtrlAssistant.main.getCtrl("VoucherCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.showReceiveView(data.result.promoAmount,VoucherType.LuckyMoney);
                ApiManager.Voucher.GetLuckyMoneyList(0,1,20);
                break;
        }
        vc.CloseLoad();
    }

    private AnaylizeLuckyMoney(data:any){
        let vc = CtrlAssistant.main.getCtrl("VoucherCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.showLuckyMoneyVIew(data.result.total,data.result.data);
                break;
        }
        vc.CloseLoad();
    }

    private AnaylizeReceiveVoucher(data:any){
        let vc = CtrlAssistant.main.getCtrl("VoucherCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.showReceiveView(data.result.promoAmount,VoucherType.Voucher);
                break;
        }
        vc.ReceivedBtnStatus(false);
    }

    private AnaylizeVerifyCode(data:any){
        let vc = CtrlAssistant.main.getCtrl("VoucherCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.showVorcherActiveView(data.result);
                break;
        }
    }
}
