var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

cc.Class({
    extends: IHierarchicalState,

    properties: {
        detail:null
    },

    ctor(){
        this.apilistenter = async function(type,data){
            let temp = data;
            switch (type) {
                case ApiType.boundDetailType:     //優惠活動詳細頁
                    let detail = CtrlAssistant.main.getCtrl("BoundDetailCtrl");
                    if(data.code == "1"){
                        VM.setValue("bound.detail", temp.result);
                        detail.setBoundDetail();
                    }
                    break;
            }
        };
        this.apiindex = 0;
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        this.detail = CtrlAssistant.main.getCtrl("BoundDetailCtrl");
        this.list = CtrlAssistant.main.getCtrl("BoundCtrl");
        this.detail.init();
        UIAssistant.main.SetPanelVisible("Load",true);
        ApiManager.get.BoundDetail(VM.getValue("bound.boundId"));
    },

    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },

});
