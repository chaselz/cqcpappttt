import {VM} from "../../modelView/ViewModel";
import PageManager from "../../ManagerUI/PageManager";

var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
var ApiType = require("ApiType");
var CtrlAssistant = require("CtrlAssistant");
var GameListType = require("GameListType");

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameHallStatus extends IHierarchicalState {

    private apiindex : number = 0;
    async StateBegin(){
        this.Init();
        this.apiindex = this.apiindex = ApiManager.addListenter(this.Listenter.bind(this));
        ApiManager.Game.get.HotGame("zh-cn",2);
        ApiManager.Game.get.GameProviderList(2);//1:彩票 2:電子 3:棋牌 4:視訊 5:體育
    }

    async StateUpdate(){

    }

    async StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private Init(){
        VM.setValue("hall.gameListLoading",null);
    }


    private Listenter(type,data:any){
        switch (type) {
            case ApiType.gameHotType:
                this.AnaylizeHotGame(data);
                break;
            case ApiType.gameListType:
                this.AnaylizeGameList(data);
                break;
            case ApiType.gameLoginV2Type:
                this.AnaylizeGameLoginV2(data);
                break;
            case ApiType.gameProviderType:
                this.AnaylizeGameProvider(data);
                break;
        }
    }

    private AnaylizeGameLoginV2(data:any){
        switch (parseInt(data.code)) {
            case 1:
                CtrlAssistant.main.getCtrl("MainCtrl").setWebUrl(data.result.url);
                break;
        }
        CtrlAssistant.main.getCtrl("GameHallCtrl").CloseLoad();
    }

    private AnaylizeGameProvider(data:any){
        if(data.code == "1") {
            VM.setValue("hall.gameAllClass", data.result);
            let cq9 = data.result.find(temp=>temp.provider.code == "cq9");
            if(cq9) {
                let slot = cq9.kind.find(temp => temp.code == "slot");
                VM.setValue("gamehall.cq9code", slot.id);
            }else
                //為了不要點擊無限循環，存放個-1表示錯誤代碼
                VM.setValue("gamehall.cq9code", -1);
        }
    }

    private AnaylizeHotGame(data:any){
        if(data.code == "1"){
            let vc = CtrlAssistant.main.getCtrl("GameHallCtrl");
            vc.setRecommendData(data.result.filter(x=>x.type === 2));
        }
    }

    private AnaylizeGameList(data:any){
        switch (parseInt(data.code)) {
            case 1:
                if(VM.getValue("hall.gameListLoading") == null || VM.getValue("hall.gameListLoading") != "GameHall")
                    return;
                let gClass = VM.getValue("hall.gameAllClass");
                gClass.forEach((item) => {        //取得遊戲分類項目
                    if (item.provider.code == "cq9") {
                        gClass = item.kind;
                    }
                });
                VM.setValue("core.tagCode",2);
                VM.setValue("hall.gameListType",GameListType.GameClass);
                VM.setValue("hall.gameClass", gClass);
                VM.setValue("hall.gameList", data.result);
                VM.setValue("hall.gameTitle",VM.getValue("hall.gameClass")[0]["display"]);
                PageManager.GameListPage();
                break;
        }
        CtrlAssistant.main.getCtrl("GameHallCtrl").CloseLoad();
    }
}
