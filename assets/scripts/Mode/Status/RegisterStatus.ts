import GeneralFunction from "../../Tools/GeneralFunction";
import AnalyticsHelper from "../../Extension/Nactive/AnalyticsHelper";
import PageManager from "../../ManagerUI/PageManager";
import Awaiter from "../../Extension/AwaitExtension";


const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');

@ccclass
export default class RegisterStatus extends IHierarchicalState {

    private  apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        if(!VM.getValue("login.isToTermsView")){
            VM.setValue("login.hasErr",false);
            ApiManager.Login.userregister();
            ApiManager.get.FrontDocList(3);
            this.setRandomNum();
            let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
            vc.Init();
        }
        this.setRegisterEvent();
        VM.setValue("login.isToTermsView",false);
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
        vc.ClearRegister(this.setRegisterEvent,this);
    }
    /*
     * 更新驗證碼
     */
    private setRandomNum(){
        let lvc = CtrlAssistant.main.getCtrl("SignUpCtrl");
        lvc.randomVCode();
    }

    private async apilistente(type: ApiType, data:any){
        switch(type){
            case ApiType.registerType:
                this.AnaylizeRegister(data);
                break;
            case ApiType.frontDocListType:
                this.AnaylizeFront3(data);
                break;
            case ApiType.userregister:
                this.setUserRegisterSetting(data);
                break;
            case ApiType.loginType:
                this.AnaylizeLogin(data);
                break;
        }
    }

    /***
     * 分析登入結果
     * @param data
     * @constructor
     */
    private async AnaylizeLogin(data:any){
        if(data.code == "1"){
            console.log("登入成功");
            let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
            await Awaiter.until(_=>!ApiManager.APIToken.isNull() && !ApiManager.publicKey.isNull());
            GeneralFunction.LoginMustFunc(vc.getInputBoxValue().account);
            VM.setValue("core.lock",false);
            cc.sys.localStorage.setItem("account",vc.getInputBoxValue().account);
            PageManager.HomePage();
            return;
        }else
            AnalyticsHelper.loginFailed(data.code);
    }
    /***
     * 分析註冊結果
     * @param data
     * @constructor
     */
    private AnaylizeRegister(data:any){
        if(data.code == "1"){
            console.log("註冊完成");
            let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
            let registerJson = vc.getInputBoxValue();
            ApiManager.Login.userlogin(registerJson.account,registerJson.pw);
            return;
        }
        VM.setValue("core.lock",false);
    }

    /***
     * 分析文案資料
     * @param data
     * @constructor
     */
    private AnaylizeFront3(temp){
        let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
        if(temp.code == "1"){
            let active = temp.result.length > 0 ;
            let name = temp.result.length > 0 ? temp.result[0].main_title : "";
            let data = temp.result.length > 0 ? temp.result[0].detail : null;
            cc.log("data =",data);
            VM.setValue("help.list",data);
            vc.setTerms(active,name,temp)
        }else{
            vc.setTerms(false,null,null)
        }
    }

    /***
     * 設定註冊設定
     * @param data
     */
    private setUserRegisterSetting(data:any){
        if(data.code == "1"){
            const value = JSON.parse(data.result.code);
            VM.setValue("login.registersetting",value);
            let signupvc = CtrlAssistant.main.getCtrl("SignUpCtrl");
            signupvc.setNickNameSetting(value.demand.nickname);
            signupvc.setPromotionSetting(value.switch.promotion_agent,value.demand.promotion);
            signupvc.setNameSetting(value.demand.name);
            signupvc.setEmailSetting(value.demand.email);
            signupvc.setBirthDaySetting(value.demand.birthday);
            signupvc.setPhoneSetting(value.demand.phone);
            signupvc.setQQSetting(value.demand.qq);
            signupvc.setWeChatSetting(value.demand.wechat);
        }
    }
    /*
     * 設定註冊功能
     */
    private setRegisterEvent(){
        let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
        vc.setRegister(this.RegisterEvent.bind(this),this);
    }

    private RegisterEvent(){
        let vc = CtrlAssistant.main.getCtrl("SignUpCtrl");
        let self = this;

        VM.setValue("login.hasErr",false);
        if(!vc.VerificationCodeＣorrect()){
            self.setErrStatus();
            vc.showVerificationCodeErr();
        }

        if(!vc.PWCorrect()){
            self.setErrStatus();
            vc.showCPWErr();
        }
        if(!vc.AgreeTerms()) {
            self.setErrStatus();
            vc.showTermsErr();
        }
        let setting = VM.getValue("login.registersetting");
        let registerJson = vc.getInputBoxValue();

        if(registerJson.account.length == 0){
            self.setErrStatus();
            vc.showAccountErr(false);
        }

        if(registerJson.account.length < 6 && registerJson.account.length > 0){
            self.setErrStatus();
            vc.showAccountErr();
        }
        if(!registerJson.account.OnlyNumAndEn() && registerJson.account.length > 0){
            self.setErrStatus();
            vc.showAccountFormatErr();
        }
        if(registerJson.pw.length == 0){
            self.setErrStatus();
            vc.showPWErr(false);
        }
        if(registerJson.cpw.length == 0){
            self.setErrStatus();
            vc.showCPWErr(false);
        }
        if(registerJson.pw.length < 6 && registerJson.pw.length > 0){
            self.setErrStatus();
            vc.showPWErr();
        }
        if(registerJson.cpw.length < 6 && registerJson.cpw.length > 0){
            self.setErrStatus();
            vc.showCPWErr();
        }

        if( (setting.demand.email == 2 || registerJson.email.length > 0) && !registerJson.email.isEmail()){
            self.setErrStatus();
            vc.showEmailErr();
        }

        if((setting.demand.phone == 2 || registerJson.phone.length > 0) && !registerJson.phone.isPhoneNumber()){
            self.setErrStatus();
            vc.showPhoneErr();
        }

        if(registerJson.qq.length > 0 && registerJson.qq.length < 5){
            self.setErrStatus();
            vc.showQQErr();
        }

        if(registerJson.qq.length > 11 ){
            self.setErrStatus();
            vc.showQQErrTooLong();
        }

        if((setting.demand.wechat.length < 2 && registerJson.wechat.length > 0)){
            self.setErrStatus();
            vc.showWeChatErr();
        }

        if(!setting.demand.wechat.toString().isWeChatNum()){
            self.setErrStatus();
            vc.showWeChatErrWithZH();
        }

        if((setting.demand.nickname == 2 || registerJson.nickname.length > 0) && registerJson.nickname.length < 2){
            self.setErrStatus();
            vc.showNickNameErr();
        }

        if((setting.demand.name == 2 ||registerJson.name.length > 0 || !registerJson.name.isName())){
            if(registerJson.name.length <2 && registerJson.name.length > 0){
                self.setErrStatus();
                vc.showNameErr();
            }

            if(!registerJson.name.isName() && registerJson.name.length > 0){
                self.setErrStatus();
                vc.showNameFormatErr();
            }
            if( registerJson.name.length == 0){
                self.setErrStatus();
                vc.showNameFormatErr(false);
            }

        }

        if(setting.demand.promotion == 2 && registerJson.promotion.length > 0){
            self.setErrStatus();
        }

        if(VM.getValue("login.hasErr")){
            self.showErr();
            return;
        }
        console.log("可以註冊");
        VM.setValue("core.lock",true);
        ApiManager.Login.Register(registerJson.promotion,registerJson.account,registerJson.pw,registerJson.nickname,registerJson.name,registerJson.phone,registerJson.email,registerJson.birthday,registerJson.qq,registerJson.wechat);
    }

    showErr(){
        TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.inputErr"));

    }

    setErrStatus(){
        VM.setValue("login.hasErr",true);
    }
}
