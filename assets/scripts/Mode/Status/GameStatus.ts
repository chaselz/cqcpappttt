
import Awaiter from "../../Extension/AwaitExtension";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
import { VM } from './../../modelView/ViewModel';
import {ApiStatus} from "../../Enum/ApiStatus";
import {PageViewType} from "../../Enum/PageViewType";
import Alert from "../../UI/Alert";
import GeneralFunction from "../../Tools/GeneralFunction";
import MobileExtension from "../../Extension/Nactive/MobileExtension";
import PageManager from "../../ManagerUI/PageManager";
var UIAssistant = require("UIAssistant");
var ApiManager = require("ApiManager");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

@ccclass
export default class GameStatus extends IHierarchicalState {

    private apiindex = 0;
    private errcount = 0;
    private vc = null;
    StateBegin(){
        this.vc = CtrlAssistant.main.getCtrl("InitViewCtrl");
        this.StateInit();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private async StateInit(){
        this.vc.initProgress();
        this.setInitViewStat(0.1,"connectongserver");
        ApiManager.init(false,VM.getValue("setting.platform"));
        //等候取的可以連線的domain才開始初始化
        await Awaiter.until(_=>ApiManager.status != ApiStatus.Init);
        this.checkApiStatus()
    }

    private checkApiStatus(){
        ApiManager.get.SiteInfo();
        if(ApiManager.status == ApiStatus.Limit){
            UIAssistant.main.SetPanelVisible("InitView",false);
            UIAssistant.main.SetPanelVisible("LimitView",true);
        } else if(ApiManager.status == ApiStatus.Err) {
            this.errcount++;
            this.setInitViewStatWithString(0.2,"connectserver_err".format(this.errcount));
            if(this.errcount < 4){
                this.StateInit();
            }else{
                this.showAlert("connecterr","connecterrcontent");
            }
        }else if(ApiManager.status == ApiStatus.NoIp){
            this.showAlert("err","iperr");
        }
        else {
            this.setInitViewStat(0.5,"beconnectserver");
            UIAssistant.main.SetPanelVisible("LimitView",false);
            this.CheckHaveAcoount();
            this.apiindex = this.apiindex = ApiManager.addListenter(this.Listenter.bind(this));
        }
    }

    private showAlert(titlekey:string,contentkey:string){
        Alert.main.Show("lang."+titlekey,VM.getValue("lang."+contentkey),this.connectToCustomer.bind(this),null);
    }

    private connectToCustomer(){
        let url = cc.sys.localStorage.getItem("serviceurl");
        if(!url)
            url = VM.getValue("setting.serviceurl");

        if(url){
            let vc = CtrlAssistant.main.getCtrl("MainCtrl");
            vc.setWebUrl(url);
        }else{
            TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.customerserviceconnecterr"));
        }
    }


    private setInitViewStat(progress,titlekey=""){
        this.vc.setState(progress,VM.getValue("lang."+titlekey));
    }
    private setInitViewStatWithString(progress,title:string){
        this.vc.setState(progress,title);
    }
    /*
     * 檢查有無之前帳號資料
     */
    private async CheckHaveAcoount(){
        let account = cc.sys.localStorage.getItem('account');
        let token = cc.sys.localStorage.getItem('token');
        let rsa = cc.sys.localStorage.getItem('rsa');
        this.setInitViewStat(0.8,"loadingobj");
        if((account != null || account != undefined) && (token != null || token != undefined)){
            ApiManager.publicKey = rsa;
            ApiManager.APIToken = token;
            ApiManager.User.get.UserData();
        }else{
            this.setInitViewStat(1,"loadedobj");
            await Awaiter.until(_=> !VM.getValue("core.lock") );
            await Awaiter.until(_=>this.vc.finish);
            VM.setValue("core.initapp",false);
            await PageManager.HomePageAsync();
            this.checkHasPushNotificationData();
        }
    }

    private async Listenter(type, data){
        if(VM.getValue("core.islogin"))
            return;

        if(type == ApiType.userDataType){
            this.setInitViewStat(1,"loadedobj");
            if(data.code == "1"){
                VM.setValue("core.initapp",false);
                GeneralFunction.LoginMustFunc();
                await Awaiter.until(_=>!VM.getValue("core.lockinfunc"));
                await Awaiter.until(_=>this.vc.finish);
                await PageManager.HomePageAsync();
            }else{
                await Awaiter.until(_=>this.vc.finish);
                VM.setValue("core.initapp",false);
                PageManager.SignInPage();
                await Awaiter.until(_=> UIAssistant.main.GetCurrentPage() == PageViewType.SignIn);
            }
            this.checkHasPushNotificationData();
        }
    }

    /***
     * 檢查是否有點擊推播而開啟App的推播事件
     */
    private checkHasPushNotificationData(){
        let jsonStr = MobileExtension.GetPushNotificationData();
        if(!jsonStr.isNull()){
            let vc = CtrlAssistant.main.getCtrl("MainCtrl");
            vc.PushNotificationEvent(jsonStr);
        }
    }
}
