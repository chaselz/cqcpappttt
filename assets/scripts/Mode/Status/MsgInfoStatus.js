var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
    },

    StateBegin(){
        let vc = CtrlAssistant.main.getCtrl("MsgInfoCtrl");
        vc.setMsgInfo(VM.getValue("msg.msgInfo"));
    },

    StateUpdate(){
    },

    StateEnd(){
    },
});
