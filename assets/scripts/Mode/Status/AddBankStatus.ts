import DropDownOptionData from "../../UI/DropDown/DropDownOptionData";
import Awaiter from "../../Extension/AwaitExtension";
import Alert from "../../UI/Alert";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

@ccclass
export default class AddBankStatus extends IHierarchicalState {

    private apiindex:Number = 0;

    StateBegin(){
        this.Init();
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        this.NeedApi();
        this.asyncAwaitLoad();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private NeedApi(){
        ApiManager.User.get.CheckIsSetPasswordWithDraw();
        ApiManager.Deposit.get.BankList();
    }

    private Init(){
        // UIAssistant.main.SetPanelVisible("Load",false);
        VM.setValue("core.lock",false);
        VM.setValue("addbank.selectedIndex",-1);
        VM.setValue("addbank.banklist",null);
        VM.setValue("addbank.checkissetpw",false);

    }

    private apilistente(type:ApiType, data:any){
        switch (type) {
            case ApiType.bankListType:
                this.AnalyzeBankList(data);
                break;
            case ApiType.checkIsSetPWType:
                this.checkIsSetBankPw(data);
                break;
            case ApiType.savebankcardType:
                this.AnalyzeSaveBankResult(data);
                break;
        }
    }

    private AnalyzeBankList(data:any){
        if(data.code == "1") {
            let vc = CtrlAssistant.main.getCtrl("AddBankCtrl");
            let temp = [];
            VM.setValue("addbank.banklist",data.result);
            data.result.forEach(x=>{
                let datatemp = new DropDownOptionData();
                datatemp.setValue(x.bank_name);
                temp.push(datatemp)
            });
            vc.SetBankDropBoxValue(temp);
        }
    }

    private checkIsSetBankPw(data:any){
        if(data.code == "1"){
            let view = CtrlAssistant.main.getCtrl("AddBankCtrl");
            view.setPwEditorBoxActive(data.result.isSet);
            VM.setValue("addbank.checkissetpw",true);
        }
    }

    private async asyncAwaitLoad(){
        await Awaiter.until(_=>VM.getValue("addbank.banklist")!= null);
        await Awaiter.until(_=>VM.getValue("addbank.checkissetpw"));
        UIAssistant.main.SetPanelVisible("Load",false);
    }

    private AnalyzeSaveBankResult(data:any){
        let self = this;
        switch (parseInt(data.code)) {
            case 1:
                UIAssistant.main.ShowPreviousPage();
                break;
            case 160302:
                //帳號重複
                self.Init();
                self.NeedApi();
                let vc = CtrlAssistant.main.getCtrl("AddBankCtrl");
                vc.Init();
                vc.ShowErrAlert(TitleAlertType.Error,"lang.bankcardisregister");
                break;
        }
        VM.setValue("core.lock",false);
        Alert.main.Hide();
    }
}
