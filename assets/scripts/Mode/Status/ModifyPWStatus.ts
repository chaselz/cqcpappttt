import {WhiteBaitError} from "../../Enum/Error/WhiteBaitError";
import PageManager from "../../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');

@ccclass
export default class ModifyPWStatus extends IHierarchicalState {

    private apiindex:Number = 0;

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        ApiManager.Deposit.get.UserBankList();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private apilistente(type:ApiType,data:any) {
        if (type == ApiType.updateUserPWType)
            this.updateUserPw(data);
        else if(type == ApiType.logoutType)
            this.logoutFunc(data);
        else if(type == ApiType.updatePwWithDraw)
            this.updateWithDrawPw(data);
        else if(type == ApiType.userBankListType)
            this.AnalyzeBankList(data);
    }

    private AnalyzeBankList(data:any){
        if(data.code = "1"){
            VM.setValue("addbank.userbank",data.result.length);
        }
    }

    private logoutFunc(data:any){
        if(data.code == "1") {
            VM.setValue("core.islogin",false);
            PageManager.SignInPage();
        }
        VM.setValue("core.lock",false);
    }

    private updateUserPw(data:any){
        switch (parseInt(data.code)) {
            case 1:
                this.showAlert(TitleAlertType.Fulfill,"lang.modifypwfinish");
                setTimeout(function(){
                    ApiManager.Login.userlogout();
                },2500);
                break;
            case WhiteBaitError.UPDATE_PASSWORD_ERROR:
                this.showAlert(TitleAlertType.Error,"lang.UPDATEPASSWORDERR");
                break;
            case WhiteBaitError.PASSWORD_REPEAT:
                this.showAlert(TitleAlertType.Error,"lang.PASSWORDREPEATERR");
                break;
        }
        VM.setValue("core.lock",false);
    }

    private updateWithDrawPw(data:any){
        switch (parseInt(data.code)) {
            case 1:
                this.showAlert(TitleAlertType.Fulfill,"lang.modifywithdrawpwfinish");
                setTimeout(function(){
                    PageManager.MemberPage();
                },1000);
                break;
            case WhiteBaitError.OLD_PASSWORD_ERROR:
                this.showAlert(TitleAlertType.Error,"lang.OLDPASSWORDERR");
                break;
            case WhiteBaitError.PASSWORD_REPEAT:
                this.showAlert(TitleAlertType.Error,"lang.PASSWORDREPEATERR");
                break;
        }
        VM.setValue("core.lock",false);
    }

    private showAlert(type:TitleAlertType,key:string){
        let vc = CtrlAssistant.main.getCtrl("ModifyPWCtrl");
        vc.ShowErrAlert(type,key)
    }
}
