import DropDownOptionData from "../../UI/DropDown/DropDownOptionData";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

@ccclass
export default class newDetailsStatus extends IHierarchicalState {

    private apiindex :Number = 0;

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        vc.InitPage();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setShowPicker(false);
    }

    private apilistente(type:ApiType, data:any){
        switch (type) {
            case ApiType.transDepositReportsType:
            case ApiType.transWithDrawReportsType:
                this.AnalyeDWReports(data);
                break;
            case ApiType.gameProviderItemType:
                this.AnalyzeGameProviderItem(data);
                break;
            case ApiType.betsSummaryType:
                this.AnalyzeBetsSummary(data);
                break;
            case ApiType.reportListType:
                this.AnalyzeReportList(data);
                break;
            case ApiType.frontNewTransType:
                this.AnalyzeFundsItem(data);
                break;
        }
    }
    private AnalyzeReportList(data:any){
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.setFundsDate(data.result);
                break;
            default:
                vc.setLoadActive(false);
                vc.showNoDataView(true);
                break;

        }
    }
    private AnalyzeBetsSummary(data:any){
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.setBetDate(data.result);
                break;
            default:
                vc.setLoadActive(false);
                vc.showNoDataView(true);
                break;

        }
    }

    private AnalyeDWReports(data:any){
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.setDWData(data.result);
                break;
            default:
                vc.setLoadActive(false);
                vc.showNoDataView(true);
                break;
        }
    }

    private AnalyzeGameProviderItem(data:any){
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        if(data.code == "1") {
            VM.setValue("betrecord.gameproviderlist",data.result);
            let temp = [];
            temp.push(VM.getValue("lang.all"));
            data.result.forEach(x => {
                temp.push(x.name);
            });

            vc.setGameProvider(temp);
        }
    }

    private AnalyzeFundsItem(data:any){
        let vc = CtrlAssistant.main.getCtrl("newDetailsCtrl");
        vc.setFundsMenuData(data.result);
    }
}
