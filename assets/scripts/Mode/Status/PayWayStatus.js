import wechatMode from "../../Paymode/wechatMode";
import BankOnlineMode from "../../Paymode/BankOnlineMode";
import qqMode from "../../Paymode/qqMode";
import jingDongMode from "../../Paymode/jingDongMode";
import unionPayMode from "../../Paymode/unionPayMode";
import CGPMode from "../../Paymode/CGPMode";
import gpMode from "../../Paymode/gpMode";
import AlipayCardMode from "../../Paymode/AlipayCardMode";
import wechatCardMode from "../../Paymode/wechatCardMode";

var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var CommonMode = require("CommonMode");
var alipayMode = require("alipayMode");
const {PayType} = require("PayType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        /**
         * Api監聽事件
         * @param type
         * @param data
         * @returns {Promise<void>}
         */
        this.apilistenter = async function (type,data) {
            switch (type) {
                case ApiType.depositModeMenuType:
                    if(this.mode)
                        this.mode.AnalyzedepositModeMenuTypeFunc(data);
                    break;
                case ApiType.depositModeBankMenuType:
                    if(this.mode)
                        this.mode.AnalyzedepositModeBankMenuTypeFunc(data);
                    break;
                case ApiType.depositProviderType:
                    if(this.mode)
                        this.mode.AnalyzeDepositResponceFunc(data);
                    break;
                case ApiType.depositFourthType:
                    if(this.mode)
                        this.mode.AnalyzeDepositResponceFunc(data);
                    break;
            }
        };
        /**
         * 設定選擇支付頁的DropDown callback
         */
        this.setDropDownCB = function () {
            let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
            let self = this;
            vc.setDropDownCallBack(vc.choosepath+"PayWay/DropDown",function (index) {
                self.mode.setPayWayDropDownEvent(index);
            });

            vc.setDropDownCallBack(vc.choosepath+"transferWay/DropDown",function (index) {
                self.mode.setTransferDropDownEvent(index);
            });
        };
        /**
         * 初始化
         */
        this.InitValue = function () {
            VM.setValue("recharge.payway",undefined);
            VM.setValue("recharge.bankindex",undefined);
            VM.setValue("recharge.cardinfo",undefined);
            VM.setValue("recharge.paywaylist",undefined);
            VM.setValue("recharge.depositway",undefined);
            VM.setValue("recharge.depositbranch","");
            VM.setValue("recharge.depositname",undefined);
            VM.setValue("recharge.deposittime",undefined);

            VM.setValue("recharge.bankcardid",undefined);
            VM.setValue("recharge.bankpoint",undefined);
            VM.setValue("recharge.bankusername",undefined);
            VM.setValue("recharge.bankname",undefined);
            VM.setValue("recharge.bankid",undefined);
            VM.setValue("recharge.depositwayindex",undefined);
        }
        /**
         * 設定支付模組
         * @constructor
         */
        this.InitMode = function () {
            let self = this;
            console.log("init ode = ",VM.getValue("recharge.paytype"));
            switch (VM.getValue("recharge.paytype")) {
                case PayType.Common:
                    self.mode = new CommonMode.default();
                    break;
                case PayType.AliPay:
                    self.mode = new alipayMode.default();
                    break;
                case PayType.Wechat:
                    self.mode = new wechatMode();
                    break;
                case PayType.BankOnline:
                    self.mode = new BankOnlineMode();
                    break;
                case PayType.QQ:
                    self.mode = new qqMode();
                    break;
                case PayType.JingDong:
                    self.mode = new jingDongMode();
                    break;
                case PayType.UnionPay:
                    self.mode = new unionPayMode();
                    break;
                case PayType.CGP:
                    self.mode = new CGPMode();
                    break;
                case PayType.GP:
                    self.mode = new gpMode();
                    break;
                case PayType.AlipayCard:
                    self.mode = new AlipayCardMode();
                    break;
                case PayType.WechatCard:
                    self.mode = new wechatCardMode();
                    break;
            }
        };
        this.setChooseNextBtnEvent = function () {
            if(!this.mode)
                return;

            let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
            vc.SetChooseNextBtnEvent(this.mode.ChooseNextBtnEvent,this.mode);
        };
        this.clearChooseNextBtnEvent = function () {
            let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
            vc.ClearChooseNextBtnEvent();
        };
        this.setTitle = function(){
            let title = VM.getValue("lang."+VM.getValue("recharge.paytype")+"pay");
            VM.setValue("core.title",title);
        };
        this.apiindex = 0;
        this.mode = undefined;
    },

    StateBegin(){
        this.setTitle();
        this.InitValue();
        this.InitMode();
        this.setChooseNextBtnEvent();
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.Deposit.get.DepositModeMenu(VM.getValue("recharge.paytype"),VM.getValue("recharge.money"));

        // let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        // vc.init();
        this.setDropDownCB();
    },

    StateUpdate(){

    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        this.clearChooseNextBtnEvent();
    },
});
