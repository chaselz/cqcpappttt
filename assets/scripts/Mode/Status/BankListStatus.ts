
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');

@ccclass
export default class BankListStatus extends IHierarchicalState {

    private apiindex :Number = 0;

    StateBegin(){
        let vc = CtrlAssistant.main.getCtrl("BankListCtrl");
        vc.ShowLoad();
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        ApiManager.Deposit.get.UserBankList();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private apilistente(type:ApiType,data:any){
        if(type == ApiType.userBankListType){
            this.AnalyzeBankList(data);
        }else if(type = ApiType.bankDefaultType){
            this.AnalyzeDefaultApi(data);
        }
    }

    private AnalyzeBankList(data:any){
        if(data.code = "1"){
            let vc = CtrlAssistant.main.getCtrl("BankListCtrl");
            VM.setValue("addbank.userbank",data.result.length);
            if(data.result.length == 0)
                vc.ShowAddBankView();
            else
                vc.setBankList(data.result);

            vc.CloseLoad();
        }
    }

    private AnalyzeDefaultApi(data:any){
        if(data.code = "1"){
            ApiManager.Deposit.get.UserBankList();
        }
        VM.setValue("core.lock",false);
    }
}
