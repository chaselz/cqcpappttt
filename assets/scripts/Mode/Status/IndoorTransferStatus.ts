import {WhiteBaitError} from "../../Enum/Error/WhiteBaitError";
import {ApiErr} from "../../Enum/Error/ApiErr";
import BalanceManager from "../../ManagerUI/BalanceManager";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

@ccclass
export default class IndoorTransferStatus extends IHierarchicalState {

    private apiindex:Number = 0;
    private vc : any = null;
    StateBegin(){
        this.vc = CtrlAssistant.main.getCtrl("IndoorTransferCtrl");
        this.Init();
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        ApiManager.User.get.UserGameBalanceProgress();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private Init(){
        this.vc.LoadActive(false);
        VM.getValue("transfer.from",null);
        VM.getValue("transfer.to",null);
    }

    private apilistente(type:ApiType,data:any) {
        switch (type) {
            case ApiType.userBalanceType:
                this.AnalyzeUserBalanceFunc(data);
                break;
            case ApiType.gameBalanceProgressType:
                this.AnalyzegamebalanceprogressFunc(data);
                break;
            case ApiType.changeRedeemType:
                this.AnalyzechangeRedeemFunc(data);
                break;
            case ApiType.gameRedeemType:
                //this.AnalyzeGameRedeem(data);//
                break;
        }
    }

    private AnalyzeGameRedeem(value) {
        ApiManager.User.get.UserBalance();
        ApiManager.User.get.UserGameBalanceProgress();
    };

    private AnalyzechangeRedeemFunc(value:any){

        switch (parseInt(value.code)) {
            case 1:
                ApiManager.User.get.UserGameBalanceProgress();
                ApiManager.User.get.UserBalance();
                this.vc.showAlert(TitleAlertType.Fulfill,"transfersuccess");
                this.vc.transferFinish();
                break;
            default:
                this.vc.LoadActive(false);
                break;
        }
    }

    private AnalyzeUserBalanceFunc(value:any) {
        if(value.code == "1"){
            if(value.result.center){
                VM.setValue("member.cash",value.result.center);
                this.vc.setUserMoney(value.result.center);
            }
        }
    };

    private AnalyzegamebalanceprogressFunc(value) {
        if(value.code == "1"){
            this.vc.setGameBalance(value.result.data);
        }
    };

}
