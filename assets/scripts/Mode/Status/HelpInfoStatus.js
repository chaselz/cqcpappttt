var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var UIAssistant = require("UIAssistant");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apilistenter = async function(type,value){
            let vc = CtrlAssistant.main.getCtrl("HelpInfoCtrl");
            switch (type) {
                case ApiType.frontDocContentType:
                    this.AnalyzeHelpInfoFunc(value,vc);
                    break;
            }
        };

        this.AnalyzeHelpInfoFunc = function (value,vc) {
            UIAssistant.main.SetPanelVisible("Load",false);
            if(value.code == "1"){
                vc.setHelpInfo(value.result[0]);
            }
        };
        this.apiindex = 0;
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.get.FrontDocContent(VM.getValue("help.helpid"));
        UIAssistant.main.SetPanelVisible("Load",true);
    },

    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        let vc = CtrlAssistant.main.getCtrl("HelpInfoCtrl");
        vc.clearHelpInfo();
    },

});
