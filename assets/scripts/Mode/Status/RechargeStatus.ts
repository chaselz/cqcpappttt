
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
import { VM } from './../../modelView/ViewModel';
var UIAssistant = require("UIAssistant");

@ccclass
export default class RechargeStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.Listener.bind(this));
        UIAssistant.main.SetPanelVisible("Load",true);
        ApiManager.Deposit.get.DepositMenu();
        this.InitValue();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private InitValue(){
        VM.setValue("recharge.money","");
        VM.setValue("recharge.ischeckmoney",false);
        VM.setValue("recharge.payJson",[]);
        VM.setValue("core.title",VM.getValue("lang.recharge"));
    }

    private async Listener(type : ApiType ,data : any){

        if(type != ApiType.depositMenuTypeOne && type != ApiType.depositMenuType)
            return;

        let vc = CtrlAssistant.main.getCtrl("RechargeCtrl");
        let t = VM.getValue("recharge.payJson");
        UIAssistant.main.SetPanelVisible("Load",false);

        if(data.code == "1") {
            let temp = data.result;
            vc.setShowData(temp);
            VM.setValue("recharge.payJson",temp);
            if(t.length == 0)
                return;

            let difference = t.filter(v=>temp.findIndex(e=>e.mode ==v.mode) < 0);
            vc.setHideData(difference);

            if(type == ApiType.depositMenuTypeOne)
                VM.setValue("recharge.ischeckmoney",true);
        }
        VM.setValue("core.lock",false);
    }

}
