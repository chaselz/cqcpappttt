const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

@ccclass
export default class DWRecordsStatus extends IHierarchicalState {

    private  apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        let vc = CtrlAssistant.main.getCtrl("DWRecordsCtrl");
        vc.Init();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private apilistente(type:ApiType,data:any){
        if(type == ApiType.transDepositReportsType || type == ApiType.transWithDrawReportsType)
            this.AnalyzeReports(data);
    }

    private AnalyzeReports(data:any){
        if(data.code == "1") {
            let vc = CtrlAssistant.main.getCtrl("DWRecordsCtrl");
            vc.setRecordList(data.result);
        }
    }

}
