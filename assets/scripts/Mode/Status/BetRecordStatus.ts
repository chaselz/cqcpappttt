import {OctopusError} from "../../Enum/Error/OctopusError";
import Awaiter from "../../Extension/AwaitExtension";
import DropDownOptionData from "../../UI/DropDown/DropDownOptionData";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');

@ccclass
export default class BetRecordStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.ApiListener.bind(this));
        let vc = CtrlAssistant.main.getCtrl("BetRecordCtrl");
        vc.ShowLoadView();
        vc.InitSearchView();
        ApiManager.Game.get.GameProviderItem("");
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private ApiListener(type:ApiType,data:any){
        switch (type) {
            case ApiType.gameProviderItemType:
                this.AnalyzeGameProviderItem(data);
                break;
        }
    }

    private AnalyzeGameProviderItem(data:any){
        let vc = CtrlAssistant.main.getCtrl("BetRecordCtrl");
        if(data.code == "1") {
            VM.setValue("betrecord.gameproviderlist",data.result);
            let temp = [];
            let datatemp = new DropDownOptionData();
            datatemp.setValue(VM.getValue("lang.all"));
            temp.push(datatemp);
            data.result.forEach(x => {
                let datatemp = new DropDownOptionData();
                datatemp.setValue(x.name);
                temp.push(datatemp);
            });

            vc.setGameProviderDropDown(temp);
        }
        vc.CloseLoadView();
    }

}
