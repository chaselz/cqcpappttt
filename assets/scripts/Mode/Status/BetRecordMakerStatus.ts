
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');

@ccclass
export default class BetRecordMakerStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.ApiListener.bind(this));
        let vc = CtrlAssistant.main.getCtrl("BetRecordMakerCtrl");
        vc.ShowLoadView();
        vc.InitMakerView();
        ApiManager.Report.BetsSummaryDateNew(VM.getValue("betrecord.targettime"),8,VM.getValue("betrecord.provider"));
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private ApiListener(type:ApiType,data:any){
        switch (type) {
            case ApiType.betsSummaryDateType:
                this.AnalyzebetsSummaryDate(data);
                break;
        }
    }

    private AnalyzebetsSummaryDate(data:any){
        if(data.code == "1") {
            let vc = CtrlAssistant.main.getCtrl("BetRecordMakerCtrl");
            vc.setRecordMakerItem(data.result.data);
            vc.setSumViewValue(data.result.total_bets,data.result.total_profits);
        }
    }
}
