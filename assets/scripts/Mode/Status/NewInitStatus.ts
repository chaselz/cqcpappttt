import MainStatus from "./MainStatus";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var UI_manager = require("UI_manager");
import GameStatus from "./GameStatus";
import newUIDatePicker from "../../../resources/UIDatePicker/newUIDatePicker";
import BirthDayPicker from "../../../resources/birthday/BirthDayPicker";
import {UpdataStatus} from "../../Enum/UpdataStatus";
import MobileExtension from "../../Extension/Nactive/MobileExtension";
import UpdataApp from "../../Tools/UpdataApp";
import {VM} from "../../modelView/ViewModel";
var CtrlAssistant = require("CtrlAssistant");

var TitleAlertView = require("TitleAlertView");

@ccclass
export default class NewInitStatus extends IHierarchicalState {

    private m_node:cc.Node = null;

    constructor(_node:cc.Node) {
        super();
        this.m_node = _node;
    }

    // update (dt) {}
    async StateBegin(){
        VM.setValue("core.initapp",true);
        MobileExtension.GetPlatform();
        MobileExtension.GetVersion();
        this.CheckInterNet();
        await this.CreatorPage();
        this.TransTo(new GameStatus());
        this.TransToLevel(1,new MainStatus());
    }

    LoadLang(){

    }

    private CheckInterNet(){
        console.log("目前網路是 ＝",cc.sys.getNetworkType());
    }

    async CreatorPage(){
        await UI_manager.show_ui_atSync(this.m_node,"Login");
        // await UI_manager.show_ui_atSync(this.m_node,"Main")
        //讓TitleAlertVIew 圖層到最前面
        newUIDatePicker.main.node.setSiblingIndex(10);
        BirthDayPicker.main.node.setSiblingIndex(10);
        TitleAlertView.main.node.setSiblingIndex(10);
    }
}
