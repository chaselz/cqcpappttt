import {RouterError} from "../../Enum/Error/RouterError";
import {WhiteBaitError} from "../../Enum/Error/WhiteBaitError";
import {WhaleError} from "../../Enum/Error/WhaleError";
import {ClownfishError} from "../../Enum/Error/ClownfishError";
import {OctopusError} from "../../Enum/Error/OctopusError";
import {LophiiformesError} from "../../Enum/Error/LophiiformesError";
import {PageViewType} from "../../Enum/PageViewType";
import {UIInterfaceOrientation} from "../../Extension/Nactive/MobileExtension";
import UpdataApp from "../../Tools/UpdataApp";
import PageManager from "../../ManagerUI/PageManager";
import GeneralFunction from "../../Tools/GeneralFunction";
import Awaiter from "../../Extension/AwaitExtension";



const {ccclass, property} = cc._decorator;
const UI_manager = require("UI_manager");
var IHierarchicalState = require("IHierarchicalState");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");
var ApiManager = require("ApiManager");
var TitleAlertType = require("TitleAlertType");
var ApiType = require("ApiType");

@ccclass
export default class MainStatus extends IHierarchicalState {
    private apiindex = 0;
    private intervalID = null;
    private timecount = 0;
    StateBegin(){
        UIAssistant.main.AddLister(this.showPage);
        this.apiindex = this.apiindex = ApiManager.addListenter(this.Listenter.bind(this));
        this.Init();
        this.setPushNotificationEvent();
        this.setIntervalCheckUserStatus();
        this.setResizeCallbackEvent();
        this.setWebViewCloseListener();
        this.getWebViewIndex();


        cc.game.on(cc.game.EVENT_HIDE, function(){
            clearInterval(this.intervalID);
            console.log("游戏进入后台");
        },this);
        cc.game.on(cc.game.EVENT_SHOW, function(){
            // this.timecount = 3;
            if(VM.getValue("core.islogin")) {
                ApiManager.User.get.UserBalance();
                this.setIntervalCheckUserStatus();
            }
            console.log("重新返回游戏");
        },this);
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private Init(){
        VM.setValue("core.isPORTRAIT",UIInterfaceOrientation.Portrait);
        VM.setValue("core.isweb",false);
    }

    /***
     * 設定監聽 PushNotification 事件
     */
    private setPushNotificationEvent(){
        if(!cc.sys.isNative)
            return;
        cc.game.on("pushnotification",(value)=>{
            let vc = CtrlAssistant.main.getCtrl("MainCtrl");
            vc.PushNotificationEvent(value);
        });
    }

    private getWebViewIndex()
    {
        if(!cc.sys.isNative)
            return;
        cc.game.on("webindex",(index)=>{
            VM.setValue("core.webIndex",index);
        });
    }

    private setWebViewCloseListener(){
        if(!cc.sys.isNative)
            return;
        cc.game.on("closeTouchBtn",()=>{
            let vc = CtrlAssistant.main.getCtrl("MainCtrl");
            vc.CloseWebView();
        });
    }

    private setResizeCallbackEvent(){
        if(!cc.sys.isNative)
            return;
        cc.game.on("DeviceOrientation",(value)=>{
            let isPORTRAIT = VM.getValue("core.isPORTRAIT") == UIInterfaceOrientation.Portrait;
            if(!VM.getValue("core.isweb") && isPORTRAIT)
                return;

            let vc = CtrlAssistant.main.getCtrl("MainCtrl");
            vc.reSetMainWidget(value);
        });

        if(cc.sys.os == cc.sys.OS_ANDROID) {
            //jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'isOpenCocosApp');
        }else if(cc.sys.os == cc.sys.OS_IOS)
            jsb.reflection.callStaticMethod('AppController', 'isOpenCocosApp');
    }


    private showPage(page_name: string) {
        VM.setValue("core.gameType",page_name);
        let canShowBackBtn = !VM.getValue("setting.back_blacklist").includes(page_name);
        let showservice = VM.getValue("setting.service_whitelist").includes(page_name);
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setShowBackBtn(canShowBackBtn);
        vc.setShowSignInAlert(!canShowBackBtn && !VM.getValue("core.islogin"));
        vc.setShowTitle(page_name != PageViewType.Home);
        vc.setShowServiceBtn(showservice);
        vc.setShowJBIcon(page_name == PageViewType.Home);
        if(showservice)
            vc.setShowPicker(false);
        else///在投注明細的後面個類型列表需隱藏日曆按鈕
            if(page_name == PageViewType.BetRecordMaker || page_name == PageViewType.BetRecordNum || page_name == PageViewType.BetRecordGame)
                vc.setShowPicker(false);

        if(VM.getValue("setting.toolbarlist").includes(page_name))
            vc.setToolBarStatus(page_name);
    }

    private async Listenter(type,data:any){
        switch (parseInt(data.code)) {
            //case RouterError.TOKEN_INVALID_ERROR:
            case 1:
                this.timecount = 0;
                break;
            case 781234:
                this.timecount++;
                // if(this.timecount>3){
                //     clearInterval(this.intervalID);
                // }
                break;
            case WhiteBaitError.USER_TOKEN_FAIL:
            case WhaleError.USER_TOKEN_FAIL:
            case ClownfishError.USER_TOKEN_FAIL:
            case OctopusError.USER_TOKEN_FAIL:
            case LophiiformesError.USER_TOKEN_FAIL:
            case RouterError.TOKEN_INVALID_ERROR:
            case RouterError.TOKEN_REQUIRED_ERROR:
                if(VM.getValue("core.initapp"))
                    return ;

                VM.setValue("core.islogin",false);
                VM.setValue("member.msgcount",0);
                PageManager.SignInPage();
                break;
            case RouterError.RSA_KEY_ERROR:
                await Awaiter.sleep(1000);
                this.ShowAlert(TitleAlertType.Error,"lang.tokenorrsaerr");
                this.AnalyzelogoutFunc({code : "1"});
                break;
        }
        switch (type) {
            case ApiType.gameEnableType:
                this.saveGameEnabled(data);
                break;
            case ApiType.logoutType:
                this.AnalyzelogoutFunc(data);
                break;
            case ApiType.userDataType:
                this.AnalyzeUserData(data);
                break;
            case ApiType.msgListType:
                this.AnalyzemsgListFunc(data);
                break;
            case ApiType.siteInfoType:
                this.AnalysisSiteInfo(data);
                break;
            case ApiType.luckymoneyType:
                this.AnalysisNeverLuckMoney(data);
                break;
            case ApiType.loginType:
                this.AnalysisLoginEvent(data);
                break;
        }
    }

    private AnalysisLoginEvent(data:any){
        ApiManager.APIToken = data.result.api_token;
        ApiManager.publicKey = data.result.key;
        cc.sys.localStorage.setItem("token",data.result.api_token);
        cc.sys.localStorage.setItem("rsa",data.result.key);
    }

    /***
     * 存儲未領取的紅包數量
     * @param data
     * @constructor
     */
    private AnalysisNeverLuckMoney(data:any){
        var count =0;
        if(data.code == 1 && data.result.data){
            var never = data.result.data.filter(temp => temp.status == 2);
            count = never.length;
        }
        VM.setValue("member.luckmoneycount",count);
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setMemberAlertState();
    }

    private AnalysisSiteInfo(data:any){
        if(data.code == "1") {
            VM.setValue("setting.serviceurl",data.result.customer_url);
            cc.sys.localStorage.setItem("serviceurl",data.result.customer_url);
        }
    }


    private AnalyzemsgListFunc(value:any) {
        var count =0;
        if(value.code == "1"){
            if(value.result){
                let noread = value.result.filter(x=>x.readed == 0);
                count = noread.length;
            }
        }
        VM.setValue("member.msgcount",count);
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setMemberAlertState();
    };
    private AnalyzeUserData(value:any){
        if(value.code == "1"){
            VM.setValue("member.name",value.result.name);
        }
    }
    private AnalyzelogoutFunc(value:any) {
        if(value.code == "1"){
            PageManager.SignInPage();
            VM.setValue("core.islogin",false);
            VM.setValue("member.msgcount",0);
            ApiManager.publicKey = "";
            ApiManager.APIToken = "";
            cc.sys.localStorage.setItem("token","");
            cc.sys.localStorage.setItem("rsa","");
        }
    };

    private saveGameEnabled(data:any){
        if(data.code == "1"){
            VM.setValue("core.gameenable",data.result);
        }
    }
    /***
     *
     * @param tyep
     * @param key
     * @constructor
     */
    private ShowAlert(tyep,key){
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.ShowTitleAlert(tyep,key);
    }

    /***
     * 設定檢查Token的計時器
     */
    private setIntervalCheckUserStatus(){
        this.intervalID = setInterval(function () {
            if(VM.getValue("core.islogin"))
                ApiManager.User.get.UserBalance();
        },8000);
    }

}
