var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var ApiType = require("ApiType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apilistenter = async function(type,value){
            let vc = CtrlAssistant.main.getCtrl("HelpCenterCtrl");
            UIAssistant.main.SetPanelVisible("Load",false);
            switch (type) {
                case ApiType.frontDocListType:
                    this.AnalyzeHelpListFunc(value,vc);
                    break;
            }
        };

        this.AnalyzeHelpListFunc = function (value,vc) {
            if(value.code == "1"){
                VM.setValue("help.helpList",value.result); //0:常見問題 1:存款幫助 2:取款幫助 3:代理加盟 4:關於我們
                vc.setCommonProblemList(vc.getHelpData("常见问题"));
                // vc.setCommonProblemList(value.result[0].detail);
            }
        };
        this.apiindex = 0;
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.get.FrontDocList(1);
        UIAssistant.main.SetPanelVisible("Load",true);
    },

    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },

});
