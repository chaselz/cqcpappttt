
var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var UIAssistant = require("UIAssistant");
var HallType = require("HallType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apilistenter = async function(type,value){
            let vc = CtrlAssistant.main.getCtrl("HallCtrl");
            switch (type) {
                case ApiType.userDataType:
                    if(value.code == "1"){
                        vc.setUserName(value.result.account);
                    }else{
                        this.ErrEvent(value.code);
                    }
                break;
                case ApiType.userBalanceType:
                    if(value.code == "1"){
                        vc.setUserMoney(value.result.center);
                    }else{
                        this.ErrEvent(value.code);
                    }
                break;
                case ApiType.gameProviderType:
                    if(value.code == "1"){
                        if(value.result[0].kind[0].code == "slot" && VM.getValue("core.tagCode") != 2)  //阻擋第一頁大廳點擊太快,造成執行兩個 api 的問題
                            return;
                        
                        VM.setValue("hall.gameAllClass",value.result);
                        let type = this.getHallType();
                        vc.setHallData(type,value.result);
                    }else{
                        this.ErrEvent(value.code);
                    }
                    break;
                case ApiType.gameListType:
                    this.AnaylsisGameList(type,value);
                    break;
                case ApiType.gameLoginV2Type:
                    vc.setLoad(false)
                    switch (parseInt(value.code)) {
                        case 1:
                            let type = this.getHallType();
                            vc.setWebUrl(type,value.result.url);
                            break;
                        default:
                            this.ErrEvent(value.code);
                            break;
                    }
                    break;
            }
        };

        /***
         * 分析GameList資料
         * @param type
         * @param value
         * @constructor
         */
        this.AnaylsisGameList = function (type,value) {
            if(value.code == "1"){
                let vc = CtrlAssistant.main.getCtrl("HallCtrl");
                if(VM.getValue("hall.gameListLoading") == null || VM.getValue("hall.gameListLoading") != "Games")
                    return;
                VM.setValue("hall.gameListLoading",null);
                let type = this.getHallType();
                //另外處理的廠商
                if(VM.getValue("hall.platformCode") == "ae"){
                    this.runGameLoginV2(value);
                    return;
                }
                if(VM.getValue("hall.platformCode") == "sb" && type == HallType.sport){
                    this.runGameLoginV2(value);
                    return;
                }
                //正常邏輯
                if(value.result.length > 1){        //同一頁開啟
                    vc.setGameListData(type,value.result);
                }else if(value.result.length == 1){ //開新分頁
                    this.runGameLoginV2(value);
                }else{
                    console.log("回傳的清單沒有內容, 格式錯誤");
                }
            }else{
                this.ErrEvent(value.code);
            }
        };
        /***
         * 執行Game LoginV2 Api
         * @param value
         */
        this.runGameLoginV2 = function (value) {
            if(cc.sys.isNative)
                VM.setValue("hall.gameAllClass",[]);

            VM.setValue("hall.gameCode",value.result[0].code);
            ApiManager.Game.LoginV2(VM.getValue("hall.gameId"),VM.getValue("hall.gameCode"));

        };

        /**
         * 錯誤碼判斷
         */
        this.ErrEvent = function(code){
            switch(String(code)){
                case "784444":  //連線異常，返回上頁
                case "781234":
                    UIAssistant.main.ShowPreviousPage();
                    break;
            }
        };
        this.apiindex = 0;
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.Game.get.GameProviderList(VM.getValue("core.tagCode"));	//1:彩票 2:電子 3:棋牌 4:視訊 5:體育
        let vc = CtrlAssistant.main.getCtrl("HallCtrl");
        VM.setValue("hall.gameListLoading",null);
        let type = this.getHallType();
        vc.InitPage(type);
    },

    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },

    getHallType(){
        let type = VM.getValue("core.tagCode");
        let result = null;
        switch (type) {
            case 1:
                result = HallType.lottery;
                break;
            case 2:
                result = HallType.casino;
                break;
            case 3:
                result = HallType.chesscard;
                break;
            case 4:
                result = HallType.live;
                break;
            case 5:
                result = HallType.sport;
                break;
            case 6:
                result = HallType.fish;
                break;
        }
        return result;
    }

});