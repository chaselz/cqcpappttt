import {ApiErr} from "../../Enum/Error/ApiErr";
import BalanceManager from "../../ManagerUI/BalanceManager";

var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var UIAssistant = require("UIAssistant");
var TitleAlertType = require("TitleAlertType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apilistenter = async function(type,value){
            let vc = CtrlAssistant.main.getCtrl("MemberCtrl");
            switch (type) {
                case ApiType.frontDocListType:
                    break;
                case ApiType.userDataType:
                    this.AnalyzeUserDataFunc(value,vc);
                    break;
                case ApiType.userBalanceType:
                    this.AnalyzeUserBalanceFunc(value,vc);
                    break;
                case ApiType.gameBalanceProgressType:
                    //this.AnalyzegamebalanceprogressFunc(value,vc);
                    break;
                case ApiType.msgListType:
                    this.AnalyzemsgListFunc(value,vc);
                    break;
                case ApiType.luckymoneyType:
                    this.AnalysisNeverLuckMoney(value,vc);
                    break;
                case ApiType.gameRedeemType:
                    //this.AnalyzeGameRedeem(value,vc);
                    break;
            }
        };
        /***
         * 分析未領取紅包數量
         * @param value
         * @param vc
         * @constructor
         */
        this.AnalysisNeverLuckMoney = function (value,vc) {
            if(value.code == "1"){
                let never = null;
                if(value.result.data) {
                    never = value.result.data.filter(temp=> temp.status == 2);
                }
                vc.setLuckMoneyAlert(never);
            }
        };
        //
        this.AnalyzeGameRedeem = function (value,vc) {
            ApiManager.User.get.UserBalance();
            ApiManager.User.get.UserGameBalanceProgress();
        };

        this.AnalyzeFrontDocListFunc = function (value,vc) {
            if(value.code == "1"){

            }
        };
        this.AnalyzeUserDataFunc = function (value,vc) {
            if(value.code == "1"){
                if(value.result){
                    VM.setValue("member.data",value.result);
                    VM.setValue("member.name",value.result.name);
                    vc.setUserData(value.result);
                }
            }
        };
        this.AnalyzeUserBalanceFunc = function (value,vc) {
            if(value.code == "1"){
                if(value.result.center){
                    VM.setValue("member.cash",value.result.center);
                    vc.setMalletMoney(value.result.center);
                }
            }
        };
        this.AnalyzemsgListFunc = function (value,vc) {
            if(value.code == "1"){
                let neverread = null;
                if(value.result){
                    neverread = value.result.filter(temp=>temp.readed == 0);
                }
                vc.setMsgAlert(neverread);
            }
        };

        this.AnalyzegamebalanceprogressFunc = function (value,vc) {
            if(value.code == "1"){
                vc.setGameBalance(value.result.data);
            }
        };
        this.apiindex = 0;
    },

    StateBegin(){
        //IHierarchicalState.prototype.StateBegin.call(this);         //super       //繼承父類function內容   可以同時執行兩個StateBegin
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.get.FrontDocList(1);
        ApiManager.get.FrontDocList(4);

        let isLogin = this.checkIsLogin();
        let vc = CtrlAssistant.main.getCtrl("MemberCtrl");
        vc.setMode(isLogin);
        if(isLogin){
            this.IsLoginApi();
        }else{
            UIAssistant.main.SetPanelVisible("SignInAlertView",true);
        }
        
    },

    StateUpdate(){
    },
    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },

    checkIsLogin(){
        let key = VM.getValue("core.islogin");
        let mode = VM.getValue("core.demo_mode");
        return key && !mode;

    },
    IsLoginApi(){
        ApiManager.User.get.UserData();
        ApiManager.User.get.UserBalance();
        ApiManager.User.get.UserMsg();
        //抓取未領取的紅包api
        ApiManager.Voucher.GetLuckyMoneyList(2,1,20);
        ApiManager.User.get.UserGameBalanceProgress();
    }
});
