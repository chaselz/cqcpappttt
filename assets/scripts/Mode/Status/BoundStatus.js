var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");
var TitleAlertType = require("TitleAlertType");

cc.Class({
    extends: IHierarchicalState,

    properties: {
        list:null,
        detail:null
    },
    ctor(){
        this.apilistenter = function(type,data){
            switch (type) {
                case ApiType.boundListType:     //優惠活動清單
                    if(data.code == "1"){
                        VM.setValue("bound.loading",false);
                        UIAssistant.main.SetPanelVisible("Load",false);
                        //let list = CtrlAssistant.main.getCtrl("BoundCtrl");
                        if(data.result != null & data.result != undefined & data.result.length > 0){
                            VM.setValue("bound.list",data.result);
                            //list.setBoundData();
                        }
                    }
                    break;
                case ApiType.NewPromotionListType:          //切到優惠活動
                    let vc = CtrlAssistant.main.getCtrl("BoundCtrl");
                    if(data.code == "1"){
                        let boundList = data.result.data;
                        if(boundList != null & boundList != undefined & boundList.length > 0){
                            boundList.forEach(boundClass => {
                                boundClass.promo = boundClass.promo.filter((item) => {
                                    return item.mTitleImg != "";
                                });
                            });
                            VM.setValue("bound.list",boundList);
                            vc.setBoundClassHide();
                            vc.newsetBoundData();
                        }
                    }
                    break;
            }
        } ;
        this.apiindex = 0;
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        this.list = CtrlAssistant.main.getCtrl("BoundCtrl");
        this.list.init();
        VM.setValue("bound.loading",true);
        UIAssistant.main.SetPanelVisible("Load",true);
        // ApiManager.get.BoundList(0);
        ApiManager.Game.get.NewPromotionList();
    },


    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },


});
