
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
import { VM } from './../../modelView/ViewModel';
var UIAssistant = require("UIAssistant");

@ccclass
export default class RechargeV2Status extends IHierarchicalState {

    private apiindex = 0;
    private vc = null;
    StateBegin(){
        this.vc = CtrlAssistant.main.getCtrl("RechargeV2Ctrl");
        this.apiindex = ApiManager.addListenter(this.Listener.bind(this));
        this.vc.Init();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private Listener(type: any, data: any){
        switch (type) {
            case ApiType.depositMenuType:
                this.AnalysisDepositMenu(data);
                break;
            case ApiType.depositModeAllMenuType:
                this.AnalysisModeAllMenu(data);
                    break;
            case ApiType.depositModeBankMenuAllType:
                this.AnalysisDepositModeBankMenu(data);
                break;
            case ApiType.bankListType:
                this.AnalysisBankList(data);
                break;
            case ApiType.depositProviderType://轉帳
                this.AnalysisDepositProvider(data);
                break;
            case ApiType.depositFourthType: //四方支付
                this.AnalysisDepositFourth(data);
                break;
        }
    }

    private AnalysisDepositFourth(data:any){
        cc.log("AnalysisDepositFourth");
        this.vc.sentDepositFourthResult(data);
    }

    private AnalysisDepositProvider(data:any){
        switch (parseInt(data.code)) {
            case 1:
                this.vc.sentDepositProviderResutl(data.result);
                break;
            default:
                break;
        }
    }

    private AnalysisBankList(data:any){
        switch (parseInt(data.code)) {
            case 1:
                this.vc.setBankList(data.result);
                break;
            default:
                break;
        }
    }

    private AnalysisDepositModeBankMenu(data:any){
        switch (parseInt(data.code)) {
            case 1:
                VM.setValue("recharge.paywaylist",data.result);
                this.vc.setModeBankMenu(data.result);
                break;
            default:
                break;
        }
    }

    private AnalysisDepositMenu(data:any){
        switch (parseInt(data.code)) {
            case 1:
                this.vc.setRechargeMenuData(data.result);
                break;
            default:
                break;
        }
    }

    private AnalysisModeAllMenu(data:any){
        switch (parseInt(data.code)) {
            case 1:
                VM.setValue("recharge.paywaylist",data.result);
                this.vc.setModeMenu(data.result);
                break;
            default:
                break;
        }
    }
}
