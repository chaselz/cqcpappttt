
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');

@ccclass
export default class BetRecordGameStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.ApiListener.bind(this));
        let vc = CtrlAssistant.main.getCtrl("BetRecordGameCtrl");
        vc.ShowLoadView();
        vc.InitGameView();
        ApiManager.Report.BetsSummaryGameNew(VM.getValue("betrecord.targettime"),8,VM.getValue("betrecord.targetprovider"),"zh-cn");
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private ApiListener(type:ApiType,data:any){
        switch (type) {
            case ApiType.betsSummaryGameType:
                this.AnalyzebetsSummaryGame(data);
                break;
        }
    }

    private AnalyzebetsSummaryGame(data:any){
        if(data.code == "1") {
            let vc = CtrlAssistant.main.getCtrl("BetRecordGameCtrl");
            vc.setRecordGameItem(data.result.data);
            vc.setSumViewValue(data.result.total_bets,data.result.total_profits);
        }
    }
}
