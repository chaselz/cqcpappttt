var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
    },

    StateBegin(){
        let vc = CtrlAssistant.main.getCtrl("HelpListCtrl");
        vc.setHelpList();
    },

    StateUpdate(){
    },

    StateEnd(){
    },

});
