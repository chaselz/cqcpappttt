
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');

@ccclass
export default class BetRecordDateStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.ApiListener.bind(this));
        let vc = CtrlAssistant.main.getCtrl("BetRecordCtrl");
        vc.ShowLoadView();
        ApiManager.Report.BetsSummaryNew(VM.getValue("betrecord.starttime"),VM.getValue("betrecord.endtime"),8,VM.getValue("betrecord.gameprovider"));
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private ApiListener(type:ApiType,data:any){
        switch (type) {
            case ApiType.betsSummaryType:
                this.AnalyzeBetsSummary(data)
                break;
        }
    }

    private AnalyzeBetsSummary(data:any){
        if(data.code == "1"){
            let vc = CtrlAssistant.main.getCtrl("BetRecordCtrl");
            if(data.result.data.length == 0)
                vc.ShowNoDataView();
            else {
                vc.setRecordDateItem(data.result.data);
                vc.setSumViewValue(data.result.total_bets,data.result.total_profits);
            }
        }
    }
}
