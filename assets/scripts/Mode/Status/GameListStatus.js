var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var GameListType = require("GameListType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apiindex = 0;
        this.apilistenter = function(type,value){
            let vc = CtrlAssistant.main.getCtrl("GameListCtrl");
            switch (type) {
                case ApiType.userDataType:
                    if(value.code == "1"){
                        vc.setUserName(value.result.account);
                    }
                break;
                case ApiType.userBalanceType:
                    if(value.code == "1"){
                        vc.setUserMoney(value.result.center);
                    }
                break;
                case ApiType.gameVisionCategory:
                    vc.setLoad(false);
                    VM.setValue("hall.lotteryClassName",value.result);
                    vc.setLotteryClassData();
                    vc.setLotteryClassItems(VM.getValue("hall.lotteryList"));
                    break;
                case ApiType.gameListType:
                    vc.setLoad(false);
                    if(value.code == "1"){
                        if(VM.getValue("core.tagCode") == 1){
                            vc.setLotteryClassItems(value.result);
                        }else{
                            vc.setGameClassData(value.result);
                        }
                    }
                    break;
                case ApiType.gameLoginV2Type:
                    vc.setLoad(false);
                    switch (parseInt(value.code)) {
                        case 1:
                            vc.ShowInWebView(value.result.url);
                            break;
                    }
                    break;
            }
        };
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        let vc = CtrlAssistant.main.getCtrl("GameListCtrl");
        vc.init();
        VM.setValue("hall.search","");
        let gameType = VM.getValue("hall.gameListType");
        vc.InitPage(gameType);
        if(gameType == GameListType.LotteryClass){
            vc.setTimer(true);
        }
    },

    StateUpdate(){
    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        let vc = CtrlAssistant.main.getCtrl("GameListCtrl");
        vc.setTimer(false);
    },

});
