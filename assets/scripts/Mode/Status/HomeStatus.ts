import {VM} from "../../modelView/ViewModel";

var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");

var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var UIAssistant = require("UIAssistant");
var DropDownOptionData = require("DropDownOptionData");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

const {ccclass, property} = cc._decorator;

@ccclass
export default class HomeStatus extends IHierarchicalState {

    private apiindex : number = 0;
    async StateBegin(){
        this.Init();
        this.apiindex = this.apiindex = ApiManager.addListenter(this.Listenter.bind(this));
        this.sentAllTypeApi();
    }

    async StateUpdate(){

    }

    async StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private Init(){
        VM.setValue("bulletin.issetbulletin", false);
        VM.setValue("bulletin.bulletinlist",[]);
        let vc = CtrlAssistant.main.getCtrl("HomeCtrl");
        vc.Init();
    }

    private sentAllTypeApi(){
        ApiManager.get.BannerList(0);
        ApiManager.get.AllBulletinList();
        ApiManager.Game.get.HotActivityList();
        ApiManager.Game.get.GameWinner(0);
    }

    private Listenter(type,data:any){
        switch (type) {
            case ApiType.bannerListType:
                this.AnaylizeBannerList(data);
                break;
            case ApiType.gameWinnerType:
                this.AnaylizeGameWinner(data);
                break;
            case ApiType.bulletinListType:
                this.AnaylizeBulletinList(data);
                break;
            // case ApiType.gameEnableType:
            //     this.saveGameEnabled(data);
            //     break;
            case ApiType.hotactivitylistType:
                this.AnaylizeHotActivityListType(data);
                break;
        }
    }

    private AnaylizeHotActivityListType(data:any){
        if(data.code == "1"){
            let vc = CtrlAssistant.main.getCtrl("HomeCtrl");
            vc.setHotPage(data.result.data);
        }
    }

    // private saveGameEnabled(data:any){
    //     if(data.code == "1"){
    //         VM.setValue("core.gameenable",data.result);
    //     }
    // }

    private AnaylizeBannerList(data:any){
        if(data.code == "1"){
            let vc = CtrlAssistant.main.getCtrl("HomeCtrl");
            let result = data.result.filter(temp=>!temp.mobile_img.isNull());
            vc.setADPage(result);
        }
    }

    private AnaylizeGameWinner(data:any){
        if(data.code == "1"){
            let vc = CtrlAssistant.main.getCtrl("HomeCtrl");
            vc.setGameWinnerList(data.result);
        }
    }

    /***
     * 公告資料整理
     * @param data
     * @constructor
     */
    private AnaylizeBulletinList(data:any){
        if(data.code == "1"){
            let home_bulletinData = VM.getValue("bulletin.bulletinlist");
            Array.prototype.push.apply(home_bulletinData,data.result);
            if(!VM.getValue("bulletin.issetbulletin") ) {
                VM.setValue("bulletin.issetbulletin",true);
                let vc = CtrlAssistant.main.getCtrl("HomeCtrl");
                vc.setBulletinList(home_bulletinData.slice(0,3));
            }
            VM.setValue("bulletin.bulletinlist",home_bulletinData);
        }
    }
}
