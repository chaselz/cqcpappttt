var IHierarchicalState = require("IHierarchicalState");
var UI_manager = require("UI_manager");
var GameStatus = require("GameStatus");
var TitleAlertView = require("TitleAlertView");

cc.Class({
    extends: IHierarchicalState,

    main_node:null,

    properties: {

    },
    ctor(node){
        this.main_node = node;
        console.log("InitStatus Ctor");
    },

    StateBegin(){
        this.LoadLang();
        this.CreatePage();
        this.TransTo(new GameStatus());
    },

    LoadLang(){

    },

    CreatePage(){
        UI_manager.show_ui_at(this.main_node,"Login");
        // UI_manager.show_ui_at(this.main_node,"Main");

    },
    // update (dt) {},
});
