var ApiManager = require("ApiManager");
var IHierarchicalState = require("IHierarchicalState");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var UIAssistant = require("UIAssistant");
var TitleAlertType = require("TitleAlertType");

cc.Class({
    extends: IHierarchicalState,

    ctor(){
        this.apiindex = 0;

        this.apilistenter = function (type,data) {
            let vc = CtrlAssistant.main.getCtrl("MyMessageCtrl");
            switch (type) {
                case ApiType.msgListType:
                    this.AnalyzeMsgListFunc(data,vc);
                    break;
            }
        };

        this.AnalyzeMsgListFunc = function (data,vc) {
            if(data.code == "1"){
                if(data.result.length){
                    vc.setMessageList(data.result);
                }else{
                    UIAssistant.main.SetPanelVisible("Load",false);
                    vc.showMsgNode("MsgNo");
                }
            }else{
                vc.showAlert(TitleAlertType.Error,VM.getValue("lang.err"+data.code.slice(-4)));
            }
        };
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistenter.bind(this));
        ApiManager.User.get.UserMsg();
        UIAssistant.main.SetPanelVisible("Load",true);
        this.init();
    },

    StateUpdate(){

    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    },

    init(){
        VM.setValue("msg.msgInfo",null);
    },
});
