import {PageViewType} from "../../Enum/PageViewType";
import GeneralFunction from "../../Tools/GeneralFunction";
import AnalyticsHelper from "../../Extension/Nactive/AnalyticsHelper";
import PageManager from "../../ManagerUI/PageManager";
import Awaiter from "../../Extension/AwaitExtension";

var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");
var LoginType = require("LoginType");
var UI_manager = require("UI_manager");

let f = cc.Class({
    extends: IHierarchicalState,

    registerSetting : null,

    ctor(){
        this.Init = function () {
            //初始化紅線
            let vc0 = CtrlAssistant.main.getCtrl("LoginCtrl");
            vc0.setToggleBtn(LoginType.SignIn);
            let vc = CtrlAssistant.main.getCtrl("SigninCtrl");
            vc.Init();
        };
        this.apilistente = async function(type,data){
            let temp = data;
            switch (type) {
                case ApiType.loginType:
                    this.AnalyzeLogin(temp);
                    break;
                case ApiType.userregister:
                    this.AnalyzeUserRegister(temp);
                    break;
            }
        };
        this.apiindex = 0;

        this.AnalyzeLogin = async function (data) {
            if(data.code == "1"){
                let signin = CtrlAssistant.main.getCtrl("SigninCtrl");
                await Awaiter.until(_=>!ApiManager.APIToken.isNull() && !ApiManager.publicKey.isNull());
                GeneralFunction.LoginMustFunc(signin.getAccount());
                cc.sys.localStorage.setItem("account",signin.getAccount());
                PageManager.HomePage();
            }else{
                AnalyticsHelper.loginFailed(data.code);
                VM.setValue("login.hasErr",true);
            }
            VM.setValue("core.lock",false);
        };

        this.AnalyzeUserRegister = function (data) {
            if(data.code == "1"){
                const value = JSON.parse(data.result.code);
                VM.setValue("login.registersetting",value);
                let lvc = CtrlAssistant.main.getCtrl("LoginCtrl");
                lvc.setRegister(value.switch.register == 1 );
            }
            VM.setValue("core.lock",false);
        }
    },

    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        ApiManager.Login.userregister();
        this.Init();
        this.setLoginEvent();
        this.setAccountLabel();
    },

    StateUpdate(){

    },

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
        let vc = CtrlAssistant.main.getCtrl("SigninCtrl");
        vc.ClearLoginEvent(this.loginEvent,this);
    },


    /*
     * 設定帳號id
     */
    setAccountLabel(){
        let account = cc.sys.localStorage.getItem("account");
        if(account != undefined && account != null && account.length > 0 ) {
            let signin = CtrlAssistant.main.getCtrl("SigninCtrl");
            signin.setAccount(account);
        }
    },

    /*
     * 設定登入功能
     */
    setLoginEvent(){
        let signin = CtrlAssistant.main.getCtrl("SigninCtrl");
        signin.setLoginEvent(this.loginEvent,this);
    },
    /*
     * 登入功能
     */
    loginEvent(){
        VM.setValue("login.hasErr",false);
        let signin = CtrlAssistant.main.getCtrl("SigninCtrl");
        var err = [];
        if(signin.getAccount().length == 0 && signin.getPassword().length == 0 ){
            err.push("inputaccountpw");
            this.setErrStatus();
        }else if(signin.getAccount().length == 0){
            err.push("inputaccount");
            this.setErrStatus();
        }else if(signin.getPassword().length == 0) {
            err.push("inputpw");
            this.setErrStatus();
        }

        if(err.length > 0){
            this.showErr(err);
            return;
        }

        VM.setValue("core.lock",true);
        ApiManager.Login.userlogin(signin.getAccount(),signin.getPassword());
    },
    /*
     * 顯示錯誤
     */
    showErr(errarr){
        let key ="";
        if(errarr.length>1)
            key = "inputErr";
        else
            key = errarr[0];
        TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang."+key));
    },
    /*
     * 設定錯誤狀態
     */
    setErrStatus(){
        VM.setValue("login.hasErr",true);
    },

});
