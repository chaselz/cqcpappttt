
const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
const {VM} = require('ViewModel');
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");

@ccclass
export default class BetRecordNumStatus extends IHierarchicalState {

    private apiindex = 0;
    StateBegin(){
        this.apiindex = ApiManager.addListenter(this.ApiListener.bind(this));
        this.initValue();
        let vc = CtrlAssistant.main.getCtrl("BetRecordNumCtrl");
        vc.ShowLoadView();
        vc.InitView();
        this.getGameCodeLang();
        vc.BetsSummaryDetailApi(1);
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }
    private initValue(){
        VM.setValue("betrecord.gameplaytype",[]);
    }
    // 取玩法翻譯字典檔API
    private getGameCodeLang () {
        if (VM.getValue("betrecord.targetprovider") === 'vision') {
            ApiManager.Game.get.GamePlayWay(VM.getValue("betrecord.targetgame"),"zh-cn");
        } else if (VM.getValue("betrecord.targetprovider") === 'ag') {
            ApiManager.Game.get.AgPlayFrontend("zh-cn");
        }
    }

    private ApiListener(type:ApiType,data:any){
        switch (type) {
            case ApiType.betsSummaryDetailType:
                this.AnalyzebetsSummaryDetail(data);
                break;
            case ApiType.gameReportLinkType:
                this.AnalyzeBetsSummaryDetailInfo(data);
                break;
            case ApiType.GamePlayWayType:
                this.updataGamePlayType(data);
                break;
            case ApiType.agPlayFrontendType:
                this.updataAgGamePlayType(data);
                break;
        }
    }
    private updataAgGamePlayType(data:any){
        if(data.code == "1"){
            let gameCodeArray = [];
            for (let node in data.result) {
                gameCodeArray.push({ key: data.result[node].PlayType, name: data.result[node].Descripion })
            }
            VM.setValue("betrecord.gameplaytype",gameCodeArray);
        }
    }

    private updataGamePlayType(data:any){
        if(data.code == "1"){
            let gameCodeArray = [];
            for (let node in data.result) {
                gameCodeArray.push({ key: node, name: data.result[node] })
            }
            VM.setValue("betrecord.gameplaytype",gameCodeArray);
        }
    }

    private AnalyzebetsSummaryDetail(data:any){
        if(data.code == "1") {
            let vc = CtrlAssistant.main.getCtrl("BetRecordNumCtrl");
            vc.setRecordNumItem(data.result.data);
            vc.setSumViewValue(data.result.total_bets,data.result.total_profits);
            let count = Math.ceil(data.result.total/10);
            VM.setValue("betrecord.pagecount",count);
            vc.setPreAndNextView(count);
        }
    }

    private AnalyzeBetsSummaryDetailInfo(data:any){
        if(data.code = "1"){
            let vc = CtrlAssistant.main.getCtrl("BetRecordNumCtrl");
            if(data.result.isNull()){
                vc.showAlert(TitleAlertType.Warn,"betdetailnodata");
            }else {
                vc.ShowBetInfoView(data.result);
            }
        }
        UIAssistant.main.SetPanelVisible("Load",false);
    }
}
