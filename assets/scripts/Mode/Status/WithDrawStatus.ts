import {OctopusError} from "../../Enum/Error/OctopusError";
import Awaiter from "../../Extension/AwaitExtension";

const {ccclass, property} = cc._decorator;
var IHierarchicalState = require("IHierarchicalState");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var ApiType = require("ApiType");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");

@ccclass
export default class WithDrawStatus extends IHierarchicalState {

    private apiindex :Number = 0;

    StateBegin(){
        this.Init();
        this.apiindex = ApiManager.addListenter(this.apilistente.bind(this));
        ApiManager.User.get.CheckIsSetPasswordWithDraw();
        this.asyncAwaitLoad();
    }

    StateUpdate(){

    }

    StateEnd(){
        ApiManager.removeListenter(this.apiindex);
    }

    private apilistente(type:ApiType, data:any){
        switch (type) {
            case ApiType.checkIsSetPWType:
                this.checkIsSetBankPw(data);
                break;
            case ApiType.userBankListType:
                this.userbanklist(data);
                break;
            case  ApiType.providerLimitType:
                this.setMinAndMax(data);
                break;
            case ApiType.providerType:
                this.AnalyzeProvider(data);
                break;
        }
    }

    private checkIsSetBankPw(data:any){
        if(data.code == "1") {
            if (data.result.isSet) {
                ApiManager.WithDraw.get.ProviderLimit();
                ApiManager.Deposit.get.UserBankList();
            }else
                this.GoToAddBankView();
        }
    }

    private userbanklist(data:any){
        if(data.code == "1"){
            VM.setValue("addbank.userbank",data.result.length);
            if (data.result.length == 0) {
                this.GoToAddBankView();
                return;
            }
            let bank = data.result.find(x=>x.bank_default == 1);
            let view = CtrlAssistant.main.getCtrl("WithDrawCtrl");
            let lastfouthnum = bank.account.slice(-4);
            VM.setValue("withdraw.bankid",bank.id);
            view.SetBankName(bank.bank_name+VM.getValue("lang.lastnum")+"："+lastfouthnum);
        }
    }

    private setMinAndMax(data:any){
        if(data.code = "1"){
            VM.setValue("withdraw.min",data.result.withdraw_minimum);
            VM.setValue("withdraw.max",data.result.withdraw_maximum);
        }
    }

    private AnalyzeProvider(data:any){
        let vc = CtrlAssistant.main.getCtrl("WithDrawCtrl");
        switch (parseInt(data.code)) {
            case 1:
                vc.ShowAlert(TitleAlertType.Fulfill,"lang.withdrawsuccess");
                UIAssistant.main.ShowPreviousPage();
                break;
            case OctopusError.MEMBER_WITHDRAW_PASS_ERROR:
                vc.PwErrorMode();
                vc.ShowAlert(TitleAlertType.Error,"lang.memberwithdrawpwerr");
                break;
            case OctopusError.MEMBER_TRANS_DEPOSIT_NOT:
                vc.ShowAlert(TitleAlertType.Error,"lang.memberneverdeposit");
                break;
            case OctopusError.BALANCE_NOT_ENOUGH_ERROR:
                vc.ShowAlert(TitleAlertType.Error,"lang.balancenotenough");
                break;
        }
        VM.setValue("core.lock",false);
    }

    private Init(){
        UIAssistant.main.SetPanelVisible("Load",true);
        VM.setValue("core.lock",false);
        VM.setValue("withdraw.min",0);
        VM.setValue("withdraw.max",0);
        VM.setValue("withdraw.bankid",-1);
    }

    private async asyncAwaitLoad(){
        await Awaiter.until(_=>VM.getValue("withdraw.max")!=0);
        await Awaiter.until(_=>VM.getValue("withdraw.bankid")!=-1);
        UIAssistant.main.SetPanelVisible("Load",false);
    }

    private GoToAddBankView(){
        let view = CtrlAssistant.main.getCtrl("WithDrawCtrl");
        view.ShowAddBankView();
    }
}
