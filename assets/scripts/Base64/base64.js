// base64 二进制加解密

var object_type = (function(){
    var t = {};
    var gettype = Object.prototype.toString;
    t.getType = function(o){
        return gettype.call(o);
    }
    t.isObject = function(o){
        return gettype.call(o) == "[object Object]";
    }
    t.isString = function(o){
        return gettype.call(o) == "[object String]";
    }
    t.isNumber = function(o){
        return gettype.call(o) == "[object String]";
    }
    t.isBoolean = function(o){
        return gettype.call(o) == "[object Boolean]";
    }
    t.isUndefined = function(o){
        return gettype.call(o) == "[object Undefined]";
    }
    t.isNull = function(o){
        return gettype.call(o) == "[object Null]";
    }
    t.isArray = function(o){
        return gettype.call(o) == "[object Array]";
    }
    t.isInt8Array = function(o){
        return gettype.call(o) == "[object Int8Array]";
    }
    t.isUint8Array = function(o){
        return gettype.call(o) == "[object Uint8Array]";
    }
    t.isInt16Array = function(o){
        return gettype.call(o) == "[object Int16Array]";
    }
    t.isUint16Array = function(o){
        return gettype.call(o) == "[object Uint16Array]";
    }
    t.isInt32Array = function(o){
        return gettype.call(o) == "[object Int32Array]";
    }
    t.isUint32Array = function(o){
        return gettype.call(o) == "[object Uint32Array]";
    }
    t.isArrayBuffer = function(o){
        return gettype.call(o) == "[object ArrayBuffer]";
    }
    t.isTypedArray = function(o){
        if (t.isInt8Array(o) || t.isUint8Array(o) || t.isInt16Array(o) || t.isUint16Array(o) || t.isInt32Array(o) || t.isUint32Array(o)){
            return true;
        }
        return false;
    }
    t.isBuffer = function(o){
        return gettype.call(o) == "[object Buffer]";
    }
    return t;
}());

var base64 = (function(){
    var t = {};

    function array2arraybuffer(array) {
        var b = new ArrayBuffer(array.length);
        var v = new DataView(b, 0);
        for (var i = 0; i < array.length; i++) {
            v.setUint8(i, array[i]);
        }
        return b;
    }
    
    function arraybuffer2array(buffer) {
        var v = new DataView(buffer, 0);
        var a = new Array();
        for (var i = 0; i < v.byteLength; i++) {
            a[i] = v.getUint8(i);
        }
        return a;
    }

    function string2utf8(str){
        var back = [];
        var byteSize = 0;
        for (var i = 0; i < str.length; i++) {
            var code = str.charCodeAt(i);
            if (0x00 <= code && code <= 0x7f) {
                byteSize += 1;
                back.push(code);
            } else if (0x80 <= code && code <= 0x7ff) {
                byteSize += 2;
                back.push((192 | (31 & (code >> 6))));
                back.push((128 | (63 & code)))
            } else if ((0x800 <= code && code <= 0xd7ff) || (0xe000 <= code && code <= 0xffff)) {
                byteSize += 3;
                back.push((224 | (15 & (code >> 12))));
                back.push((128 | (63 & (code >> 6))));
                back.push((128 | (63 & code)))
            }
        }
        for (i = 0; i < back.length; i++) {
            back[i] &= 0xff;
        }
        return back;
    };

    function utf82string(arr){
        if (typeof arr === 'string') {
            return null;
        }
        var UTF = '';
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == null){
                break;
                //cc.log("error fatal");
            }
            var one = arr[i].toString(2);
            var v = one.match(/^1+?(?=0)/);
            if (v && one.length == 8) {
                var bytesLength = v[0].length;
                var store = arr[i].toString(2).slice(7 - bytesLength);
                for (var st = 1; st < bytesLength; st++) {
                    store += arr[st + i].toString(2).slice(2);
                }
                UTF += String.fromCharCode(parseInt(store, 2));
                i += bytesLength - 1;
            } else {
                UTF += String.fromCharCode(arr[i]);
            }
        }
        return UTF;
    };

    //传入数组或者string
    t.encode = function(data){
        var encoding = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        var is_arraybuffer = false;
        var input_array;
        if (object_type.isArray(data)){
            input_array = data;
        } else if(object_type.isArrayBuffer(data)){
            input_array = arraybuffer2array(data);
        } else if (object_type.isTypedArray(data)){
            input_array = Array.apply( [], data );
        }
        else if(object_type.isString(data)) {
            input_array = string2utf8(data);
        }
        var sz = input_array.length;
        var encode_sz = parseInt((sz + 2)/3) * 4;
        var buffer = new Array(encode_sz);
        var i, j=0;
        for (i=0; i<sz-2; i+=3){
            var v = input_array[i] << 16 | input_array[i+1] << 8 | input_array[i+2];
            buffer[j] = encoding[v >> 18];
            buffer[j+1] = encoding[(v >> 12) & 0x3f];
            buffer[j+2] = encoding[(v >> 6) & 0x3f];
            buffer[j+3] = encoding[(v) & 0x3f];
            j+=4;
        }
        var padding = sz-i;
        var v;
        switch(padding){
            case 1:
                v = input_array[i];
                buffer[j] = encoding[v >> 2];
                buffer[j+1] = encoding[(v & 3) << 4];
                buffer[j+2] = '=';
                buffer[j+3] = '=';
                break;
            case 2:
                v = input_array[i] << 8 | input_array[i+1];
                buffer[j] = encoding[v >> 10];
                buffer[j+1] = encoding[(v >> 4) & 0x3f];
                buffer[j+2] = encoding[(v & 0xf) << 2];
                buffer[j+3] = '=';
                break;
        }
        return buffer.join("");
    }

    function b64index(c){
        var decoding = [62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-2,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51];
        if (c < 43){
            return -1;
        }
        c -= 43;
        if (c >= decoding.length){
            return -1;
        }
        return decoding[c];
    }

    //传出二进制
    t.decode = function(data){
        var text;
        var name = object_type.getType(data);
        if (object_type.isArray(data)){
            text = data;
        } else if (object_type.isString(data)){
            text = string2utf8(data);
        } else if (object_type.isArrayBuffer(data)) {
            text = Array.apply( [], data );
        } else if (object_type.isTypedArray(data)){
            text = Array.apply( [], data );
        } else {
            return [];
        }
        
        var sz = text.length;
        var decode_sz = parseInt((sz+3)/4) * 3;
        var buffer = [];
        var i,j;
        var output = 0;
        for (i=0; i<sz;){
            var padding = 0;
            var c = [];
            for (j=0; j<4; ){
                if (j>=sz){
                    return [];
                }
                c[j] = b64index(text[i]);
                if (c[j] == -1){
                    ++i;
                    continue;
                }
                if (c[j] == -2){
                    ++padding;
                }
                ++i;
                ++j;
            }
            var v;
            switch(padding){
                case 0:
                    v = c[0] << 18 | c[1] << 12 | c[2] << 6 | c[3];
                    buffer[output] = v >> 16;
                    buffer[output+1] = (v >> 8) & 0xff;
                    buffer[output+2] = v & 0xff;
                    output += 3;
                    break;
                case 1:
                    if (c[3] != -2 || (c[2] & 3) != 0){
                        return [];
                    }
                    v = c[0] << 10 | c[1] << 4 | c[2] >> 2;
                    buffer[output] = v >> 8;
                    buffer[output+1] = v & 0xff;
                    output += 2;
                    break;
                case 2:
                    if (c[3] != -2 || c[2] != -2 || (c[1] & 0xf) !=0)  {
                        return [];
                    }
                    v = c[0] << 2 | c[1] >> 4;
                    buffer[output] = v;
                    ++output;
                    break;
                default:
                    return [];
            }
        }
        return buffer;
    }

    t.decode2string = function(str){
        return utf82string(t.decode(str));
    }

    t.decode2arraybuffer = function(str){
        return array2arraybuffer(t.decode(str));
    }
    
    return t;
}());

module.exports = base64;
