import CheckItem from "../Item/CheckItem";
import {ChecckStatus} from "../Enum/ChecckStatus";
import VMLabel from "../modelView/VMLabel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
@ccclass
export default class CheckMenuView extends cc.Component {

    @property(cc.ScrollView)
    main: cc.ScrollView = null;

    @property(cc.Node)
    betcontent: cc.Node = null;

    @property(cc.Node)
    cashcontent: cc.Node = null;

    @property(cc.Node)
    BetAllBtn: cc.Node = null;

    @property(cc.Node)
    CashAllBtn: cc.Node = null;

    @property(VMLabel)
    betTitle: VMLabel = null;

    @property(VMLabel)
    cashTitle: VMLabel = null;

    public static main: CheckMenuView = null;

    private callback : void = null;

    private kind = [];
    private sub_kind = [];
    private items = [];

    private betState:ChecckStatus = ChecckStatus.NO;
    private cashState:ChecckStatus = ChecckStatus.NO;
    private betlock : boolean = false;
    private cashlock : boolean = false;
    protected onEnable(): void {
        this.main.scrollToTop(0);
        this.betcontent.getComponentsInChildren(CheckItem).forEach(item => item.hideSub());
        this.cashcontent.getComponentsInChildren(CheckItem).forEach(item => item.hideSub());
    }

    protected onLoad(): void {
        CheckMenuView.main = this;
        this.BetAllBtn.on("click",  async function () {

            if(this.betState == ChecckStatus.All){
                await this.betcontent.getComponentsInChildren(CheckItem).asyncForEach(async(item) =>{ await item.setAllCancel()});
            }else {
                await this.betcontent.getComponentsInChildren(CheckItem).asyncForEach(async(item) =>{ await  item.setAll()});
            }
            this.betTitle.setWatchPath("lang."+(this.betState==ChecckStatus.All ? "selectall":"cancel"));

                this.menuItemCB();


        },this);
        this.CashAllBtn.on("click",async function () {

            if(this.cashState == ChecckStatus.All){
                await this.cashcontent.getComponentsInChildren(CheckItem).asyncForEach(async(item) =>{ await item.setAllCancel()});
            }else {
                await this.cashcontent.getComponentsInChildren(CheckItem).asyncForEach(async(item) =>{ await item.setAll()});
            }
            this.cashTitle.setWatchPath("lang."+(this.cashState==ChecckStatus.All ? "selectall":"cancel"));

                this.menuItemCB();

        },this);
        this.node.active = false;
    }

    /***
     * 製造及更新物件
     * @param path 父物件
     * @param itemname 物件名
     * @param data 資料
     * @param func 方法
     * @returns {Promise<void>}
     * @constructor
     */
    private async CreatItems(path,itemname,data,func = null,post = false) {
        let dataKey = Object.keys(data);
        await dataKey.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, data[value],value, func);
        });

        for(let i = data.length;i<path.childrenCount;i++){
            path.children[i].active = false;
        }
        if(post) {
            path.getComponentsInChildren(CheckItem).forEach(item => item.setAll());
        }
    }
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    private async creatFactory(path,itemname,index,data,key,onClickFunc) {
        let item = null;
        if(index >= path.childrenCount) {
            item = await UI_manager.create_item_Sync(path, itemname);
            item.getComponent("Iitem").setValue(data,onClickFunc);
        }else {
            item = path.children[index];
            item.getComponent("Iitem").setValue(data);
        }

        item.getComponent(CheckItem).setKey(key);
        item.active = true;

    }

    private menuItemCB() {
        this.kind.length = 0;
        this.sub_kind.length =0;
        this.items.length = 0;
        var bet_sub_kind = [];
        var cash_sub_kind = [];
        this.betcontent.getComponentsInChildren(CheckItem).forEach(item=>{
            if(item.Status == ChecckStatus.All)
                bet_sub_kind.push(item.getKey());
            else if(item.Status == ChecckStatus.Section) {
                this.items = this.items.concat(item.getItems());
            }
        });
        if(bet_sub_kind.length == this.betcontent.childrenCount)
        {
            cc.log("bet all =",bet_sub_kind.length);
            this.betState = ChecckStatus.All;
            this.kind.push(1);
        }else {
            cc.log("bet sel =",bet_sub_kind.length);
            this.betState = ChecckStatus.Section;
            this.sub_kind=this.sub_kind.concat(bet_sub_kind);
        }

        this.cashcontent.getComponentsInChildren(CheckItem).forEach(item=>{
            if(item.Status == ChecckStatus.All)
                cash_sub_kind.push(item.getKey());
            else if(item.Status == ChecckStatus.Section)
                this.items = this.items.concat(item.getItems());
        });
        if(cash_sub_kind.length == this.cashcontent.childrenCount)
        {
            cc.log("cash all");
            this.cashState = ChecckStatus.All;
            this.kind.push(2);
        }else{
            cc.log("cash sel =",cash_sub_kind.length);
            this.cashState = ChecckStatus.Section;
            this.sub_kind=this.sub_kind.concat(cash_sub_kind);
        }
        cc.log("====================");
        cc.log("Kind =",this.kind);
        cc.log("Sub Kind =",this.sub_kind);
        cc.log("Items  =",this.items);
        cc.log("====================");
        if(this.callback)
            this.callback(this.kind,this.sub_kind,this.items);
    }


    public async setData(data:any,cb:void,initpost:boolean = false){
        this.callback = cb;
        this.CreatItems(this.betcontent,"CheckItem",data["1"],this.menuItemCB.bind(this),initpost);
        this.CreatItems(this.cashcontent,"CheckMoneyItem",data["2"],this.menuItemCB.bind(this),initpost);
        if(initpost){
            this.betTitle.setWatchPath("lang.cancel");
            this.cashTitle.setWatchPath("lang.cancel");
            this.betState = ChecckStatus.All;
            this.cashState = ChecckStatus.All;
        }
    }
}
