import DropDownOptionData from "./DropDownOptionData";
import DropDownItem from "./DropDownItem";
import {VM} from "../../modelView/ViewModel";
import {DropDownType} from "../../Enum/DropDownType";
import DropDownAlert from "./DropDownAlert";
import Color = cc.Color;
import AppLog from "../../Extension/AppLog";

var UI_manager = require("UI_manager");

const { ccclass, property } = cc._decorator;

@ccclass()
export default class DropDown extends cc.Component {
    @property
    get interactable () {
        return this._interactable;
    }

    set interactable (value) {
        if(!CC_EDITOR)
            this.switchTouchedEvent(value,this._interactable);

        this._interactable = value;
        this.labelCaption.node.color = VM.getValue("color."+(value?"graylabel3":"graylabel"));
    }
    @property({
        type: cc.Enum(DropDownType),
        displayName: "彈跳類型",
    })
    InputType = DropDownType.Old;
    @property(cc.Node)
    template: cc.Node = undefined;
    @property(cc.Label)
    labelCaption: cc.Label = undefined;
    @property(cc.Label)
    labelOption: cc.Label = undefined;
    @property(cc.Sprite)
    arrowCaption: cc.Sprite = undefined;
    @property(cc.Label)
    labelItem: cc.Label = undefined;
    @property(cc.Label)
    optionItem: cc.Label = undefined;
    @property(cc.Sprite)
    spriteItem: cc.Sprite = undefined;
    @property(cc.Node)
    colorItem: cc.Node = undefined;

    @property([DropDownOptionData])
    optionDatas: DropDownOptionData[] = [];

    private _interactable:boolean = true;
    private _dropDown: cc.Node;
    private validTemplate: boolean = false;
    private items: DropDownItem[] = [];
    private isShow: boolean = false;

    private _selectedIndex: number = -1;
    private get selectedIndex(): number{
        return this._selectedIndex;
    }
    private set selectedIndex(value: number){
        this._selectedIndex = value;
        this.refreshShownValue();
    }
    private m_callback = undefined;
    private origincontentString = "";

    public get selected():number{
        return this._selectedIndex;
    }

    public set selected(value:number){
        this._selectedIndex = value;
        this.refreshShownValue();
    }

    public init(msg:string,optionmsg:string = "",changecolor:boolean = true){
        if(changecolor)
            this.labelCaption.node.color = new Color(72,72,72);
        this.labelCaption.string = msg;
        this.origincontentString = msg;
        if(this.labelOption && optionmsg)
            this.labelOption.string = optionmsg;
    }

    public initwithorigin(){
        this.labelCaption.node.color = new Color(72,72,72);
        this.labelCaption.string = this.origincontentString;
    }

    public addOptionDatas(optionDatas: DropDownOptionData[],select:number=-1) {
                        //[優惠卷活動,紅包活動]
        //this.hide();
        optionDatas && optionDatas.forEach(data => {
            this.optionDatas.push(data);
        });
        this.selectedIndex = select;
        if (this.selected > -1) {
            this.refreshShownValue();
            if (this.m_callback)
                this.m_callback(this.selectedIndex, this.optionDatas[this.selectedIndex].titleString);
        }
        if(this.InputType == DropDownType.Old) {
            if (this.isShow) {
                this.delayedDestroyDropdownList(0.15);
                this.show();
            }
        }else{
            // DropDownAlert.main.addOptionDatas(optionDatas,select);
        }
    }

    public clearOptionDatas(){
        //this.hide();
        this.optionDatas = [];
        this.selectedIndex = -1;
        this.initwithorigin();
        this.refreshShownValue();
    }

    public show() {
        if(this.InputType == DropDownType.Old)
            this.oldShow();
        else {
            DropDownAlert.main.clearOptionDatas();
            DropDownAlert.main.addOptionDatas(this.optionDatas,this.selectedIndex);
            DropDownAlert.main.SetSelectIndexCallBack(this.DropDownAlertCB.bind(this));
            //Sconsole.log("注入DropDownAlertCB");
            DropDownAlert.main.show();
        }
    }

    private oldShow(){
        if (!this.validTemplate) {
            this.setUpTemplate();
            if (!this.validTemplate) { return; }
        }
        UI_manager.setSprite(this.arrowCaption,"universal/icon-dropdown-hide");
        this.isShow = true;

        this._dropDown = this.createDropDownList(this.template);
        this._dropDown.name = "DropDown List";
        this._dropDown.active = true;
        this._dropDown.setParent(this.template.parent);

        let itemTemplate = this._dropDown.getComponentInChildren<DropDownItem>(DropDownItem);
        let content = itemTemplate.node.parent;
        itemTemplate.node.active = true;

        cc.js.clear(this.items);

        for(let i =0, len = this.optionDatas.length; i < len; i++){
            let data = this.optionDatas[i];
            let item : DropDownItem = this.addItem(data, i == this.selectedIndex, itemTemplate, this.items);
            if(!item){
                continue;
            }
            // item.toggle.isChecked = i == this.selectedIndex;

            item.toggle.node.on("toggle", this.onSelectedItem, this);
            // if(i == this.selectedIndex){
            //     this.onSelectedItem(item.toggle);
            // }
        }
        itemTemplate.node.active = false;

        if(this.optionDatas.length == 0)
            this.noDataShow(itemTemplate);

        content.height = itemTemplate.node.height * this.optionDatas.length;
    }

    public SetSelectIndexCallBack(cb){
        this.m_callback = cb;
    }

    private DropDownAlertCB(sel,title,option){
        this.selectedIndex = sel;
        if(this.m_callback)
        {
            this.m_callback(sel,title,option);
        }
        
    }

    private addItem(data: DropDownOptionData, selected: boolean, itemTemplate: DropDownItem, dropDownItems: DropDownItem[]): DropDownItem{
        let item = this.createItem(itemTemplate);
        item.node.setParent(itemTemplate.node.parent);
        item.node.active = true;
        item.node.name = `item_${this.items.length + data.titleString?data.titleString:""}`;
        if(item.toggle){
            item.toggle.isChecked = false;
        } 
        if(item.label){
            item.label.string = data.titleString;
        }
        if(item.optionlabel){
            item.optionlabel.string = data.optionString;
        }
        if(selected)
            item.onToggle();
        // if(item.sprite){
        //     item.sprite.spriteFrame = data.optionSf;
        //     item.sprite.enabled = data.optionSf != undefined;
        // }
        this.items.push(item);
        return item;
    }

    public hide() {
        if(this.InputType == DropDownType.Old) {
            UI_manager.setSprite(this.arrowCaption, "universal/icon-dropdown-show" + (this.interactable ? "" : "-d"));
            this.isShow = false;
            if (this._dropDown != undefined) {
                this.delayedDestroyDropdownList(0.15);
            }
        }else
            DropDownAlert.main.hide();
    }

    private async delayedDestroyDropdownList(delay: number)
    {
        // await WaitUtil.waitForSeconds(delay);
        // wait delay;
        for (let i = 0, len= this.items.length; i < len; i++)
        {
            if (this.items[i] != undefined)
                this.destroyItem(this.items[i]);
        }
        cc.js.clear(this.items);
        if (this._dropDown != undefined)
            this.destroyDropDownList(this._dropDown);
        this._dropDown = undefined;
    }

    private destroyItem(item){

    }

    private noDataShow(itemTemplate: DropDownItem){
        let data : DropDownOptionData = new DropDownOptionData();
        data.setValue(VM.getValue("lang.loading"));
        let item : DropDownItem = this.addItem(data, false, itemTemplate, this.items);
    }

    // 设置模板，方便后面item
    private setUpTemplate() {
        this.validTemplate = false;

        if (!this.template) {
            cc.error("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item");
            return;
        }
        this.template.active = true;
        let itemToggle: cc.Toggle = this.template.getComponentInChildren<cc.Toggle>(cc.Toggle);
        this.validTemplate = true;
        // 一些判断
        if (!itemToggle || itemToggle.node == this.template) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The template must have a child Node with a Toggle component serving as the item.");
        } else if (this.labelItem != undefined && !this.labelItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Label must be on the item Node or children of it.");
        } else if (this.spriteItem != undefined && !this.spriteItem.node.isChildOf(itemToggle.node)) {
            this.validTemplate = false;
            cc.error("The dropdown template is not valid. The Item Sprite must be on the item Node or children of it.");
        }

        if (!this.validTemplate)
        {
            this.template.active = false;
            return;
        }
        let item = itemToggle.node.addComponent<DropDownItem>(DropDownItem);
        item.label = this.labelItem;
        item.optionlabel = this.optionItem;
        item.Bg = this.colorItem;
        // item.sprite = this.spriteItem;
        item.toggle = itemToggle;
        item.node = itemToggle.node;

        this.template.active = false;
        this.validTemplate = true;
    }

    // 刷新显示的选中信息
    private refreshShownValue(){
        if(this.optionDatas.length <= 0 || this.selectedIndex == -1){
            return;
        }
        let data = this.optionDatas[this.clamp(this.selectedIndex, 0, this.optionDatas.length -1)];
        if(this.labelCaption) {
            if (data && data.titleString) {
                this.labelCaption.string = data.titleString;
                this.labelCaption.node.color = new Color(211, 211, 211);
                if (this.labelOption)
                    this.labelOption.string = data.optionString;
            }
        }
    }

    protected createDropDownList(template: cc.Node):  cc.Node {
        return cc.instantiate(template);
    }

    protected destroyDropDownList(dropDownList: cc.Node){
        dropDownList.destroy();
    }

    protected createItem(itemTemplate: DropDownItem): DropDownItem{
        let newItem = cc.instantiate(itemTemplate.node);
        return newItem.getComponent<DropDownItem>(DropDownItem);
    }

    /** 当toggle被选中 */
    private onSelectedItem(toggle: cc.Toggle) {
        if(this.optionDatas.length == 0)
            return;

        let parent = toggle.node.parent;
        for (let i = 0; i <parent.childrenCount; i++)
            {
                if (parent.children[i] == toggle.node)
                {
                    // Subtract one to account for template child.
                    this.selectedIndex = i - 1;
                    break;
                }
            }
        if(this.m_callback)
            this.m_callback(this.selectedIndex,this.optionDatas[this.selectedIndex].titleString);
        this.hide();
    }

    private onClick() {
        if(!this.isShow){
            this.show();
        }else{
            this.hide();
        }
    }

    start(){
        if(this.template)
            this.template.active = false;
        // this.refreshShownValue();
        this.origincontentString = this.labelCaption.string;
    }

    onEnable() {
        if(this._interactable)
            this.switchTouchedEvent(true);
    }

    onDisable() {
        this.switchTouchedEvent(false);
    }

    private switchTouchedEvent(_switch,_oldswitch = false){
        if(_switch && _oldswitch)
            return;

        if(_switch) {
            this.node.on("touchend", this.onClick, this);
            UI_manager.setSprite(this.arrowCaption,"universal/icon-dropdown-show");
        }else {
            this.node.off("touchend", this.onClick, this);
            UI_manager.setSprite(this.arrowCaption,"universal/icon-dropdown-show-d");
        }
    }

    private clamp(value: number, min: number, max: number): number{
        if(value < min) return min;
        if(value > max) return max;
        return value;
    }
}
