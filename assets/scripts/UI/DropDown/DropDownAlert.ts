
import DropDownOptionData from "./DropDownOptionData";
import RechargeBtn from "../../Item/RechargeBtn";
import Awaiter from "../../Extension/AwaitExtension";
import DropDownAlertItem from "./DropDownAlertItem";
import AppLog from "../../Extension/AppLog";

const { ccclass, property } = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class DropDownAlert extends cc.Component {

    @property(cc.Node)
    alert: cc.Node = null;

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Node)
    view: cc.Node = null;

    @property(cc.Node)
    main: cc.Node = null;

    @property(cc.Node)
    bg: cc.Node = null;

    @property(cc.Node)
    speaceNode: cc.Node = null;

    @property(cc.Node)
    item: cc.Node = null;

    @property(cc.Graphics)
    topGP: cc.Graphics = null;

    private select: number = -1
    private get selectedIndex() {
        return this.select;
    }
    private set selectedIndex(value) {
        this.valuelock = true;
        this.select = value;
        this.valuelock = false;
    }
    private contentlength: number;
    private optionDatas: DropDownOptionData[] = [];
    private m_callback: void;
    private lock: boolean = false;
    private valuelock: boolean = false;
    public static main: DropDownAlert;

    protected onLoad(): void {
        DropDownAlert.main = this;
        this.fixHeight();
        this.drawGraphics();
        this.setTouchEvent();
        this.bg.active = false;
    }

    start() {
        //
    }

    private setTouchEvent() {
        // this.topGP.node.parent.on(cc.Node.EventType.TOUCH_START,function(t){
        //     console.log("触摸开始");
        // },this);
        //监听
        this.topGP.node.parent.on(cc.Node.EventType.TOUCH_MOVE, this.on_touch_move, this);
        //触摸抬起
        this.topGP.node.parent.on(cc.Node.EventType.TOUCH_END, this.on_touch_end, this);
        this.topGP.node.parent.on(cc.Node.EventType.TOUCH_CANCEL, this.on_touch_end, this);
    }

    private on_touch_end(t) {
        if ((cc.winSize.height / 2) * -1 == this.alert.y)
            return;

        if ((cc.winSize.height / 2) * -1 - this.alert.height * 0.2 > this.alert.y) {
            cc.log("大於 80%");
            this.hideSlide();
        } else {
            cc.log("小於 80%");
            this.showSlide(0.2, true);
        }
    }

    private on_touch_move(t) {
        //定义一个n_pos变量存储当前触摸点的位置
        // var n_pos=t.getLocation();
        var delta = t.getDelta();
        // this.node.x+=delta.x;
        if (this.alert.y + delta.y > (cc.winSize.height / 2) * -1)
            this.alert.y = (cc.winSize.height / 2) * -1;
        else
            this.alert.y += delta.y;

    }

    private drawGraphics() {
        this.topGP.roundRect(0 - this.topGP.node.width / 2, 0 - this.topGP.node.height,
            this.topGP.node.width, this.topGP.node.height, 4);
        this.topGP.fill();
    }

    public async addOptionDatas(optionDatas: DropDownOptionData[], select: number = -1) {
        this.lock = true;
        this.selectedIndex = select;
        this.optionDatas = optionDatas;
        await optionDatas.asyncForEach(async (value, index) => {
            await this.creatFactory(index, value, null);
        });

        for (let i = optionDatas.length; i < this.content.childrenCount; i++) {
            this.content.children[i].active = false;
        }

        if (select > -1) {
            if (this.m_callback)
                this.m_callback(this.selectedIndex, this.optionDatas[this.selectedIndex].titleString, this.optionDatas[this.selectedIndex].optionString);
        }
        this.fixHeight();
        this.lock = false;
    }

    private fixHeight() {
        this.content.getComponent(cc.Layout).updateLayout();
        let hegiht = this.content.height > cc.winSize.height * 0.9 ? cc.winSize.height * 0.9 - 40 : this.content.height;
        this.view.height = hegiht;
        this.main.height = hegiht;
        this.alert.height = hegiht + 40;
        this.alert.y = (cc.winSize.height / 2) * -1 - this.alert.height;
    }
    /***
     * 物件工廠
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    protected async creatFactory(index, data, onClickFunc, type = null) {
        let item = null;
        if (index >= this.content.getComponentsInChildren(DropDownAlertItem).length) {
            item = cc.instantiate(this.item);
            item.setParent(this.content);
            item.getComponent(DropDownAlertItem).toggle.node.on("toggle", this.onSelectedItem, this);
            item.active = true;
        } else {
            item = this.content.getComponentsInChildren(DropDownAlertItem)[index];
            item.node.active = true;
        }
        item.getComponent(DropDownAlertItem).setDate(data);
        item.getComponent(DropDownAlertItem).toggle.isChecked = false;
        item.getComponent(cc.Layout).updateLayout();

    };

    public clearOptionDatas() {
        if ((cc.winSize.height / 2) * -1 == this.alert.y)
            this.hide();
        this.optionDatas = [];
    }

    public SetSelectIndexCallBack(cb) {
        this.m_callback = cb;
    }

    public async show() {
        await Awaiter.until(_ => !this.lock);
        await Awaiter.until(_ => !this.valuelock);
        setTimeout(() => {
            this.speaceNode.active = true;
        }, 300);

        this.refreshToggle();
        this.node.active = true;
        this.bg.active = true;
        this.showSlide(1.5);
    }

    public hide() {
        this.hideSlide();
    }

    private showSlide(delay: number, noanim: boolean = false) {
        //从当前坐标移动到point(x, y)
        var moveto = cc.moveTo(delay, cc.v2(0, (cc.winSize.height / 2) * -1));
        if (!noanim)
            moveto.easing(cc.easeElasticOut(1));
        var finished = cc.callFunc(function () {
            this.speaceNode.active = false;
        }, this, null);
        var spawn = cc.sequence(moveto, finished);
        this.alert.runAction(spawn);
    }

    private hideSlide() {
        if (this.lock)
            return;
        this.lock = true;
        this.speaceNode.active = false;
        var moveto = cc.moveTo(0.3, cc.v2(0, (cc.winSize.height / 2) * -1 - this.alert.height));
        var finished = cc.callFunc(function () {
            this.lock = false;
            this.bg.active = false;
        }, this, null);
        var spawn = cc.sequence(moveto, finished);
        this.alert.runAction(spawn);
    }
    /** 当toggle被选中 */
    private onSelectedItem(toggle: cc.Toggle) {
        if (!toggle.isChecked)
            return;

        if (this.optionDatas.length == 0)
            return;
        let parent = toggle.node.parent;
        for (let i = 0; i < parent.childrenCount; i++) {
            if (parent.children[i] == toggle.node) {
                // Subtract one to account for template child.
                this.selectedIndex = i;
                break;
            }
        }

        if (this.m_callback) {
            var data = this.optionDatas[this.selectedIndex];
            this.m_callback(this.selectedIndex, data.titleString, data.optionString);
        }
        this.hide();
    }

    private refreshToggle() {
        for (let i = 0; i < this.content.childrenCount; i++) {
            if (i == this.selectedIndex)
                this.content.children[i].getComponent(DropDownAlertItem).onToggle();
            else
                this.content.children[i].getComponent(DropDownAlertItem).unToggle();
        }
    }
}
