import DropDown from "./DropDown";
import color = cc.color;
import {VM} from "../../modelView/ViewModel";
import DropDownOptionData from "./DropDownOptionData";

const { ccclass, property } = cc._decorator;
@ccclass()
export default class DropDownItem extends cc.Component {

    @property(cc.Label)
    public label: cc.Label = undefined;
    @property(cc.Label)
    public optionlabel: cc.Label = undefined;
    // @property(cc.Sprite)
    // public sprite: cc.Sprite = undefined;
    //
    @property(cc.Toggle)
    public toggle: cc.Toggle = undefined;
    @property(cc.Node)
    public Bg: cc.Node = undefined;

    protected setDate(value:DropDownOptionData){

    }

    public onToggle(){
        this.label.node.color = cc.Color.WHITE;
        this.Bg.color = new cc.Color(49 ,49,49);
    }
}