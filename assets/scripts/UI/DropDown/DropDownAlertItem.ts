
import DropDownItem from "./DropDownItem";
import DropDownOptionData from "./DropDownOptionData";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class DropDownAlertItem extends DropDownItem {

    protected setDate(value:DropDownOptionData) {
        this.label.string = value.titleString;
        this.optionlabel.string = value.optionString;
        this.optionlabel.node.active = !value.optionString.isNull();
        this.optionlabel.node.parent.getComponent(cc.Layout).updateLayout();
    }

    onToggle() {
        this.node.color = VM.getValue("color.bg31color");
    }

    public unToggle(){
        this.node.color =VM.getValue("color.bg27color");
    }
}
