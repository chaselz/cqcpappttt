const {ccclass, property} = cc._decorator;

@ccclass("DropDownOptionData")
export default class DropDownOptionData{
    @property(cc.String)
    public optionString: string = "";

    @property(cc.String)
    public titleString: string = "";

    setValue(title: string, option:string = ""){
        this.titleString = title;
        this.optionString = option;
    }
}
