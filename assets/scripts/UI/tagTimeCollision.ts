
import Collider = cc.Collider;
import tagTimeItem from "../Item/tagTimeItem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class tagTimeCollision extends cc.Component {

    @property(cc.RichText)
    timeLabel: cc.RichText = null;
    public time : string = "";
    public histime : any = [];
    protected onEnable(): void {

    }

    protected onLoad(): void {

    }


    InitVlaue(str){
        this.timeLabel.string = "<b>{0}</b>".format(str);
        this.time = str;
        this.histime = [];
    }
    /**
     * 当碰撞产生的时候调用
     * @param  {Collider} other 产生碰撞的另一个碰撞组件
     * @param  {Collider} self  产生碰撞的自身的碰撞组件
     */
    onCollisionEnter (other, self) {
        console.log('on collision enter');
        // this.setTimestr(other.getComponent(tagTimeItem).Key);
    }

    onCollisionStay(other, self) {
        let other_y = this.localConvertWorldPoint(other.node).y;
        let self_y = this.localConvertWorldPoint(self.node).y;
        var otherClass = other.getComponent(tagTimeItem);
        var selfClass = self.getComponent(tagTimeCollision);
        if(other_y > self_y - (self.node.height / 2)){
            if(otherClass.Key != selfClass.time) {
                selfClass.histime.push(selfClass.time);
                selfClass.setTimestr(otherClass.Key);
            }
        }else{
            if(otherClass.Key == selfClass.time){
                let item = selfClass.histime.pop();
                if(item)
                    selfClass.setTimestr(item);
            }
        }
    }

    /***
     *
     * @param other 別人
     * @param self  自己
     */
    onCollisionExit(other, self) {
        console.log('on collision Exit');
    }

    public setTimestr(str){
        this.timeLabel.string = "<b>{0}</b>".format(str);
        this.time = str;
    }

    /**
     * 得到一個節點的世界座標
     * node的原點在左下邊
     * @param {*} node
     */
    private localConvertWorldPoint(node) {
        if (node) {
            return node.convertToWorldSpaceAR(cc.v2(0, 0));
        }
        return null;
    }
    // update (dt) {}
}
