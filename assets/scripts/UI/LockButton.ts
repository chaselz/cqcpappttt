// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import VMBase from './../modelView/VMBase'
import { StringFormatFunction } from './../modelView/StringFormat';

const LABEL_TYPE = {
    CC_BUTTON: 'cc.Button',
}

;
@ccclass
export default class NewClass extends VMBase {

    @property(cc.String)
    watchPath: string = "";


    @property({
        //type:cc.Enum(LABEL_TYPE),
        readonly: true
    })
    private buttonType: string = LABEL_TYPE.CC_BUTTON;

    @property(cc.Animation)
    loadAnim: cc.Animation = null;

    // LIFE-CYCLE CALLBACKS:

    protected watchPathArr: string[] = [];

    //按照路径参数顺序保存的 值的数组（固定）
    protected templateValueArr: any[] = [];

    //保存着字符模板格式的数组 (只会影响显示参数)
    private templateFormatArr: string[] = [];

    // LIFE-CYCLE CALLBACKS:

    onRestore() {
        this.checkButton();
    }

    onLoad() {
        super.onLoad();
        this.checkButton();
        // if (!CC_EDITOR) {
        //     this.onValueInit();
        // }
    }

    /**初始化获取数据 */
    onValueInit() {
        //更新信息
        this.setButtonLock(this.VM.getValue(this.watchPath)); 
    }

    /**监听数据发生了变动的情况 */
    onValueChanged(n, o, pathArr: string[]) {
        let self = this;
        setTimeout(function () {
            self.setButtonLock(n);
        },n ? 0 : 500);

    }

    private setButtonLock(value) {
        this.getComponent(this.buttonType).interactable = !value;
        if(this.loadAnim  == null)
            return;

        this.loadAnim.node.active = value;
        if(value)
            this.loadAnim.play();
        else
            this.loadAnim.stop();
    }

    checkButton() {
        let checkArray = [
            'cc.Button',
        ];

        for (let i = 0; i < checkArray.length; i++) {
            const e = checkArray[i];
            let comp = this.node.getComponent(e);
            if (comp) {
                this.buttonType = e;
                return true;
            }

        }

        cc.error('没有挂载任何Button组件');

        return false;

    }
}
