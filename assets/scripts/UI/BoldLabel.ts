const { ccclass, property, menu} = cc._decorator;

@ccclass
@menu('添加渲染组件/BoldLabel')
export default class BoldLabel extends cc.Label{
    @property(cc.Boolean)
    private _bold:boolean=false;
    @property({Type:cc.Boolean})
    public set bold(value:boolean){
        this._bold=value;
        this["_ßsgNode"].enableBold(value);
    }
    public get bold(){
        return this._bold;
    }

    public start(){
        this["_sgNode"].enableBold(this.bold);//this.bold=true则加粗

        if(!CC_EDITOR)//如果不是在编辑器环境中
            this.node.y-=3;

    }
}
