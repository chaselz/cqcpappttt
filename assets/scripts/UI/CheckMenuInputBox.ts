import MenuView from "../ui_ctrls/MenuView";
import {VM} from "../modelView/ViewModel";
import CheckMenuView from "./CheckMenuView";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UIAssistant = require("UIAssistant");
@ccclass
export default class CheckMenuInputBox extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Node)
    numNode: cc.Node = null;

    @property(cc.Label)
    numLabel: cc.Label = null;

    private _data;any = null;
    private callback :void = null;
    private isinit : boolean = false;
    start () {
        this.node.on("click",function () {
            if(this._data){
                CheckMenuView.main.setData(this._data,this.menuViewCallBack.bind(this),this.isinit);
                this.isinit = false;
                PageManager.CheckMenuViewPage();
            }
        },this);
    }

    public Init(){
        this._data = null;
        this.label.string = VM.getValue("lang.loading");
        this.label.node.color = VM.getValue("color.graylabel2");
    }

    public setData(data:any){
        this._data = data;
    }

    public setCallBack(cb:void){
        if(cb)
            this.callback = cb;
    }

    public setInitCheck(){
        let keys = Object.keys(this._data);
        if(keys.length > 0 ) {
            this.isinit = true;
            this.menuViewCallBack(keys,[],[]);
        }
    }


    private menuViewCallBack(kind:any,sub_kind:any,items:any){
        this.setLableStr(kind,sub_kind,items);
        this.label.node.color = VM.getValue("color.graylabel3");
        if(this.callback)
            this.callback(kind,sub_kind,items);
    }

    private setLableStr(kind:any,sub_kind:any,items:any){
        if(kind.length == Object.keys(this._data).length){
            this.label.string = VM.getValue("lang.all");
            this.numNode.active = false;
        }else{
            var str = "";
            var count = 0;
            var all = items.length + sub_kind.length + kind.length - 3;
            kind.forEach(value=>{
                str += VM.getValue("lang."+(value == 1 ? "bet":"cashflow")) +"，";
                count++;
            });
            sub_kind.forEach(value=>{
                if(count > 3)
                    return;
                str += VM.getValue("fundsname.id"+value) +"，";
                count++;
            });
            if(count < 3)
                items.forEach(value=>{
                    if(count > 3)
                        return;
                    str += VM.getValue("fundsname.id"+value) +"，";
                    count++;
                });

            if(count>3){
                this.numLabel.string = all + "+";
                //更新Label的數值;
                this.numLabel._forceUpdateRenderData();
                this.numNode.getComponent(cc.Layout).updateLayout();
                this.DrawGPNumNode();
            }
            cc.log("ALL count =",count);
            this.numNode.active = count > 3;
            this.label.string = str.substring(0, str.length-1);
        }
    }

    private DrawGPNumNode(){
        var gp = this.numNode.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0-gp.node.width,0-gp.node.height/2,gp.node.width,gp.node.height,16);
        gp.fill();
    }
}
