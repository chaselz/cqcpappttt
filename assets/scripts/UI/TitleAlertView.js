var TitleAlertType = require("TitleAlertType");
var UI_manager = require("UI_manager");

var TitleAlertView =cc.Class({
    extends: cc.Component,

    properties: {
        icon: {
            default: null,        // The default value will be used only when the component attaching// to a node for the first time
            type: cc.Sprite, // optional, default is typeof default

        },
        msg: {
            default: null,        // The default value will be used only when the component attaching// to a node for the first time
            type: cc.RichText, // optional, default is typeof default
        },
    },

    ctor(){
        if(TitleAlertView.main == null)
            TitleAlertView.main = this;
        let self = this;
        this.setImg = function (name) {
            UI_manager.setSprite(this.icon,"icon-"+name);
            this.node.active = true;
            if(cc.ENGINE_VERSION != "2.2.2"){
                if(this.msg.node.width >= 516)
                    this.msg.maxWidth = 516;
    
                this.node.height = this.msg.node.height + 14;
            }
        };

        this.playAnim = function () {
            this.getComponent(cc.Animation).play("TitleAlertAnim");
        };
        this.onFinished = function (x) {
            self.node.active = false;
        }
        //
    },

    statics(){
        main = null;
    },

    onLoad(){
        this.node.active = false;
        this.FixLongViewInsetsDidChange();
        this.getComponent(cc.Animation).on('finished',  this.onFinished,    null);
    },

    show(type,_msg){
        this.msg.maxWidth = 0;
        this.msg.string = _msg;
        if(cc.ENGINE_VERSION == "2.2.2"){
            if(this.msg.node.width >= 516)
                this.msg.maxWidth = 516;

            this.node.height = this.msg.node.height + 14;
        }
        if(type == TitleAlertType.Error){
            this.setImg("error");
        }else if(type == TitleAlertType.Warn){
            this.setImg("notice");
        }else{
            this.setImg("success");
        }
        this.playAnim();
    },

    FixLongViewInsetsDidChange(){
        let size = cc.view.getFrameSize();
        let router = 0;

        router = size.width > size.height ? size.width / size.height : size.height / size.width;

        if(router > 1.8){
            this.getComponent(cc.Widget).top += 44;
        }
    },
});
