

const {ccclass, property} = cc._decorator;

@ccclass
export default class TimeSlider extends cc.Component {

    @property(cc.Slider)
    slider: cc.Slider = null;

    @property(cc.RichText)
    timelabel: cc.RichText = null;

    @property(cc.RichText)
    ampmlabel: cc.RichText = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    public get value(){
        return this._value;
    }
    public set value(data){
        this.slider.progress = data*(1.0 / 23.0).toFixed(4);
        this.showAmPm(data);
        this._value = data;
    }

    private _value:number = 0;
    private IntervalValue : number = 0;
    private callback : void = null;
    start () {
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
            cc.log("TOUCH_END event=", event.type);
        });
        this.node.on(cc.Node.EventType.MOUSE_UP, function (event) {
            cc.log("TOUCH_END event=", event.type);
        });
        this.IntervalValue = (1.0 / 23.0).toFixed(4);
        cc.log("IntervalValue =",this.IntervalValue);
    }

    public onSlidercallback(slider:cc.Slider) {
        // 回调的参数是 slider
        // do whatever you want with the slider
        let temp = Math.round(slider.progress / this.IntervalValue);
        this.showAmPm(temp);
        this._value = temp;
        if(this.callback)
            this.callback(temp);
    }

    public setTimeSilderCB(cb){
        this.callback = cb;
    }

    private showAmPm(temp:number){
        this.ampmlabel.string = "<b>{0}</b>".format((temp > 11 ? "PM" : "AM"));
        this.timelabel.string = "<b>{0}</b>".format((Math.floor(temp / 13)+(temp % 13)).preZeroFill(2));
    }
    // update (dt) {}
}
