import VMBase from './../modelView/VMBase';
import { StringFormatFunction } from './../modelView/StringFormat';

const { ccclass, property, menu, executeInEditMode } = cc._decorator;

const LABEL_TYPE = {
    CC_LABEL: 'cc.Label',
    CC_RICH_TEXT: 'cc.RichText',
    CC_EDIT_BOX: 'cc.EditBox'
}

@ccclass
export default class NewClass extends VMBase {

    @property()
    watchPath: string = "";


    @property({
        //type:cc.Enum(LABEL_TYPE),
        readonly: true
    })
    private labelType: string = LABEL_TYPE.CC_LABEL;

    originText: string = null;

    // LIFE-CYCLE CALLBACKS:

    onRestore() {
        this.checkLabel();
    }

    onLoad() {
        super.onLoad();
        this.checkLabel();
        if (!CC_EDITOR) {
            this.onValueInit();
        }

    }


    /** 格式化字符串 */
    getValueFromFormat(value: number | string, format: string): string {
        return StringFormatFunction.deal(value, format);
    }

    /**初始化获取数据 */
    onValueInit() {
        //更新信息
        this.setLabelValue(this.VM.getValue(this.watchPath)); //
      
    }

    /**监听数据发生了变动的情况 */
    onValueChanged(n, o, pathArr: string[]) {
        this.setLabelValue(n);
    }

    setLabelValue(value) {
        this.getComponent(this.labelType).fontSize = this.getRandom(30,40);
        this.node.angle = this.getRandom(-20,20);
        this.node.color = new cc.Color(this.getRandom(0,255),this.getRandom(0,255),this.getRandom(0,255))
        this.getComponent(this.labelType).string = value + '';
    }

    getLabelValue(): string {
        return this.getComponent(this.labelType).string;
    }

    getRandom(min,max){
        return Math.floor(Math.random()*(max-min+1))+min;
    }
    checkLabel() {
        let checkArray = [
            'cc.Label',
            'cc.RichText',
            'cc.EditBox',
        ];

        for (let i = 0; i < checkArray.length; i++) {
            const e = checkArray[i];
            let comp = this.node.getComponent(e);
            if (comp) {
                this.labelType = e;
                return true;
            }

        }

        cc.error('没有挂载任何label组件');

        return false;

    }
}
