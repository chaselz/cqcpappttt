import {VM} from "../modelView/ViewModel";
import { isNull } from "util";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class newInputBox extends cc.Component {

    @property(cc.EditBox)
    editBox: cc.EditBox = null;

    @property(cc.Node)
    titleNode: cc.Node = null;

    @property(cc.Node)
    line: cc.Node = null;

     private setUIColor(color,linecolor){
            if(color == this.titleNode.color)
                return;

            this.line.color = linecolor;
            this.titleNode.color = color;
            this.editBox.textLabel.node.color = color
     };


    getKeyWordType(){
        return this.editBox.getComponent(cc.EditBox).inputFlag;
    }

    setKeyWordType(flag){
        this.editBox.getComponent(cc.EditBox).inputFlag = flag;
    }

    setFocus(){
        this.setUIColor(VM.getValue("color.graylabel3"),VM.getValue("color.graylabel3"));
    }

    Focus(){
        this.setUIColor(VM.getValue("color.graylabel3"),VM.getValue("color.linecolor"));
    }

    setRed(){
        this.setUIColor(VM.getValue("color.red"),VM.getValue("color.red"));ㄋㄛ
    }

    setNoInput(){
        this.setUIColor(VM.getValue("color.graylabel2"),VM.getValue("color.linecolor"));
    }

    add_BeginEditor_listen(caller, func){
        this.editBox.node.on("editing-did-began",func,caller);
    }
    add_DidEndEditor_listen(caller, func){
        this.editBox.node.on("editing-did-ended",func,caller);
    }
    add_EditorChanged_listen(caller, func){
        this.editBox.node.on("text-changed",func,caller);
    }
    
    getString(){
        return this.editBox.getComponent(cc.EditBox).string;
    }

    setString(value){
        this.editBox.getComponent(cc.EditBox).string = value;
    }
}
