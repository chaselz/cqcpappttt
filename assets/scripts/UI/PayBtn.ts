
const {ccclass, property} = cc._decorator;
var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");

@ccclass
export default class PayBtn extends UI_ctrl {

    mode : string;

    onLoad() {
        super.onLoad();
    }

    setData(data){
        this.mode = data.mode;
        this.setTitle(data.mode);
        this.setIcon(data.mode);
        this.setLower(data.deposit_minimum);
        this.setUpper(data.deposit_maximum);
        this.active = true;
    }

    Hide(){
        this.active = false;
    }

    private setTitle(key:string){
        let vm = this.view["Title"].getComponent("VMLabel");
        vm.setWatchArrValue(["lang."+key+"pay"]);
    }

    private setLower(num:string){
        this.view["upperlower/lowernum"].getComponent(cc.Label).string = num;
    }

    private setUpper(num:string){
        this.view["upperlower/uppernum"].getComponent(cc.Label).string = num;
    }

    private setIcon(img:string){
        UI_manager.setSprite(this.view["icon"],"payway/icon-"+img);
    }
}
