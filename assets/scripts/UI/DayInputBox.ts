import newUIDatePicker, {DateType} from "../../resources/UIDatePicker/newUIDatePicker";
import {DayInputType} from "../Enum/DayInputType";
import BirthDayPicker from "../../resources/birthday/BirthDayPicker";

const {ccclass, property} = cc._decorator;
@ccclass
export default class DayInputBox extends cc.Button {
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    @property(cc.Label)
    BirthDayTet:cc.Label = null;

    @property(cc.Label)
    PlaceHolderTet:cc.Label = null;

    @property({
        type: cc.Enum(DayInputType),
        displayName: "輸入類型",
    })
    InputType = DayInputType.Date;

    @property({
        type: cc.Enum(DateType),
        displayName: "日曆類型",
        visible: function () { return this.InputType === DayInputType.Date },
    })
    type = DateType.OnlyDay;
    private m_finish = undefined;
    private self : DayInputBox = null;

    public get string(){
        return this._string;
    }

    public set string(value){
        this._string = value;
        let ishide = value == "";
        this.BirthDayTet.node.active = !ishide;
        this.PlaceHolderTet.node.active = ishide;
        this.BirthDayTet.string = value;
    }

    public _string : string = "";

    constructor() {
        super();
    }
    onLoad(){
        if(CC_EDITOR)
            return;
        this.self = this;
    }

    start () {
        if(CC_EDITOR)
            return;

        this.node.on("click", function () {
            this.setCallBack();
            this.showDatePicker();
        }, this);
    }

    showDatePicker(){
        if(this.InputType == DayInputType.Birthday){
            // cc.log(BirthDayPicker.main);
            BirthDayPicker.main.show(this);
        }else
            newUIDatePicker.main.show(this,this.type);
    }

    private setCallBack(){
        if(this.InputType == DayInputType.Birthday) {
            // cc.log(BirthDayPicker.main);
            BirthDayPicker.main.setCallBack(function(node,year,month,day){
                node.string = "{0}-{1}-{2}".format(year,node.preZeroFill(month, 2),node.preZeroFill(day, 2));

            });
        }else {
            newUIDatePicker.main.setPickDateCallback(function (node, year, month, day, hour) {

                if (node.type == DateType.OnlyDay)
                    node.string = "{0}-{1}-{2}".format(year, node.preZeroFill(month, 2), node.preZeroFill(day, 2));
                else if (node.type == DateType.WithTime)
                    node.string = "{0}-{1}-{2} {3}:00".format(year, node.preZeroFill(month, 2), node.preZeroFill(day, 2), node.preZeroFill(hour, 2));

                if (node.m_finish)
                    node.m_finish(node.BirthDayTet.string);
            });
        }
    }

    public finishlisten(func){
        this.m_finish = func;
    }

    private preZeroFill (num, size) {
        if (num >= Math.pow(10, size)) { //如果num本身位数不小于size位
            return num.toString();
        } else {
            var _str = Array(size + 1).join('0') + num;
            return _str.slice(_str.length - size);
        }
    }
}

