import MenuView from "../ui_ctrls/MenuView";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UIAssistant = require("UIAssistant");
@ccclass
export default class MenuInputBox extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    private _data;any = null;
    private _index : number = 0;
    private callback :void = null;

    start () {
        this.node.on("click",function () {
            if(this._data){
                MenuView.main.setData(this._data,this.menuViewCallBack.bind(this),this._index,this._index == 0);
                PageManager.MenuViewPage();
            }
        },this);
    }

    public Init(){
        this._index = 0;
        this._data = null;
        this.label.string = VM.getValue("lang.loading");
        this.label.node.color = VM.getValue("color.graylabel2");
    }

    public setData(data:any){
        this._data = data;
    }

    public setCallBack(cb:void){
        if(cb)
            this.callback = cb;
    }

    public setInitSwtich(index){
        if(this._data.length > 0 && this._data.length > index) {
            this.menuViewCallBack(this._data[index]);
        }
    }

    private menuViewCallBack(key:any){
        this._index = this._data.indexOf(key);
        this.label.string = key;
        this.label.node.color = VM.getValue("color.graylabel3");
        if(this.callback)
            this.callback(key);
    }
}
