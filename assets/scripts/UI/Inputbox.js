const {VM} = require('ViewModel');
var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");

cc.Class({
    extends: UI_ctrl,

    properties: {
        id:"",
    },
    onLoad(){
        UI_ctrl.prototype.onLoad.call(this);
    },
    //
    ctor(){
        this._setUIColor=function(color,linecolor,type){
            this.view["buttomLine"].color = linecolor;
            this.view["EditorBoxMark"].color = color;
            UI_manager.setSprite(this.view["editBoxIcon"],"universal/icon-"+this.id+type);
            if(type.length > 0)
                this.view["EditBox/TEXT_LABEL"].color = color

        }
    },

    getKeyWordType(){
        return this.view["EditBox"].getComponent(cc.EditBox).inputFlag;
    },

    setKeyWordType(flag){
        this.view["EditBox"].getComponent(cc.EditBox).inputFlag = flag;
    },

    setFocus(){
        this._setUIColor(VM.getValue("color.graylabel3"),VM.getValue("color.graylabel3"),"-focus");
    },

    Focus(){
        this._setUIColor(VM.getValue("color.graylabel"),VM.getValue("color.linecolor"),"");
    },

    setRed(){
        this._setUIColor(VM.getValue("color.red"),VM.getValue("color.red"),"-err");
    },

    add_BeginEditor_listen(caller, func){
        this.view["EditBox"].on("editing-did-began",func,caller);
    },
    add_DidEndEditor_listen(caller, func){
        this.view["EditBox"].on("editing-did-ended",func,caller);
    },
    add_EditorChanged_listen(caller, func){
        this.view["EditBox"].on("text-changed",func,caller);
    },

    getString(){
        return this.view["EditBox"].getComponent(cc.EditBox).string;
    },

    setString(value){
        this.view["EditBox"].getComponent(cc.EditBox).string = value;
    }
});
