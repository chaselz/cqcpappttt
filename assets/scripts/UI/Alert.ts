import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class Alert extends cc.Component {
    @property(cc.Label)
    titleLabel: cc.Label = null;

    @property(cc.RichText)
    contentLabel: cc.RichText = null;

    @property(cc.Button)
    closeBtn:cc.Button = null;

    @property(cc.Button)
    cancelBtn:cc.Button = null;

    @property(cc.Button)
    okBtn:cc.Button = null;

    public static main = null;

    private onClikFunc : void = null;
    private onCancelClikFunc : void = null;

    protected onLoad(): void {
        this.node.active = false;
        Alert.main = this;
    }

    start () {
        this.drawCancelStroke();
        this.setCloseView();
        this.setOKBtnEvent();
    }

    private drawCancelStroke(){
        let graphics = this.cancelBtn.getComponent(cc.Graphics);
        graphics.roundRect(0,0,this.cancelBtn.node.width,this.cancelBtn.node.height,16);
        graphics.stroke();
        graphics.fill();
    }

    private setCloseView(){
        UI_manager.add_button_listen(this.closeBtn.node,this,this.Hide);
        UI_manager.add_button_listen(this.cancelBtn.node,this,this.Hide);
    }

    private setOKBtnEvent(){
        UI_manager.add_button_listen(this.okBtn.node,this,()=>{
            VM.setValue("core.lock",true);
            if(this.onClikFunc)
                this.onClikFunc();
        });
    }

    /***
     *
     * @param titlekey
     * @param msg
     * @param func
     * @constructor
     */
    public Show(titlekey:string,msg:string,func:void,cancelfunc:void = null,showbtns:boolean = true){
        this.titleLabel.string = VM.getValue(titlekey);
        this.contentLabel.string = msg;
        this.node.active = true;
        this.onClikFunc = func;
        this.onCancelClikFunc = cancelfunc;
        this.okBtn.node.active = showbtns;
        this.cancelBtn.node.active = showbtns;
    }

    /***
     * 隱藏
     * @constructor
     */
    public Hide(){
        this.node.active = false;
        if(this.onCancelClikFunc)
            this.onCancelClikFunc();

    }
}


