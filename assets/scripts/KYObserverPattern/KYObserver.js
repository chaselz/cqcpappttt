/**
 *  KYObserver Author ellis
 *  Created Time: 2018/9/6.
 *  Description:
 */


class KYObserver{
    constructor(){
        this.observer = [];
    }
    addNotify(_key , _callBack){
        this.observer.push({key : _key , callBack:_callBack})
    }
    removeNotify(_key){
        for (let index in this.observer){
            if (this.observer[index].key == _key){
                this.observer.splice(index , 1);
            }
        }
    }
    update(subject, data){
        // debugPrint("KYObserver update");
        for (let key in data){
            for (let index in this.observer){
                if (this.observer[index].key == key){
                    this.observer[index].callBack(subject , key ,data[key]);
                }
            }
        }

    }
}

module.exports = KYObserver;