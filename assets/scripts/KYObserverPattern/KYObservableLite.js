/**
 *  KYObserverLite Author ellis
 *  Created Time: 2018/8/31.
 *  Description:
 */

class KYObservableLite {
    constructor(){
        this.observers = {};
        this.observerData = {ObservableLite:{}};
        this.lastID = 0;
    }

    addObserver(subject , observer){
        if (!subject || !observer){
            cc.error("[KYObserverLite addObserver] subject and Observer can't not be undefined");
            return;
        }
        this.observers[subject] = this.observers[subject] || [];
        if (this.observers[subject].indexOf(observer) == -1) {
            this.observers[subject].push({
                observer: observer,
                id: ++this.lastID
            });
        }else{
            cc.error("[KYObserverLite] Observer already exists")
        }
        // debugPrint("this.observers = " , this.observers);
        return this.lastID;
    }

    removeObserver(subject , id){
        if (!subject || !id){
            cc.error("[KYObserverLite removeObserver] subject and id can't not be undefined");
            return;
        }
        for(let index in this.observers[subject]){
            if (this.observers[subject][index].id == this.lastID){
                this.observers[subject].splice(index, 1)
            }
        }
        if (!ky.util.getObjectSize(this.observers[subject])){
            this.removeObserverSubject(subject)
        }
        // debugPrint("this.observers = " , this.observers);
    }
    removeObserverSubject(subject){
        if (!subject){
            cc.error("[KYObserverLite removeObserverSubject] subject can't not be undefined");
            return;
        }
        delete this.observers[subject];
        // debugPrint("this.observers = " , this.observers);
    }
    removeAllObserver(){
        this.observers = {};
    }
    setObserverData(subject, obj){
        // debugPrint("setObserverData!!!");
        // debugPrint("subject = " , subject);
        // debugPrint("obj = " , obj);
        subject = subject || "ObservableLite";
        this.observerData[subject] = this.observerData[subject] || {};
        this.observerData[subject] = ky.util.clone(obj , this.observerData[subject]);
        // debugPrint("this.observerData = " , this.observerData);
        this.notifyObservers();

    }
    getObserverData(){
        return this.observerData;
    }
    notifyObservers(){
        // debugPrint("---- notifyObservers ----");
        // debugPrint("this.observerData = " , this.observerData);
        // debugPrint("this.observers = " , this.observers);
        for (let subject in this.observerData){
            // debugPrint("subject = " , subject);
            for (let observerSubject in this.observers){
                // debugPrint("observerSubject = " , observerSubject);
                if (subject == observerSubject){
                    // debugPrint("this.observers[observerSubject] = " , this.observers[observerSubject]);
                    for (let observerObj in this.observers[observerSubject]){
                        // debugPrint("this.observers[observerSubject][observer] = " , this.observers[observerSubject][observerObj].observer);
                        this.observers[observerSubject][observerObj].observer.update(subject,this.observerData[subject]);
                    }

                }
            }
        } 


    }
}

module.exports = KYObservableLite;