
Array.prototype.randomElemt = function () {
    return this[Math.floor(Math.random() * this.length)];
};

Array.prototype.asyncForEach = async function(callback) {
    for (let index = 0; index < this.length; index++) {
        await callback(this[index], index, this);
    }
};

Array.prototype.arrayCombine = function (arr) {
    Array.prototype.push.apply(this,arr);
};

Array.prototype.remove = function(from, to){
    this.splice(from, (to=[0,from||1,++to-from][arguments.length])<0?this.length+to:to);
    return this.length;
};

Array.prototype.remove2 = function(val){
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};