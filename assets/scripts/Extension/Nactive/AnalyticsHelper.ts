
export default class AnalyticsHelper  {

    public static ApiCodeErr = "ApiCodeErr";
    public static ApiErr = "ApiErr";
    public static getIp3FuncErr = "getIp3FuncErr";
    public static getIp2FuncErr = "getIp2FuncErr";
    public static getIp1FuncErr = "getIp1FuncErr";

    public static logeEvent(eventName:string,eventParams:any){

        if(!cc.sys.isNative)
            return;

        if(cc.sys.os == cc.sys.OS_IOS){
            jsb.reflection.callStaticMethod("AnalyticsHelper", "logEventName:eventParams:",eventName,JSON.stringify(eventParams));
        }else{
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/Extension/AnalyticsHelper', 'logEvent', '(Ljava/lang/String;Ljava/lang/String;)V', eventName,eventParamsStr);
        }

        cocosAnalytics.CACustomEvent.onSuccess(eventName,eventParams);
        this.customAnalyticsApi(eventName,eventParams);
    }

    public static endTimedEvent(eventName:string,eventParamsStr:string){
        if(!cc.sys.isNative)
            return;

        if(cc.sys.os == cc.sys.OS_IOS){
            jsb.reflection.callStaticMethod("AnalyticsHelper", "endTimedEventName:eventParams:",eventName,eventParamsStr);

        }else{
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/Extension/AnalyticsHelper', 'endTimedEvent', '(Ljava/lang/String;Ljava/lang/String;)V', eventName,eventParamsStr);
        }
        cocosAnalytics.CACustomEvent.onSuccess(eventName,JSON.parse(eventParamsStr));
    }

    public static logError(errorId:string, errorDescription:any, eventFailedReason : string = ""){
        if(!cc.sys.isNative)
            return;

        var errorDescriptionStr = JSON.stringify(errorDescription);
        if(cc.sys.os == cc.sys.OS_IOS){
            jsb.reflection.callStaticMethod("AnalyticsHelper", "logErrorID:eventParams:",errorId,errorDescriptionStr);

        }else{
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/Extension/AnalyticsHelper', 'logError', '(Ljava/lang/String;Ljava/lang/String;)V', errorId,errorDescriptionStr);
        }
        cocosAnalytics.CACustomEvent.onFailed(errorId, errorDescriptionStr, eventFailedReason);
        this.customAnalyticsApi(errorId,errorDescription);
    }

    public static setUserId(id:string){
        if(!cc.sys.isNative)
            return;

        if(cc.sys.os == cc.sys.OS_IOS){
            jsb.reflection.callStaticMethod("AnalyticsHelper", "setUserID:",id);

        }else{
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/Extension/AnalyticsHelper', 'setUserID', '(Ljava/lang/String;)V', id);
        }
        cocosAnalytics.CAAccount.loginSuccess({
            userID: id,
            age: 0,             // 年龄
            sex: 3,             // 性别：1为男，2为女，其它表示未知
            channel: '99999',   // 获客渠道，指获取该客户的广告渠道信息
        });
    }

    public static logout(){
        if(!cc.sys.isNative)
            return;

        cocosAnalytics.CAAccount.logout();
    }

    public static loginFailed(reasonCode:string){
        if(!cc.sys.isNative)
            return;

        cocosAnalytics.CAAccount.loginFailed({
            reason: reasonCode,
            channel: "99999",
        });
    }

    private static customAnalyticsApi(event:string,body_data:any){
        var ApiManager = require("ApiManager");
        let xhr = new XMLHttpRequest();
        xhr.open("POST", ApiManager.server+"/v1/e",true);

        xhr.onload = async function(){
            if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                cc.log("成功");
            }
        };
        xhr.onerror = async function(e){
            cc.log("失敗 =",e);
        };
        xhr.ontimeout = async function(){
            cc.log("超時");
        };
        xhr.onabort = async function(){
            cc.log("abort");
        };
        xhr.setRequestHeader("App-Origin", ApiManager.domain);
        body_data.event = event;
        xhr.send(JSON.stringify(body_data));
    }
}
