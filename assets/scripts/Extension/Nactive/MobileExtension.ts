import {VM} from "../../modelView/ViewModel";
import {PlatFormType} from "../../Enum/PlatFormType";
import {InternetState} from "../../Enum/InternetState";

export default class MobileExtension {

    public static Copy(str: string){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            jsb.reflection.callStaticMethod("AppExtension", "copy:", str);
            return true;
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "copy", "(Ljava/lang/String;)V", str);
            return true;
        }else{
            var input = str;
            const el = document.createElement('textarea');
            el.value = input;
            el.setAttribute('readonly', '');
            el.style.fontSize = '12pt';
            document.body.appendChild(el);
            el.select();
            el.selectionStart = 0;
            el.selectionEnd = input.length;
            var success = false;
            try {
                success = document.execCommand('copy');
            } catch (err) {}
            document.body.removeChild(el);
            return success;
        }
    }

    public static ChangeUIInterfaceOrientation(orientation : UIInterfaceOrientation){
        if(!cc.sys.isNative)
            return;

        if(cc.sys.os == cc.sys.OS_IOS){
            if(orientation == UIInterfaceOrientation.All) {
                cc.view.setOrientation(cc.macro.ORIENTATION_AUTO);
                jsb.reflection.callStaticMethod("AppController", "backToAll");
            }else {
                cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT);
                jsb.reflection.callStaticMethod("AppController", "backToPortrait");
            }
        }else{
            if(orientation == UIInterfaceOrientation.All) {
                cc.view.setOrientation(cc.macro.ORIENTATION_AUTO);
                console.log("轉換成senser 模式");
                jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'setOrientation', '(Ljava/lang/String;)V', "L");

            }else {
                cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT);
                console.log("轉換成 直立 模式");
                jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'setOrientation', '(Ljava/lang/String;)V', "V");

            }
        }
    }

    public static GetUpdataUrl(){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS){
            return  jsb.reflection.callStaticMethod("AppExtension", "GetAppUpdataUrl");
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
            if(VM.getValue("setting.platform") == PlatFormType.Official)
                return "https://jbapk45.1485745.com/Download/Update/Updata.json";
            else
                return "https://s3-ap-northeast-1.amazonaws.com/mlbapp.354785.com/Download/Update/Updata.json";
            //return  jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'GetUpdataUrl', '()Ljava/lang/String;');
        }else{
            return "https://s3-ap-northeast-1.amazonaws.com/mlbapp.354785.com/Download/Update/Updata.json";
        }
    }

    public static GetPlatform(){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS){
             var platform = jsb.reflection.callStaticMethod("AppExtension", "GetPlatform");
             VM.setValue("setting.platform",platform);
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
            var platform = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'GetPlatform', '()I');
            VM.setValue("setting.platform",platform);
        }

        return VM.getValue("setting.platform");
    }

    public static GetVersion(){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS){
            return  jsb.reflection.callStaticMethod("AppExtension", "GetVersion");
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
             // return VM.getValue("setting.appversion");
            return  jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'GetVersion', '()Ljava/lang/String;');
        }

        return VM.getValue("setting.appversion");
    }

    public static GetNetState(){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS){
            return  jsb.reflection.callStaticMethod("AppExtension", "getNetworkType");
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
            return  jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'GetNetType', '()I');
        }

        return InternetState.ReachableViaWWAN;
    }

    /***
     * 取的推播事件資料
     * @constructor
     */
    public static GetPushNotificationData(){
        if(cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS){
            return  jsb.reflection.callStaticMethod("AppController", "getPushNotificationData");
        }else if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
            return  jsb.reflection.callStaticMethod('org/cocos2dx/javascript/service/CqCpIntentService', 'getPushNotificationData', '()Ljava/lang/String;');
        }

        return "";
    }
}
export enum UIInterfaceOrientation {
    All = 1,
    Portrait = 2,
}