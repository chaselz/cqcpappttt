String.prototype.isEmail = function () {
    return /^\w{1,63}@[a-zA-Z0-9]{2,63}\.[a-zA-Z]{2,63}(\.[a-zA-Z]{2,63})?$/.test(this);
};

String.prototype.isPhoneNumber = function () {
    return /^(?:\+?86)?1(?:3\d{3}|5[^4\D]\d{2}|8\d{3}|7(?:[01356789]\d{2}|4(?:0\d|1[0-2]|9\d))|9[189]\d{2}|6[567]\d{2}|4(?:[14]0\d{3}|[68]\d{4}|[579]\d{2}))\d{6}$/.test(this);
};

String.prototype.hasNumber = function () {
    return /\d+/.test(this);
};

String.prototype.onlyNumber = function(){
    return /^[0-9]*$/.test(this);
};

String.prototype.isWeChatNum = function(){
    return /^[\d|a-zA-Z|-]+$/.test(this);
};

String.prototype.OnlyNumAndEn = function () {
    return /^[\d|a-zA-Z]+$/.test(this);
};

String.prototype.isName = function () {
    return /^[\u4e00-\u9fa5a-zA-Z]+$/.test(this);
};

String.prototype.isZH = function () {
    return /^[\u4e00-\u9fa5]+$/.test(this);
};

String.prototype.isNull = function () {
    return this == null || this == undefined || this.length == 0 ;
};

String.prototype.isURL = function () {
    // 验证url
    return /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/.test(this);
};
/***
 * 貨幣正規化
 * @param precision 小數點後幾位
 * @param separator 取代的符號
 * @returns {string}
 */
String.prototype.formatNumber = function(precision, separator) {
    var parts;
    var num;
    // 判断是否为数字
    if (!isNaN(parseFloat(this)) && isFinite(this)) {
        // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
        // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(nßum))
        // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
        // 的值变成了 12312312.123456713
        num = Number(this);
        // 处理小数点位数
        num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
        // 分离数字的小数部分和整数部分
        parts = num.split('.');
        // 整数部分加[separator]分隔, 借用一个著名的正则表达式
        parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

        return parts.join('.');
    }
    return "0.00";
};

String.prototype.replaceAll = function(search,replacement){
    return this.replace(new RegExp(search, 'g'),replacement);
};

//给数字字符串补零，不支持负数
String.prototype.preZeroFill = function(num, size) {
    if (num >= Math.pow(10, size)) { //如果num本身位数不小于size位
        return num.toString();
    } else {
        var _str = Array(size + 1).join('0') + num;
        return _str.slice(_str.length - size);
    }
};

/// <summary>
/// 格式化字串
/// </summary>
String.prototype.format = function () {
    var a = this;
    for (var k in arguments) {
        a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
    }
    return a
};

String.prototype.GameTimeWithMin = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}-{1}-{2} {3}:{4}".format(now1.getFullYear(),(now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2),now1.getHours().preZeroFill(2),now1.getMinutes().preZeroFill(2))
};

String.prototype.GameTimeWithSecond = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}-{1}-{2} {3}:{4}:{5}".format(now1.getFullYear(),(now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2),now1.getHours().preZeroFill(2),now1.getMinutes().preZeroFill(2),now1.getSeconds().preZeroFill(2))
};

String.prototype.GameTimeOnyDay = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}-{1}-{2}".format(now1.getFullYear(),(now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2));
};

String.prototype.GameTimeOnyTime = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}:{1}".format((now1.getHours()).preZeroFill(2),now1.getMinutes().preZeroFill(2));
};

String.prototype.DetailTimeAll = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}/{1}/{2}".format(now1.getFullYear(),(now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2));
};

String.prototype.DetailTimeOnlyMD = function(){
    var now1 = new Date(this+" +0000");
    return  "{0}/{1}".format((now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2));
};

String.prototype.ToApiTime = function(){
    cc.log(this.replaceAll("/","-"));
    var temp = this.replaceAll("/","-");
    var now1 = new Date(temp+":00");
    cc.log("now1 ",now1);
    return "{0}-{1}-{2} {3}".format(now1.getUTCFullYear(),(now1.getUTCMonth()+1).preZeroFill(2),now1.getUTCDate().preZeroFill(2),now1.getUTCHours().preZeroFill(2));
};

Number.prototype.formatNumber = function(precision, separator) {
    var parts;
    var num;
    // 判断是否为数字
    if (!isNaN(parseFloat(this)) && isFinite(this)) {
        // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
        // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(nßum))
        // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
        // 的值变成了 12312312.123456713
        num = Number(this);
        // 处理小数点位数
        num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
        // 分离数字的小数部分和整数部分
        parts = num.split('.');
        // 整数部分加[separator]分隔, 借用一个著名的正则表达式
        parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

        return parts.join('.');
    }
    return "0.00";
};

Number.prototype.preZeroFill = function (size) {
    if(this >= Math.pow(10,size)){
       return this.toString();
    }else{
        var _str = Array(size + 1).join('0') + this;
        return _str.slice(_str.length - size);
    }
};

