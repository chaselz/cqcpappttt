
function pad(num, n) {
   var len = num.toString().length;
   while(len < n) {
       num = "0"+num;
       len++;
   }
   return num;
};

Date.prototype.UTCZoomTime = function () {
    // create Date object for current location
    var d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*0));

    // return time as a string
    return nd.getFullYear()+"-"+pad(nd.getMonth(),2)+"-"+pad(nd.getDate(),2)+" "+pad(nd.getHours(),2);
};

Date.prototype.UTCZoomTimeFrom = function (time) {
    // convert to msec
    // add local time zone offset
    // get UTC time in msec

    if(typeof time == "string"){
        if(/^\d{4}-\d{2}-\d{2} \d{2}$/.test(time)){
            var regexp = /\d+/g;
            var lll2 = time.match(regexp);
            var data = new Date(lll2[0],lll2[1],lll2[2],lll2[3]);
        }else{
            var data = new Date(time);s
        }
        var utc = data.getTime() + (data.getTimezoneOffset() * 60000);
    }else {
        var utc = time.getTime() + (time.getTimezoneOffset() * 60000);
    }

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000*0));

    // return time as a string
    return nd.getFullYear()+"-"+pad(nd.getMonth(),2)+"-"+pad(nd.getDate(),2)+" "+pad(nd.getHours(),2);
};

Date.prototype.UTCDateWithHour = function () {
    return this.getUTCFullYear()+"-"+pad(this.getUTCMonth()+1,2)+"-"+pad(this.getUTCDate(),2)+" "+pad(this.getUTCHours(),2);
};

Date.prototype.UTCDate = function () {
    return this.getUTCFullYear()+"-"+pad(this.getUTCMonth()+1,2)+"-"+pad(this.getUTCDate(),2)+" "+pad(this.getUTCHours(),2)+":"+pad(this.getUTCMinutes(),2);
};