
const {ccclass, property} = cc._decorator;

@ccclass
export default class AppLog {

    private static getDateString(){
        var d = new Date();
        var str = d.getHours();
        var timeStr = "";
        timeStr += (str.toString().length==1? "0"+str : str) + ":";
        str = d.getMinutes();
        timeStr += (str.toString().length==1? "0"+str : str) + ":";
        str = d.getSeconds();
        timeStr += (str.toString().length==1? "0"+str : str) + ":";
        str = d.getMilliseconds();
        if( str.toString().length==1 ) str = "00"+str;
        if( str.toString().length==2 ) str = "0"+str;
        timeStr += str;

        timeStr = "[" + timeStr + "]";
        return timeStr;
    }
    private static stack(index:number){
        var e = new Error();
        return e.stack;
    }

    public static log = function(){
        var backLog = console.log || cc.log ;

        if(true){
            backLog.call(this,"%s%s"+cc.js.formatStr.apply(cc,arguments),this.stack(2),this.getDateString());
        }
    };

    public static info = function () {
        var backLog = console.log || cc.log ;
        if(true){
            backLog.call(this,"%c%s:"+cc.js.formatStr.apply(cc,arguments),"color:#00CD00;",this.getDateString());
        }
    };

    public static warn = function(){
        var backLog = console.log || cc.log ;
        if(true){
            backLog.call(this,"%c%s:"+cc.js.formatStr.apply(cc,arguments),"color:#ee7700;",this.getDateString());
            //cc.warn
        }
    };

    public static err = function(){
        var backLog = console.log || cc.log ;
        if(true){
            backLog.call(this,"%c%s:"+cc.js.formatStr.apply(cc,arguments),"color:red",this.getDateString());
        }
    };
}
