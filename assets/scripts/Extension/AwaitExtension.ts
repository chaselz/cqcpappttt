// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { resolve } from "dns";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Awaiter  {
    public static until(conditionFunction) {
                            //BalanceManager.main.isReady                            
        const poll = function(resolve ){                            
            if(conditionFunction()) 
            // BalanceManager.main.isReady
            {
            resolve();          //確定promise結束繼續往下做
            }
            else {
            setTimeout(function(_ )
            {
                poll(resolve),400
            });
            }

        }
       /*
        const poll = resolve => {
            if(conditionFunction()) resolve();
            else setTimeout(_ => poll(resolve), 400);
        };
        */
        return new Promise(poll);
    }

    public static sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
