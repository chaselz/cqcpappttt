
import IPayMode from "./IPayMode";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var ApiManager = require("ApiManager");

export default class BankOnlineMode extends IPayMode {

    public AnalyzedepositModeBankMenuTypeFunc(data: any) {
        this.FourthPayParsing(data);
    }

    public AnalyzeDepositResponceFunc(data : any) {
        this.ForthPayDepositResponse(data);
    }

    public setTransferDropDownEvent(index){
        super.setTransferDropDownEvent(index);
    }

    public setPayWayDropDownEvent(index) {
        super.setPayWayDropDownEvent(index);
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        let mode = VM.getValue("recharge.paywaylist")[index].type;
        ApiManager.Deposit.get.DepositModeBankMenu(VM.getValue("recharge.paytype"),mode,VM.getValue("recharge.money"));
        VM.setValue("recharge.payway",index);

        vc.NormalMod(vc.choosepath+"PayWay");

        vc.activeObj(vc.choosepath+"money",true);
        vc.activeObj(vc.choosepath+"transferWay",true);
        vc.ErrorMod(vc.choosepath+"transferWay");

        vc.setChooseBtnName(VM.getValue("lang.recharge"));
    }

    public ChooseNextBtnEvent() {
        let noerr = true;
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        if(VM.getValue("recharge.payway") == undefined){
            cc.log("bankindex ");
            noerr = false;
            vc.ErrorMod(vc.choosepath+"PayWay");
        }
        if(vc.isUIActive(vc.choosepath+"transferWay") && VM.getValue("recharge.bankindex") == undefined){
            noerr = false;
            vc.ErrorMod(vc.choosepath+"transferWay");
        }

        if(noerr){
            VM.setValue("core.lock",true);
            let fourid = VM.getValue("recharge.paywaylist")[VM.getValue("recharge.payway")].type;
            ApiManager.Deposit.get.BankPay(VM.getValue("recharge.bankid"),fourid,VM.getValue("recharge.money"),VM.getValue("recharge.point"));
        }
        else{
            vc.showAlertErr();
        }
    }

}
