
import IPayMode from "./IPayMode";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;

var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var ApiManager = require("ApiManager");

export default class wechatMode extends IPayMode {
    /**
     * 支付方式類型
     */
    private _mode:Number = -1;

    public AnalyzedepositModeBankMenuTypeFunc(data: any) {
        if(this._mode == 0)
            this.TransferParsing(data);
        else
            this.FourthPayParsing(data);
    }

    public AnalyzeDepositResponceFunc(data : any) {
        if(this._mode == 0)
            this.TransferDepositResponse(data);
        else
            this.ForthPayDepositResponse(data);
    }

    public setTransferDropDownEvent(index){
        super.setTransferDropDownEvent(index);
        if(this._mode == 0) {
            let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
            let bankcard = VM.getValue("recharge.cardinfo")[index];
            vc.setBankIcon(bankcard.href);
            vc.activeObj(vc.choosepath + "cardInfo", true);
            vc.setCardInfo(bankcard);
        }
    }

    public setPayWayDropDownEvent(index) {
        super.setPayWayDropDownEvent(index);
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        let mode = VM.getValue("recharge.paywaylist")[index].type;
        ApiManager.Deposit.get.DepositModeBankMenu(VM.getValue("recharge.paytype"),mode,VM.getValue("recharge.money"));
        VM.setValue("recharge.payway",index);
        vc.NormalMod(vc.choosepath+"PayWay");
        this._mode = mode;
        vc.activeObj(vc.choosepath+"warnview",mode == 0);
        vc.activeObj(vc.choosepath + "money", mode != 0);
        vc.setChooseBtnName(VM.getValue("lang."+ (mode != 0 ? "recharge" : "nextpage")));
        if(mode != 0) {
            vc.activeObj(vc.choosepath + "cardInfo", false);
            vc.activeObj(vc.choosepath + "BankIconlayout", false);
        }

        vc.activeObj(vc.choosepath+"transferWay",true);
        vc.ErrorMod(vc.choosepath+"transferWay");
    }

    public ChooseNextBtnEvent() {
        let noerr = true;
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        if(VM.getValue("recharge.payway") == undefined){
            noerr = false;
            vc.ErrorMod(vc.choosepath+"PayWay");
        }
        if(vc.isUIActive(vc.choosepath+"transferWay") && VM.getValue("recharge.bankindex") == undefined){
            noerr = false;
            vc.ErrorMod(vc.choosepath+"transferWay");
        }

        if(noerr)
            if(this._mode == 0) {
                PageManager.InputDepositPage();
                super.ChooseNextBtnEvent();
            }else
                this.ShowPayAlert();
        else
            vc.showAlertErr();
    }
}
