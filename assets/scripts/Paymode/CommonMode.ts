

import IPayMode from "./IPayMode";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");

export default class CommonMode extends IPayMode {

    public AnalyzedepositModeBankMenuTypeFunc(data:any) {
        this.TransferParsing(data);
    }

    public AnalyzeDepositResponceFunc(data:any) {
        this.TransferDepositResponse(data);
    }

    public setTransferDropDownEvent(index){
        super.setTransferDropDownEvent(index);
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        vc.activeObj(vc.choosepath+"cardInfo",true);
        let bankcard = VM.getValue("recharge.cardinfo")[index];
        vc.setCardInfo(bankcard);
        vc.setBankIcon(bankcard.href);
    }

    public setPayWayDropDownEvent(index) {
        super.setPayWayDropDownEvent(index);
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        let mode = VM.getValue("recharge.paywaylist")[index].type;

        ApiManager.Deposit.get.DepositModeBankMenu(VM.getValue("recharge.paytype"),mode,VM.getValue("recharge.money"));
        VM.setValue("recharge.payway",index);
        vc.NormalMod(vc.choosepath+"PayWay");

        vc.activeObj(vc.choosepath+"warnview",true);

        vc.activeObj(vc.choosepath+"transferWay",true);
    }

    public ChooseNextBtnEvent() {
        let noerr = true;
        let vc = CtrlAssistant.main.getCtrl("PayViewCtrl");
        if(VM.getValue("recharge.payway") == undefined){
            noerr = false;
            vc.ErrorMod(vc.choosepath+"PayWay");
        }
        if(vc.isUIActive(vc.choosepath+"transferWay") && VM.getValue("recharge.bankindex") == undefined){
            noerr = false;
            vc.ErrorMod(vc.choosepath+"transferWay");
        }

        if(noerr){
            PageManager.InputDepositPage();
            super.ChooseNextBtnEvent();
        }
        else{
            vc.showAlertErr();
        }
    }

}
