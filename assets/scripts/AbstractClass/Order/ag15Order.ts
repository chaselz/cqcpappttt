

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class ag15Order extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.bettype2"),data.Sport);
        str += "{0}：{1}\n".format(VM.getValue("lang.betteam"),data.Competition);
        str += "{0}：{1}\n".format(VM.getValue("lang.gameplaytype"),data.Market);
        str += "{0}：{1}\n".format(VM.getValue("lang.betselection"),data.Selection);
        str += "{0}：{1}".format(VM.getValue("lang.prize"),data.Odds);
        label.string = str;
    }
}
