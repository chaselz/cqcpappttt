import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class aeOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.table"),data.GameInfo.TableID);
        str += "{0}：{1}\n".format(VM.getValue("lang.rang"),data.RoundID);
        str += "{0}：{1}".format(VM.getValue("lang.type"),data.BetType);
        label.string = str;
    }
}
