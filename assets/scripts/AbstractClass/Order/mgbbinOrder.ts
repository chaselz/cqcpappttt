

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class mgbbinOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "";
        if(data.SerialID && data.TableCode && data.Result) {
            str += "{0}：{1}\n".format(VM.getValue("lang.rangnum"), data.SerialID);
            str += "{0}：{1}\n".format(VM.getValue("lang.tablenum"), data.TableCode);
            str += "{0}：{1}".format(VM.getValue("lang.result"), data.Result);
        }
        label.string = str;
    }
}
