

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class BcOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.bettype"),this.translateTo("bettype",data.BetType));
        str += "{0}：{1}\n".format(VM.getValue("lang.bccap"),this.translateTo("oddtype",data.OddType));
        str += "{0}：{1}".format(VM.getValue("lang.prize"),data.TotalPrice);
        label.string = str;
    }

}
