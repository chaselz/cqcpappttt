

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class VrOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.issuenum"),data.IssueNumber);
        str += "{0}：{1}\n".format(VM.getValue("lang.opencode"),data.WinningNumber);
        str += "{0}：{1}\n".format(VM.getValue("lang.buycode"),data.Number);
        str += "{0}：{1}\n".format(VM.getValue("lang.playtype"),data.BetTypeName);
        str += "{0}：{1}".format(VM.getValue("lang.vrodds"),data.Odds);
        label.string = str;
    }

}
