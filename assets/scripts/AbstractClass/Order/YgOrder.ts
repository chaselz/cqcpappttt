
import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;


export default class YgOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.issuenum"),data.Period);
        str += "{0}：{1}\n".format(VM.getValue("lang.opencode"),data.OpenHaoMa);
        str += "{0}：{1}\n".format(VM.getValue("lang.playtype"),data.PlayName);
        str += "{0}：{1}".format(VM.getValue("lang.betcontent"),data.HaoMa);
        label.string = str;
    }

}
