

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";
import Awaiter from "../../Extension/AwaitExtension";

const {ccclass, property} = cc._decorator;

export default class BacOrder extends IOrder {

    async setList(label: cc.Label, data: any) {
        if(data.AGGameCode && data.TableCode && data.PlayType) {
            let str = "{0}：{1}\n".format(VM.getValue("lang.rangnum"), data.AGGameCode);
            str += "{0}：{1}\n".format(VM.getValue("lang.tablenum"), data.TableCode);
            str += "{0}：{1}".format(VM.getValue("lang.playtype"), await this.getPlayTypeName(data.PlayType));
            label.string = str;
        }
    }

    private async getPlayTypeName(type){
        await Awaiter.until(_=>VM.getValue("betrecord.gameplaytype").length > 0);
        return VM.getValue("betrecord.gameplaytype").find(x=>x.key === type).name
    }


}
