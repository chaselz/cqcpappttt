

import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class BgOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.issuenum"),data.Issue);
        str += "{0}：{1}\n".format(VM.getValue("lang.opencode"),data.OpenCode);
        str += "{0}：{1}\n".format(VM.getValue("lang.buycode"),data.BuyCodeCase);
        str += "{0}：{1}\n".format(VM.getValue("lang.playtype"),data.PlayName);
        str += "{0}：1:{1}".format(VM.getValue("lang.prize"),data.Prize);
        label.string = str;
    }

}
