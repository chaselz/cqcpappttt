import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export abstract class IOrder  {
    abstract setList(label:cc.Label,data:any);

    protected translateTo(library:string,key:string){
        return  VM.getValue("{0}.b{1}".format(library,key));
    }
}
