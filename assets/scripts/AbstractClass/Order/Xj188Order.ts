
import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

var base64 = require("base64");

export default class Xj188Order extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = "{0}：{1}\n".format(VM.getValue("lang.bettype"),data.BetType);
        str += "{0}：{1}".format(VM.getValue("lang.bccap"),this.translateTo("xj188",data.OddsType));
        str += this.base64decodedetail(atob(data.BetsData));
        label.string = str;
    }

    private base64decodedetail(str:string){

        let json = JSON.parse(str);
        cc.log("base64decodedetail =",json);
        let res = "";
        json.forEach(temp=>{
            res += "\n{0}\n".format(temp.Event.EventType);
            res += "{0}：{1}\n".format(VM.getValue("lang.matchname"),temp.Event.Tag);
            res += "{0}：{1}\n".format(VM.getValue("lang.bpvs"),temp.Event.Name);
            res += "{0}：{1} {2} @{3}".format(VM.getValue("lang.betchoose"),temp.Selection,temp.Handicap || "",temp.Odds);
        });
        return res;
    }
}
