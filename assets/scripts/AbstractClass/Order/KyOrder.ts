
import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

export default class KyOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        var str = "{0}：{1}\n".format(VM.getValue("lang.position"),data.ChairID);
        str += "{0}：{1}".format(VM.getValue("lang.tablenum"),data.TableID);
        label.string = str;
        label._forceUpdateRenderData();
    }
}
