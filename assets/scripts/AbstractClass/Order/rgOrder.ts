import {IOrder} from "./IOrder";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class rgOrder extends IOrder {

    setList(label: cc.Label, data: any) {
        let str = this.arraydetail(data.Detail);
        label.string = str;
    }

    private arraydetail(arr:any){
        let str = "";
        arr.forEach(item => {
            str += "{0}：{1}\n".format(VM.getValue("lang.matchname"),item.MatchName);
            str += "{0}：{1}\n".format(VM.getValue("lang.betchoose"),item.Title);
        });
        return str.slice(0,-2);
    }
}
