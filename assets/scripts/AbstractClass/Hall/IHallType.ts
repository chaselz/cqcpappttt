import {HallType} from "../../Type/HallType";
import {VM} from "../../modelView/ViewModel";
import {PageViewType} from "../../Enum/PageViewType";
import PageManager from "../../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

export abstract class IHallType {

    protected view : any = null;

    constructor(_view:any) {
        this.view = _view;
    }
    abstract Init();
    abstract async setData(type:HallType,data:any);
    public abstract setLoad(is_show:boolean);
    public abstract setUserName(type:HallType,value:string);
    public abstract setUserMoney(type:HallType,value:string);
    /***
     * 製造及更新物件
     * @param path 父物件
     * @param itemname 物件名
     * @param data 資料
     * @param func 方法
     * @returns {Promise<void>}
     * @constructor
     */
    protected async CreatItems(path,itemname,data,func = null) {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });

        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    };
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    protected async creatFactory(path,itemname,index,data,onClickFunc,type = null) {
        let item = null;
        if(index >= this.view[path].getComponentsInChildren(itemname).length) {
            item = await UI_manager.create_item_Sync(this.view[path], itemname);
            item.getComponent("Iitem").setValue(data,onClickFunc);
            item.active = true;
        }else {
            item = this.view[path].getComponentsInChildren(itemname)[index];
            item.getComponent("Iitem").setValue(data);
            item.node.active = true;
        }
    };
    
    /**
     * 送出取得使用者資料 api
     */
    protected sentUserApi(){
        ApiManager.User.get.UserData();
        ApiManager.User.get.UserBalance();
    }

    /**
     * 檢查是否登入
     */
    protected checkIsLogin(){
        let islogin = VM.getValue("core.islogin");
        let mode = VM.getValue("core.demo_mode");
        return islogin && !mode;
    }

    /**
     * 顯示錯誤提示
     */
    protected showWarn(){
        TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.hallwarn"));
    }

    /**
     * 顯示 GameList 頁面
     */
    protected showGameListView(){
        PageManager.GameListPage();
    }

    /**
     * 打開 WebView
     */
    protected showWebView(url){
        CtrlAssistant.main.getCtrl("MainCtrl").setWebUrl(url);
    }
}
