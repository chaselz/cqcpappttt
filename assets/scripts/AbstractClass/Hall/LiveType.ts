import { IHallType } from "./IHallType";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UIAssistant = require("UIAssistant");
var HallType = require("HallType");

export default class LiveType extends IHallType {

    private loading: boolean = false;
    private loading_userName: boolean = false;
    private loading_userMoney: boolean = false;
    private loading_Item: boolean = false;
    private isLogin: boolean = false;

    Init() {
        this.setLoad(true);
        this.HideItem();
        this.isLogin = this.checkIsLogin();
        this.DefaultScreen();
        this.loading_Item = true;
        if(this.isLogin){
            this.loading_userMoney = true;
            this.loading_userName = true;
            this.sentUserApi();
        }
    }

    /**
     * 設定遊戲項目資料
     * @param type 線上真人類型
     * @param data 資料
     */
    async setData(type:HallType,data:any) {
        if(type != HallType.live)
            return;
        let result = data.length ==0;
        if(!result)
            this.CreatItems("ScrollView/view/content/LiveNode","ProviderItem",this.AnaylizeData(data),this.checkSentApi.bind(this));
        this.loading_Item = false;
        this.setLoad(false);
    }

    /**
     * 解析拿回來的資料
     * @param data 資料
     */
    private AnaylizeData(data){
        let result = [];
        data.forEach(item => {
            let resItem = {
                code:item.provider.code,
                id:item.kind[0].id,
                name:item.provider.name,
                display:item.kind[0].display,
                imageName:"hall-live-"+item.provider.code,
            };
            result.push(resItem);
        });
        return result;
    }

    /**
     * 設定 GameList 的資料並開啟頁面
     * @param type 線上真人類型
     * @param data url資料
     */
    private setGameListData(type:HallType,data:any){
        if(type != HallType.live)
            return;
        this.setLoad(false);
        let gameId = VM.getValue("hall.gameId");
        let allClass = VM.getValue("hall.gameAllClass");
        let arr = [];
        allClass.forEach(item => {
            if(item.kind[0].id == gameId){
                arr = item.kind;
            }
        });
        VM.setValue("hall.gameClass",arr);
        VM.setValue("hall.gameList",data);
        this.showGameListView();
    }

    /**
     * 設定 WebView 的 url
     * @param type 線上真人類型
     * @param data url資料
     */
    private setWebUrl(type:HallType,data:any){
        if(type != HallType.live)
            return;
        this.showWebView(data);
    }

    constructor(view:any) {
        super(view);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }

    /**
     * 檢查送出的 api
     */
    private checkSentApi(){
        if(this.loading)
            return true;
        if(!this.isLogin){
            this.showWarn();
            return true;
        }
        this.setLoad(true);
    }

    /**
     * 預設畫面
     */
    private DefaultScreen(){
        this.view["ScrollView/view/content/UserNode"].active = this.isLogin;
        this.view["ScrollView/view/content"].getComponent(cc.Layout).paddingTop = this.isLogin ? 16 : 24;
        this.view["ScrollView/view/content/LotteryNode"].active = false;
        this.view["ScrollView/view/content/CasinoNode"].active = false;
        this.view["ScrollView/view/content/ChesscardNode"].active = false;
        this.view["ScrollView/view/content/LiveNode"].active = true;
        this.view["ScrollView/view/content/SportNode"].active = false;
    }

    /**
     * Load 的顯示狀態
     * @param is_show 顯示狀態
     */
    public setLoad(is_show:boolean){
        if(!is_show){
            let loadStatus = this.getLoadStatus();
            if(loadStatus)
                return;
        }
        this.loading = is_show;
        UIAssistant.main.SetPanelVisible("Load",is_show);
        
    }

    /**
     * 取得所有 loading 的狀態
     */
    private getLoadStatus(){
        if(this.loading_userName)
            return true;
        if(this.loading_userMoney)
            return true;
        if(this.loading_Item)
            return true;
        return false;
    }

    /**
     * 設定玩家名稱
     * @param type 線上真人類型
     * @param value 玩家名稱
     */
    public setUserName(type:HallType,value:String){
        if(type != HallType.live)
            return;
        if(this.loading_userName){
            this.loading_userName = false;
            this.setLoad(false);  
            this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = value;
            VM.setValue("member.account",value);
        }
    }

    /**
     * 設定玩家錢包金額
     * @param type 線上真人類型
     * @param value 錢包金額
     */
    public setUserMoney(type:HallType,value:String){
        if(type != HallType.live)
            return;
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+value;
        VM.setValue("member.cash",value);
        if(this.loading_userMoney){
            this.loading_userMoney = false;
            this.setLoad(false);
        }
    }

    /**
     * 隱藏項目
     */
    private HideItem(){
        this.view["ScrollView/view/content/LiveNode"].children.forEach(node=>node.active = false);
    }

}
