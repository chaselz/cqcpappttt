import {IDetail} from "./IDetail";
import {DetailType} from "../../Enum/DetailType";
import {VM} from "../../modelView/ViewModel";
import newUIDatePicker, {DateType} from "../../../resources/UIDatePicker/newUIDatePicker";
import CheckMenuInputBox from "../../UI/CheckMenuInputBox";
import tagTimeItem from "../../Item/tagTimeItem";
import tagTimeCollision from "../../UI/tagTimeCollision";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

export default class FundsDetail extends IDetail {
    private kind : any = [];
    private sub_kind:any = [];
    private item:any = [];
    private starttime : string = "";
    private endtime : string = "";
    private mustpost: boolean = false;

    constructor(view:any) {
        super(view);
        this.view["FundsView/Input"].getComponent(CheckMenuInputBox).setCallBack(this.menuInputLisitener.bind(this));
    }

    onEnable() {
        if(this.mustpost)
            this.checkSentApi();
    }

    Init() {
        ApiManager.Report.FrontNewTrans();
        this.setTime(VM.getValue("lang.time"));
        this.view["FundsView/Input"].getComponent(CheckMenuInputBox).Init();
        var now = new Date();
        var nowstring ="{0}-{1}-{2} ".format(now.getFullYear(),(now.getMonth()+1).preZeroFill(2),now.getDate().preZeroFill(2));
        this.setLoad(true);
        this.starttime = nowstring +"00";
        this.endtime = nowstring + "23";
        newUIDatePicker.main.setPickDateCallback(this.PickerLisistener.bind(this));
        this.setTimeTitle();
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setPickDateType(DateType.IntervalTime);
    }

    async setData(type:DetailType,data:any) {
        if(type != DetailType.FOUNDS)
            return;

        let result = data.data.length ==0;
        this.setNoDataActive(result);
        this.setListActive(!result);
        this.view["BetView/List"].active = !result;
        if(!result) {
            this.view["FundsView/List/view/content"].removeAllChildren();
            this.CreatItems("FundsView/List/view/content", "FundsItem", data.data, null);
        }
        this.setLoad(false);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        let time = data[0].OperatingTime.DetailTimeOnlyMD();
        this.setTime(time);
        var targettimecount = 0;
        await data.asyncForEach(async (value,index) =>{
            var target_time = value.OperatingTime.DetailTimeOnlyMD();
            if(time != target_time){
                await this.creatFactory(path,"FundstagTimeItem", targettimecount, target_time, null);
                time = target_time;
                targettimecount++;
            }
            await this.creatFactory(path,itemname, index, value, func);
        });

        let recordItems = this.view[path].getComponentsInChildren(itemname);
        for(let i = data.length;i<recordItems.length;i++){
            recordItems[i].node.active = false;
        }

        let tagTimeItems = this.view[path].getComponentsInChildren(tagTimeItem);
        for(let i = targettimecount;i<tagTimeItems.length;i++){
            tagTimeItems[i].node.active = false;
        }
    }

    private setTime(timestr:string){
        this.view["FundsView/tagNode"].getComponent(tagTimeCollision).InitVlaue(timestr);
    }

    setLoad(is_show: boolean) {
        this.view["FundsView/Load"].active = is_show;
        if(is_show)
            this.view["FundsView/Load/icon"].getComponent(cc.Animation).play("buttonload");
        else
            this.view["FundsView/Load/icon"].getComponent(cc.Animation).play("buttonload");
    }

    setNoDataActive(is_show: boolean) {
        this.view["FundsView/NoData"].active = is_show;
        this.setTime(VM.getValue("lang.time"));
    }

    setListActive(is_show: boolean){
        this.view["FundsView/List"].active = is_show;
    }

    private PickerLisistener(target:any,y:any,m:any,d:any,h:any){
        this.setNoDataActive(true);
        this.setListActive(false);
        this.starttime = "{0}-{1}-{2} {3}".format(y[0],m[0].preZeroFill(2),d[0].preZeroFill(2),h[0].preZeroFill(2));
        this.endtime = "{0}-{1}-{2} {3}".format(y[1],m[1].preZeroFill(2),d[1].preZeroFill(2),h[1].preZeroFill(2));
        this.setTimeTitle();
        this.checkSentApi();
    }

    private setTimeTitle(){
        this.view["timeNode/time"].getComponent(cc.Label).string = "{0}:00 - {1}:59".format(this.starttime.replaceAll("-","/"),this.endtime.replaceAll("-","/"));
    }

    private menuInputLisitener(_kind,_sub_kind,_items){
        this.kind = _kind;
        this.sub_kind = _sub_kind;
        this.item = _items;
        this.checkSentApi();
    }

    private checkSentApi(){
        if(!this.view["FundsView"].parent.active){
            this.mustpost = true;
            return;
        }
        this.mustpost = false;
        cc.log("差多少 =",this.DateDiff());
        if(!this.starttime.isNull() && this.DateDiff() >= 7.00){
            TitleAlertView.main.show(TitleAlertType.Error,VM.getValue("lang.overdetailtime"));
            return;
        }
        if((this.kind.length > 0 || this.sub_kind.length > 0 || this.item.length > 0 ) && !this.starttime.isNull()){
            ApiManager.Report.ReportList(this.starttime.ToApiTime(),this.endtime.ToApiTime(),20,1,(this.kind.length > 0 ? this.kind : 0),(this.sub_kind.length > 0 ? this.sub_kind : 0),this.item);
            this.setLoad(true);
        }
    }

    private DateDiff = function () {
        var oDate1 = new Date(this.starttime+":00");
        var oDate2 = new Date(this.endtime+":00");
        var iDays = parseFloat(Math.abs(oDate1 - oDate2) / 1000 / 60 / 60 / 24); // 把相差的毫秒數轉換為天數
        return iDays;
    };
}
