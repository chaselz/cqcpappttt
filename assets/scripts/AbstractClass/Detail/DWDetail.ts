
import {IDetail} from "./IDetail";
import VMLabel from "../../modelView/VMLabel";
import tagTimeItem from "../../Item/tagTimeItem";
import {DetailType} from "../../Enum/DetailType";
import tagTimeCollision from "../../UI/tagTimeCollision";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");

export default class DWDetail extends IDetail {

    private Dw_detailStatus = DWDetailType.Deposit;

    constructor(_view:any) {
        super(_view);
        this.setDWDetailToolBarBtn();
        this.setDWStatusToolBarBtn();
    }

    onEnable() {
    }

    Init() {
        this.setTime(VM.getValue("lang.time"));
        this.depositApi(DWStatus.Apply);
        this.setDWDetailToolBar(DWDetailType.Deposit);
        this.setDWStatusToolBar(DWStatus.Apply);
    }

    async setData(type:DetailType,data:any) {
        if(type != DetailType.DW)
            return;
        this.view["DWView/List"].getComponent(cc.ScrollView).scrollToTop(0);
        let status = data.length == 0;
        this.view["DWView/List"].active = !status;
        this.view["DWView/NoData"].active = status;
        if(data.length == 0){
            this.setNoDataView();
        }else{
            this.view["DWView/List/view/content"].removeAllChildren();
            await this.CreatItems("DWView/List/view/content","DWRecordItem",data);
        }
        this.setLoad(false);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        let time = data[0].application_time.DetailTimeOnlyMD();
        this.setTime(time);
        var targettimecount = 0;
        await data.asyncForEach(async (value,index) =>{
            var target_time = value.application_time.DetailTimeOnlyMD();
            if(time != target_time){
                await this.creatFactory(path,"tagTimeItem", targettimecount, target_time, null);
                time = target_time;
                targettimecount++;
            }
            await this.creatFactory(path,itemname, index, value, func);
        });

        let recordItems = this.view[path].getComponentsInChildren(itemname);
        for(let i = data.length;i<recordItems.length;i++){
            recordItems[i].active = false;
        }

        let tagTimeItems = this.view[path].getComponentsInChildren(tagTimeItem);
        for(let i = targettimecount;i<tagTimeItems.length;i++){
            tagTimeItems[i].active = false;
        }
    }

    public setLoad(is_show:boolean){
        this.view["DWView/Load"].active = is_show;
        if(is_show)
            this.view["DWView/Load/icon"].getComponent(cc.Animation).play("buttonload");
        else
            this.view["DWView/Load/icon"].getComponent(cc.Animation).play("buttonload");
    }

    setNoDataActive(is_show: boolean) {
        this.view["DWView/NoData"].active = is_show;
        this.setTime(VM.getValue("lang.time"));
        this.setNoDataView();
    }

    private setTime(timestr:string){
        this.view["DWView/tagNode"].getComponent(tagTimeCollision).InitVlaue(timestr);
    }

    /***
     * 設定DW 存取款紀錄類型狀態 UI 顯示
     * @param type
     */
    private setDWStatusToolBar(type:DWStatus) {
        this.view["DWView/statustoolbar/statusBtns/main"].children.forEach((value,index)=>{
            value.getComponentInChildren(VMLabel).setOriginLabelValue((index == type ? "<b>{{0}}</b>":"{{0}}"));
        });
        this.view["DWView/statustoolbar/statusBtns/target"].runAction(cc.moveTo(0.2,(type-1)*188,0));
    };

    /***
     * 設定DW 存取款紀錄類型狀態Btn
     */
    private setDWStatusToolBarBtn() {
        this.view["DWView/statustoolbar/statusBtns/main"].children.forEach((value,index)=>{
            UI_manager.add_button_listen(value,this,function () {
                if(this.Dw_detailStatus == DWDetailType.Deposit)
                    this.depositApi(index);
                else
                    this.withDrawApi(index);

                this.setDWStatusToolBar(index);
            });
        });
    };
    /***
     * 設定DW 存取款紀錄類型 UI 顯示
     * @param type
     */
    private setDWDetailToolBar(type:DWDetailType) {
        this.view["DWView/detailtoolbar"].children.forEach((value,index)=>{
            value.getComponentInChildren(VMLabel).setOriginLabelValue((index == type ? "<b>{{0}}</b>":"{{0}}"));
            let gp = value.getComponent(cc.Graphics);
            if(index == type){
                gp.roundRect(0-value.width/2,0-value.height/2,value.width,value.height,16);
                gp.fill();
            }else
                gp.clear();
        });
    };
    /***
     * 設定DW 存取款紀錄類型Btn
     */
    private setDWDetailToolBarBtn() {
        this.view["DWView/detailtoolbar"].children.forEach((value,index)=>{
            UI_manager.add_button_listen(value,this,function () {
                this.Dw_detailStatus = index;
                if(this.Dw_detailStatus == DWDetailType.Deposit)
                    this.depositApi(DWStatus.Apply);
                else
                    this.withDrawApi(0);

                this.setDWDetailToolBar(index);
                this.setDWStatusToolBar(DWStatus.Apply);
            });
        });
    };
    /***
     * 設定DW沒有資料畫面
     */
    private setNoDataView() {
        UI_manager.setSprite(this.view["DWView/NoData/icon"],"detail/"+(this.Dw_detailStatus == 0 ? "pic-withdraw-null":"pic-deposit-null"));
        this.view["DWView/NoData/text"].getComponent(cc.Label).string = VM.getValue("lang."+(this.Dw_detailStatus == 0 ? "nodepositrecord": "nowithdrawrecord"));
    };

    /***
     * 存取Api
     * @param mode
     */
    private depositApi = function (mode:DWStatus) {
        this.setLoad(true);
        ApiManager.Report.DepositReport(mode);
    };
    /***
     * 取款Api
     * @param mode
     */
    private withDrawApi = function (mode:DWStatus) {
        this.setLoad(true);
        ApiManager.Report.WithDrawReport(mode);
    };


}

enum DWStatus {
    Apply = 0,
    Finish = 1,
    NoPass = 2,
}

enum DWDetailType {
    Deposit = 0,
    WithDraw = 1,
}