import {IDetail} from "./IDetail";
import MenuInputBox from "../../UI/MenuInputBox";
import newUIDatePicker, {DateType} from "../../../resources/UIDatePicker/newUIDatePicker";
import {VM} from "../../modelView/ViewModel";
import {DetailType} from "../../Enum/DetailType";
import PageManager from "../../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var UI_manager = require("UI_manager");

export default class BetDetail extends IDetail {

    private chooseKey : any = null;
    private starttime : string = "";
    private endtime : string = "";

    onEnable() {
        this.view["BetView/List"].getComponent(cc.ScrollView).scrollToTop(0);
    }

    Init() {
        this.HideItem();
        var now = new Date();
        var nowstring ="{0}-{1}-{2}".format(now.getFullYear(),(now.getMonth()+1).preZeroFill(2),now.getDate().preZeroFill(2));
        this.setLoad(true);
        this.starttime = nowstring;
        this.endtime = nowstring;
        this.chooseKey = null;
        this.setTimeTitle();
        ApiManager.Game.get.GameProviderItem("");
        newUIDatePicker.main.setPickDateCallback(this.PickerLisistener.bind(this));
        this.view["BetView/Input"].getComponent(MenuInputBox).Init();
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.setPickDateType(DateType.IntervalDay);
    }

    async setData(type:DetailType,data:any) {
        if(type != DetailType.BET)
            return;
        let result = data.data.length ==0;
        this.view["BetView/NoData"].active = result;
        this.view["BetView/List"].active = !result;
        if(!result)
            this.CreatItems("BetView/List/view/content","BetRecordDateItem",data.data,this.ShowByMakerView);
        this.setLoad(false);
    }

    constructor(view:any) {
        super(view);
        this.view["BetView/Input"].getComponent(MenuInputBox).setCallBack(this.menuInputLisitener.bind(this));
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }


    public setLoad(is_show:boolean){
        this.view["BetView/Load"].active = is_show;
        if(is_show)
            this.view["BetView/Load/icon"].getComponent(cc.Animation).play("buttonload");
        else
            this.view["BetView/Load/icon"].getComponent(cc.Animation).play("buttonload");
    }

    setNoDataActive(is_show: boolean) {
        this.view["BetView/NoData"].active = is_show;
    }

    /***
     * 顯示廠商排序的畫面
     * @constructor
     */
    private ShowByMakerView(){
        PageManager.BetRecordMakerPage();
    }

    private setTimeTitle(){
        this.view["timeNode/time"].getComponent(cc.Label).string = "{0} 00:00 - {1} 23:59".format(this.starttime.replaceAll("-","/"),this.endtime.replaceAll("-","/"));
    }

    private PickerLisistener(target:any,y:any,m:any,d:any,h:any){
        this.starttime = "{0}-{1}-{2}".format(y[0],m[0].preZeroFill(2),d[0].preZeroFill(2));
        this.endtime = "{0}-{1}-{2}".format(y[1],m[1].preZeroFill(2),d[1].preZeroFill(2));
        this.setTimeTitle();
        this.checkSentApi();
    }

    private menuInputLisitener(key){
        this.chooseKey = key;
        this.checkSentApi();
    }

    private checkSentApi(){
        if(this.chooseKey && !this.starttime.isNull()){
            cc.log(this.chooseKey ,this.starttime,this.endtime);
            let game_provider = VM.getValue("betrecord.gameproviderlist").find(x=>x.name == this.chooseKey);
            VM.setValue("betrecord.provider",game_provider ? game_provider.code : "");
            ApiManager.Report.BetsSummaryNew(this.starttime,this.endtime,8,game_provider ? game_provider.code : "");
            this.setLoad(true);
        }
    }


    private HideItem(){
        this.view["BetView/List/view/content"].children.forEach(node=>node.active = false);
        this.view["BetView/NoData"].active = false;
        this.view["BetView/Load"].active = false;
    }

    private InitMenuInputBox(){

    }
}
