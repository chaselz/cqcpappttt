import {IPay} from "./IPay";
import {VM} from "../../modelView/ViewModel";
import {RechargeType} from "../../Enum/RechargeType";
import Alert from "../../UI/Alert";
import {PageViewType} from "../../Enum/PageViewType";
import ChooseRecharge from "../Recharge/ChooseRecharge";
import VMLabel from "../../modelView/VMLabel";


const {ccclass, property} = cc._decorator;
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");


export default class commonPay extends IPay {

    constructor(rg:ChooseRecharge,view:cc.Node[],_vc:any){
        super(rg,view,_vc);
        let vmlabel = this.view["RechargeMain/view/content/RechargeChooseView/PayBank/top/Title"].getComponent(VMLabel);
        vmlabel.setWatchArrValue(["lang.onlinebanktransfer"]);
    }

    public DepositHandle(data:any) {

    }

    nextBtnEvent() {
        this.TransferBtnEvent();
    }

    presetMenuMode(data: any) {
        this.rg.PayWayInteractable(false);
        this.rg.PayWayIsCommomMode(true);
        this.rg.showMoneyLimitLabel(true);
        this.rg.TransferMode();
        if(VM.getValue("recharge.banklist").length == 0)
            ApiManager.Deposit.get.BankList();

        this.rg.callBankMenuApi();
    }


}
