import ChooseRecharge from "../Recharge/ChooseRecharge";
import {VM} from "../../modelView/ViewModel";
import {RechargeType} from "../../Enum/RechargeType";

export abstract class IPay  {

    protected rg:ChooseRecharge;
    protected view:cc.Node[];
    protected vc:any;
    constructor(rg:ChooseRecharge,view:cc.Node[],_vc:any) {
        this.rg = rg;
        this.view = view;
        this.vc = _vc;
    }
    public abstract nextBtnEvent();
    public abstract presetMenuMode(data:any);
    public abstract DepositHandle(data:any);

    /***
     * 轉帳的 Btn 事件
     * @constructor
     */
    protected TransferBtnEvent(){
        var money = this.rg.getMoney();
        var err = [];
        let max = parseFloat(VM.getValue("recharge.max").replaceAll("\,",""));
        let min = parseFloat(VM.getValue("recharge.min").replaceAll("\,",""));
        //判斷金額是否超過小數點兩位
        if(/^(([^0][0-9]+|0)\.([0-9]{3,100}))$/.test(money.toString())) {
            err.push("moneyonlytwopoint");
            this.rg.setMoneyUIRed();
        }
        ///為輸入金額
        if(money == 0){
            err.push("inputrechargemoney");
            this.rg.setMoneyUIRed();
        }
        //判斷金額是否在範圍內
        if(money != 0 && (max<money || min>money)){
            err.push("limitupperlower");
            this.rg.setMoneyUIRed();
        }
        if(this.view[this.rg.path+"Name"].active) {
            let name = this.rg.getUserName();
            var err2 =[];
            if(name.length > 0 && !name.isName()){
                err2.push("inputErr");
                this.rg.setNodeActive(this.rg.path+"Name/nameErr2",true);
            }

            if(name.length == 1){
                err2.push("inputErr");
                this.rg.setNodeActive(this.rg.path+"Name/nameErr1",true);
            }

            if(name.length == 0){
                err2.push("inputdepositname");
            }
            if(err2.length > 0){
                err.arrayCombine(err2);
                this.rg.setNameUIRed();
            }
        }

        //當是所屬支行時判斷規則
        if(this.view[this.rg.path+"DepositBranch"].active){
            var brabch = this.rg.getBranch();
            var err2 =[];
            if(!brabch.isName()){
                err2.push("onlychoren");
                this.rg.setNodeActive(this.rg.path+"DepositBranch/branchErr2",true);
            }

            if(brabch.length > 0 && brabch.length < 2){
                err2.push("inputErr");
                this.rg.setNodeActive(this.rg.path+"DepositBranch/branchErr1",true);
            }

            if(brabch.length == 0){
                err2.push("inputrechargemoney");
                this.rg.setNodeActive(this.rg.path+"DepositBranch/branchErr1",true);
            }

            if(brabch.length > 50){
                err2.push("limit50");
                this.rg.setNodeActive(this.rg.path+"DepositBranch/branchErr3",true);
            }

            if(err2.length > 0){
                err.arrayCombine(err2);
                cc.log(err);
                this.rg.setBranchUIRed();
            }
        }

        //當是所屬支行時判斷規則
        if(this.view[this.rg.path+"DepositOther"].active){
            var brabch = this.rg.getOtherBank();
            var err2 =[];
            if(brabch.length > 0 && !brabch.isName()){
                err2.push("onlychoren");
                this.rg.setNodeActive(this.rg.path+"DepositOther/branchErr2",true);
            }

            if(brabch.length > 0 && brabch.length < 2){
                err2.push("inputErr");
                this.rg.setNodeActive(this.rg.path+"DepositOther/branchErr1",true);
                this.rg.ShowAlert(TitleAlertType.Warn,VM.getValue("lang.inputErr"));
            }

            if(brabch.length == 0){
                err2.push("inputotherbank");
            }

            if(brabch.length > 50){
                err2.push("limit50");
                this.rg.setNodeActive(this.rg.path+"DepositOther/branchErr3",true);
            }

            if(err2.length > 0){
                err.arrayCombine(err2);
                cc.log(err);
                this.rg.setOtherUIRed();
            }
        }

        VM.setValue("recharge.iserr",err.length > 0);
        if(err.length > 0){
            this.rg.showWarnAlert(err);
        }else{
            cc.log("沒有問題");
            var branchpath = this.rg.path+(this.view[this.rg.path+"DepositOther"].active?"DepositOther":"DepositBranch")+"/EditBox";
            VM.setValue("recharge.depositbranch",
                this.view[branchpath].
                getComponent(cc.EditBox).string);
            VM.setValue("recharge.money", this.view[this.rg.path+"MoneyInput/EditBox"].getComponent(cc.EditBox).string);
            VM.setValue("recharge.transfername",this.rg.getUserName());
            this.vc.setMode(RechargeType.Transfer);
        }
    }
}
