
import {IPay} from "./IPay";
import {VM} from "../../modelView/ViewModel";
import {RechargeType} from "../../Enum/RechargeType";
import Alert from "../../UI/Alert";
import {PageViewType} from "../../Enum/PageViewType";
import ChooseRecharge from "../Recharge/ChooseRecharge";
import VMLabel from "../../modelView/VMLabel";
import PageManager from "../../ManagerUI/PageManager";


const {ccclass, property} = cc._decorator;
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var UIAssistant = require("UIAssistant");

export default class wechatPay extends IPay {

    constructor(rg:ChooseRecharge,view:cc.Node[],_vc:any){
        super(rg,view,_vc);
        let vmlabel = this.view["RechargeMain/view/content/RechargeChooseView/PayBank/top/Title"].getComponent(VMLabel);
        vmlabel.setWatchArrValue(["lang.paybank"]);
        console.log("wechatePay init");
    }

    public DepositHandle(data:any) {
        cc.log("DepositHandle");
        if(VM.getValue("recharge.payway") == 0){

        }else
            this.ForthPayDepositResponse(data);
    }

    nextBtnEvent() {

        let paydata = VM.getValue("recharge.payway");
        if(paydata == 0) {
            this.TransferBtnEvent();
        }else{
            let index = VM.getValue("recharge.paybankindex");
            //沒有選擇到通道跟銀行會跳出錯誤檢查
            if(index == -1){
                this.checkDespiteAmountCanPay();
                return;
            }
            let payway = VM.getValue("recharge.now_paywaylist")[index];
            if(payway.id == 4)//JXPay
                this.JXPayBtnEvent();
            else
                this.ThirdPayBtnEvent();
        }

    }

    presetMenuMode(data: any) {
        this.rg.PayWayInteractable(true);
        this.rg.PayWayIsCommomMode(false);
        this.rg.showMoneyLimitLabel(data.type == 0);
        if(data.type == 0) {
            this.rg.TransferMode();
            if(VM.getValue("recharge.banklist").length == 0)
                ApiManager.Deposit.get.BankList();
        }else
            this.rg.ThirdPayMode();

        this.rg.callBankMenuApi();
    }


    /***
     * 三方跟四方的 Btn 事件
     * @constructor
     */
    private ThirdPayBtnEvent(){
        cc.log("ThirdPayBtnEvent");
        if(this.checkDespiteAmountCanPay()) {
            VM.setValue("recharge.money", this.view[this.rg.path + "MoneyInput/EditBox"].getComponent(cc.EditBox).string);
            let msg = VM.getValue("lang.payway") + "：" + VM.getValue("core.title") + "\n";
            msg += "{0}：{1}\n\n".format(VM.getValue("lang.paymoney"), VM.getValue("recharge.money").formatNumber(2));
            msg += VM.getValue("lang.payinfocontent1") + "\n";
            msg += VM.getValue("lang.payinfocontent2");
            Alert.main.Show("lang.paymsg", msg, this.sentDespitApi.bind(this));
        }
    }

    /***
     * JXPay 的 下一步 Btn  事件
     * @constructor
     */
    private JXPayBtnEvent(){
        cc.log("JXPayBtnEvent");
        if(this.checkDespiteAmountCanPay()) {
            VM.setValue("recharge.money", this.view[this.rg.path + "MoneyInput/EditBox"].getComponent(cc.EditBox).string);
            this.sentDespitApi();
        }
    }

    private checkDespiteAmountCanPay(){
        var money = this.rg.getMoney();
        var err = [];
        let max = parseFloat(VM.getValue("recharge.max").replaceAll("\,",""));
        let min = parseFloat(VM.getValue("recharge.min").replaceAll("\,",""));
        //判斷金額是否超過小數點兩位
        if(/^(([^0][0-9]+|0)\.([0-9]{3,100}))$/.test(money.toString())) {
            cc.log("..");
            err.push("moneyonlytwopoint");
            this.rg.setMoneyUIRed();
        }
        ///為輸入金額
        if(money == 0){
            err.push("inputrechargemoney");
            this.rg.setMoneyUIRed();
        }
        //判斷金額是否在範圍內
        if(money != 0 && (max<money || min>money)){
            err.push("limitupperlower");
            this.rg.setMoneyUIRed();
        }

        VM.setValue("recharge.iserr",err.length > 0);
        if(err.length > 0) {
            this.rg.showWarnAlert(err);
            return false;
        }

        return true;
    }

    /***
     * 發送支付api
     */
    private sentDespitApi(){
        this.rg.LockRechargeBtn(true);
        let four = VM.getValue("recharge.now_paywaylist")[VM.getValue("recharge.paybankindex")];
        let point = "{0}-{1}".format(VM.getValue("core.title"),four.front_name);
        ApiManager.Deposit.get.FourthPay(four.id,parseInt(four.bank_id),parseFloat(VM.getValue("recharge.money")),point);
    }

    /**
     * 四方支付結果分析
     */
    protected ForthPayDepositResponse(data:any) {
        VM.setValue("core.lock",false);
        let datatemp = "{\"code\":\"1\",\"result\":{\"type\":3,\"url\":\"<form name='fourth' action='https:\\/\\/www.istmys.com\\/tingapi\\/jump' method='post'><input type='hidden' name='userid' value='mark2'><input type='hidden' name='merid' value='dddddddddesk'><input type='hidden' name='orderid' value='f_d_55'><input type='hidden' name='amount' value='200'><input type='hidden' name='userip' value='125.227.199.147'><input type='hidden' name='paytype' value='qrcode'><input type='hidden' name='bankflag' value='ALIPAY'><input type='hidden' name='notifyurl' value='https:\\/\\/jiflow.gakkixmasami.com\\/deposit\\/fourth\\/notify\\/1'><input type='hidden' name='syncurl' value='http:\\/\\/jarint01.gakkixmasami.com'><input type='hidden' name='sign' value='3c006fb07e860e7131d0246840d279e7'><\\/form><script type='text\\/javascript'>document.fourth.submit();<\\/script>\"}}";
        let datatemp1 = "{\"code\":\"1\",\"result\":{\"type\":0,\"url\":\"data:image\\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFACAIAAABC8jL9AAAJFklEQVR4nO3d0Y7bNgBE0bjo\\/\\/9y+lAgCNL1VrJJkdc653ljyZJmQ+yI5OPnz58\\/gKa\\/Vp8A8DoBhjABhjABhjABhjABhjABhjABhjABhjABhjABhrC\\/j\\/zQ4\\/GYfR6jnHq1+5vv5RXxH+evzwvPydnPmf3zWznyEPofGMIEGMIEGMIEGMIEGMIEGMIEGMIO9cB1o3q\\/gf3h7H5yVB8+W6WS3da7AV7ywoO7fplT9\\/fswzDqPg58CHPPsyE0hAkwhAkwhAkwhAkwhAkwhE3pgVf1mQPNbqoqn6+x+7H383yLFzmeXbUNX5wYcoMvmFg\\/ioUT3mQIDWECDGECDGECDGECDGECDGG3qJFWlZkvHPfZP5ld8yT6Xut4\\/9ctAvyCJRPuN+ylV30+BxlCQ5gAQ5gAQ5gAQ5gAQ5gAQ5ga6Wtnm4\\/d5vee6pNHMQn5ercI8MD1iodsMH32h2dvYL3bfGmOM4SGMAGGMAGGMAGGMAGGMAGGsFvUSAvnkc6e37tbX73quEt67x1MCfDHX7U\\/XNCjrjK7x07Y+Xk2hIYwAYYwAYYwAYYwAYYwAYawd2ukekMwxAX97VY+eH3m3H25xYscCw2ZP7zw52ern\\/9yhtAQJsAQJsAQJsAQJsAQJsAQpka6l9nzhyvzkz\\/GoQDX2\\/mz57\\/b9x11\\/gv71anraQ85mShDaAgTYAgTYAgTYAgTYAgTYAh7HPmTemi62WyV6W+7Xbfd7DZt8+Vmy4scN7JqH+ANf8F9DENoCBNgCBNgCBNgCBNgCBNgCNu9RtIovOa2++VOsmR\\/4yM3690Ar+r90vNLz\\/58vUe9YD7zqENPNaOHN4SGMAGGMAGGMAGGMAGGMAGGsN3nA4+SqFs+gOv8vVPX54oeeJRTfd0HbzC9m92u56jn5NTn79zDG0JDmABDmABDmABDmABDmABD2C410tS\\/vOshX5a4dAtPcvl+yJfuDzx7\\/eHZn1Pvn2+4P\\/CQz1m1nvYRhtAQJsAQJsAQJsAQJsAQJsAQdqhG2rAMHHVKW321UbXKVl\\/qX0NO6YKbvqoOXLYu9Cj1B7Ti1HV+of\\/car3uOzCEhjABhjABhjABhjABhjABhrBdaqT6franzn+3Gmzg+WzV9476qN3u1++S84EH7q875BA7zAsd\\/iEvHGLnB\\/0Psy\\/RZc+PITSECTCECTCECTCECTCECTCE7dIDP3P2L+y79caj1g3+1Gl6q5qn2ce97HvtHuDZKusGrzJ7\\/jBvMoSGMAGGMAGGMAGGMAGGMAGGsMeRnuCD1ys+qz6NjpD8utCz1x++IHhDeuYNv9dZU\\/dhvuA\\/mNn\\/UdkfGO5IgCFMgCFMgCFMgCFMgCHs3RrpU+dzjpJeL3pDU+dXLzR3f+CB8zy3ml+6210cZbd1s0d9+Ow+vPg8GEJDmABDmABDmABDmABDmABD2KEaabf9Wgced1VzcKvKt75+9c43690XObaaZ7vqQl\\/Qu05VWc85kfZ\\/2R8Y+H8CDGECDGECDGECDGECDGG7LCt71qiGY7em5Kz6jLlV5z\\/7vYD2\\/sCzn5JR81cXpjexrvWo+1jpmYsMoSFMgCFMgCFMgCFMgCFMgCHs0h5YbfDLqPWNZx834QPmq7+s+iLHbi8krNr\\/dvbnzDb7PtofGNiXAEOYAEOYAEOYAEOYAEPY48jfrzesH86q1Cp3c9nyq0eOu7BGevmgV\\/fAQ3q\\/2RcoFOzdHsTd+vlTiidvCA1hAgxhAgxhAgxhAgxhAgxhV9dIu+0PfDeJ63DBSZ5aF3rn\\/Y0PBfiFvnT2ixNLPv+Z3W7wB\\/TbzyReyLlyv2hDaAgTYAgTYAgTYAgTYAgTYAirLis7uzkYtX\\/s1L5xt\\/rqmd1qnm\\/MXq97+H15K8Bnz+aGffJWcrtXH3Rl7zrDO\\/fFEBrCBBjCBBjCBBjCBBjCBBjCpvTAlX5yodl945B+8oW6ZauGZquTmWSLFzlW9ckbmv2CxyiV83xmq\\/XJ32EIDWECDGECDGECDGECDGECDGGHaqRVk29\\/7Fc\\/zPbBX22I+vU5df4r9weu9K5DhObZnjrVUf38qJ+\\/237FRxhCQ5gAQ5gAQ5gAQ5gAQ5gAQ9isGmn2usqrjPpeCaumZ+5203e2xXzgF9ztBY9TPvK3yfe22of5mRnPpyE0hAkwhAkwhAkwhAkwhAkwhFVrpFHrJNf7zHRzNvDkR+3DnPNugIdcoB36tINWVaxbrf98gVPHDQVy+H00hIYwAYYwAYYwAYYwAYYwAYawag88yux9d0fZqilZ2G+HGsdrTAnw7PLwhf2Bhxxi1brEoybWf8D+yV9atV\\/0qM9\\/52QMoSFMgCFMgCFMgCFMgCFMgCHs0h54VA0TKvcqp3q381y45fVxK\\/cHXmXqPNIL+uepVvXnz1Su284MoSFMgCFMgCFMgCFMgCFMgCHs02qkL1XKQzjrUIBHTRbd7XMukDjVVf32qHm8C+f9DjmudaHhpgQYwgQYwgQYwgQYwgQYwh5H\\/t4d6j8r84dnn+du12Gr7VEH1lRT3XE+8FnpG3wB83s3ZwgNYQIMYQIMYQIMYQIMYQIMYXevkVbtD5xYl3jDz+EP7wZ4yXzXDddz3mqf3g9YQH\\/q+Qx8aJfMT\\/6dITSECTCECTCECTCECTCECTCETemBV9Uzz6yqQ1447rN\\/Mrvm2a2nrfTPy6\\/b3V\\/kOCuxzvM3Rv0iWDVPeLe+ejlDaAgTYAgTYAgTYAgTYAgTYAhTI51z58bid6f6an459fxYF3q9xLzW2fOlz57nhvO0hxx3xm9\\/Q2gIE2AIE2AIE2AIE2AIE2AIUyPNVZnXOtXCecuhqeCvEeBz6vNpz6q8mLFkw\\/Rnrtxf2hAawgQYwgQYwgQYwgQYwgQYwtRI58zudRO1TbqUfs22ff6UACeewu9N3fd11P7GZ125b+2Rz5\\/dl16wL\\/SpQ8+4\\/obQECbAECbAECbAECbAECbAEPZujXTDSvBLW\\/XDA\\/clTgt9qS9P1brQ\\/+9UMAb2iqv2uV0yn3nqhyw89A6\\/IAyhIUyAIUyAIUyAIUyAIUyAIezxAVP\\/4Lb8DwxhAgxhAgxhAgxhAgxhAgxhAgxhAgxhAgxhAgxhAgxh\\/wBikPnFT5Q5qQAAAABJRU5ErkJggg==\"}}";
        let datatemp2 = "{\"code\":\"1\",\"result\":{\"type\":2,\"url\":\"https:\\/\\/cgppublic.blob.core.windows.net\\/rpublic\\/Qr\\/Qr20200114\\/DCCA1FFB8800E75C83FDD5E8F6D80A42.html\"}}";
        let datatemp3 = "{\n" +
            "   \"code\": \"1\",\n" +
            "   \"result\": {\n" +
            "       \"type\": 4,\n" +
            "       \"data\": {\n" +
            "           \"fourth_code\": \"Jxpay\",\n" +
            "           \"bank_name\": \"招商银行\",\n" +
            "           \"card_number\": \"6214832189843312\",\n" +
            "           \"payee_name\": \"李发\",\n" +
            "           \"location\": \"\",\n" +
            "           \"amount\": 100\n" +
            "       }\n" +
            "   }\n" +
            "}";
        // data = JSON.parse(datatemp1);
        switch(parseInt(data.code)){
            case 1:
                switch (data.result.type) {
                    case 0:
                        this.rg.ShowQrCodeFromData(data.result.url);
                        break;
                    case 1:
                        this.rg.ShowQrCodeImg(data.result.url);
                        break;
                    case 4:
                        this.rg.ShowTransferAccountInfo(data.result.data);
                        break;
                    default:
                        this.rg.ShowWebViewFormData(data.result.url);
                        break;
                }
                break;
            default:
                PageManager.MemberPage();
                break;
        }
    }

}
