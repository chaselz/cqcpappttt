
import {IRecharge} from "./IRecharge";
import MobileExtension from "../../Extension/Nactive/MobileExtension";
import {VM} from "../../modelView/ViewModel";
import {DetailType} from "../../Enum/DetailType";
import PageManager from "../../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

export default class TransferRecharge extends IRecharge {

    private path = "RechargeMain/view/content/TransferInfoView/";
    private choosepath = "RechargeMain/view/content/RechargeChooseView/";
    Init() {
        this.setCollectionInfo();
        this.setDepositInfo();
        this.LockBtn(false);
    }

    constructor(vc:any,view:any) {
        super(vc,view);
        this.setCopy(this.path+"Account/BankName");
        this.setCopy(this.path+"Account/Payee");
        this.setCopy(this.path+"Account/Account");
        this.setPayFinishBtn();
    }

    onEnable() {
    }

    async setData(data: any,type:any) {
        VM.setValue("core.defaultdetail",DetailType.DW);
        TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.transationapplying"));
        PageManager.DetailsPage();
    }

    private setPayFinishBtn(){
        UI_manager.add_button_listen(this.view["RechargeMain/view/content/TransferNextBtn"],this,function () {
            var bankid = VM.getValue("recharge.now_paywaylist")[VM.getValue("recharge.paybankindex")].id;
            var now = new Date().UTCDate();
            var method = this.view[this.path+"Info/TransferWay/content"].getComponent(cc.Label).string;
            var point = this.view[this.path+"Info/"+(this.view[this.path+"Info/Bank"].active ? "Bank" : "Branch")+"/content"].getComponent(cc.Label).string;
            this.LockBtn(true);
            ApiManager.Deposit.get.DepositProvider(bankid,now,VM.getValue("recharge.transfername"),method,point,parseFloat(VM.getValue("recharge.money")));
        });

    }

    private LockBtn(lick:boolean){
        this.view["RechargeMain/view/content/TransferNextBtn"].getComponent(cc.Button).interactable = !lick;
        this.view["RechargeMain/view/content/TransferNextBtn/load"].active = lick;
    }

    private setCopy(path){
        UI_manager.add_button_listen(this.view[path+"/copy"],this,function () {
            if(MobileExtension.Copy(this.view[path+"/content"].getComponent(cc.Label).string)){
                TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.copysuccess"));
            }
        });
    }

    private setContent(path:string,str:string){
        this.view[path+"/content"].getComponent(cc.Label).string = str;
    }

    private setCollectionInfo(){
        var payway = VM.getValue("recharge.now_paywaylist")[VM.getValue("recharge.paybankindex")];
        this.setContent(this.path+"Account/BankName","{0}-{1}".format(payway.bank_name,payway.account_point));
        this.setContent(this.path+"Account/Payee",payway.name);
        this.setContent(this.path+"Account/Account",payway.account);
        this.view[this.path+"Account/QR"].active = payway.href != null;
        if(payway.href)
            UI_manager.setSpriteFormUrl(this.view[this.path+"Account/QR/Img"],payway.href);
    }

    private setDepositInfo(){
        let arr = ["onlinetransfer","bankcounters","atmcashdeposit","atmcashmachine"];
        let depositway = VM.getValue("recharge.depositway");
        this.setContent(this.path+"Info/Money",VM.getValue("recharge.money").formatNumber(2));
        this.setContent(this.path+"Info/TransferWay",VM.getValue("lang."+arr[depositway]));
        //depositway 0 為 存款方式的 網銀轉帳
        this.view[this.path + "Info/Bank"].active = depositway == 0;
        this.view[this.path + "Info/Branch"].active = depositway != 0;
        if(depositway == 0){
            var banktarget = VM.getValue("recharge.bankindex");
            var bank = VM.getValue("recharge.banklist")[banktarget];
            //bank_id 106 為其他銀行
            if(bank.bank_id == 106)
                this.setContent(this.path + "Info/Bank", "{0}-{1}".format(bank.bank_name,VM.getValue("recharge.depositbranch")));
            else
                this.setContent(this.path + "Info/Bank", bank.bank_name);
        }else {
            this.setContent(this.path + "Info/Branch", VM.getValue("recharge.depositbranch"));
        }
    }
}
