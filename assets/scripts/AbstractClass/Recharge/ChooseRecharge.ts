
import {IRecharge} from "./IRecharge";
import {VM} from "../../modelView/ViewModel";
import DropDownOptionData from "../../UI/DropDown/DropDownOptionData";
import DropDown from "../../UI/DropDown/DropDown";
import Awaiter from "../../Extension/AwaitExtension";
import array = cc.js.array;
import {RechargeType} from "../../Enum/RechargeType";
import {PayType} from "../../Enum/PayType";
import {IPay} from "../Pay/IPay";
import wechatPay from "../Pay/wechatPay";
import commonPay from "../Pay/commonPay";
import VMLabel from "../../modelView/VMLabel";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var ApiType = require("ApiType");
var UI_manager = require("UI_manager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");
var CtrlAssistant = require("CtrlAssistant");

export default class ChooseRecharge extends IRecharge {

    public path = "RechargeMain/view/content/RechargeChooseView/";
    private paymode : IPay;
    constructor(ctrl:any,view:cc.Node[]) {
        super(ctrl,view);
        this.drawGp();
        this.setTransFerDropDownData();
        this.setDropboxCB();
        this.setEditBox();
        this.setAddMoneyBtns();
        this.InitNextBtn();
    }

    Init() {
        this.openLoadView(true);
        this.InitIPay();
        this.InitName();
        this.InitValue();
        ApiManager.Deposit.get.DepositModeMenuAll(VM.getValue("recharge.paytype"));
        this.InitUI();
    }

    onEnable() {
    }

    async setData(data: any,type:any){
        switch (type) {
            case ApiType.depositModeAllMenuType:
                // this.PresetMenu(data[0]);
                this.updataModeMenu(data);
                break;
            case ApiType.depositModeBankMenuAllType:
                this.AnalizeMoneyRangeDropDown();
                break;
            case ApiType.bankListType:
                this.updataBanklist(data);
                break;
            case ApiType.depositFourthType:
                this.DepositHandle(data);
                break;
        }
    }

    private DepositHandle(data:any){
        this.paymode.DepositHandle(data);
    }

    private async awaitCloseLoadViewAsync(){
        this.openLoadView(true);
        await Awaiter.until(_=>
            VM.getValue("recharge.paybankindex") > -1);

        this.openLoadView(false);
    }

    private InitIPay(){
        cc.log("InitIPay",VM.getValue("recharge.paytype"));
        switch (VM.getValue("recharge.paytype")){
            case PayType.Common:
                this.paymode = new commonPay(this,this.view,this.vc);
                break;
            default:
                this.paymode = new wechatPay(this,this.view,this.vc);
                break;
        }
    }

    private InitValue(){
        VM.setValue("recharge.payway","");
        VM.setValue("recharge.banklist",[]);
        VM.setValue("recharge.depositway",-1);
        VM.setValue("recharge.paybankindex",-1);
        VM.setValue("recharge.bankindex",-1);
        VM.setValue("recharge.depositbranch","");
        VM.setValue("recharge.money","");
        VM.setValue("recharge.depositway",0);
        VM.setValue("recharge.iserr",false);
    }

    private InitUI(){
        this.view[this.path+"DepositBank/DropDown"].getComponent(DropDown).hide();
        this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).hide();
        this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).hide();
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).hide();
        this.view[this.path+"MoneyInput/EditBox"].getComponent(cc.EditBox).string = "";
        this.view[this.path+"DepositBranch/EditBox"].getComponent(cc.EditBox).string = "";
        this.view[this.path+"DepositOther/EditBox"].getComponent(cc.EditBox).string = "";
        this.setOtherUINormal();
        this.setMoneyUINormal();
        this.setBranchUINormal();
        this.setNameUINormal();
        this.setNodeActive(this.path+"DepositBranch/branchErr1",false);
        this.setNodeActive(this.path+"DepositBranch/branchErr2",false);
        this.setNodeActive(this.path+"DepositBranch/branchErr3",false);
        this.setNodeActive(this.path+"DepositOther/branchErr1",false);
        this.setNodeActive(this.path+"DepositOther/branchErr2",false);
        this.setNodeActive(this.path+"DepositOther/branchErr3",false);
        this.LockRechargeBtn(false);
    }

    private InitNextBtn(){
        UI_manager.add_button_listen(this.view["RechargeMain/view/content/RechargeNextBtn"],this,function () {
            this.paymode.nextBtnEvent();
        });
    }
    public showWarnAlert(err:any){
        let str = "";
        if(err.length > 1)
            str = VM.getValue("lang.inputErr");
        else {
            if(err[0]=="limitupperlower"){
                str = VM.getValue("lang.limitupperlower").format(VM.getValue("recharge.min"),VM.getValue("recharge.max"));
            }else
                str = VM.getValue("lang." + err[0]);
        }

        this.ShowAlert(TitleAlertType.Warn,str);
    }
    /***
     * 設定＋錢的Btns
     */
    private setAddMoneyBtns(){
        this.view[this.path+"MoneyInput/moneylist/content"].children.forEach(node=>{
            UI_manager.add_button_listen(node,this,function () {
                this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).hide();
                this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).hide();
                var editBox = this.view[this.path+"MoneyInput/EditBox"].getComponent(cc.EditBox);
                var money = this.getMoney() +parseFloat(node.name);
                let min = parseFloat(VM.getValue("recharge.min").replaceAll("\,",""));
                //先加錢（這樣等等再判斷範圍時才會儲存sel）
                editBox.string = money;
                if(this.checkCanAddMoney(money) || money < min) {//檢查是否有超過最大值 || 輸入金額低於最小金額（這樣才不會跳出警告）

                }else {
                    editBox.string = money - parseFloat(node.name);
                    this.ShowAlert(TitleAlertType.Warn, VM.getValue("lang.limitupperlower").format(VM.getValue("recharge.min"), VM.getValue("recharge.max")));
                }
            });
        });
    }

    private setEditBox(){
        UI_manager.add_editorbox_text_changed_listen(this.view[this.path+"MoneyInput/EditBox"],this,function (editbox) {
            editbox.string =  parseFloat(editbox.string).toString();
            if(editbox.string == "NaN")
                editbox.string = 0;

            this.AnalizeMoneyRangeDropDown();

        });

        UI_manager.add_editorbox_text_changed_listen(this.view[this.path+"DepositOther/EditBox"],this,function (editbox) {
            if(VM.getValue("recharge.iserr")){
                var otherbankname = editbox.string;

                this.setNodeActive(this.path+"DepositOther/branchErr2",!otherbankname.isName() && otherbankname.length > 0);
                this.setNodeActive(this.path+"DepositOther/branchErr1",otherbankname.length < 2 && otherbankname.length > 0);
                this.setNodeActive(this.path+"DepositOther/branchErr3",otherbankname.length > 50);
                cc.log(!otherbankname.isName(),otherbankname.length < 2 ,otherbankname.length > 50);
                if(!otherbankname.isName() || otherbankname.length < 2 || otherbankname.length > 50)
                    this.setOtherUIRed();
                else
                    this.setOtherUINormal();
            }
        });

        UI_manager.add_editorbox_text_changed_listen(this.view[this.path+"DepositBranch/EditBox"],this,function (editbox) {
            if(VM.getValue("recharge.iserr")){
                var brabch = editbox.string;

                this.setNodeActive(this.path+"DepositBranch/branchErr2",!brabch.isName() && brabch.length > 0);
                this.setNodeActive(this.path+"DepositBranch/branchErr1",brabch.length < 2 && brabch.length > 0);
                this.setNodeActive(this.path+"DepositBranch/branchErr3",brabch.length > 50);
                if(!brabch.isName() || brabch.length < 2 || brabch.length > 50)
                    this.setBranchUIRed();
                else
                    this.setBranchUINormal();
            }
        });

        UI_manager.add_editorbox_text_changed_listen(this.view[this.path+"Name/EditBox"],this,function (editbox) {
            if(VM.getValue("recharge.iserr")){
                var brabch = editbox.string;

                this.setNodeActive(this.path+"Name/nameErr2",!brabch.isName() && brabch.length > 0);
                this.setNodeActive(this.path+"Name/nameErr1",brabch.length < 2 && brabch.length > 0);
                if(!brabch.isName() || brabch.length < 2 || brabch.length > 50)
                    this.setNameUIRed();
                else
                    this.setNameUINormal();
            }
        });
    }

    private setDropboxCB(){
        this.view[this.path+"DepositWay/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(sel=>{
            VM.setValue("recharge.depositway",sel);
            this.setDepositBankorBranch();
        });

        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(sel=>{
            let data = VM.getValue("recharge.payJson")[sel];
            this.PresetMenu(data);
        });

        this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(sel=>{
            this.CheckPayBankAndAisleSelect(sel);
        });

        this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(sel=>{
            this.CheckPayBankAndAisleSelect(sel);
        });

        this.view[this.path+"DepositBank/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(sel=>{
            VM.setValue("recharge.bankindex",sel);
            this.checkIsOtherBank(sel);
        });
    }
    private checkIsOtherBank(sel:number){
        //判斷是否為其他銀行選項
        this.setNodeActive(this.path+"DepositOther",VM.getValue("recharge.banklist")[sel].bank_id == 106);
    }
    /***
     * 檢查支付銀行跟通道的dropbox 的選擇
     * @param sel
     * @constructor
     */
    private CheckPayBankAndAisleSelect(sel:number){
        var target = VM.getValue("recharge.now_paywaylist")[sel];
        let max = parseFloat(target.deposit_maximum.replaceAll("\,",""));
        let min = parseFloat(target.deposit_minimum.replaceAll("\,",""));
        let money = this.getMoney();
        if(max>=money && (min<= money || money == 0)) {
            VM.setValue("recharge.paybankindex", sel);
        }else{
            VM.setValue("recharge.paybankindex", -1);
        }
    }

    /***
     * 取的輸入的money
     */
    public getMoney(){
        var editBox = this.view[this.path+"MoneyInput/EditBox"].getComponent(cc.EditBox);
        var money = (parseFloat(editBox.string) ? parseFloat(editBox.string) : 0);
        return money;
    }

    public getUserName(){
        var editBox = this.view[this.path+"Name/EditBox"].getComponent(cc.EditBox);
        return editBox.string;
    }
    /***
     * 取的分行資料
     */
    public getBranch(){
        var editBox = this.view[this.path+"DepositBranch/EditBox"].getComponent(cc.EditBox);
        return editBox.string;
    }

    /***
     * 取的其他銀行資料
     */
    public getOtherBank(){
        var editBox = this.view[this.path+"DepositOther/EditBox"].getComponent(cc.EditBox);
        return editBox.string;
    }
    public setNodeActive(path,_active){
        this.view[path].active = _active;
    }
    /**
     * 設定存款方式的DropDown的值
     */
    private setTransFerDropDownData() {
        let temp = [];
        let arr = ["onlinetransfer","bankcounters","atmcashdeposit","atmcashmachine"];
        arr.forEach(x=>{
            let datatemp = new DropDownOptionData();
            datatemp.setValue(VM.getValue("lang."+x));
            temp.push(datatemp);
        });
        this.view[this.path+"DepositWay/DropDown"].getComponent(DropDown).addOptionDatas(temp,0);
    };
    private drawGp(){
        let moneylist = this.view[this.path+"MoneyInput/moneylist/content"].getComponentsInChildren(cc.Graphics);
        moneylist.forEach(gp=>{
            gp.roundRect(0-gp.node.width/2,0-gp.node.height/2,gp.node.width,gp.node.height,28);
            gp.stroke();
        });
    }

    /***
     * 檢查新增後的金額是否符合最大小值
     * @param money
     */
    private checkCanAddMoney(money:number){
        var target = VM.getValue("recharge.paywaylist").filter(temp=>{
            let max = parseFloat(temp.deposit_maximum.replaceAll("\,",""));
            let min = parseFloat(temp.deposit_minimum.replaceAll("\,",""));
           return  max>=money && min<= money
        });
        /***
         * 刷新對應的DropDown
         */
        if(target && target.length > 0){
            this.setMoneyUINormal();
            VM.setValue("recharge.max",target[0].deposit_maximum);
            VM.setValue("recharge.min",target[0].deposit_minimum);

            if(VM.getValue("recharge.payway") == 0) {  //轉帳
                this.updataModeBankMenu(target);
            }else{
                this.updataModeBranchMenu(target);
            }
        }else{
            VM.setValue("recharge.paybankindex", -1);
            if(VM.getValue("recharge.payway") == 0) {  //轉帳
                this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).init(VM.getValue("lang.reinputrechargemoney"),VM.getValue("lang.noaccordrechagrgelimit"),false);
                this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).interactable = false;
            }else{
                this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).init(VM.getValue("lang.reinputrechargemoney"),VM.getValue("lang.noaccordrechagrgelimit"),false);
                this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).interactable = false;
            }
            if(VM.getValue("recharge.iserr"))
                this.setMoneyUIRed();
        }

        return target != null && target.length > 0;
    }

    /***
     * 分析充值金額改變要對應相對的dropdown清單
     * @constructor
     */
    private AnalizeMoneyRangeDropDown(){
        let money = this.getMoney();
        var target = VM.getValue("recharge.paywaylist").filter(temp=>{
            let max = parseFloat(temp.deposit_maximum.replaceAll("\,",""));
            let min = parseFloat(temp.deposit_minimum.replaceAll("\,",""));
            return  max>=money && (min<= money || money == 0)
        });

        if(/[0-9].[0-9]\d{2}/.test(money.toString())){
            if(VM.getValue("recharge.iserr")) {
                this.setMoneyUIRed();
                return;
            }
        }

        /***
         * 刷新對應的DropDown
         */
        if(target && target.length > 0){
            VM.setValue("recharge.max",target[0].deposit_maximum);
            VM.setValue("recharge.min",target[0].deposit_minimum);

            if(VM.getValue("recharge.payway") == 0) {  //轉帳
                this.updataModeBankMenu(target);
            }else{
                this.updataModeBranchMenu(target);
            }
            this.setMoneyUINormal();
        }else{
            VM.setValue("recharge.paybankindex", -1);
            if(VM.getValue("recharge.payway") == 0) {  //轉帳
                this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).init(VM.getValue("lang.reinputrechargemoney"),VM.getValue("lang.noaccordrechagrgelimit"),false);
                this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).interactable = false;
            }else{
                this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).init(VM.getValue("lang.reinputrechargemoney"),VM.getValue("lang.noaccordrechagrgelimit"),false);
                this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).interactable = false;
            }
            if(VM.getValue("recharge.iserr"))
                this.setMoneyUIRed();
        }
    }

    /***
     * 設定輸入密碼欄位錯誤
     */
    public setMoneyUIRed(){
        this.view[this.path+"MoneyInput/top/title"].color = VM.getValue("color.red");
        this.view[this.path+"MoneyInput/top/limit"].color = VM.getValue("color.red");
        this.view[this.path+"MoneyInput/EditBox/line"].color = VM.getValue("color.red");
        this.view[this.path+"MoneyInput/EditBox/TEXT_LABEL"].color = VM.getValue("color.red");
    }

    /***
     * 設定輸入密碼欄位正常
     */
    public setMoneyUINormal(){
        this.view[this.path+"MoneyInput/top/title"].color = VM.getValue("color.graylabel3");
        this.view[this.path+"MoneyInput/top/limit"].color = VM.getValue("color.graylabel3");
        this.view[this.path+"MoneyInput/EditBox/line"].color = VM.getValue("color.linecolor");
        this.view[this.path+"MoneyInput/EditBox/TEXT_LABEL"].color = VM.getValue("color.graylabel3");
    }

    public setBranchUIRed(){
        this.setEditorRed("DepositBranch");
    }

    public setBranchUINormal(){
        this.setEditorNormal("DepositBranch");
    }

    public setNameUIRed(){
        this.setEditorRed("Name");
    }

    public setNameUINormal(){
        this.setEditorNormal("Name");
    }

    public setOtherUIRed(){
        this.setEditorRed("DepositOther");
    }

    public setOtherUINormal(){
        this.setEditorNormal("DepositOther");
    }

    private setEditorRed(ui:string){
        this.view[this.path+ui+"/top/title"].color = VM.getValue("color.red");
        this.view[this.path+ui+"/EditBox/TEXT_LABEL"].color = VM.getValue("color.red");
        this.view[this.path+ui+"/line"].color = VM.getValue("color.red");
    }

    private setEditorNormal(ui:string){
        this.view[this.path+ui+"/top/title"].color = VM.getValue("color.graylabel3");
        this.view[this.path+ui+"/EditBox/TEXT_LABEL"].color = VM.getValue("color.graylabel3");
        this.view[this.path+ui+"/line"].color = VM.getValue("color.linecolor");
    }


    /***
     * 轉帳模式
     * @constructor
     */
    public TransferMode(){
        this.view[this.path+"DepositWay"].active = true;
        this.view[this.path+"aisleWay"].active = false;
        this.view[this.path+"PayBank"].active = true;
        this.view[this.path+"Name"].active = true;
        this.setDepositBankorBranch();
    }

    /***
     *  三方支付模式
     * @constructor
     */
    public ThirdPayMode(){
        this.view[this.path+"aisleWay"].active = true;
        this.view[this.path+"Name"].active = false;
        this.view[this.path+"PayBank"].active = false;
        this.view[this.path+"DepositWay"].active = false;
        this.view[this.path+"DepositBank"].active = false;
        this.view[this.path+"DepositBranch"].active = false;
        this.view[this.path+"DepositOther"].active = false;
    }

    /***
     * 設定是否網銀轉帳的判斷
     */
    private setDepositBankorBranch(){
        let state = VM.getValue("recharge.depositway") == 0;
        this.setNodeActive(this.path+"DepositBank", state);
        this.setNodeActive(this.path+"DepositBranch", !state);
        var banktemp = VM.getValue("recharge.banklist")[VM.getValue("recharge.bankindex")];
        //判斷是否為其他銀行
        this.setNodeActive(this.path+"DepositOther",state
            && (banktemp && banktemp.bank_id == 106));
    }

    /***
     * 預設Pay Menu
     * @param data
     * @constructor
     */
    private PresetMenu(data:any){
        VM.setValue("recharge.payway",data.type);
        VM.setValue("recharge.max",data.deposit_maximum);
        VM.setValue("recharge.min",data.deposit_minimum);
        VM.setValue("core.title",data.name);
        VM.setValue("recharge.paybankindex",-1);
        this.paymode.presetMenuMode(data);
    }

    public LockTransferBtn(lock:boolean){
        this.view["RechargeMain/view/content/TransferNextBtn"].getComponent(cc.Button).interactable = !lock;
        this.view["RechargeMain/view/content/TransferNextBtn/load"].active = lock;

    }

    public LockRechargeBtn(lock:boolean){
        this.view["RechargeMain/view/content/RechargeNextBtn"].getComponent(cc.Button).interactable = !lock;
        this.view["RechargeMain/view/content/RechargeNextBtn/load"].active = lock;
    }

    private updataModeMenu(data:any){
        VM.setValue("recharge.payJson",data);
        let temparr = [];
        data.forEach(temp=>{
            let datatemp = new DropDownOptionData();
            var optionstr = VM.getValue("lang.limitupperlower").format(temp.deposit_minimum,temp.deposit_maximum);
            datatemp.setValue(temp.name,optionstr);
            temparr.push(datatemp);
        });
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).clearOptionDatas();
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).addOptionDatas(temparr,0);
    }

    private updataModeBankMenu(data:any){
        var index = 0;
        if(VM.getValue("recharge.paybankindex")> -1) {
            let oldaccount = VM.getValue("recharge.now_paywaylist")[VM.getValue("recharge.paybankindex")].account;
            data.find(function(item, i){
                if(item.account === oldaccount){
                    index = i;
                    return i;
                }
            });
        }
        VM.setValue("recharge.now_paywaylist",data);
        let temparr = [];
        data.forEach(temp=>{
            let datatemp = new DropDownOptionData();
            var title = "{0}-{1}".format(temp.bank_name,temp.account_point);
            datatemp.setValue(title);
            temparr.push(datatemp);
        });
        this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).interactable = true;
        this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).clearOptionDatas();
        this.view[this.path+"PayBank/DropDown"].getComponent(DropDown).addOptionDatas(temparr,index);
    }

    private updataModeBranchMenu(data:any){
        var index = 0;
        if(VM.getValue("recharge.paybankindex")> -1) {
            let oldbankid = VM.getValue("recharge.now_paywaylist")[VM.getValue("recharge.paybankindex")].id;
            data.find(function(item, i){
                if(item.id === oldbankid){
                    index = i;
                    return i;
                }
            });
        }
        VM.setValue("recharge.now_paywaylist",data);
        let temparr = [];
        data.forEach(temp=>{
            let datatemp = new DropDownOptionData();
            var optionstr = VM.getValue("lang.limitupperlower").format(temp.deposit_minimum,temp.deposit_maximum);
            datatemp.setValue(temp.front_name,optionstr);
            temparr.push(datatemp);
        });
        this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).interactable = true;
        this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).clearOptionDatas();
        this.view[this.path+"aisleWay/DropDown"].getComponent(DropDown).addOptionDatas(temparr,index);
    }

    private InitName(){
        this.view[this.path+"Name/EditBox"].getComponent(cc.EditBox).string = VM.getValue("member.name");
    }

    /***
     * 呼叫取得通道跟支付銀行的api
     */
    public callBankMenuApi(){
        this.awaitCloseLoadViewAsync();
        ApiManager.Deposit.get.DepositModeBankMenuAll(VM.getValue("recharge.paytype"),VM.getValue("recharge.payway"));
    }

    public ShowTransferAccountInfo(data:any){
        VM.setValue("recharge.jxpay",data);
        this.vc.setMode(RechargeType.JXPay);
    }

    public ShowQrCodeFromData(data:any){
        VM.setValue("recharge.qrdata",data);
        VM.setValue("recharge.qr","");
        this.vc.setMode(RechargeType.QR);
    }

    public ShowQrCodeImg(data:any){
        VM.setValue("recharge.qr",data);
        VM.setValue("recharge.qrdata","");
        this.vc.setMode(RechargeType.QR);
    }

    public ShowWebViewFormData(data:any){
        this.vc.setMode(RechargeType.FourthPay);
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.AllScreenWebUrl(data);
    }

    private updataBanklist(data:any){
        VM.setValue("recharge.banklist",data);
        let temparr = [];
        data.forEach(temp=>{
            let datatemp = new DropDownOptionData();
            datatemp.setValue(temp.bank_name);
            temparr.push(datatemp);
        });
        this.view[this.path+"DepositBank/DropDown"].getComponent(DropDown).clearOptionDatas();
        this.view[this.path+"DepositBank/DropDown"].getComponent(DropDown).addOptionDatas(temparr,0);
    }

    public ShowAlert(type,str){
        TitleAlertView.main.show(type,str);
    }

    public openLoadView(open:boolean){
        UIAssistant.main.SetPanelVisible("Load",open);
    }

    public PayWayInteractable(_interactable:boolean){
        cc.log("PayWayInteractable =",_interactable);
        this.view[this.path+"PayWay/top/Title"].color = VM.getValue("color."+(_interactable?"graylabel3":"graylabel"));
        this.view[this.path+"PayWay/top/Title"].getComponent(VMLabel).onValueInit();
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).interactable = _interactable;
    }

    public PayWayIsCommomMode(ismode:boolean){
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).arrowCaption.node.active = !ismode;
        this.view[this.path+"PayWay/DropDown"].getComponent(DropDown).labelOption.node.active = !ismode;
    }

    public showMoneyLimitLabel(show:boolean){
        this.view[this.path+"MoneyInput/top/limit"].active = show;
    }
}
