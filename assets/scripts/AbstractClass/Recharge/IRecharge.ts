
const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");

export abstract class IRecharge  {

    protected vc : any = null;
    protected view : cc.Node[] = null;
    constructor(ctrl:any,_view:cc.Node[]) {
        this.vc = ctrl;
        this.view = _view;
    }
    abstract onEnable();
    abstract Init();
    abstract async setData(data:any,type:any);
    public setLoad(is_show:boolean){
        UIAssistant.main.SetPanelVisible("Load",is_show);
    };

}
