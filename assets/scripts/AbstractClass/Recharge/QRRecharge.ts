import {IRecharge} from "./IRecharge";
import {VM} from "../../modelView/ViewModel";
import VMLabel from "../../modelView/VMLabel";
import {PageViewType} from "../../Enum/PageViewType";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

export default class QRRecharge extends IRecharge {

    public path = "RechargeMain/view/content/QRView/";

    Init() {
        this.view[this.path+"QR/Img"].getComponent(cc.Sprite).sprite = null;
        if(VM.getValue("recharge.qr"))
            this.updataQRUrl(VM.getValue("recharge.qr"));
        else
            this.updateQRData(VM.getValue("recharge.qrdata"));
    }

    onEnable() {
    }

    async setData(data: any, type: any) {

    }

    private updataQRUrl(url : string){
        UI_manager.setSpriteFormUrl(this.view[this.path+"QR/Img"],url);
    }

    private updateQRData(str: string){
        let qrcode = this.view[this.path+"QR/Img"].getComponent(cc.Sprite);
        // if(cc.sys.isNative)
        //     this.NativeDevice_base64ToTexture(str,qrcode);
        // else
            this.webDevice_base64ToTexture(qrcode,str);
    }

    private webDevice_base64ToTexture(qrcode:cc.Sprite,str:string){
        let imgData = str;
        let image = new Image();

        image.onload = function() {
            let texture = new cc.Texture2D();
            texture.initWithElement(image);
            texture.handleLoadedTexture();
            qrcode.spriteFrame = new cc.SpriteFrame(texture);
        };
        image.src = imgData;
    }

    //sprite is the cc.Sprite component you wish to display the image on
    private NativeDevice_base64ToTexture(base64:string, sprite:cc.Sprite) {
        const buffer = new Buffer(base64.slice(22), 'base64');
        const len = buffer.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = buffer[i];
        }

        const extName = 'png';
        const randomFileName = `base64_img_${new Date().getTime()}.${extName}`;
        const dir = `${jsb.fileUtils.getWritablePath()}${randomFileName}`;
        if (jsb.fileUtils.writeDataToFile(bytes, dir)) {
            cc.loader.load(dir, (err, texture) => {
                if (!err && texture) {
                    sprite.spriteFrame = new cc.SpriteFrame(texture);
                    cc.loader.release(dir);
                }else{
                    console.log("err =",err);
                }
                jsb.fileUtils.removeFile(dir);
            });
        }
    }

}
