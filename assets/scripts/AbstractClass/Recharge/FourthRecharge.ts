
import {IRecharge} from "./IRecharge";
import {VM} from "../../modelView/ViewModel";
import {PageViewType} from "../../Enum/PageViewType";

const {ccclass, property} = cc._decorator;

export default class FourthRecharge extends IRecharge {

    public path = "RechargeMain/view/content/FourthPayView/Account/";
    Init() {
        this.setContent(this.path+"Payway",VM.getValue("core.title"));
        this.setContent(this.path+"Amount",VM.getValue("recharge.money"));
    }

    onEnable() {
    }

    async setData(data: any, type: any) {

    }

    private setContent(path:string,str:string){
        this.view[path+"/content"].getComponent(cc.Label).string = str;
    }

}
