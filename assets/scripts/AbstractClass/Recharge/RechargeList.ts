
import {IRecharge} from "./IRecharge";
import RechargeBtn from "../../Item/RechargeBtn";
import {RechargeType} from "../../Enum/RechargeType";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var UI_manager = require("UI_manager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

export default class RechargeList extends IRecharge {

    Init() {
        VM.setValue("core.title",VM.getValue("lang.recharge"));
        this.closeChild();
        this.setLoad(true);
        ApiManager.Deposit.get.DepositMenu();
    }

    constructor(vc:any,view:any) {
        super(vc,view);
    }

    onEnable() {
    }

    async setData(data: any,type:any) {
        this.setShowRechargeMenuList(data);
    }

    private closeChild(){
        this.view["RechargePaylist/view/content"].children.forEach(node=>node.active = false);
    }

    private async createObj(value:any){
        let temp = await UI_manager.create_item_Sync(this.view["RechargePaylist/view/content"],"RechargeBtn");
        temp.getComponent(RechargeBtn).setValue(value,this.toChooseView.bind(this));
    };

    private async setShowRechargeMenuList(data:any){
        let contens = this.view["RechargePaylist/view/content"].getComponentsInChildren(RechargeBtn);
        let difference = contens.filter((e)=>{
            return data.find(x=>x.mode == e.Mode) == undefined;
        });
        cc.log("difference =",difference);
        await data.asyncForEach(async (x) =>{
            let temp = contens.find(y=>y.mode == x.mode);
            if(temp == null || temp == undefined)
                await this.createObj(x);
            else {
                temp.setValue(x);
                temp.node.active = true;
            }
        });

        difference.forEach(item=>item.Hide());
        this.setLoad(false);
    };

    private toChooseView(){
        this.vc.setMode(RechargeType.Choose);
    }
}
