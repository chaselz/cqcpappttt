

import {IRecharge} from "./IRecharge";
import VMLabel from "../../modelView/VMLabel";
import {VM} from "../../modelView/ViewModel";
import {PageViewType} from "../../Enum/PageViewType";
import PageManager from "../../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");

export default class JXPayRecharge extends IRecharge {

    public path = "RechargeMain/view/content/JXPayView/Account/";
    private level : number = 0;

    constructor(ctrl:any,_view:cc.Node[]) {
        super(ctrl,_view);
        this.InitBtn();
    }

    Init() {
        this.level = 0;
        this.view[this.path+"Alert"].active = false;
        this.view["RechargeMain/view/content/JXPayNextBtn/Background/Label"].getComponent(VMLabel).setWatchPath("lang.submitapply");
        this.setInfo();
    }

    onEnable() {
    }

    async setData(data: any, type: any) {

    }

    private InitBtn(){
        UI_manager.add_button_listen(this.view["RechargeMain/view/content/JXPayNextBtn"],this,function () {
            if(this.level ==0){
                this.view[this.path+"Alert"].active = true;
                this.view["RechargeMain/view/content/JXPayNextBtn/Background/Label"].getComponent(VMLabel).setWatchPath("lang.transferfinish");
                this.level++;
            }else{
                TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.transationapplying"));
                PageManager.MemberPage();
            }
        });
    }

    private setInfo(){
        let data = VM.getValue("recharge.jxpay");
        this.setContent(this.path+"BankName",data.bank_name);
        this.setContent(this.path+"Payee",data.payee_name);
        this.setContent(this.path+"Account",data.card_number);
        this.setContent(this.path+"Point",data.location);
        this.setContent(this.path+"Amount",data.amount.formatNumber(2));
    }

    private setContent(path:string,str:string){
        this.view[path+"/content"].getComponent(cc.Label).string = str;
    }
}
