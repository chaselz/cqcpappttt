import {IGame} from "./IGame";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var UIAssistant = require("UIAssistant");
var GameListType = require("GameListType");

export default class LotteryClass extends IGame {

    private loading_userName: boolean = false;
    private loading_userMoney: boolean = false;
    private loading_Item: boolean = false;
    private AllList: any = [];
    private List: any = [];

    Init() {
        this.setLoad(true);
        ApiManager.Game.get.GameVisionCategory("zh-cn");
        this.DefaultNodeActive();
        this.HideItem();
        this.setLoad(true);
        this.loading_userName = true;
        this.loading_userMoney = true;
        this.loading_Item = true;
        if(VM.getValue("member.account") && VM.getValue("member.cash")){
            this.setUserName(GameListType.LotteryClass,VM.getValue("member.account"));
            this.setUserMoney(GameListType.LotteryClass,VM.getValue("member.cash"));
        }else{
            this.sentUserApi();
        }
        this.AllList = VM.getValue("hall.lotteryList");
        let defaultData = this.AllList[0];
        this.List = defaultData.data;
        VM.setValue("hall.className",defaultData.game_type);
    }

    /**
     * 設定彩票分類資料
     * @param type 彩票類型
     * @param data 資料
     */
    private async setClassData(type:GameListType,data:any) {
        if(type != GameListType.LotteryClass)
            return;
        let lotteryClass = [];
        this.AllList.forEach((item,index) =>{
            let lotteryName = data[item.game_type];
            if(lotteryName == null || lotteryName == undefined)
                lotteryName = item.game_type;
            lotteryClass.push({id:index,code:item.game_type,name:lotteryName});
        });
        let result = lotteryClass.length ==0;
        if(!result)
            this.CreatItems("ScrollView/view/content/ClassNode/view/content","GameClassItem",lotteryClass,this.setClassBtn.bind(this));
    }

    /**
     * 從彩票的全部清單裡取得目前所在的清單
     */
    private getLotteryList(){
        let list = [];
        let allList = this.AllList;
        allList.forEach(item =>{
            if(item.game_type == VM.getValue("hall.className")){
                list = item.data;
			}
        });
        return list;
    }

    /**
     * 設定彩票項目
     * @param type 彩票類型
     * @param data 資料
     */
    public setLotteryItems(type:GameListType,data:any) {
        if(type != GameListType.LotteryClass)
            return;
        this.AllList = data;
        this.List = this.getLotteryList();
        let searchName = VM.getValue("hall.search");
        if(searchName.length > 0){
            this.searchItems(GameListType.LotteryClass,searchName);
        }else{
            this.setData(this.List);
        }
    }

    /**
     * 設定彩票項目的資料
     * @param data 資料
     */
    private async setData(data:any) {
        let result = data.length ==0;
        this.setNoDataActive(result);
        if(!result){
            this.CreatLotteryItems("ScrollView/view/content/LotteryNode","LotteryItem",data,this.checkSentApi.bind(this));
            this.setRefresh(GameListType.LotteryClass);
        }
        this.loading_Item = false;
        this.setLoad(false);
    }

    constructor(view:any) {
        super(view);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }

    /**
     * 建立彩票項目或更新已存在的彩票項目
     */
    protected async CreatLotteryItems(path, itemname, data, func: any = null): Promise<void> {
        let lotteryNodes = this.view[path].getComponentsInChildren(itemname);
        await data.asyncForEach(async (value,index) =>{
            let item = lotteryNodes.find(y=>y.code == value.code);
            if(item == null && item == undefined){
                await this.creatFactory(path,itemname, index, value, func);
            }else{
                if(item.node.active){
                    item.refreshData(value);
                }else{
                    await this.creatFactory(path,itemname, index, value, func);
                }
            }
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }

    /**
     * 設定彩票類別按鈕
     */
    private setClassBtn(){
        if(VM.getValue("hall.loading"))
            return true;
        this.setLoad(true);
        this.setNormalMode();
    }

    /**
     * 設定一般狀態
     */
    private setNormalMode(){
        this.view["ScrollView/view/content/ClassNode/view/content"].children.forEach(node=>{
            node.getComponent("GameClassItem").Init();
        });
    }

    /**
     * 檢查送出的 api
     */
    private checkSentApi(){
        if(VM.getValue("hall.gameId") && VM.getValue("hall.gameCode") && !VM.getValue("hall.loading")){
            this.setLoad(true);
            ApiManager.Game.LoginV2(VM.getValue("hall.gameId"),VM.getValue("hall.gameCode"));
        }
    }

    /**
     * 節點預設狀態
     */
    private DefaultNodeActive(){
        this.view["ScrollView/view/content/ClassNode"].active = true;
        this.view["ScrollView/view/content/NoClassNode"].active = false;
        this.view["ScrollView/view/content/ListNode"].active = false;
        this.view["ScrollView/view/content/LotteryNode"].active = true;
    }

    /**
     * Load 的顯示狀態
     * @param is_show 顯示狀態
     */
    public setLoad(is_show:boolean){
        if(!is_show){
            let loadStatus = this.getLoadStatus();
            if(loadStatus)
                return;
        }
        VM.setValue("hall.loading",is_show);
        UIAssistant.main.SetPanelVisible("Load",is_show);
    }

    /**
     * 取得所有 loading 的狀態
     */
    private getLoadStatus(){
        if(this.loading_userName)
            return true;
        if(this.loading_userMoney)
            return true;
        if(this.loading_Item)
            return true;
        return false;
    }

    /**
     * 設定玩家名稱
     * @param type 彩票類型
     * @param value 玩家名稱
     */
    public setUserName(type:GameListType,value:String){
        if(type != GameListType.LotteryClass)
            return;
        if(this.loading_userName){
            this.loading_userName = false;
            this.setLoad(false);
            this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = value;
            VM.setValue("member.account",value);
        }
    }

    /**
     * 設定玩家錢包金額
     * @param type 彩票類型
     * @param value 錢包金額
     */
    public setUserMoney(type:GameListType,value:String){
        if(type != GameListType.LotteryClass)
            return;
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+value;
        VM.setValue("member.cash",value);
        if(this.loading_userMoney){
            this.loading_userMoney = false;
            this.setLoad(false);
        }
    }

    /**
     * 查無結果的顯示狀態
     */
    setNoDataActive(is_show: boolean) {
        this.view["ScrollView/view/content/SearchNo"].active = is_show;
        this.view["ScrollView/view/content/LotteryNode"].active = !is_show;
    }

    /**
     * 隱藏項目
     */
    private HideItem(){
        this.view["ScrollView/view/content/LotteryNode"].children.forEach(node=>node.active = false);
        this.view["ScrollView/view/content/SearchNo"].active = false;
    }

    /**
     * 取得原始資料
     * @param type 彩票類型
     */
    public getAllData(type:GameListType){
        if(type != GameListType.LotteryClass)
            return;
        this.setData(this.List);
    }

    /**
     * 搜尋資料
     * @param type 彩票類型
     * @param value 查詢的值
     */
	public searchItems(type:GameListType,value){
        if(type != GameListType.LotteryClass)
            return;
        let resArr = this.getSearch(this.List,value);
        let result = resArr.length == 0;
        this.setNoDataActive(result);
		if(!result){
            this.HideItem();
            this.setData(resArr);
        }
    }

    /**
	 * 取得彩票項目的時間格式及是否封盤
     * @param value 封盤時間
	 */
	private getTimeType(value){
        let now = Math.floor(new Date().getTime()/1000);
        let finalize = Math.floor(new Date(value*1000).getTime()/1000);
        let res = [];
		if(now >= finalize){
            res["isFinalize"] = true;
		}else{
            res["isFinalize"] = false;
            let countdown = Math.abs(finalize-now);
			res["h"] = Math.floor(countdown/60/60);
			res["m"] = Math.floor(countdown/60%60);
			res["s"] = Math.floor(countdown%60);
			res["h"] = res["h"].toString().length < 2 ? "0"+res["h"] : res["h"];
			res["m"] = res["m"].toString().length < 2 ? "0"+res["m"] : res["m"];
			res["s"] = res["s"].toString().length < 2 ? "0"+res["s"] : res["s"];
        }
		return res;
    }
	
	/**
     * 刷新全部彩票項目的時間
     * @param type 彩票類型
	 */
	public setRefresh(type:GameListType){
        if(type != GameListType.LotteryClass)
            return;
        let list = this.List;
    	this.view["ScrollView/view/content/LotteryNode"].children.forEach(item => {
            let temp = item.getComponent("Iitem");
            let data = list.find(y=>y.code == item.name);
            if(data != null && data != undefined && temp.getActive()){
                let time = this.getTimeType(data.close_unix);
                if(!time["isFinalize"]){
                    temp.setTimeType(time);
                }else{
                    temp.setFinalize();
                    if(!VM.getValue("hall.loading")){
                        if(temp.isfinalize)
                            return;
                        temp.isfinalize = true;
                        this.setLoad(true);
                        ApiManager.Game.get.GameList(VM.getValue("hall.gameId"));
                    }
                }
            }
        });
	}

}
