import {GameListType} from "../../Type/GameListType";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

export abstract class IGame {

    protected view : any = null;

    constructor(_view:any) {
        this.view = _view;
    }
    abstract Init();
    abstract async setData(type:GameListType,data:any);
    abstract DefaultNodeActive();
    public abstract setLoad(is_show:boolean);
    public abstract setNoDataActive(is_show:boolean);
    public abstract getAllData(type:GameListType);
    public abstract searchItems(type:GameListType,value:string);
    public abstract setUserName(type:GameListType,value:string);
    public abstract setUserMoney(type:GameListType,value:string);
    /***
     * 製造及更新物件
     * @param path 父物件
     * @param itemname 物件名
     * @param data 資料
     * @param func 方法
     * @returns {Promise<void>}
     * @constructor
     */
    protected async CreatItems(path,itemname,data,func = null) {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });

        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    };
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    protected async creatFactory(path,itemname,index,data,onClickFunc,type = null) {
        let item = null;
        if(index >= this.view[path].getComponentsInChildren(itemname).length) {
            item = await UI_manager.create_item_Sync(this.view[path], itemname);
            item.getComponent("Iitem").setValue(data,onClickFunc);
            item.active = true;
        }else {
            item = this.view[path].getComponentsInChildren(itemname)[index];
            item.getComponent("Iitem").setValue(data);
            item.node.active = true;
        }
    };

    /***
     * 取得查詢結果
     * @param list	查詢的清單
     * @param value	要查詢的名字
     */
    protected getSearch(list,value){
		let resArr = [];
		list.forEach(x=>{
			if(x.name.indexOf(value) >= 0){
				resArr.push(x);
			}
        });
        return resArr;
    }
    
    /**
     * 送出取得使用者資料 api
     */
    protected sentUserApi(){
        ApiManager.User.get.UserData();
        ApiManager.User.get.UserBalance();
    }
}
