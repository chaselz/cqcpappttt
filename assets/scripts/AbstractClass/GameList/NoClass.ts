import { IGame } from "./IGame";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var UIAssistant = require("UIAssistant");
var GameListType = require("GameListType");

export default class NoClass extends IGame {

    private loading_userName: boolean = false;
    private loading_userMoney: boolean = false;
    private loading_Item: boolean = false;
    private List: any = [];

    Init() {
        this.setLoad(true);
        this.DefaultNodeActive();
        this.HideItem();
        this.setLoad(true);
        this.loading_userName = true;
        this.loading_userMoney = true;
        this.loading_Item = true;
        if(VM.getValue("member.account") && VM.getValue("member.cash")){
            this.setUserName(GameListType.NoClass,VM.getValue("member.account"));
            this.setUserMoney(GameListType.NoClass,VM.getValue("member.cash"));
        }else{
            this.sentUserApi();
        }
        let listName = VM.getValue("core.tagCode") == 1 ? "hall.lotteryList" : "hall.gameList";
        this.List = VM.getValue(listName);
        this.setData(GameListType.NoClass,this.List);
    }

    /**
     * 設定遊戲項目資料
     * @param type 不分類類型
     * @param data 資料
     */
    async setData(type:GameListType,data:any) {
        if(type != GameListType.NoClass)
            return;
        let result = data.length ==0;
        this.setNoDataActive(result);
        if(!result)
            await this.CreatItems("ScrollView/view/content/NoClassNode","GameItem",data,this.checkSentApi.bind(this));
        this.loading_Item = false;
        this.setLoad(false);
    }

    constructor(view:any) {
        super(view);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }

    /**
     * 檢查送出的 api
     */
    private checkSentApi(){
        if(VM.getValue("hall.gameId") && VM.getValue("hall.gameCode") && !VM.getValue("hall.loading")){
            this.setLoad(true);
            ApiManager.Game.LoginV2(VM.getValue("hall.gameId"),VM.getValue("hall.gameCode"));
        }
    }

    /**
     * 節點預設狀態
     */
    private DefaultNodeActive(){
        this.view["ScrollView/view/content/ClassNode"].active = false;
        this.view["ScrollView/view/content/NoClassNode"].active = true;
        this.view["ScrollView/view/content/ListNode"].active = false;
        this.view["ScrollView/view/content/LotteryNode"].active = false;
    }

    /**
     * Load 的顯示狀態
     * @param is_show 顯示狀態
     */
    public setLoad(is_show:boolean){
        if(!is_show){
            let loadStatus = this.getLoadStatus();
            if(loadStatus)
                return;
        }
        VM.setValue("hall.loading",is_show);
        UIAssistant.main.SetPanelVisible("Load",is_show);
    }

    /**
     * 取得所有 loading 的狀態
     */
    private getLoadStatus(){
        if(this.loading_userName)
            return true;
        if(this.loading_userMoney)
            return true;
        if(this.loading_Item)
            return true;
        return false;
    }

    /**
     * 設定玩家名稱
     * @param type 彩票類型
     * @param value 玩家名稱
     */
    public setUserName(type:GameListType,value:String){
        if(type != GameListType.NoClass)
            return;
        if(this.loading_userName){
            this.loading_userName = false;
            this.setLoad(false);
            this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = value;
            VM.setValue("member.account",value);
        }
    }

    /**
     * 設定玩家錢包金額
     * @param type 彩票類型
     * @param value 錢包金額
     */
    public setUserMoney(type:GameListType,value:String){
        if(type != GameListType.NoClass)
            return;
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+value;
        VM.setValue("member.cash",value);
        if(this.loading_userMoney){
            this.loading_userMoney = false;
            this.setLoad(false);
        }
    }

    /**
     * 查無結果的顯示狀態
     */
    setNoDataActive(is_show: boolean) {
        this.view["ScrollView/view/content/SearchNo"].active = is_show;
        this.view["ScrollView/view/content/NoClassNode"].active = !is_show;
    }

    /**
     * 隱藏項目
     */
    private HideItem(){
        this.view["ScrollView/view/content/NoClassNode"].children.forEach(node=>node.active = false);
        this.view["ScrollView/view/content/SearchNo"].active = false;
    }

    /**
     * 取得原始資料
     * @param type 不分類類型
     */
    public getAllData(type:GameListType){
        if(type != GameListType.NoClass)
            return;
        this.setData(GameListType.NoClass,this.List);
    }

    /**
     * 搜尋資料
     * @param type 不分類類型
     * @param value 查詢的值
     */
	public searchItems(type:GameListType,value){
        if(type != GameListType.NoClass)
            return;
		let resArr = this.getSearch(this.List,value);
        let result = resArr.length == 0;
        this.setNoDataActive(result);
		if(!result){
            this.HideItem();
            this.setData(GameListType.NoClass,resArr);
        }
	}
}
