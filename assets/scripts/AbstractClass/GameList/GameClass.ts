import {IGame} from "./IGame";
import {VM} from "../../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");
var UIAssistant = require("UIAssistant");
var GameListType = require("GameListType");

export default class GameClass extends IGame {

    private loading_userName: boolean = false;
    private loading_userMoney: boolean = false;
    private loading_Item: boolean = false;
    private List: any = [];

    Init() {
        this.DefaultNodeActive();
        this.HideItem();
        this.setLoad(true);
        this.loading_userName = true;
        this.loading_userMoney = true;
        this.loading_Item = true;
        if(VM.getValue("member.account") && VM.getValue("member.cash")){
            this.setUserName(GameListType.GameClass,VM.getValue("member.account"));
            this.setUserMoney(GameListType.GameClass,VM.getValue("member.cash"));
        }else{
            this.sentUserApi();
        }
        let defaultData = VM.getValue("hall.gameClass")[0];
        VM.setValue("hall.className",defaultData.code);
        this.setClassItem(VM.getValue("hall.gameClass"));
        this.setGameItems(GameListType.GameClass,VM.getValue("hall.gameList"));
    }

    /**
     * 設定遊戲類別項目
     * @param data 資料
     */
    private async setClassItem(data:any) {
        let result = data.length ==1;
        this.view["ScrollView/view/content/ClassNode/view/content"].active = !result;
        if(!result)
            this.CreatItems("ScrollView/view/content/ClassNode/view/content","GameClassItem",data,this.setClassBtn.bind(this));
    }

    /**
     * 設定遊戲項目
     * @param type 遊戲類型
     * @param data 資料
     */
    public setGameItems(type:GameListType,data:any){
        if(type != GameListType.GameClass)
            return;
        this.List = data;
        let searchName = VM.getValue("hall.search");
        if(searchName.length > 0)
            this.searchItems(GameListType.GameClass,searchName);
        else
            this.setData(data);
    }

    /**
     * 設定遊戲項目資料
     * @param data 資料
     */
    private async setData(data:any) {
        let result = data.length ==0;
        this.setNoDataActive(result);
        if(!result)
            await this.CreatItems("ScrollView/view/content/ListNode","GameItem",data,this.checkSentApi.bind(this));
        this.loading_Item = false;
        this.setLoad(false);
    }

    constructor(view:any) {
        super(view);
    }

    protected async CreatItems(path, itemname, data, func: any = null): Promise<void> {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(path,itemname, index, value, func);
        });
        for(let i = data.length;i<this.view[path].childrenCount;i++){
            this.view[path].children[i].active = false;
        }
    }

    /**
     * 設定遊戲類別按鈕
     */
    private setClassBtn(){
        if(VM.getValue("hall.loading"))
            return true;
        this.setLoad(true);
        this.setNormalMode();
    }

    /**
     * 設定一般狀態
     */
    private setNormalMode(){
        this.view["ScrollView/view/content/ClassNode/view/content"].children.forEach(node=>{
            node.getComponent("GameClassItem").Init();
        });
    }

    /**
     * 遊戲清單 api
     * @param mode 
     */
    private gamelistApi(mode){
        ApiManager.Game.get.GameList(mode);
        this.setLoad(true);
    }

    /**
     * 檢查送出的 api
     */
    private checkSentApi(){
        if(VM.getValue("hall.gameId") && VM.getValue("hall.gameCode") && !VM.getValue("hall.loading")){
            this.setLoad(true);
            ApiManager.Game.LoginV2(VM.getValue("hall.gameId"),VM.getValue("hall.gameCode"));
        }
    }

    /**
     * 節點預設狀態
     */
    private DefaultNodeActive(){
        this.view["ScrollView/view/content/ClassNode"].active = true;
        this.view["ScrollView/view/content/NoClassNode"].active = false;
        this.view["ScrollView/view/content/ListNode"].active = true;
        this.view["ScrollView/view/content/LotteryNode"].active = false;
    }

    /**
     * Load 的顯示狀態
     * @param is_show 顯示狀態
     */
    public setLoad(is_show:boolean){
        if(!is_show){
            let loadStatus = this.getLoadStatus();
            if(loadStatus)
                return;
        }
        VM.setValue("hall.loading",is_show);
        UIAssistant.main.SetPanelVisible("Load",is_show);
    }

    /**
     * 取得所有 loading 的狀態
     */
    private getLoadStatus(){
        if(this.loading_userName)
            return true;
        if(this.loading_userMoney)
            return true;
        if(this.loading_Item)
            return true;
        return false;
    }

    /**
     * 設定玩家名稱
     * @param type 彩票類型
     * @param value 玩家名稱
     */
    public setUserName(type:GameListType,value:String){
        if(type != GameListType.GameClass)
            return;
        if(this.loading_userName){
            this.loading_userName = false;
            this.setLoad(false);
            this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = value;
            VM.setValue("member.account",value);
        }
    }

    /**
     * 設定玩家錢包金額
     * @param type 彩票類型
     * @param value 錢包金額
     */
    public setUserMoney(type:GameListType,value:String){
        if(type != GameListType.GameClass)
            return;
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+value;
        VM.setValue("member.cash",value);
        if(this.loading_userMoney){
            this.loading_userMoney = false;
            this.setLoad(false);
        }
    }

    /**
     * 查無結果的顯示狀態
     */
    setNoDataActive(is_show: boolean) {
        this.view["ScrollView/view/content/SearchNo"].active = is_show;
        this.view["ScrollView/view/content/ListNode"].active = !is_show;
    }

    /**
     * 隱藏項目
     */
    private HideItem(){
        this.view["ScrollView/view/content/ListNode"].children.forEach(node=>node.active = false);
        this.view["ScrollView/view/content/SearchNo"].active = false;
    }

    /**
     * 取得原始資料
     * @param type 不分類類型
     */
    public getAllData(type:GameListType){
        if(type != GameListType.GameClass)
            return;
        this.setData(this.List);
    }

    /**
     * 搜尋資料
     * @param type 不分類類型
     * @param value 查詢的值
     */
	public searchItems(type:GameListType,value){
        if(type != GameListType.GameClass)
            return;
		let resArr = this.getSearch(this.List,value);
        let result = resArr.length == 0;
        this.setNoDataActive(result);
		if(!result){
            this.HideItem();
            this.setData(resArr);
        }
	}
}
