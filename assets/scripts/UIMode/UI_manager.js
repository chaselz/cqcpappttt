import AppLog from "../Extension/AppLog";

var UI_manager = {

    setEditBoxKeyBoard(editBox_node,keyboard){
        let editbox = editBox_node.getComponent(cc.EditBox);
        if (!editbox)
            return;

        editbox.inputMode = keyboard;
    },

    setLabelNodeVlaue(label_node,text){
        let checkArray = [
            'cc.Label',
            'cc.RichText',
            'cc.EditBox',
        ];

        for (let i = 0; i < checkArray.length; i++) {
            const e = checkArray[i];
            let comp = label_node.getComponent(e);
            if (comp) {
                comp.string = text;
                return true;
            }

        }

        cc.error('没有挂载任何label、RichLavel、EditBox组件');
        return false;

    },

    setEditBoxNodeValue(editbox_node,text){
        let editbox = editbox_node.getComponent(cc.EditBox);
        this.setEditBoxValue(editbox,text);
    },

    setEditBoxValue(editbox,text){
        if(!editbox)
            return;

        editbox.string = text;
    },

    setSpriteFormUrl(view_node, url , color= null, localimg=null) {
        let sprite = view_node.getComponent(cc.Sprite);
        if (!sprite)
            return;

        if(url == null || url.length ==0) {
            if(localimg)
                this.setSprite(view_node,localimg,color);
            else
                return;
        }

        cc.loader.load(url, function (err, texture) {
            if(err) {
                if(localimg){
                    UI_manager.setSprite(view_node,localimg,color);
                }else
                    return;
            }

            // cc.warn("url =",url);
            let sp  = new cc.SpriteFrame(texture);
            sprite.spriteFrame = sp;
            if(color)
                sprite.node.color = color;
        });
    },

    setSprite(view_node, image_name, color = null ) {
        let sprite = view_node.getComponent(cc.Sprite);
        if (!sprite)
            return;


        cc.loader.loadRes("Img/"+image_name,cc.SpriteFrame,function (err, spriteFrame) {
            if(err == null || err == undefined) {
                sprite.spriteFrame = spriteFrame;
                view_node.active = true;
                if (color)
                    sprite.node.color = color;
            }
            else
                console.warn(err);
        });
    },

    add_button_listen(view_node, caller, func) {
        let button = view_node.getComponent(cc.Button);
        if (!button)
            return;

        view_node.on("click", func, caller);
    },

    clear_button_listen(view_node) {
        let button = view_node.getComponent(cc.Button);
        if (!button) {
            return;
        }

        view_node.off("click");
    },

    add_editorbox_text_changed_listen(view_node,caller, func){
        if(!view_node)
            return;

        view_node.on("text-changed",func,caller);
    },

    add_editorbox_text_did_ended_listen(view_node,caller, func){
        if(!view_node)
            return;

        view_node.on("editing-did-ended",func,caller);
    },

    show_ui_at(parent, ui_name) {
        cc.loader.loadRes("ui_prefabs/" + ui_name, function(err, prefab) {
            if(err){
                console.log("ui_name =",ui_name);
                console.log("UI_manager show ui Errpr1 : ",err);
            }
            let item = cc.instantiate(prefab);
            parent.addChild(item);
            item.parent = parent;
            item.addComponent(ui_name + "_ctrl");

        });
    },
    show_ui_atSync(parent, ui_name) {
        return new Promise((resolve,reject)=>{
            cc.loader.loadRes("ui_prefabs/" + ui_name, function(err, prefab) {
                if(err){
                    AppLog.log();
                    AppLog.warn();
                    AppLog.err();
                    console.log("ui_name =",ui_name);
                    console.log("UI_manager show ui Errpr2 : ",err);
                }
                let item = cc.instantiate(prefab);
                parent.addChild(item);
                item.addComponent(ui_name + "_ctrl");
                resolve(item);
            });
        })
    },

    create_prefab_at(parent,ui_name){
        cc.loader.loadRes("ui_prefabs/" + ui_name, function(err, prefab) {
            if(err){
                console.log("UI_manager show ui Errpr3 : ");
                console.error(err);
            }
            let item = cc.instantiate(prefab);
            parent.addChild(item);
            item.parent = parent;
        });
    },

    create_prefab_atSync(parent,ui_name){
        return new Promise((resolve,reject)=>{
            cc.loader.loadRes("ui_prefabs/" + ui_name, function(err, prefab) {
                if(err){
                    console.log("UI_manager show ui Errpr4 : ");
                    console.error(err);
                }
                let item = cc.instantiate(prefab);
                parent.addChild(item);
                item.parent = parent;
                resolve(item);
            });
        })
    },

    create_item_Sync(parent,ui_name){
        return new Promise((resolve,reject)=>{
            cc.loader.loadRes("item/" + ui_name, function(err, prefab) {
                if(err){
                    console.log("UI_manager item Errpr : ");
                    console.error(err);
                }
                let item = cc.instantiate(prefab);
                parent.addChild(item);
                item.parent = parent;
                resolve(item);
            });
        })
    },

    create_item_Sync_NoParent(ui_name){
        return new Promise((resolve,reject)=>{
            cc.loader.loadRes("item/" + ui_name, function(err, prefab) {
                if(err){
                    console.log("UI_manager item Errpr : ");
                    console.error(err);
                }
                let item = cc.instantiate(prefab);
                resolve(item);
            });
        })
    },

    async ShowOrCreateView(parent, _name, _panelArr,level = 10) {
        //console.log("level:" + level);
        var Page = require("Page");
        var UIAssistant = require("UIAssistant");
        let panel = parent.children.find(x=>x.name == _name);
        if(panel == null){
            await UI_manager.show_ui_atSync(parent,_name);
            if(_panelArr != null && _panelArr.length > 0){
                let temp = new Page();
                temp.pageLevel = level;
                temp.NAME = _name;
                temp.panels.push(_name);
                _panelArr.forEach(x=>{
                    temp.panels.push(x);
                });
                UIAssistant.main.AddPage(temp);
            }
        }
        UIAssistant.main.ShowPage(_name);
    }
};

module.exports = UI_manager;

