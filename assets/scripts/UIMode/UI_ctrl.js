
var UI_ctrl = cc.Class({
    extends: cc.Component,

    properties: {
    },

    load_all_object(root, path) {
        for(let i = 0; i < root.childrenCount; i ++) {
            // console.log(path + root.children[i].name);
            if(Object.keys(this.view).includes(path + root.children[i].name))
                continue;

            // console.log(path + root.children[i].name);
            this.view[path + root.children[i].name] = root.children[i];
            this.load_all_object(root.children[i], path + root.children[i].name + "/");
        }
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        this.view = {};
        this.load_all_object(this.node, "");
    },

    start () {

    },

    // update (dt) {},
});
module.exports = UI_ctrl;