var HallData = /** @class */ (function () {
    function HallData() {
        this.gameId = null;
        this.gameCode = null;
        this.gameTitle = null;
        this.gameListLoading = null;
        this.loading = false;

        this.gameList = [];
        this.gameAllClass = [];
        this.gameClass = [];
        this.lotteryClassName = [];
        this.lotteryList = [];
        this.className = "";
        this.search = "";
        this.gamename = "";
        this.gameListType = null;
        /***
         * 是否pt jackpot
         * @type {boolean}
         */
        this.ptjackpot = false;
        /***
         * 平台廠商代碼
         * @type {string}
         */
        this.platformCode = "";
    }
    return HallData;
}());

module.exports = HallData;