var ViewModel_1 = require("ViewModel");
var Lang = require("Lang");
var Member = require("Member");
var Core = require("Core");
var Ctrls = require("Ctrls");
var LoginDate = require("LoginDate");
var Color = require("Color");
var BoundData = require("BoundData");
var RechargeData = require("RechargeData");
var HallData = require("HallData");
var WithDraw = require("WithDraw");
var AddBank = require("AddBank");
var ModifyPwData = require("ModifyPwData");
var BetRecord = require("BetRecord");
var MsgData = require("MsgData");
var HomeData = require("HomeData");
var BulletinData = require("BulletinData");
var Provider = require("Provider");
var ProviderInt = require("ProviderInt");
var Transfer = require("TransferData");
var ProviderName = require("ProviderName");
var Help = require("HelpData");
var Setting = require("Setting");
var GameHallData = require("GameHallData");
var FundsData = require("FundsData");
var FundsName = require("FundsName");
var BetType = require("BetType");
var OddType = require("OddType");
var xj188 = require("xj188");
var ErrCode = require("ErrData");

exports.lang = new Lang();
exports.member = new Member();
exports.core = new Core();
exports.ctrls = new Ctrls();
exports.login_date = new LoginDate();
exports.color = new Color();
exports.bound_data = new BoundData();
exports.rechargeData = new RechargeData();
exports.hall_data = new HallData();
exports.withdraw = new WithDraw();
exports.addbank = new AddBank();
exports.modifypw = new ModifyPwData();
exports.betrecord = new BetRecord();
exports.msg = new MsgData();
exports.home = new HomeData();
exports.bulletin = new BulletinData();
exports.provider = new Provider();
exports.transfer = new Transfer();
exports.providerint = new ProviderInt();
exports.providername = new ProviderName();
exports.help = new Help();
exports.setting = new Setting();
exports.gamehall = new GameHallData();
exports.funds = new FundsData();
exports .fundsname = new FundsName();
exports.bettype = new BetType();
exports.oddtype = new OddType();
exports .xj188 = new xj188();
exports.err = new ErrCode();

//数据模型绑定,定义后不能修改顺序
ViewModel_1.VM.add(exports.lang, 'lang');
ViewModel_1.VM.add(exports.member, 'member');
ViewModel_1.VM.add(exports.core, 'core');
ViewModel_1.VM.add(exports.login_date, 'login');
ViewModel_1.VM.add(exports.color, 'color');
ViewModel_1.VM.add(exports.bound_data, 'bound');
ViewModel_1.VM.add(exports.rechargeData, 'recharge');
ViewModel_1.VM.add(exports.hall_data, 'hall');
ViewModel_1.VM.add(exports.withdraw,"withdraw");
ViewModel_1.VM.add(exports.addbank,"addbank");
ViewModel_1.VM.add(exports.modifypw,"modifypw");
ViewModel_1.VM.add(exports.betrecord,"betrecord");
ViewModel_1.VM.add(exports.msg,"msg");
ViewModel_1.VM.add(exports.home,"home");
ViewModel_1.VM.add(exports.bulletin,"bulletin");
ViewModel_1.VM.add(exports.provider,"provider");
ViewModel_1.VM.add(exports.transfer,"transfer");
ViewModel_1.VM.add(exports.providerint,"providerint");
ViewModel_1.VM.add(exports.providername,"providername");
ViewModel_1.VM.add(exports.help,"help");
ViewModel_1.VM.add(exports.setting,"setting");
ViewModel_1.VM.add(exports.gamehall,"gamehall");
ViewModel_1.VM.add(exports.funds,"funds");
ViewModel_1.VM.add(exports.fundsname,"fundsname");
ViewModel_1.VM.add(exports.bettype,"bettype");
ViewModel_1.VM.add(exports.oddtype,"oddtype");
ViewModel_1.VM.add(exports.xj188,"xj188");
ViewModel_1.VM.add(exports.err,"err");
//使用注意事项
//VM 得到的回调 onValueChanged ，不能强制修改赋值
//VM 的回调 onValueChanged 中，不能直接操作VM数据结构,否则会触发 循环调用


        