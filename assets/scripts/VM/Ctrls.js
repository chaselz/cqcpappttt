var Ctrls = /** @class */ (function () {
    function Ctrls() {
        this.homectrl = null;
        this.gamectrl = null;
        this.boundctrl = null;
        this.memberctrl = null;
        this.loginctrl = null;
    }
    return Ctrls;
}());

module.exports = Ctrls;