var OddType = /** @class */ (function () {
    function OddType() {
        this.b0= '欧洲盘';
        this.b1= '英国盘';
        this.b2= '美国盘';
        this.b3= '香港盘';
        this.b4= '马来西亚盘';
        this.b5= '印度尼西亚盘';

    }
    return OddType;
}());

module.exports = OddType;
