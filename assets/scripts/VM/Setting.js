import {PlatFormType} from "../Enum/PlatFormType";
import {PageViewType} from "../Enum/PageViewType";

var Setting = /** @class */ (function () {
    function Setting() {
        this.toolbarlist = [PageViewType.Home,PageViewType.Bound,PageViewType.GameHall,PageViewType.newMember,PageViewType.MoreView];
        this.service_whitelist = [PageViewType.Home,PageViewType.Bound,PageViewType.GameHall];
        this.back_blacklist = [PageViewType.Web2,PageViewType.Home,PageViewType.Bound,PageViewType.GameHall,PageViewType.newMember,PageViewType.MoreView];
        /***
         * 需要縮瀏海的遊戲
         * @type {number[]}
         */
        this.webviewblacklist = [1,5];
        this.appversion = "4.2.0";
        this.platform = PlatFormType.Official;
        this.serviceurl = "https://zh.kf5.com/kchat/1019486";
    }
    return Setting;
}());

module.exports = Setting;