var BulletinData = /** @class */ (function () {
    function BulletinData() {
        this.bulletinindex = null;
        this.bulletinlist = [];
        this.issetbulletin = false;
    }
    return BulletinData;
}());

module.exports = BulletinData;
