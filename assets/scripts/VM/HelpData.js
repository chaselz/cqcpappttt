var HelpCenterData = /** @class */ (function () {
    function HelpCenterData() {
        this.helpList = [];
        this.list = [];
        this.helpid = null;
    }
    return HelpCenterData;
}());

module.exports = HelpCenterData;