var Core = /** @class */ (function () {
    function Core() {
        /***
         * 是否已經登入
         * @type {boolean}
         */
        this.islogin = false;
        this.toolbarStatus = 1;
        /***
         * lock鎖
         * @type {boolean}
         */
        this.lock = false;
        this.lockinfunc = false;
        this.demo_mode = false;
        /***
         * title文字
         * @type {string}
         */
        this.title = "";
        this.gameType = "";
        this.previousPage = "";
        /***
         * 遊戲類別
         * @type {number}
         */
        this.tagCode = 0;
        this.gameenable = null;
        /***
         * 是否為webview狀態
         * @type {boolean}
         */
        this.isweb = false;
        /***
         * 裝置目前的方向
         * @type {number}
         */
        this.isPORTRAIT = 0;
        this.webIndex = -1;
        this.isLongScreen = false;
        this.initapp = true;
        /***
         * 預設的資金明細模式
         * @type {undefined}
         */
        this.defaultdetail = undefined;
        /***
         * 是否全板的webview
         * @type {boolean}
         */
        this.isallscreenwebview = false;
    }
    return Core;
}());

module.exports = Core;