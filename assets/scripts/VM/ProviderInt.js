
var ProviderInt = /** @class */ (function () {
    function ProviderInt() {
        this.vision = 2;
        this.cq9 = 3;
        this.ky = 5;
        this.ag = 6;
        this.yg = 7;
        this.bg = 8;
        this.vr = 9;
        this.mg = 10;
        this.bbin = 11;
        this.jdb = 12;
        this.sb = 13;
        this.eBET = 14;
        this.leg = 15;
        this.pt = 16;
        this.bc= 18;
        this.majasport = 19;
        this.xj188 = 20;
        this.majaslot= 21;
        this.habanero = 22;
        this.pinnacle= 23;
        this.ae = 24;
        this.rg = 25;

    }
    return ProviderInt;
}());

module.exports = ProviderInt;
