var LoginType = require("LoginType");
var LoginDate = /** @class */ (function () {
    function LoginDate() {
        this.status = LoginType.SignIn;
        this.registersetting = null;
        this.hasErr = false;
        this.randomNum1 = 0;
        this.randomNum2 = 0;
        this.randomNum3 = 0;
        this.randomNum4 = 0;
        this.isToTermsView = false;
    }
    return LoginDate;
}());

module.exports = LoginDate;
