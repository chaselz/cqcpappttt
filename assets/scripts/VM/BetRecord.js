var BetRecord = /** @class */ (function () {
    function BetRecord() {
        this.gameproviderlist = null;
        this.starttime = "";
        this.endtime = "";
        //一開始要查詢選擇的廠商
        this.provider = "";
        //選擇的廠商
        this.gameprovider = "";
        this.targettime = "";
        this.targetprovider = "";
        this.targetgame = "";
        this.mode = "";
        this.bets = "";
        this.profits = "";
        this.pagecount = 1;
        this.nowpage = 1;
        this.targetbetslipnum = "";
        this.whitelist = ["cq9","sb","eBET","vision","majasport","habanero","majaslot"];
        this.gamename = "";
        this.gameplaytype = {};
    }
    return BetRecord;
}());

module.exports = BetRecord;
