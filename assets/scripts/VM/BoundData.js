var BoundData = /** @class */ (function () {
    function BoundData() {
        this.boundId = null;
        this.list = null;
        this.detail = null;
        this.hasErr = false;
        this.selected = "all";
        this.selectedid = 0;
        this.loading = false;
    }
    return BoundData;
}());

module.exports = BoundData;
