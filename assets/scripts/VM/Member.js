
var Member = /** @class */ (function () {
    function Member() {
        this.account = '';
        this.pw = '';
        this.token = '';
        this.cash = 0;
        this.name = '';
        this.data = null;
        this.msgcount = 0;
        this.luckmoneycount = 0;
    }
    return Member;
}());

module.exports = Member;
