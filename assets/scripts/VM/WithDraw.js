var WithDraw = /** @class */ (function () {
    function WithDraw() {
        this.min = 0;
        this.max = 0;
        this.bankid = 0;
    }
    return WithDraw;
}());

module.exports = WithDraw;