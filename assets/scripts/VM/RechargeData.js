var RechargeData = /** @class */ (function () {
    function RechargeData() {
        this.money = "";
        this.payJson = [];
        /***
         * 支付類型
         * @type {string}
         */
        this.paytype = "";
        this.paywaylist = undefined;
        /***
         * 存款銀行清單
         * @type {*[]}
         */
        this.banklist = [];
        /***
         * 支付銀行
         * @type {number}
         */
        this.payway = -1;
        /***
         * 存款銀行選擇
         * @type {number}
         */
        this.bankindex = -1;
        /***
         * 支付銀行選擇
         * @type {number}
         */
        this.paybankindex = -1;
        /***
         * 存款方式
         * @type {number}
         */
        this.depositway = -1;
        /***
         * 存款分行
         * @type {string}
         */
        this.depositbranch = "";
        /***
         * 當前通道或者支付銀行的清單
         * @type {*[]}
         */
        this.now_paywaylist = [];
        this.max = 0;
        this.min = 0;
        /***
         * 是否錯誤過
         * @type {boolean}
         */
        this.iserr = false;
        /***
         * JXPay 資料
         * @type {{}}
         */
        this.jxpay = {};
        /***
         * QRCode URL
         * @type {string}
         */
        this.qr = "";
        /***
         * QRCode Data
         * @type {string}
         */
        this.qrdata = "";
        this.transfername = undefined;



        this.ischeckmoney = false;
        this.cardinfo = undefined;
        this.depositwayindex = undefined;
        this.depositname = undefined;
        this.deposittime = undefined;
        this.depositbranch = undefined;

        this.bankcardid = undefined;
        this.bankpoint =undefined;

        this.bankname = undefined;
        this.bankid = undefined;
        this.point = undefined;
    }
    return RechargeData;
}());

module.exports = RechargeData;