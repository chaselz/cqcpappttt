var BetType = /** @class */ (function () {
    function BetType() {
        this.b1= '单注';
        this.b2= '复式过关';
        this.b3= '系统组合过关';
        this.b4= '锁链';
        this.b5= 'Trixie';
        this.b6= 'Yankee';
        this.b7= 'PermedYankee';
        this.b8= 'SuperYankee';
        this.b9= 'Heinz';
        this.b10= 'SuperHeinz';
        this.b11= 'Goliath';
        this.b12= 'Patent';
        this.b13= 'PermedPatent';
        this.b14= 'Lucky15';
        this.b15= 'Lucky31';
        this.b16= 'Lucky63';

    }
    return BetType;
}());

module.exports = BetType;
