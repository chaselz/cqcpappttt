var TransferData = /** @class */ (function () {
    function TransferData() {
        this.from = null;
        this.to = null;
    }
    return TransferData;
}());

module.exports = TransferData;