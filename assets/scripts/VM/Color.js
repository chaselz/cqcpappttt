var Color = /** @class */ (function () {
    function Color() {
        this.black = new cc.Color(48,49,51);
        this.gray = new cc.Color(192,196,204);
        this.darkgray = new cc.Color(144,147,153);
        this.red = new cc.Color(209,31,24);
        this.green = new cc.Color(103,194,58);
        this.darkblue = new cc.Color(20,31,74);
        this.skyblue = new cc.Color(64,158,255);
        this.lightskyblue = new cc.Color(159,206,255);
        this.linegray = new cc.Color(220,223,230);
        this.gold = new cc.Color(235,209,154);
        //#7d7d7d
        this.graylabel = new cc.Color(125,125,125);
        //#484848
        this.graylabel2 = new cc.Color(72,72,72);
        //#d3d3d3
        this.graylabel3 = new cc.Color(211,211,211);
        //#323232
        this.linecolor = new cc.Color(50,50,50);
        //#272727
        this.bg27color = new cc.Color(39,39,39);
        //#313131
        this.bg31color = new cc.Color(49,49,49);
    }
    return Color;
}());

module.exports = Color;