import {VM} from "../modelView/ViewModel";
import BalanceItem from "../Item/BalanceItem";
import Awaiter from "../Extension/AwaitExtension";


const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var ApiType = require("ApiType");
var TitleAlertType = require("TitleAlertType");
var TitleAlertView = require("TitleAlertView");

@ccclass
export default class BalanceManager extends cc.Component {

    @property(cc.Node)
    contentNode: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    @property(cc.Node)
    blackCloth: cc.Node = null;
    // onLoad () {}

    private _isready:boolean = false;
    private isupdata:boolean = false;
    private lock : boolean = false;
    private datalist = null;
    public get isReady(){
        return this._isready;
    }

    public static main: BalanceManager = null;

    protected onLoad(): void {
        if(!BalanceManager.main)
            BalanceManager.main = this;

        ApiManager.addListenter(this.apilistenter.bind(this));
    }

    public async setValue(data:any){
        if(this.lock) {
            this.datalist = data;
            return;
        }else
            this.datalist = null;

        this.lock = true;

        if(!BalanceManager.main)
            BalanceManager.main = this;

        await this.CreatItems(data,(name)=> {
                this.blackClothActive(true);
                this.lockitem(name);
        });

        this.revertStatus();
    }

    public revertStatus(){
        this.blackClothActive(false);
        this.contentNode.getComponentsInChildren(BalanceItem).forEach(item=>item.removeLock());
    }

    private apilistenter(type:any,data:any){
        if(type==ApiType.gameBalanceProgressType){
            if(this.isupdata) {
                this.revertStatus();
                this.isupdata = false;
            }

            if(data.code == "1"){
                this.setValue(data.result.data);
            }
        }else if(ApiType.gameRedeemType == type){
            this.AnalyzeGameRedeem(data);
        }
    }

    AnalyzeGameRedeem = function (value) {
        ApiManager.User.get.UserBalance();
        ApiManager.User.get.UserGameBalanceProgress();
        this.isupdata = true;


    };

    private lockitem(name){
        this.contentNode.getComponentsInChildren(BalanceItem).forEach(item=>item.lockitem(name));
    }

    private blackClothActive(_active:boolean){
        this.blackCloth.active = _active;
    }

    private async CreatItems(data,func = null) {
        let gameenable = VM.getValue("core.gameenable");
        await Awaiter.until(_=>gameenable!=null);
        data = data.filter((e) => {
            return gameenable.find(x=>x.provider.code == VM.getValue("provider.id"+e.providerID)) != undefined;
        });
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(index % 2 == 0 ? "BalanceLeftItem":"BalanceRightItem",index,value,func);
        });

        for(let i = data.length;i<this.contentNode.childrenCount;i++){
            this.contentNode.children[i].active = false;
        }
        this._isready = true;
        this.blackCloth.height = this.contentNode.height;
        this.lock = false;
        if(this.datalist)
            this.setValue(this.datalist);
    };
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    private async creatFactory(itemname,index,data,onClickFunc,type = null) {
        let item = null;

        if(index >= this.contentNode.childrenCount) {
            item = await UI_manager.create_item_Sync(this.contentNode, itemname);
            item.getComponent(BalanceItem).setManager(this);
        }else
            item = this.contentNode.children[index];


        item.active = true;

        if(type)
            item.getComponent("Iitem").setValue(data,onClickFunc,type);
        else
            item.getComponent("Iitem").setValue(data,onClickFunc);
    };


}
