import {VM} from "../modelView/ViewModel";
import Vec2 = cc.Vec2;
import platformItem from "../Item/platformItem";
const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class ChooseViewManager extends cc.Component {

    @property(cc.Node)
    mainNode: cc.Node = null;

    @property(cc.Button)
    closeBtn: cc.Button = null;

    private cb : void = null;
    private chooseID = [null,null];
    // LIFE-CYCLE CALLBACKS:
    private p_y : number = 0;
    private origin_y = 0;
    private lock: boolean = false;
    private datalist = null;

    private activeCb : void = null;
    private _isshow :boolean = false;
    private _isready : boolean = false;
    private get isshow(){
        return this._isshow;
    }
    private set isshow(value:boolean){
        if(this.activeCb)
            this.activeCb(value);
        this._isshow = value;
    }
    public get active(){
        return this._isshow;
    }
    public get isready(){
        return this._isready;
    }
    onLoad () {
        this.Init();
    }

    start () {
        UI_manager.add_button_listen(this.closeBtn.node,this,function () {
            this.Close();
        });
    }

    public Init(){
        this.isshow = false;
        this.node.position = new Vec2(0,-480);
        this.chooseID = [null,null];
        this.changeItemStatus(this.chooseID);
    }

    public setHeightAndPosition(_h:number,_py:number){
        this.node.height = _h;
        this.p_y = _py;
    }

    public changeTransferID(){
        if(this.chooseID[0] && this.chooseID[1]) {
            let temp = this.chooseID[0];
            this.chooseID[0] = this.chooseID[1];
            this.chooseID[1] = temp;
            this.changeItemStatus([this.chooseID[0].id,this.chooseID[1].id]);
            let temp1 = this.mainNode.getComponentsInChildren(platformItem).find(item=>item.ID ==this.chooseID[0].id);
            this.chooseID[0]["sprite"] = temp1.iconImg.spriteFrame;
            let temp2 = this.mainNode.getComponentsInChildren(platformItem).find(item=>item.ID ==this.chooseID[1].id);
            this.chooseID[1]["sprite"] = temp2.iconImg.spriteFrame;
            this.callBackData();
        }
    }

    /***
     * 設定選擇數值回傳
     * @param _cb
     */
    public setCallBack(_cb:void){
        this.cb = _cb;
    }

    public Show(){
        if(this.isshow)
            return;
        this.isshow = true;
        this.node.active = true;
        this.updataPlatitemsIconState();
        var finished = cc.callFunc(function () {
        }, this);
        var spawn = cc.sequence(cc.moveTo(0.5,0,this.p_y),finished);
        this.node.runAction(spawn);

    }

    private updataPlatitemsIconState(){
        this.mainNode.getComponentsInChildren(platformItem).forEach(x=>x.updataIconState());
    }

    public Close(){
        var finished = cc.callFunc(function () {
            this.node.active = false;
            this.isshow = false;
        }, this);
        var seq = cc.sequence(cc.moveTo(0.5,0,-480), finished);
        this.node.runAction(seq);
    }

    public async setValue(data:any){
        if(this.lock) {
            this.datalist = data;
            return;
        }else
            this.datalist = null;

        this.lock = true;
        var func = this.choosePlatformfunc.bind(this);
        this.mainNode.children[0].getComponent(platformItem).setCallBack(func);
        await this.CreatItems(data, func);

        if(this.datalist)
            this.setValue(this.datalist);
    }

    /***
     * 設定畫面是否顯示監聽
     * @param cb
     */
    public viewActiveLisitener(cb:void){
        if(cb)
            this.activeCb = cb;
    }

    private choosePlatformfunc(id:number,item:platformItem){

        if(this.chooseID[0] && this.chooseID[0].id == id){
            this.chooseID[0] = null;
            item.Init();
            this.callBackData();
            return;
        }else if(this.chooseID[1] && this.chooseID[1].id == id){
            this.chooseID[1] = null;
            item.Init();
            this.callBackData();
            return;
        }
        else if(this.chooseID[0] == null){

            this.chooseID[0] = { "id" : id,"name" : item.nameLabel.string, "sprite" : item.iconImg.spriteFrame, "amount" : item.amountLabel.string, "mustBet":item.mustBet,"validBet":item.validBet, "bankruptcyAmount":item.bankruptcyAmount, "status" : item.status, "betPercent":item.betPercent};
            item.ToutMode();
            this.callBackData();
            return;
        }else if(this.chooseID[1] == null){
            this.chooseID[1] = { "id" : id,"name" : item.nameLabel.string, "sprite" : item.iconImg.spriteFrame, "amount" : item.amountLabel.string, "mustBet":item.mustBet,"validBet":item.validBet, "bankruptcyAmount":item.bankruptcyAmount, "status" : item.status, "betPercent":item.betPercent};
            item.TinMode();
            this.callBackData();
            return;
        }
//
    }

    private changeItemStatus(ids){
        this.mainNode.children.forEach(x=>x.getComponent(platformItem).changeStatus(ids));
    }

    private callBackData(){
        if(this.cb)
            this.cb(this.chooseID);
    }


    // update (dt) {}
    private async CreatItems(data,func = null) {
        let gameenable = VM.getValue("core.gameenable");
        data = data.filter((e) => {
            return gameenable.find(x=>x.provider.code == VM.getValue("provider.id"+e.providerID)) != undefined;
        });
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory("platformItem", index+1, value, func);
        });

        for(let i = data.length+1;i<this.mainNode.childrenCount;i++){
            this.mainNode.children[i].active = false;
        }
        if(!this.isready)
            this.Init();
        this._isready = true;
        this.lock = false;
        if(this.datalist)
            this.setValue(this.datalist);
    };
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    private async creatFactory(itemname,index,data,onClickFunc,type = null) {
        let item = null;
        if(index >= this.mainNode.childrenCount) {
            item = await UI_manager.create_item_Sync(this.mainNode, itemname);
        }else
            item =  this.mainNode.children[index];

        item.active = true;

        if(type)
            item.getComponent("Iitem").setValue(data,onClickFunc,type);
        else
            item.getComponent("Iitem").setValue(data,onClickFunc);

        // item.getComponent(platformItem).Init();
    };
}
