import { PageViewType } from "../Enum/PageViewType";

const UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");

export default class PageManager  {

    public static HomePage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Home,["Top","Content","Bottom"],1);
    }

    public static async HomePageAsync(){
       await UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Home,["Top","Content","Bottom"],1);
    }

    public static GameHallPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.GameHall,["Top","Content","Bottom"],2);
    }

    public static BoundPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Bound,["Top","Content","Bottom"],3);
    }

    public static MemberPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/TopContent"),PageViewType.newMember,["Bottom","TopContent"],4);
    }

    public static MorePage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/TopContent"),PageViewType.MoreView,["Bottom","TopContent"]);
    }

    public static SignInPage(){
        UIAssistant.main.ShowPage(PageViewType.SignIn);
    }

    public static BetRecordMakerPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.BetRecordMaker,["Top","BelowContent"]);
    }

    public static GameListPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.GameList,["Top","Content","Bottom"]);
    }

    public static DetailsPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.newDetails,["Top","BelowContent"]);
    }

    public static BoundDetailPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.BoundDetail,["Top","Content","Bottom"]);
    }

    public static HelpInfoPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.HelpInfo,["Top","BelowContent"]);
    }

    public static MsgInfoPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.MsgInfo,["Top","BelowContent"]);
    }

    public static AddBankPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.AddBank,["Top","BelowContent"]);
    }

    public static BetRecordNumPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.BetRecordNum,["Top","BelowContent"]);
    }

    public static BetRecordGamePage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.BetRecordGame,["Top","BelowContent"]);
    }

    public static BulletinDetailPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.BulletinDetail,["Top","Content","Bottom"]);
    }

    public static GamesPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Games,["Top","Content","Bottom"],6);
    }

    public static WithDrawPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.WithDraw,["Top","BelowContent"],6);
    }

    public static HelpListPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.HelpList,["Top","BelowContent"]);
    }

    public static BulletinPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Bulletin,["Top","Content","Bottom"],6);
    }

    public static BetRecordPage(){
        UIAssistant.main.ShowPage(PageViewType.BetRecord);
        //UI_manager.ShowOrCreateView(cc.find("Canvas/Main/Content"),PageViewType.Bulletin,["Top","Content","Bottom"],6);
    }

    public static HelpCenterPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.HelpCenter,["Top","BelowContent"],6);
    }

    public static AgentJoinPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.AgentJoin,["Top","BelowContent"]);
    }

    public static RechargePage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.RechargeV2,["Top","BelowContent","RechargePaylist"],6);
    }

    public static IndoorTransferPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.IndoorTransfer,["Top","BelowContent"]);
    }

    public static MemberInfoPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.MemberInfo,["Top","BelowContent"]);
    }

    public static VoucherPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.Voucher,["Top","BelowContent"]);
    }

    public static ModifyPWPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.ModifyPW,["Top","BelowContent"]);
    }

    public static BanklistPage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.Banklist,["Top","BelowContent"]);
    }

    public static MyMessagePage(){
        UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),PageViewType.MyMessage,["Top","BelowContent"]);
    }

    public static InputDepositPage(){
        UIAssistant.main.ShowPage(PageViewType.InputDeposit);
    }

    public static QrCodePage(){
        UIAssistant.main.ShowPage(PageViewType.QrCode);
    }

    public static TransferInfoPage(){
        UIAssistant.main.ShowPage(PageViewType.TransferInfo);
    }

    public static CheckMenuViewPage(){
        UIAssistant.main.ShowPage(PageViewType.CheckMenuView);
    }

    public static MenuViewPage(){
        UIAssistant.main.ShowPage(PageViewType.MenuView);
    }

    public static SignUpPage(){
        UIAssistant.main.ShowPage(PageViewType.SignUp);
    }

    public static CheckDepositPage(){
        UIAssistant.main.ShowPage(PageViewType.CheckDeposit);
    }

    public static BetRecordNoDataPage(){
        UIAssistant.main.ShowPage(PageViewType.BetRecordNoData);
    }

    public static BetRecordDate(){
        UIAssistant.main.ShowPage(PageViewType.BetRecordDate);
    }

    public static InitViewPage(){
        UIAssistant.main.ShowPage(PageViewType.InitView);
    }
}
