import DayInputBox from "../UI/DayInputBox";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var LoginType = require("LoginType");


cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		/*
		 * 設定EditBox 顯示必／選填
		 * @param name 物件名
		 * @param demand 必／選填
		 */
		this.setInputBox_PLACEHOLDER = function(demand,name){
			let vmLabel = this.view[name+"/inputbox/PLACEHOLDER_LABEL"].getComponent("VMLabel");
			vmLabel.watchPathArr[0] ="lang." + (demand == 1 ? "optional" :"mustinput");
			vmLabel.onValueInit();
		};
		/*
		 * 設定密碼顯示與隱藏
		 * @param name 物件名
		 */
		this.setShowPWBtn = function(name){
			let self = this;
			UI_manager.add_button_listen(this.view[name+"/showPWBtn"], null, function () {
				let inputbox = self.view[name+"/inputbox"].getComponent(cc.EditBox);
				let status = inputbox.inputFlag == cc.EditBox.InputFlag.DEFAULT;
				inputbox.inputFlag = status ? cc.EditBox.InputFlag.PASSWORD : cc.EditBox.InputFlag.DEFAULT;
				UI_manager.setSprite(self.view[name+"/showPWBtn/Bg"],"universal/icon-show-pw"+ (status ? "":"-show"));
			});
		};
		/*
		 * 設定所以密碼的顯示隱藏功能
		 */
		this.setAllShowPwBtn = function(){
			this.setShowPWBtn("PwTitle");
			this.setShowPWBtn("CPwTitle");
		};

		this.setEditBoxListener = function () {

		};
		/*
		 * 設定物件開關
		 * @param name 物件名
		 * @param m_active 是否顯示
		 */
		this.setNodeActive = function (name,m_active) {
			this.view[name].active = m_active;
		};
		/*
		 * 設定EditBox的狀態
		 * @param name 物件名
		 * @param mcolor EditBox 文字顏色
		 * @param bcolor 底線顏色
		 */
		this.setLabelColor = function (name,mcolor,bcolor = VM.getValue("color.linecolor")) {
			this.view[name+"/title"].color = mcolor;
			this.view[name+"/inputbox/TEXT_LABEL"].color = mcolor;
			this.view[name+"/buttomLine"].color = bcolor;
			if(this.view[name+"/warnlabel"])
				this.view[name+"/warnlabel"].color = mcolor;
		};
		/*
		 * 繪圖所以Graphics物件
		 */
		this.DrawUseGraphics = function () {
			let graphics1 = this.view["Agreeterms/off"].getComponent(cc.Graphics);
			graphics1.circle(0,0,18);
			graphics1.stroke();

			let graphics2 = this.view["CheckKeyTitle/numbg"].getComponent(cc.Graphics);
			graphics2.roundRect(0,0,142,56,25);
			graphics2.stroke();
		};
		/*
		 * 設定勾選條款按鈕
		 */
		this.setAgreeTermsClick = function () {
			let self = this;
			UI_manager.add_button_listen(this.view["Agreeterms/Click"],null,function () {
				let temp = self.view["Agreeterms/on"].active;
				self.setNodeActive("Agreeterms/on",!temp);
				self.setNodeActive("Agreeterms/off",temp);
				if(VM.getValue("login.hasErr")){
					self.setNodeActive("ListErr",temp);
				}
			});
		};
		/*
		 * 設定更新驗證碼內容按鈕功能
		 */
		this.setRandomVCodeBtn = function () {
			let self = this;
			UI_manager.add_button_listen(this.view["CheckKeyTitle/reloadnumBtn"],null,function () {
				self.randomVCode();
			});
		};
        /*
         * 設定所以EditBox 編輯中的事件
         */
		this.setInputBoxStatus = function () {
			let self = this;
			this.setEditBoxInEditorEvent("AccountTitle",function (inputbox) {
				let notstyple = VM.getValue("login.hasErr") && !inputbox.string.OnlyNumAndEn() && inputbox.string.length > 0;
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 6 ;

				self.setLabelColor("AccountTitle",VM.getValue((temp || notstyple) ? "color.red": "color.graylabel3"),VM.getValue((temp || notstyple) ? "color.red": "color.linecolor"));
				self.setNodeActive("AccountErr",temp);
				self.setNodeActive("AccountErr2",notstyple);
			});
			this.setEditBoxInEditorEvent("PwTitle",function (inputbox) {
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 6;

				self.setLabelColor("PwTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("PwErr",temp);
			});
			this.setEditBoxInEditorEvent("CPwTitle",function (inputbox) {
				let temp = VM.getValue("login.hasErr") && inputbox.string != self.view["PwTitle/inputbox"].getComponent(cc.EditBox).string;

				self.setLabelColor("CPwTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("CPwErr",temp);
			});
			this.setEditBoxInEditorEvent("NameTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.name == 1 ? true : false;
				let hasNum = inputbox.string.hasNumber();
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 2 && !checkOptional;

				self.setLabelColor("NameTitle",VM.getValue(temp || hasNum ? "color.red": "color.graylabel3"),VM.getValue(temp || hasNum ? "color.red": "color.linecolor"));
				self.setNodeActive("NameErr",temp);
				self.setNodeActive("NameErr2",hasNum);

			});
			this.setEditBoxInEditorEvent("NickNameTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.nickname == 1 ? true : false;
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 2 && !checkOptional;

				self.setLabelColor("NickNameTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("NickNameErr",temp);

			});
			this.setEditBoxInEditorEvent("MailTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.email == 1 ? true : false;
				let temp = VM.getValue("login.hasErr") && !inputbox.string.isEmail() && !checkOptional;

				self.setLabelColor("MailTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("MailErr",temp);

			});
			this.setEditBoxInEditorEvent("PhoneTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.phone == 1 ? true : false;
				let temp = VM.getValue("login.hasErr") && !inputbox.string.isPhoneNumber() && !checkOptional;

				self.setLabelColor("PhoneTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("PhoneErr",temp);

			});
			this.setEditBoxInEditorEvent("QQTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.qq == 1 ? true : false;
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 5 && !checkOptional;
				let temp2 = VM.getValue("login.hasErr") && inputbox.string.length > 11;
				self.setLabelColor("QQTitle",VM.getValue(temp || temp2 ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("QQErr",temp);
				self.setNodeActive("QQErr2",temp2);

			});
			this.setEditBoxInEditorEvent("WeChatTitle",function (inputbox) {
				let settingList = VM.getValue("login.registersetting").demand;
				let checkOptional = inputbox.string.length == 0 && settingList.wechat == 1 ? true : false;
				let temp = VM.getValue("login.hasErr") && inputbox.string.length < 2 && !checkOptional;
				let temp2 = temp || !inputbox.string.isWeChatNum() && !checkOptional;
				self.setLabelColor("WeChatTitle",VM.getValue(temp2 ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));
				self.setNodeActive("WeChatErr",temp);
				self.setNodeActive("WeChatErr2",!inputbox.string.isWeChatNum() && !checkOptional);

			});
			this.setEditBoxInEditorEvent("CheckKeyTitle",function (inputbox) {
				let temp = VM.getValue("login.hasErr") && !self.VerificationCodeＣorrect();

				self.setLabelColor("CheckKeyTitle",VM.getValue(temp ? "color.red": "color.graylabel3"),VM.getValue(temp ? "color.red": "color.linecolor"));

			});
		};
		/*
		 * 設定條款點擊事件
		 */
		this.setRickTextEvent = function () {
			let self = this;
			let text = this.view["Agreeterms/Label"].getComponent("RickTextClick");
			text.setEvent(self.ToTermsView);
		};
		/*
		 * 觀看條款內容
		 */
		this.ToTermsView = function () {
			console.log("ToTermsView");
			VM.setValue("core.title",VM.getValue("lang.registerlist"));
			PageManager.HelpListPage();
		};
		/*
         * 設定WeChat顯示選填或者必填
         */
		this.setHasAccountBtn = function () {
			UI_manager.add_button_listen(this.view["buttom/hasAccountBtn"],null,function () {
				let lvc = CtrlAssistant.main.getCtrl("LoginCtrl");
				lvc.showPage(LoginType.SignIn);
			});
		};

		this.setEditBoxValue = function(name,value){
			this.view[name+"/inputbox"].getComponent(cc.EditBox).string = value;
		};
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.signup"));
		CtrlAssistant.main.getCtrl("LoginCtrl").setToggleBtn(LoginType.SignUp);
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
	},

	start() {
		this.setAllShowPwBtn();
		this.DrawUseGraphics();
		this.setAgreeTermsClick();
		this.setRandomVCodeBtn();
		this.setInputBoxStatus();
		this.setRickTextEvent();
		this.setHasAccountBtn();
	},
	/*
	 * 初始化
	 */
	Init(){
		let titlecolor = VM.getValue("color.graylabel3");

		this.setLabelColor("AccountTitle",titlecolor);
		this.setLabelColor("PwTitle",titlecolor);
		this.setLabelColor("CPwTitle",titlecolor);
		this.setLabelColor("NameTitle",titlecolor);
		this.setLabelColor("NickNameTitle",titlecolor);
		this.setLabelColor("MailTitle",titlecolor);
		this.setLabelColor("PhoneTitle",titlecolor);
		this.setLabelColor("QQTitle",titlecolor);
		this.setLabelColor("WeChatTitle",titlecolor);
		this.setLabelColor("CheckKeyTitle",titlecolor);

		this.setEditBoxValue("AccountTitle","");
		this.setEditBoxValue("PwTitle","");
		this.setEditBoxValue("CPwTitle","");
		this.setEditBoxValue("NameTitle","");
		this.setEditBoxValue("NickNameTitle","");
		this.setEditBoxValue("MailTitle","");
		this.setEditBoxValue("PhoneTitle","");
		this.setEditBoxValue("QQTitle","");
		this.setEditBoxValue("WeChatTitle","");
		this.setEditBoxValue("CheckKeyTitle","");
		this.setEditBoxValue("PushCodeTitle","");
		this.setEditBoxValue("BirthdayTitle","");

		this.setNodeActive("Agreeterms/on",false);
		this.setNodeActive("Agreeterms/off",true);
		this.setNodeActive("ListErr",false);
		this.setNodeActive("AccountErr",false);
		this.setNodeActive("PwErr",false);
		this.setNodeActive("CPwErr",false);
		this.setNodeActive("NameErr",false);
		this.setNodeActive("NameErr2",false);
		this.setNodeActive("NickNameErr",false);
		this.setNodeActive("MailErr",false);
		this.setNodeActive("PhoneErr",false);
		this.setNodeActive("QQErr",false);
		this.setNodeActive("QQErr2",false);
		this.setNodeActive("WeChatErr",false);
		this.setNodeActive("WeChatErr2",false);

		this.view["BirthdayTitle/inputbox"].getComponent(DayInputBox).string = "";
	},
	/*
	 * 設定推廣碼顯示選填或者必填
	 * @param m_active 是否顯示
	 * @param demand 必／選填
	 */
	setPromotionSetting(m_active,demand){
		// this.view["PushCodeTitle"].active = demand != Demand.NoActive;
		// this.setInputBox_PLACEHOLDER(demand,"PushCodeTitle");
	},
	/*
	 * 設定暱稱顯示選填或者必填
	 * @param demand 必／選填
	 */
	setNickNameSetting(demand){
		this.view["NickNameTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"NickNameTitle");
	},
	/*
	 * 設定姓名顯示選填或者必填
	 * @param demand 必／選填
	 */
	setNameSetting(demand){
		this.view["NameTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"NameTitle");
	},
	/*
	 * 設定Email顯示選填或者必填
	 * @param demand 必／選填
	 */
	setEmailSetting(demand){
		this.view["MailTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"MailTitle");
	},
	/*
	 * 設定生日顯示選填或者必填
	 * @param demand 必／選填
	 */
	setBirthDaySetting(demand){
		this.view["BirthdayTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"BirthdayTitle");
	},
	/*
	 * 設定手機顯示選填或者必填
	 * @param demand 必／選填
	 */
	setPhoneSetting(demand){
		this.view["PhoneTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"PhoneTitle");
	},
	/*
	 * 設定QQ顯示選填或者必填
	 * @param demand 必／選填
	 */
	setQQSetting(demand){
		this.view["QQTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"QQTitle");
	},
	/*
	 * 設定WeChat顯示選填或者必填
	 * @param demand 必／選填
	 */
	setWeChatSetting(demand){
		this.view["WeChatTitle"].active = demand != Demand.NoActive;
		this.setInputBox_PLACEHOLDER(demand,"WeChatTitle");
	},
	/*
	 * 顯示帳號錯誤
	 */
	showAccountErr(showErr=true){
		this.setLabelColor("AccountTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("AccountErr",true);
	},
	/*
	 * 顯示帳號格式錯誤
	 */
	showAccountFormatErr(showErr=true){
		this.setLabelColor("AccountTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("AccountErr2",true);
	},
	/*
	 * 顯示密碼錯誤
	 */
	showPWErr(showErr=true){
		this.setLabelColor("PwTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("PwErr",true);
	},
	/*
	 * 顯示確認密碼錯誤
	 */
	showCPWErr(showErr=true){
		this.setLabelColor("CPwTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("CPwErr",true);
	},
	/*
	 * 顯示姓名錯誤
	 */
	showNameErr(showErr=true){
		this.setLabelColor("NameTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("NameErr",true);
	},
	/*
	 * 顯示姓名格式錯誤
	 */
	showNameFormatErr(showErr=true){
		this.setLabelColor("NameTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		if(showErr)
			this.setNodeActive("NameErr2",true);
	},
	/*
	 * 顯示暱稱錯誤
	 */
	showNickNameErr(){
		this.setLabelColor("NickNameTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("NickNameErr",true);
	},
	/*
	 * 顯示Email錯誤
	 */
	showEmailErr(){
		this.setLabelColor("MailTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("MailErr",true);
	},
	/*
	 * 顯示QQ錯誤
	 */
	showQQErr(){
		this.setLabelColor("QQTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("QQErr",true);
	},
	/*
	 * 顯示QQ錯誤
	 */
	showQQErrTooLong(){
		this.setLabelColor("QQTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("QQErr2",true);
	},
	/*
	 * 顯示WebChat錯誤
	 */
	showWeChatErr(){
		this.setLabelColor("WeChatTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("WeChatErr",true);
	},

	/*
	 * 顯示WebChat只能英文數字符號－的錯誤
	 */
	showWeChatErrWithZH(){
		this.setLabelColor("WeChatTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("WeChatErr2",true);
	},
	/*
	 * 顯示條款錯誤
	 */
	showTermsErr(){
		this.setNodeActive("ListErr",true);
	},
	/*
	 * 顯示驗證碼錯誤
	 */
	showVerificationCodeErr(){
		this.setLabelColor("CheckKeyTitle",VM.getValue("color.red"),VM.getValue("color.red"));
	},
	/*
	 * 顯示電話錯誤
	 */
	showPhoneErr(){
		this.setLabelColor("PhoneTitle",VM.getValue("color.red"),VM.getValue("color.red"));
		this.setNodeActive("PhoneErr",true);
	},
  	/*
  	 * 取的輸入的值
  	 */
	getInputBoxValue(){
		var json = {
			"account":this.view["AccountTitle/inputbox"].getComponent(cc.EditBox).string,
			"pw":this.view["PwTitle/inputbox"].getComponent(cc.EditBox).string,
			"cpw":this.view["CPwTitle/inputbox"].getComponent(cc.EditBox).string,
			"name":this.view["NameTitle/inputbox"].getComponent(cc.EditBox).string,
			"birthday":this.view["BirthdayTitle/inputbox"].getComponent(DayInputBox).string,
			"nickname":this.view["NickNameTitle/inputbox"].getComponent(cc.EditBox).string,
			"email":this.view["MailTitle/inputbox"].getComponent(cc.EditBox).string,
			"phone":this.view["PhoneTitle/inputbox"].getComponent(cc.EditBox).string,
			"qq":this.view["QQTitle/inputbox"].getComponent(cc.EditBox).string,
			"wechat":this.view["WeChatTitle/inputbox"].getComponent(cc.EditBox).string,
			"promotion":this.view["PushCodeTitle/inputbox"].getComponent(cc.EditBox).string,
		};
		return json;
	},
	/*
	 * 密碼與確認密碼是否相同
	 */
	PWCorrect(){
		return this.view["PwTitle/inputbox"].getComponent(cc.EditBox).string == this.view["CPwTitle/inputbox"].getComponent(cc.EditBox).string;
	},
	/*
	 * 驗證碼是否正確
	 */
	VerificationCodeＣorrect(){
		let inputValue = this.view["CheckKeyTitle/inputbox"].getComponent(cc.EditBox).string;
		let vCode = VM.getValue("login.randomNum1").toString();
		vCode +=  VM.getValue("login.randomNum2").toString();
		vCode +=  VM.getValue("login.randomNum3").toString();
		vCode +=  VM.getValue("login.randomNum4").toString();
		return inputValue == vCode;
	},

	/*
	 * 是否勾選同意條款
	 */
	AgreeTerms(){
		return this.view["Agreeterms/on"].active || !this.view["Agreeterms"].active;
	},
	/*
	 * 設定EditBox 編輯中的事件
	 * @param name 物件名稱
	 * @param func 事件
	 */
	setEditBoxInEditorEvent(name,func){
		let inputbox = this.view[name+"/inputbox"];
		inputbox.on("text-changed",func,this);
	},
	/*
	 * 重新設定驗證碼
	 */
	randomVCode(){
		VM.setValue("login.randomNum1",Math.Randomlimit(0,9));
		VM.setValue("login.randomNum2",Math.Randomlimit(0,9));
		VM.setValue("login.randomNum3",Math.Randomlimit(0,9));
		VM.setValue("login.randomNum4",Math.Randomlimit(0,9));
	},
	/*
	 * 設定註冊
	 * @param func 事件
	 * @param caller 呼叫者
	 */
	setRegister(func,caller){
		UI_manager.add_button_listen(this.view["SignUpBtn"],caller,func);
	},
	/*
	 * 取消註冊
	 * @param target 原本註冊事件是的物件
	 * @param caller 呼叫者
	 */
	ClearRegister(target,caller){
		// this.view["SignUpBtn"].targetOff(target);
		UI_manager.clear_button_listen(this.view["SignUpBtn"],caller,target)
	},
	/*
	 * 設定條款資料
	 * @param active 是否開啟
	 * @param name 條款名稱
	 * @param data 條款內容
	 */
	setTerms(active,name,data) {
		let text = this.view["Agreeterms/Label"].getComponent("VMLabel");
		this.view["Agreeterms"].active = active;
		if(!active)
			return;

		text.setOriginLabelValue("{{0}} <b><color=#ebd19a><on click='onClik'>"+name+"</on></c></b>");
	},

});

const Demand = Object.freeze({
	NonEssential:2,
	NonEssential:1,
	NoActive:0
});