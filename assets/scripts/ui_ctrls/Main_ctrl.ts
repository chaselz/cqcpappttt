import {PageViewType} from "../Enum/PageViewType";
import {VM} from './../modelView/ViewModel';
import {UIInterfaceOrientation} from "../Extension/Nactive/MobileExtension";
import {VoucherType} from "../Enum/VoucherType";
import newUIDatePicker, {DateType} from "../../resources/UIDatePicker/newUIDatePicker";
import UpdataApp from "../Tools/UpdataApp";
import PageManager from "../ManagerUI/PageManager";


const {ccclass, property, executeInEditMode} = cc._decorator;
const uictrl = require('UI_ctrl');
const UI_manager = require("UI_manager");
var Page = require("Page");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

@ccclass
@executeInEditMode
export default class Main_ctrl extends uictrl {

	@property(cc.Canvas)
	canvas:cc.Canvas = null;

	private pickdateType : DateType = DateType.OnlyDay;

    onLoad () {
		if(CC_EDITOR)
			return;
        super.onLoad()
    }

	start() {
		if(CC_EDITOR)
			return;
		CtrlAssistant.main.saveCtrl(this,"MainCtrl");
		this.setUIPage();
		this.FixLongViewInsetsDidChange();
		this.addPages();
		this.setToolBarBtn();
		this.drawGraphics();
		this.setSignInAlertViewBtnsEvent();
		this.setBackBtn();
		this.setPickBtn();
		this.setServiceBtn();
	}

	private FixLongViewInsetsDidChange(){
			let size = cc.view.getFrameSize();
			let router = 0;

			router = size.width > size.height ? size.width / size.height : size.height / size.width;

			if(router > 1.8 ){
				this.view["Bottom/speace"].active = true;
				let widget = this.view["Content"].getComponent(cc.Widget);
				let widget2 = this.view["TopContent"].getComponent(cc.Widget);
				let widget3 = this.view["SignInAlertView"].getComponent(cc.Widget);
				let widget4 = this.view["BelowContent"].getComponent(cc.Widget);
				let widget5 = this.view["Load"].getComponent(cc.Widget);
				widget.bottom += 34;
				widget2.bottom += 34;
				widget3.bottom += 34;
				if(cc.sys.os == cc.sys.OS_IOS || cc.ENGINE_VERSION != "2.2.2"){
					VM.setValue("core.isLongScreen",true);
					this.view["Top"].height += 44;
					widget.top += 44;
					widget4.top += 44;
					widget5.top += 44;
				}
			}
	}

	private setUIPage(){

		let temp3 = new Page();
		temp3.NAME = "Web2";
		temp3.panels.push("Web");
		temp3.panels.push("Top");
		UIAssistant.main.AddPage(temp3);

		let temp4 = new Page();
		temp4.NAME = "Web";
		temp4.panels.push("Web");

		UIAssistant.main.AddPage(temp4);
	}

    private async addPages(){
    	VM.setValue("core.lockinfunc",true);
		let content = this.view["Content"];
		let topcontent = this.view["TopContent"];

		//更新紀錄自身節點
		uictrl.prototype.onLoad.call(this);
		//刷新 UIAssistant 的 CPanel
		UIAssistant.main.ArraysConvertation();
		VM.setValue("core.lockinfunc",false);
	}

    private ideOtherPage(pagename){
		this.view["Content"].getComponentsInChildren("CPanel").forEach(function (panel) {
			if(panel.name != pagename +"<CPanel>" && panel.name != "Content<CPanel>") {
				panel.node.active = false;
			}
		});
	}

	private setToolBarBtn(){
		UI_manager.add_button_listen(this.view["Bottom/view/node/HomeBtn"],this,function () {
			if(VM.getValue("core.lockinfunc"))
				return;

			VM.setValue("core.lockinfunc",true);
			PageManager.HomePage();

			this.unluckToolbarBtns();
		});
		UI_manager.add_button_listen(this.view["Bottom/view/node/GamesBtn"],this,function () {
			if(VM.getValue("core.lockinfunc"))
				return;

			VM.setValue("core.lockinfunc",true);
			PageManager.GameHallPage()

			this.unluckToolbarBtns();
		});
		UI_manager.add_button_listen(this.view["Bottom/view/node/FreeBtn"],this,function () {
			if(VM.getValue("core.lockinfunc"))
				return;

			VM.setValue("core.lockinfunc",true);
            PageManager.BoundPage();
			VM.setValue("bound.selected","all");
			VM.setValue("bound.selectedid",0);

			this.unluckToolbarBtns();
		});
		UI_manager.add_button_listen(this.view["Bottom/view/node/OwnBtn"],this,function () {
			if(VM.getValue("core.lockinfunc"))
				return;

			VM.setValue("core.lockinfunc",true);
			if(VM.getValue("core.islogin"))
				PageManager.MemberPage();
			else
				PageManager.SignInPage();

			this.unluckToolbarBtns();
		});
		UI_manager.add_button_listen(this.view["Bottom/view/node/MoreBtn"],this,function () {
			PageManager.MorePage();
		});
	}

	private unluckToolbarBtns(){
    	setTimeout(function () {
			VM.setValue("core.lockinfunc",false);
		},500);
	}

	private setServiceBtn(){
		UI_manager.add_button_listen(this.view["Top/ServiceIcon"],this,function () {
			this.AllScreenWebUrl(VM.getValue("setting.serviceurl"));
		});
	}

	public setToolBarStatus(view:PageViewType){
		let path = "Bottom/view/node/";
    	this.setToolBarImg(path,view);
    	this.setToolBarLabelColor(path,view);
    }

    public setToolBarLabelColor(path,pv:PageViewType) {
    	let goldenString = "<color=ebd19a><b>{{0}}</b></color>";
    	let grayString = "<color=bebebe>{{0}}</color>";
		this.view[path+"HomeBtn/Label"].getComponent("VMLabel").setOriginLabelValue(pv == PageViewType.Home ? goldenString : grayString);
		this.view[path+"GamesBtn/Label"].getComponent("VMLabel").setOriginLabelValue(pv == PageViewType.GameHall ? goldenString : grayString);
		this.view[path+"FreeBtn/Label"].getComponent("VMLabel").setOriginLabelValue(pv == PageViewType.Bound ? goldenString : grayString);
		this.view[path+"OwnBtn/Label"].getComponent("VMLabel").setOriginLabelValue(pv == PageViewType.newMember ? goldenString : grayString);
		this.view[path+"MoreBtn/Label"].getComponent("VMLabel").setOriginLabelValue(pv == PageViewType.MoreView ? goldenString : grayString);
	}

	public setToolBarImg(path:string, pv:PageViewType){
    	UI_manager.setSprite(this.view[path+"HomeBtn/Background"],"nav/icon-home-"+(pv == PageViewType.Home ? "s":"n"));
		UI_manager.setSprite(this.view[path+"GamesBtn/Background"],"nav/icon-lounge-"+(pv == PageViewType.GameHall ? "s":"n"));
		UI_manager.setSprite(this.view[path+"FreeBtn/Background"],"nav/icon-bonus-"+(pv == PageViewType.Bound ? "s":"n"));
		UI_manager.setSprite(this.view[path+"OwnBtn/Background"],"nav/icon-account-"+(pv == PageViewType.newMember ? "s":"n"));
		UI_manager.setSprite(this.view[path+"MoreBtn/Background"],"nav/icon-more-"+(pv == PageViewType.MoreView ? "s":"n"));
	}

	private drawGraphics(){
    	let graphics = this.view["SignInAlertView/RegisterBtn"].getComponent(cc.Graphics);
    	graphics.roundRect(0,0,graphics.node.width,graphics.node.height,15);
    	graphics.stroke();

		var gp = this.view["VoucherAlert/main"].getComponent(cc.Graphics);
		gp.roundRect(0-gp.node.width/2,0-gp.node.height/2,gp.node.width,gp.node.height,32);
		gp.fill();

		var gp2 = this.view["Bottom/view/node/OwnBtn/alert"].getComponent(cc.Graphics);
		gp2.roundRect(0-gp2.node.width/2,0-gp2.node.height/2,gp2.node.width,gp2.node.height,8);
		gp2.fill();
	}

	private setSignInAlertViewBtnsEvent(){
    	UI_manager.add_button_listen(this.view["SignInAlertView/RegisterBtn"],null,function () {
			PageManager.SignUpPage();
		});
		UI_manager.add_button_listen(this.view["SignInAlertView/SignInBtn"],null,function () {
			PageManager.SignInPage();
		});
	}

	private setBackBtn(){
    	UI_manager.add_button_listen(this.view["Top/PreBtn"],this,function () {
    		switch (VM.getValue("core.gameType")) {
				case "SignIn":
				case "SignUp":
					PageManager.HomePage();
					break;
				case "Recharge":
				case "BetRecord":
				case PageViewType.newDetails:
					PageManager.MemberPage();
					break;
				case "AddBank":
					if(VM.getValue("addbank.userbank") == 0) {
						PageManager.MemberPage();
					}else
						UIAssistant.main.ShowPreviousPage();
					break;
				case "BetRecordNoData":
					PageManager.BetRecordPage();
					break;
				case PageViewType.RechargeQR:
				case PageViewType.RechargeFourthPay:
					PageManager.RechargePage();
					break;
				default:
					UIAssistant.main.ShowPreviousPage();

			}
		});
	}

	private setPickBtn(){
		UI_manager.add_button_listen(this.view["Top/PickerBtn"],this,function () {
			newUIDatePicker.main.showIntervalDay(null,this.pickdateType,31);
		});
	}

	public setPickDateType(Type:DateType){
    	this.pickdateType = Type;
	}

	public setShowBackBtn(isShow:Boolean){
    	this.view["Top/PreBtn"].active = isShow;
	}

	public setShowSignInAlert(isShow:Boolean){
		this.view["SignInAlertView"].active = isShow;
	}

	/***
	 * 顯示遊戲WebView
	 * @param _url
	 */
	public setWebUrl(_url){
    	console.log("setWebUrl =",_url);
    	if(!cc.sys.isNative){
			window.open(_url);
    		return;
		}

    	var state = this.SetWebviewTop();
    	this.view["Web"].getComponent(cc.WebView).url = _url;
		UIAssistant.main.ShowPage(state ? "Web2" : "Web");
		if(cc.sys.os == cc.sys.OS_ANDROID) {
			jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'setTouchBtnActive', '(Ljava/lang/String;)V', "show");
		}else if(cc.sys.os == cc.sys.OS_IOS) {
			jsb.reflection.callStaticMethod('RootViewController', 'showTouchBtn');
		}
	}

	/***
	 * 全版的WebView
	 * @param _url
	 * @constructor
	 */
	public AllScreenWebUrl(_url){
		VM.setValue("core.isallscreenwebview",true);
		this.setWebUrl(_url);
	}

	private SetWebviewTop(){
		//鎖定限制轉屏的遊戲類型(目前只有ios 手機瀏海會擋到)
		if(VM.getValue("core.isLongScreen") && cc.sys.os == cc.sys.OS_IOS) {
			let state = VM.getValue("setting.webviewblacklist").includes(VM.getValue("core.tagCode"))
				&& !VM.getValue("core.isallscreenwebview");
			this.view["Web"].getComponent(cc.Widget).top = state ? this.view["Top"].height : 0;
			return state;
		}
		return false;
	}

	/***
	 * 清空webview
	 */
	public webviewloadBlank(){
		this.view["Web"].getComponent(cc.WebView).url = "about:blank";
	}

	/***
	 * 關閉遊戲
	 * @constructor
	 */
	public CloseWebView(){
		UIAssistant.main.ShowPreviousPage();
	}

	/***
	 * 關閉遊戲內浮動按鈕
	 * @constructor
	 */
	public CloseFloatViewBtn()
	{
		if(cc.sys.isNative) {
			if (cc.sys.os == cc.sys.OS_ANDROID) {
				jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'setTouchBtnActive', '(Ljava/lang/String;)V', "hide");
			}else if (cc.sys.os == cc.sys.OS_IOS) {
				jsb.reflection.callStaticMethod('RootViewController', 'hideTouchBtn');
			}
		}
	}

	/***
	 * 設定是否顯示上方功能標題
	 * @param isShow
	 */
	public setShowTitle(isShow){
    	this.view["Top/titlemsg"].active = isShow;
	}

	/***
	 * 顯示VoucherAlert
	 * @param msg
	 * @param type
	 * @param func
	 */
	public showVoucherAlert(msg,type,func?){
    	this.view["VoucherAlert/main/msg"].getComponent(cc.RichText).string = msg;
    	UI_manager.setSprite(this.view["VoucherAlert/main/icon"],"voucher/" + (type == VoucherType.Voucher ? "money":"redpocket"));
    	UIAssistant.main.SetPanelVisible("VoucherAlert",true);
    	if(func){
			UI_manager.clear_button_listen(this.view["VoucherAlert/main/OKBtn"]);
			UI_manager.add_button_listen(this.view["VoucherAlert/main/OKBtn"],this,function () {
				func();
				this.ShowTitleAlert(TitleAlertType.Fulfill,"lang.receivesuccess");
				UIAssistant.main.SetPanelVisible("VoucherAlert",false);
			});
		}
	}

	public setShowServiceBtn(isShow){
		this.view["Top/ServiceIcon"].active = isShow;
	}

	public setShowJBIcon(isShow){
		this.view["Top/JBIcon"].active = isShow;
	}

	public setShowPicker(isShow){
    	this.view['Top/PickerBtn'].active = isShow;
	}

	public ShowTitleAlert(type:TitleAlertType,langkey:string){
		TitleAlertView.main.show(type,VM.getValue(langkey));
	}

	/***
	 * 翻轉時重新設定Canvas 長寬大小
	 * @param _dir (V = 直式，Ｌ = 橫式)
	 */
	public reSetMainWidget(_dir){
		//鎖定限制轉屏的遊戲類型
		if(VM.getValue("setting.webviewblacklist").includes(VM.getValue("core.tagCode")))
			return;

		let frameSize = cc.view.getFrameSize();
		if (_dir == 'V') {
			VM.setValue("core.isPORTRAIT",UIInterfaceOrientation.Portrait);
			console.log("set ORIENTATION_PORTRAIT");
			cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT);
			if (frameSize.width > frameSize.height)
				cc.view.setFrameSize(frameSize.height,frameSize.width);
			this.canvas.designResolution = cc.size(640,960)

			// this.canvas.fitHeight = false;
			this.canvas.fitWidth = true;
			// cc.view.setDesignResolutionSize(720, 1280, cc.ResolutionPolicy.FIXED_WIDTH);
		}
		else{
			VM.setValue("core.isPORTRAIT",UIInterfaceOrientation.All);
			console.log("set ORIENTATION_LANDSCAPE");
			cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE);
			if (frameSize.height > frameSize.width)
				cc.view.setFrameSize(frameSize.height,frameSize.width);
			this.canvas.designResolution = cc.size(960,640)

			// this.canvas.fitHeight = true;
			// this.canvas.fitWidth = false;
			// cc.view.setDesignResolutionSize(1280, 720, cc.ResolutionPolicy.FIXED_HEIGHT);
		}

		if(window.jsb) //手动调用触发 Wdiget 组件重新布局
			window.dispatchEvent(new cc.Event.EventCustom('resize', true))
	}

	/***
	 * 推播事件處理
	 * @param value
	 * @constructor
	 */
	public PushNotificationEvent(value:string){
		let data = JSON.parse(value);
		switch (data.mode) {
			case "90001":
				UpdataApp.start();
				break;
			case "90002": ///進入優惠活動
				this.GoToBoundDetailView(data.value);
				break; //開啟webview
			case "90003":
				this.setWebUrl(data.value);
				break;
		}
	}

	/***
	 * 進到優惠活動內容頁
	 * @param BoundID
	 * @constructor
	 */
	public GoToBoundDetailView(BoundID:number){
		VM.setValue("bound.boundId",BoundID);
		PageManager.BoundDetailPage();
	}

	/***
	 * 設定我的頁有未讀訊息或者未領取紅包的提示狀態
	 */
	public setMemberAlertState(){
		this.view["Bottom/view/node/OwnBtn/alert"].active = VM.getValue("member.msgcount") > 0
			|| VM.getValue("member.luckmoneycount") > 0;
	}
}
