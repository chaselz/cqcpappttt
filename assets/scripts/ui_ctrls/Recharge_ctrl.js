var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.setGateWayBtn = function () {
			UI_manager.add_button_listen(this.view["GateWayBtn"],this,function () {
				let money = this.view["moneyInputbox/Label"].getComponent(cc.Label).string;
				if(money != "" && parseInt(money) > 0){
					UIAssistant.main.SetPanelVisible("Load",true);
					ApiManager.Deposit.get.DepositMenuOne(money);
				}else
					TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.inputErr"));
			});
		};

		this.drawGraphics = function () {
			this.view["MoneyList/view/content"].children.forEach(x=>{
				let pen = x.getComponent(cc.Graphics);
				pen.roundRect(0,0,x.width,x.height,30);
				pen.stroke();
				pen.fill();
			});
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"RechargeCtrl");
		this.drawGraphics();
		this.setGateWayBtn();
	},

	start() {
		this.FixLongViewInsetsDidChange();
	},

	setShowData(data){
		let contens = this.view["paylist/view/content"].getComponentsInChildren("PayBtn");
		let difference = contens.filter((e)=>{
			return data.find(x=>x.mode == e.Mode) == undefined;
		});
		// cc.log("difference =",difference);
		data.asyncForEach(async (x) =>{
			let temp = contens.find(y=>y.Mode == x.mode);
			if(temp == null || temp == undefined)
				await this.createObj(x);
			else {
				temp.setData(x);
				temp.node.active = true;
			}
		});
		difference.forEach(x=>x.node.active = false);
	},

	setHideData(data){
		let contens = this.view["paylist/view/content"].getComponentsInChildren("PayBtn");
		data.forEach(x=>{
			let temp = contens.find(y=>y.mode == x.mode);
			if(temp != null)
				temp.Hide();
		});
	},

	FixLongViewInsetsDidChange(){
		let size = cc.view.getFrameSize();
		let router = 0;

		router = size.width > size.height ? size.width / size.height : size.height / size.width;

		if(router > 1.8){
			this.view["GateWayBtn"].getComponent(cc.Widget).bottom += 34;
			this.view["moneyInputbox"].getComponent(cc.Widget).bottom += 34;
			this.view["MoneyList"].getComponent(cc.Widget).bottom += 34;
			this.view["paylist"].getComponent(cc.Widget).bottom += 34;
			this.view["inputBg"].height += 34;
		}
	},

	async createObj(value){
		let temp = await UI_manager.create_prefab_atSync(this.view["paylist/view/content"],"PayBtn");
		temp.getComponent("PayBtn").setData(value);
		UI_manager.add_button_listen(temp,this,function () {
			if(VM.getValue("recharge.ischeckmoney")){
				VM.setValue("recharge.paytype",temp.getComponent("PayBtn").mode);
				let money = this.view["moneyInputbox/Label"].getComponent(cc.Label).string;
				VM.setValue("recharge.money",money);
				UI_manager.ShowOrCreateView(this.node.parent,"PayView",["Top","BelowContent","chooseway"]);
			}else{
				TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.inputrechargemoney"));
			}
		});
	}

});