import DropDown from "../UI/DropDown/DropDown";
import {VM} from "../modelView/ViewModel";
import DropDownOptionData from "../UI/DropDown/DropDownOptionData";
import newInputBox from "../UI/newInputBox";
import platformItem from "../Item/platformItem";
import LuckyMoneyItem from "../Item/LuckyMoneyItem";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var CtrlAssistant = require("CtrlAssistant");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.mode = -1;
		this.platform = -1;
		this.min_amount = 0;
		this.max_amount = 0;
		this.ratio = 0;
		this.flowMulti = 0;
		this.luckymoneyIndex = 0;
		this.maxPromo = 0;
		this.platformIDs = [];
		this.DropDownValueInit = function () {			//下拉初始值
			let temparr = [];
			let datatemp = new DropDownOptionData();
			datatemp.setValue(VM.getValue("lang.luckmoneyactivity"));

			let datatemp2 = new DropDownOptionData();
			datatemp2.setValue(VM.getValue("lang.voucheractivity"));
			temparr.push(datatemp);
			temparr.push(datatemp2);
			this.view["main/choose/banks/DropDown"].getComponent(DropDown).addOptionDatas(temparr);
		};

		this.setDropDownCB = function () {				
				this.view["main/choose/banks/DropDown"].getComponent(DropDown).SetSelectIndexCallBack(index=>{		//先生成一個SetSelectIndexCallBack的function  稱cb
				//在DropDown的DropDownAlertCB使用				CallBack																		等需要的時候在調用cb(方法)
				//點擊優惠卷下方 如紅包活動 優惠卷活動
				this.mode = index;
				if(index == 0){					
					this.showLuckyMoneyModeView();
				}else{
					this.showVoucherModeView();

				}
			});
			this.view["main/activity/platform/DropDown"].getComponent(DropDown).SetSelectIndexCallBack((index)=>{			//這個是？
				console.log("1");
				this.platform = index;
				this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = !this.view["main/activity/amount"].getComponent(newInputBox).getString().isNull() && index >= 0;
				//                                                          bool類型 false時 button組件禁用
			}
			);
		};

		this.showVoucherModeView = function () {
			this.view["main/choose/code"].active = true;
			this.view["main/luckymoney"].active = false;
			this.view["main/LuckyMoneyList"].active = false;
			this.view["main/NoData"].active = false;
			this.view["main/buttom"].active = true;
			this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = false;
		};

		this.showLuckyMoneyModeView = function () {
			this.luckymoneyIndex = 0;
			this.setToolbarStatusLine(0);
			this.view["main/luckymoney"].active = true;
			this.view["main/activity"].active = false;
			this.view["main/choose/code"].active = false;
			this.view["main/buttom"].active = false;
			this.view["main"].getComponent(cc.Layout).updateLayout();
			this.view["main/LuckyMoneyList"].height = this.node.height - this.view["main"].height + (VM.getValue("core.isLongScreen") ? 126 : 82);
			// this.view["main/LuckyMoneyList"].active = true;
			this.view["LoadNode"].height =  this.view["main/LuckyMoneyList"].height;
			this.showLoad();
			ApiManager.Voucher.GetLuckyMoneyList(0,1,20);
		};

		this.sentCheckVoucherCodeApi = function () {
			let code = this.view["main/choose/code/inputbox"].getComponent(cc.EditBox).string;
			ApiManager.Voucher.CheckVoucherCode(code);
		};

		this.checkCanJoinVoucherActivity = function () {
			let amount = this.view["main/activity/amount"].getComponent(newInputBox).getString();
			if(this.min_amount > parseInt(amount)){
				this.showAlertString(TitleAlertType.Warn,VM.getValue("lang.vouchermoneywarn").format(this.min_amount));
				return;
			}
			if(this.max_amount < parseInt(amount)){
				this.showAlertString(TitleAlertType.Warn,VM.getValue("lang.vouchermaxmoneywarn").format(this.max_amount));
				return;
			}
			let code = this.view["main/choose/code/inputbox"].getComponent(cc.EditBox).string;
			var key = this.platformIDs[this.platform];
			this.ReceivedBtnStatus(true);
			ApiManager.Voucher.ReceiveVoucher(code,amount,key);
		};

		this.setReceiveBtn = function () {
			UI_manager.add_button_listen(this.view["main/buttom/ReceiveBtn"],this,function () {
				if(this.platform < 0) {

					if (this.mode == 1)
						this.sentCheckVoucherCodeApi();
					else{

					}
				}else{
					this.checkCanJoinVoucherActivity();
				}
			});
		};

		this.setInputBoxEvent = function () {
			let inputbox = this.view["main/choose/code"].getComponent(newInputBox);
			inputbox.add_EditorChanged_listen(this,function (target) {
				this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = !target.string.isNull();
			});
			inputbox.add_BeginEditor_listen(this,function (target) {
				inputbox.setFocus();
			});
			inputbox.add_DidEndEditor_listen(this,function (target) {
				inputbox.Focus();
			});
			let inputbox2 = this.view["main/activity/amount"].getComponent(newInputBox);
			inputbox2.add_EditorChanged_listen(this,function (target) {
				let str = target.string;
				if(!str.isNull() && str.indexOf("." != -1)) {
					let num = parseInt(target.string);
					target.string = num.toString();
					if (target.string == "NaN")
						target.string = 0;

					if (num > this.max_amount) {
						inputbox2.setRed();
					} else if (num < this.min_amount) {
						inputbox2.setRed();
						this.setDiscountedprice(target.string);
					}else {
						inputbox2.setFocus();
						this.setDiscountedprice(target.string);
					}
					this.view["main/activity/amount/amountErr"].active = num > this.max_amount;
					this.view["main/activity/amount/amountErr2"].active = num < this.min_amount;
				}else {
					this.setDiscountedprice(0);
					inputbox2.setFocus();
					this.view["main/activity/amount/amountErr"].active = false;
					this.view["main/activity/amount/amountErr2"].active = false;
				}
				this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = !target.string.isNull() && this.platform >= 0;
			});
			inputbox2.add_BeginEditor_listen(this,function (target) {
				if(parseInt(target.string) > this.max_amount)
					inputbox2.setRed();
				else
					inputbox2.setFocus();
			});
			inputbox2.add_DidEndEditor_listen(this,function (target) {
				if(!target.string.isNull() && (parseInt(target.string) > this.max_amount || parseInt(target.string) < this.min_amount ))
					inputbox2.setRed();
				else
					inputbox2.Focus();
			});
		};

		this.setPlatformDropDown = function (ids) {
			let gameenable = VM.getValue("core.gameenable");
			ids = ids.filter((e) => {
				return gameenable.find(x=>x.provider.code == VM.getValue("provider.id"+e)) != undefined;
			});
			this.platformIDs = ids;
			this.view["main/activity/platform/DropDown"].getComponent(DropDown).clearOptionDatas();
			let temp = [];
			ids.forEach(v=>{
				let datatemp = new DropDownOptionData();
				datatemp.setValue(VM.getValue("providername.id"+v));
				temp.push(datatemp);
			});
			this.view["main/activity/platform/DropDown"].getComponent(DropDown).addOptionDatas(temp);
		};

		// update (dt) {}
		this.CreatItems = async function(path,data,func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,"LuckyMoneyItem", index, value, func);
			});

			for(let i = data.length;i<this.view[path].childrenCount;i++){
				this.view[path].children[i].active = false;
			}
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(index >= this.view[path].childrenCount) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent("Iitem").setValue(data,onClickFunc);
			}else {
				item = this.view[path].children[index];
				item.getComponent("Iitem").setValue(data);
			}

			item.active = this.luckymoneyIndex == 0 || (this.luckymoneyIndex == 1 && data.status == 3) || (this.luckymoneyIndex == 2 && data.status == 2);
		};

		this.setLuckyMoneyTabBtn = function () {
			var path = "main/luckymoney/";
			UI_manager.add_button_listen(this.view[path+"toolbar/All"],this,function () {
				if(this.luckymoneyIndex == 0)
					return;
				ApiManager.Voucher.GetLuckyMoneyList(0,1,20);
				this.showLoad();
				this.setToolbarStatusLine(0);
			});
			UI_manager.add_button_listen(this.view[path+"toolbar/Got"],this,function () {
				if(this.luckymoneyIndex == 1)
					return;
				ApiManager.Voucher.GetLuckyMoneyList(3,1,20);
				this.showLoad();
				this.setToolbarStatusLine(1);
			});
			UI_manager.add_button_listen(this.view[path+"toolbar/Never"],this,function () {
				if(this.luckymoneyIndex == 2)
					return;
				ApiManager.Voucher.GetLuckyMoneyList(2,1,20);
				this.showLoad();
				this.setToolbarStatusLine(2);
			});
		};

		this.setToolbarStatusLine = function (index) {
			this.luckymoneyIndex = index;
			this.view["main/luckymoney/line2/targetline"].runAction(cc.moveTo(0.2,((index*213.35)-213.35),0));
		};

		this.setDiscountedprice = function (amount) {
			let num = parseFloat(amount) * this.ratio / 100;
			num = Math.floor(num);
			num = num > this.maxPromo ? this.maxPromo : num;
			this.view["main/activity/Discountedprice/msg"].getComponent(cc.Label).string = num.toString();
		};
	},
	Init(){
		this.platform = -1;
		this.mode = -1;
		this.view["main/choose/banks/DropDown"].getComponent(DropDown).selected = -1;
		this.view["main/choose/banks/DropDown"].getComponent(DropDown).init(VM.getValue("lang.pleace")+VM.getValue("lang.choose"));
		this.view["main/buttom"].active = true;
		this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = false;
		this.view["main/choose"].active = true;
		this.view["main/choose/code"].active = false;
		this.view["main/activity"].active = false;
		this.view["main/luckymoney"].active = false;
		this.view["main/LuckyMoneyList"].active = false;
		this.view["main/NoData"].active = false;
		this.view["main/choose/code"].getComponent(newInputBox).Focus();
		this.view["main/choose/code"].getComponent(newInputBox).setString("");
	},
	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.discount"));
		this.Init();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"VoucherCtrl");
		this.DropDownValueInit();
		this.setDropDownCB();
		this.setReceiveBtn();
		this.setInputBoxEvent();
	},

	start() {
		this.setLuckyMoneyTabBtn();
	},

	showAlert(type,key){
		this.showAlertString(type,VM.getValue("lang."+key));
	},

	showAlertString(type,str){
		TitleAlertView.main.show(type,str);
	},

	showVorcherActiveView(data){
		this.min_amount = data.amount;
		this.max_amount = data.maxAmount;
		this.flowMulti = data.flowMulti;
		this.maxPromo = data.maxPromo;
		this.ratio = data.ratio;
		this.view["main/choose"].active = false;
		this.view["main/activity"].active = true;
		this.view["main/activity/platform/DropDown"].getComponent(DropDown).init(VM.getValue("lang.pleace")+VM.getValue("lang.choose"));
		this.view["main/activity/amount"].getComponent(newInputBox).setString("");
		this.view["main/activity/code/msg"].getComponent(cc.Label).string = this.view["main/choose/code"].getComponent(newInputBox).getString();
		this.view["main/activity/type/msg"].getComponent(cc.Label).string = VM.getValue("lang."+(this.mode == 0 ? "luckmoneyactivity":"voucheractivity"));
		this.view["main/activity/top/activitymsg"].getComponent(cc.Label).string ="{0}\n{1}".format(VM.getValue("lang.voucheralertratio").format(this.ratio,this.flowMulti),VM.getValue("lang.vouchermoneywarn").format(this.min_amount));
		this.setDiscountedprice(0);
		this.view["main/activity/amount/amountErr/info"].getComponent(cc.Label).string = VM.getValue("lang.inputamountmax").format(this.max_amount);
		this.view["main/activity/amount/amountErr2/info"].getComponent(cc.Label).string = VM.getValue("lang.inputamountmin").format(this.min_amount);
		this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = false;
		this.setPlatformDropDown(data.providerIDs);
	},

	showReceiveView(amount,type){
		let msg = VM.getValue("lang.receivevouchermsg").format(amount);
		let vc = CtrlAssistant.main.getCtrl("MainCtrl");
		vc.showVoucherAlert(msg,type,this.Init.bind(this));
	},

	showLuckyMoneyVIew(total,data){
		if(this.mode < 0)
			return;

		let status = data && data.length > 0;
		if(status)
			this.CreatItems("main/LuckyMoneyList/view/content",data,this.showLoad.bind(this));

		this.view["main/LuckyMoneyList"].active = status;
		this.view["main/NoData"].active = !status;

		this.CloseLoad();
	},

	showLoad() {
		this.view["LoadNode"].height =  this.view["main/LuckyMoneyList"].height;
		this.view["LoadNode"].active = true;
		this.view["LoadNode/Load"].getComponent(cc.Animation).play("balanceloading");
	},

	CloseLoad(){
		this.view["LoadNode"].active = false;
		this.view["LoadNode/Load"].getComponent(cc.Animation).stop("balanceloading");

	},

	ReceivedBtnStatus(isload) {
		this.view["main/buttom/ReceiveBtn"].getComponent(cc.Button).interactable = !isload;
		this.view["main/buttom/load"].active = isload;

		if(isload)
			this.view["main/buttom/load"].getComponent(cc.Animation).play("buttonload");
		else
			this.view["main/buttom/load"].getComponent(cc.Animation).stop("buttonload");

	},

});