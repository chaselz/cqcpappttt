import {VM} from "../modelView/ViewModel";
import BalanceManager from "../ManagerUI/BalanceManager";
import Awaiter from "../Extension/AwaitExtension";
import {PageViewType} from "../Enum/PageViewType";
import PageManager from "../ManagerUI/PageManager";


var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.setTopBtns = function () {

			let path = "main/view/content/Top/";
			UI_manager.add_button_listen(this.view[path+"RechargeBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.RechargePage();			//充值
			});
			UI_manager.add_button_listen(this.view[path+"WithdrawBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.WithDrawPage();			//提現
			});
			UI_manager.add_button_listen(this.view[path+"TransferBtn"],this,async function () {
				if(this.checkCanUse()) {
					PageManager.IndoorTransferPage();	//戶內轉帳
				}
			});
			UI_manager.add_button_listen(this.view[path+"moreBalanceBtn"],this,async function () {			//點擊查看所有餘額
				if(this.checkCanUse()) {
					let active = UIAssistant.main.GetPageActive("BalanceArea");				//召喚BalanceArea Cpanel
					var load = this.view[path+"moreBalanceBtn/Load"];
					load.active = true;
					load.getComponent(cc.Animation).play("buttonload");
					await Awaiter.until(_=>BalanceManager.main.isReady) ;//怎麼讀
					/*
					await Awaiter.until(
						function(_)
						{
							BalanceManager.main.isReady;
						}
					)
					*/					
					//回來做這個
					load.active = false;
					load.getComponent(cc.Animation).stop("buttonload");
					UI_manager.setSprite(this.view[path+"moreBalanceBtn/Label/icon"],"own/icon-"+(active ? "more":"close"));
					UIAssistant.main.SetPanelVisible("BalanceArea", !active);
				}
			});
			UI_manager.add_button_listen(this.view[path+"ownBtn"],this,function () {			//---右上角的更多
				if(this.checkCanUse())
					PageManager.MemberInfoPage();
			});
		};

		this.setToolsBtn = function () {
			let path = "main/view/content/List/";
			UI_manager.add_button_listen(this.view[path+"voucherBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.VoucherPage();			//優惠卷
			});
			UI_manager.add_button_listen(this.view[path+"modifyPwBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.ModifyPWPage();			//修改密碼
			});
			UI_manager.add_button_listen(this.view[path+"bankBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.BanklistPage();			//銀行帳號
			});
			UI_manager.add_button_listen(this.view[path+"msgBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.MyMessagePage();		//站內信
			});
			UI_manager.add_button_listen(this.view[path+"detailBtn"],this,function () {
				if(this.checkCanUse())
					PageManager.DetailsPage();         //資金明細
			});
		};


		this.checkCanUse = function () {			//檢查有沒有登入
			if(!VM.getValue("core.islogin")) {
				this.showAlert(TitleAlertType.Warn, "hallwarn");
				return false;
			}

			return true;
		};

		/***
		 * 調整長板畫面
		 */
		this.FixLongViewInsetsDidChange = function(){

			if(VM.getValue("core.isLongScreen")){
				let topnode = this.view["main/view/content/Top"];
				let bgnode = this.view["main/view/content/Top/accountbg"];
				topnode.height += 44;					//加長對頂部的距離  可以看得到跑馬燈的距離
				bgnode.weight += bgnode.weight * topnode.height / bgnode.height;
				//bgnode.weight = bgnode.weight + bgnode.weight * topnode.height / bgnode.height;     先乘後除 由左至右
			}
		};

		this.setBalanceViewToPage = function () {
			if(this.view["main/view/content/BalanceNode"].childrenCount == 1)
				return;
			BalanceManager.main.node.parent = this.view["main/view/content/BalanceNode"];			//在BalanceNode的子節點下掛BalanceManager的東西		//點擊查看所有餘額
		};

		this.setLoadMoneyBtn = function () {
			UI_manager.add_button_listen(this.view["main/view/content/Top/BalanceLabel/reload"],this,function () {
				if(this.checkCanUse()){
					var anim = this.view["main/view/content/Top/BalanceLabel/reload/bg"].getComponent(cc.Animation);
					var animState = anim.getAnimationState("balanceloading");
					if(!animState.isPlaying){
						animState.play();
						ApiManager.User.get.UserBalance();	//取得錢包餘額
						setTimeout(()=>{
							anim.stop("balanceloading");
						},2000);
					}
				}
			});
		};
	},

	onEnable(){
		this.setBalanceViewToPage();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"MemberCtrl");
		this.FixLongViewInsetsDidChange();
	},

	start() {
		this.setTopBtns();
		this.setToolsBtn();
		this.setLoadMoneyBtn();			//中心錢包餘額  應該是更新錢
	},

	setMode(status){
		if(!status) {
			VM.setValue("member.name", VM.getValue("lang.visitors"));
			this.view["main/view/content/Top/money"].getComponent("BhvRollNumber").value = 0;
			this.view["main/view/content/Top/money"].getComponent(cc.RichText).string = "0.00";
		}else{

		}
	},

	setContentBtnsStatus(status){

	},

	setMalletMoney(money){
		let usermoney = money.replaceAll("\,","");

		if( parseFloat(this.view["main/view/content/Top/money"].getComponent(cc.RichText).string) == parseFloat(usermoney))
			return;
		//
		let moneyLabel = this.view["main/view/content/Top/money"].getComponent("BhvRollNumber");
		moneyLabel.targetValue = parseFloat(usermoney);
	},

	setUserData(data){
	},
	/***
	 * 設定未讀取的站內信提示
	 * @param data
	 */
	setMsgAlert(data){
		var gp = this.view["main/view/content/List/msgBtn/alert"].getComponent(cc.Graphics);
		gp.node.active = data != null && data.length > 0;
		if(data){
			var label = this.view["main/view/content/List/msgBtn/alert/Label"].getComponent(cc.Label);
			label.string = data.length;
			label._forceUpdateRenderData();

			gp.clear();
			gp.node.getComponent(cc.Layout).updateLayout();
			gp.roundRect(0-gp.node.width,0-gp.node.height/2,gp.node.width,gp.node.height,16);
			gp.fill();
		}
	},
	/***
	 * 設定未領取紅包提示
	 * @param data
	 */
	setLuckMoneyAlert(data){
		var gp = this.view["main/view/content/List/voucherBtn/alert"].getComponent(cc.Graphics);
		gp.node.active = data != null && data.length > 0;
		if(data){
			var label = this.view["main/view/content/List/voucherBtn/alert/Label"].getComponent(cc.Label);
			label.string = data.length;
			label._forceUpdateRenderData();

			gp.clear();
			gp.node.getComponent(cc.Layout).updateLayout();
			gp.roundRect(0-gp.node.width,0-gp.node.height/2,gp.node.width,gp.node.height,16);
			gp.fill();
		}
	},

	setGameBalance(data){
		this.view["main/view/content/BalanceNode/BalanceArea"].getComponent(BalanceManager).setValue(data);
	},

	InitBalance(){
		this.view["main/view/content/BalanceNode/BalanceArea"].getComponent(BalanceManager).revertStatus();
	},

	showAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue("lang."+key))		//請註冊會員
	},
});