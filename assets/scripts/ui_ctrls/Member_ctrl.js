var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){

		var self = this;

		this.runGraphics = function () {
			let graphics1 = this.view["Bg/reloadBg/reloadBtn/Bg"].getComponent(cc.Graphics);
			graphics1.clear();
			graphics1.roundRect(0,0,graphics1.node.width,graphics1.node.height,25);
			graphics1.stroke();
			graphics1.fill();

			let graphics3 = this.view["TopBg/quitBtn/Bg"].getComponent(cc.Graphics);
			graphics3.clear();
			graphics3.roundRect(0,0,graphics3.node.width,graphics3.node.height,4);
			graphics3.stroke();
		};

		this.setBtnStatus = function (name,status,spritename) {
			this.view[name].getComponent(cc.Button).interactable = status;
			UI_manager.setSprite(this.view[name+"/icon"],"icon-"+spritename + (status ? "-n":"-d"));
		};

		this.setTransFoBtnColor = function (status) {
			let graphics2 = this.view["Bg/reloadBg/transferBtn"].getComponent(cc.Graphics);
			graphics2.clear();
			graphics2.roundRect(0,0,graphics2.node.width,graphics2.node.height,25);
			graphics2.fillColor = status ? VM.getValue("color.skyblue") : VM.getValue("color.lightskyblue");
			graphics2.fill();
		};

		this.setReloadBtnColor = function (status) {
			this.view["Bg/reloadBg/reloadBtn/Bg/Label"].color = status ? VM.getValue("color.black") : VM.getValue("color.gray");
			UI_manager.setSprite(this.view["Bg/reloadBg/reloadBtn/icon"],"icon-renew"+ (status ? "-n" : "-d"));
		};

		this.setWalletTextColor = function (status) {
			let color =  VM.getValue( status ? "color.darkblue" : "color.darkgray");
			this.view["Bg/packetBg/title"].color = color;
			this.view["Bg/packetBg/money"].color = color;
		};

		this.setNamePosition = function (status) {
			let widget = this.view["TopBg/Name"].getComponent(cc.Widget);
			widget.left = status ? 266 : 224;
			widget.bottom = status ? 158.68 : 123.81;

			widget.getComponent(cc.Label).string = VM.getValue("lang.visitors");
		};

		this.setReloadEvent = function () {
			UI_manager.add_button_listen(this.view["Bg/reloadBg/reloadBtn"],null,function () {
				console.log("reloadBtn");
			});
		};

		this.setTransFerEvent = function () {
			UI_manager.add_button_listen(this.view["Bg/reloadBg/transferBtn"],null,function () {
				console.log("TransFer");
			});
		};

		this.setQuitBtn = function () {
			UI_manager.add_button_listen(this.view["TopBg/quitBtn"],this,function () {
				ApiManager.Login.userlogout();
			});

		};

		this.setFeaturesBtns = function () {
			UI_manager.add_button_listen(this.view["Bg/contentBg/recharge"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"Recharge",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/withdraw"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"WithDraw",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/bankaccount"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"Banklist",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/changepw"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"ModifyPW",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/mymsg"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"MyMessage",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/dwrecord"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"DWRecords",["Top","BelowContent"]);
			});

			UI_manager.add_button_listen(this.view["Bg/contentBg/betrecord"],this,function () {
				UI_manager.ShowOrCreateView(cc.find("Canvas/Main/BelowContent"),"BetRecord",["Top","BelowContent","RecordSearch"]);
			});
		};
		/*
		 * 調整長板畫面
		 */
		this.FixLongViewInsetsDidChange = function(){
			let size = cc.view.getFrameSize();
			let router = 0;

			router = size.width > size.height ? size.width / size.height : size.height / size.width;

			if(router > 1.8){
				let m_node = this.view["TopBg"];
				let scale = m_node.width / m_node.height;
				m_node.height += 44;
				m_node.width = m_node.height * scale;
				let b_widget = this.view["Bg"].getComponent(cc.Widget);
				b_widget.top += 44;
			}
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"MemberCtrl");
		this.FixLongViewInsetsDidChange();
	},

	start() {
		this.runGraphics();
		this.setReloadEvent();
		this.setTransFerEvent();
		this.setFeaturesBtns();
		this.setQuitBtn();
	},

	setMode(status){
		this.setContentBtnsStatus(status);
		this.runGraphics(status);
		this.setTransFoBtnColor(status);
		this.setNamePosition(status);
		// this.setReloadBtnColor(status);
		// this.setWalletTextColor(status);
		this.view["TopBg/quitBtn"].active = (status || VM.getValue("core.islogin"));
		this.view["TopBg/InfoBtn"].active = status;
		this.view["TopBg/AccountId"].active = status;
	},

	setContentBtnsStatus(status){
		this.setBtnStatus("Bg/contentBg/recharge",status,"deposit");
		this.setBtnStatus("Bg/contentBg/withdraw",status,"withdrawal");
		this.setBtnStatus("Bg/contentBg/moneydetail",status,"funddetails");
		this.setBtnStatus("Bg/contentBg/betrecord",status,"betdeal");
		this.setBtnStatus("Bg/contentBg/changepw",status,"changepw");
		this.setBtnStatus("Bg/contentBg/bankaccount",status,"bankaccount");
		this.setBtnStatus("Bg/contentBg/dwrecord",status,"dnwrecord");
		this.setBtnStatus("Bg/contentBg/mymsg",status,"message");
		this.setBtnStatus("Bg/contentBg/mypush",status,"promotion");
	},

	setMalletMoney(money){
		let usermoney = money.replaceAll("\,","");
		if( parseFloat(usermoney) == 0)
			return;
		if( parseFloat(this.view["Bg/packetBg/money"].getComponent(cc.Label).string) == parseFloat(usermoney))
			return;

		let moneyLabel = this.view["Bg/packetBg/money"].getComponent("BhvRollNumber");
		moneyLabel.targetValue = parseFloat(usermoney);
	},

	setUserData(data){
		this.view["TopBg/Name"].getComponent(cc.Label).string = data.name;
		this.view["TopBg/AccountId"].getComponent(cc.Label).string = data.account;
	},

	setMsg(data){

	},

	setGameBalance(data){

	},

});