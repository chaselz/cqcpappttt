import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.bankitems = [];
		this.setItem = function (index,data) {
			let num = (index+1).toString();
			this.bankitems.push(data.id);
			UI_manager.setLabelNodeVlaue(this.view["content/bankcard"+num+"/bank"],"<b>"+data.bank_name+"-"+data.account_point+"</b>");
			UI_manager.setLabelNodeVlaue(this.view["content/bankcard"+num+"/name"],data.name);
			UI_manager.setLabelNodeVlaue(this.view["content/bankcard"+num+"/num"],data.account);
			this.view["content/bankcard"+num+"/star"].color = VM.getValue("color."+(data.bank_default == 1 ? "gold":"graylabel2"));
		};
		this.setAddBankCardBtn = function () {
			UI_manager.add_button_listen(this.view["AddBtn"],this,function () {
				PageManager.AddBankPage();
			});
		};
		this.setBankItemDefaultBtn = function () {
			for (let i = 0;i < 3;i++){
				let num = (i+1).toString();
				UI_manager.add_button_listen(this.view["content/bankcard"+num+"/star"],this,function () {
					let index = i;
					if(!VM.getValue("core.lock")) {
						VM.setValue("core.lock",true);
						ApiManager.Deposit.set.BankDefault(this.bankitems[index]);
					}
				});
			}
		};
		this.drawGraphics = function () {
			this.view["content"].children.forEach(node=>{
				node.getComponent(cc.Graphics).roundRect(0 - node.width/2,0 - node.height /2,node.width,node.height,8);
				node.getComponent(cc.Graphics).stroke();
				node.getComponent(cc.Graphics).fill();

			});
		};
	},

	Init(){
		for (let i = 0;i < 3;i++){
			this.view["content/bankcard"+(i+1)].active = false;
		}
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.managebankcard"));
		this.Init();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BankListCtrl");
	},

	start() {
		this.drawGraphics();
		this.setAddBankCardBtn();
		this.setBankItemDefaultBtn();
	},

	setBankList(data){
		if(data.length == 0)
			return;

		this.view["AddBtn"].active = data.length != 3
		this.bankitems.length = 0;
		for (let i = 0;i < 3;i++){
			this.view["content/bankcard"+(i+1)].active = i < data.length;
			if(i < data.length)
				this.setItem(i,data[i]);
		}
	},

	ShowAddBankView(){
		PageManager.AddBankPage();
	},

	ShowLoad(){
		UIAssistant.main.SetPanelVisible("Load",true);
	},

	CloseLoad(){
		UIAssistant.main.SetPanelVisible("Load",false);
	},
});