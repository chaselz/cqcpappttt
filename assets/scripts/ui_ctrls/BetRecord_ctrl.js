import DropDown from "../UI/DropDown/DropDown";
import {BetRecordBtnType} from "../Enum/BetRecordBtnType";
import DayInputBox from "../UI/DayInputBox";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");
var Page = require("Page");
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.iserr = false;
		/***
		 * 設定時間選擇器的選擇事件
		 */
		this.setDatePickEvent = function () {
			this.setDayInputBoxFinishLister("RecordSearch/content/starttime");
			this.setDayInputBoxFinishLister("RecordSearch/content/endtime");
		};
		/***
		 * 設定時間InputBox UI 結束監聽
		 */
		this.setDayInputBoxFinishLister = function (path) {
			this.view[path+"/inputbox"].getComponent(DayInputBox).finishlisten(function (date) {
				if(this.iserr){
					this.normalMode(path);
				}
			});
		};
		/***
		 * 設定搜尋按鈕
		 */
		this.setSearChBtn = function () {
			UI_manager.add_button_listen(this.view["RecordSearch/buttom/Btn"],this,function () {
				this.iserr = false;
				let dropdown= this.view["RecordSearch/content/banks/DropDown"].getComponent("DropDown");
				let start = this.view["RecordSearch/content/starttime/inputbox"].getComponent("DayInputBox").string;
				let end = this.view["RecordSearch/content/endtime/inputbox"].getComponent("DayInputBox").string;

				if(start.isNull()){
					this.iserr = true;
					this.redMode("RecordSearch/content/starttime");
				}

				if(end.isNull()){
					this.iserr = true;
					this.redMode("RecordSearch/content/endtime");
				}

				if(this.iserr){
					this.ShowAlert(TitleAlertType.Warn,"lang.inputErr");
					return;
				}

				if(Date.parse(start).valueOf() > Date.parse(end).valueOf()){
					this.iserr = true;
					this.redMode("RecordSearch/content/starttime");
					this.redMode("RecordSearch/content/endtime");
					this.ShowAlert(TitleAlertType.Warn,"lang.starttimetoobig");
					return;
				}

				let game_provider = dropdown.selected == 0 ? "" : VM.getValue("betrecord.gameproviderlist")[dropdown.selected - 1].code;
				VM.setValue("betrecord.gameprovider",game_provider);
				VM.setValue("betrecord.starttime",start);
				VM.setValue("betrecord.endtime",end);
				this.ShowByDateView();
			});
		};

		/***
		 * EditorBox 警告模式
		 */
		this.redMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.red");
			this.view[path+"/line"].color = VM.getValue("color.red");
		};
		/***
		 * EditorBox 一般狀態
		 * @param path
		 */
		this.normalMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.black");
			this.view[path+"/line"].color = VM.getValue("color.linegray");
		};
		/***
		 * 新增需要的Page
		 */
		this.setNeedPage = function () {
			this.AddPage("BetRecordNoData",false);
			this.AddPage("BetRecordDate");
			this.AddPage("BetRecordGame");
			this.AddPage("BetRecordMaker",true,"BetRecordGame");
			this.AddPage("BetRecordNum");
			this.AddPage("BetRecordInfo");
		};
		/***
		 * 新增UIAssistant Page
		 * @param name Page名稱
		 * @param show_sum_view 是否顯示子物件總和Panel
		 * @param nodename 另外顯示的Panel
		 * @constructor
		 */
		this.AddPage = function (name,show_sum_view = true,nodename = "") {
			let temp = new Page();
			temp.NAME = name;
			temp.panels.push("BetRecord");
			temp.panels.push("Top");
			temp.panels.push("BelowContent");
			if(show_sum_view) {
				temp.panels.push("BetRecordSum");
			}
			temp.panels.push(nodename.isNull() ? name : nodename);
			UIAssistant.main.AddPage(temp);
		};
		/***
		 * 設定沒有資料頁的重新搜尋按鈕
		 */
		this.setNoDataBtn = function () {
			UI_manager.add_button_listen(this.view["BetRecordNoData/ReSearchBtn"],this,()=>{
				PageManager.BetRecordPage();
			});
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let node_parent = this.view[path];
			let item = null;
			if(index >= node_parent.childrenCount)
				item =  await UI_manager.create_prefab_atSync(node_parent, itemname);
			else
				item =  node_parent.children[index];

			item.active = true;
			if(type)
				item.getComponent(itemname).setValue(data,onClickFunc,type);
			else
				item.getComponent(itemname).setValue(data,onClickFunc);
		};
		/***
		 * 繪畫所以畫GraphicsNode
		 */
		this.setGraphicsNode = function () {
			let path = "BetRecordNum/ScrollView/view/content/btns/";
			this.drawGraphics(path+"first");
			this.drawGraphics(path+"pre");
			this.drawGraphics(path+"next");
			this.drawGraphics(path+"last");
		};
		/***
		 * 畫GraphicsNode
		 * @param path
		 */
		this.drawGraphics = function (path) {
			let node = this.view[path];
			if(!node.getComponent(cc.Graphics))
				return;

			let graphics = node.getComponent(cc.Graphics);
			graphics.roundRect(0,0,node.width,node.height,4);
			graphics.fill();
			graphics.stroke();
		};
		/***
		 * 設定上下頁功能按鈕
		 */
		this.setPreAndNextBtn = function () {
			let path = "BetRecordNum/ScrollView/view/content/btns/";
			UI_manager.add_button_listen(this.view[path+"first"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == 1)
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(1);
			});

			UI_manager.add_button_listen(this.view[path+"pre"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == 1)
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.nowpage")-1);
			});

			UI_manager.add_button_listen(this.view[path+"next"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == VM.getValue("betrecord.pagecount"))
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.nowpage") +1);
			});

			UI_manager.add_button_listen(this.view[path+"last"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == VM.getValue("betrecord.pagecount"))
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.pagecount"));
			});
		};
	},
	/***
	 * 發送注單號清單 api
	 * @param page
	 * @constructor
	 */
	BetsSummaryDetailApi(page){
		VM.setValue("betrecord.nowpage",page);
		ApiManager.Report.BetsSummaryDetailNew(VM.getValue("betrecord.targettime"),8,
			VM.getValue("betrecord.targetprovider"),VM.getValue("betrecord.targetgame"),page,20);
	},
	/***
	 * 初始化搜尋頁
	 * @constructor
	 */
	InitSearchView(){
		this.view["RecordSearch/content/starttime/inputbox"].getComponent("DayInputBox").string = "";
		this.view["RecordSearch/content/endtime/inputbox"].getComponent("DayInputBox").string = "";
		this.normalMode("RecordSearch/content/starttime");
		this.normalMode("RecordSearch/content/endtime");
	},
	/***
	 * 初始化廠商類別的表單
	 * @constructor
	 */
	InitMakerView(){
		this.view["BetRecordGame/top/maker"].getComponent("VMLabel").setWatchPath("lang.gamedealer");
		VM.setValue("betrecord.mode",BetRecordBtnType.Maker);
	},
	/***
	 * 初始化遊戲類別的表單
	 * @constructor
	 */
	InitGameView(){
		this.view["BetRecordGame/top/maker"].getComponent("VMLabel").setWatchPath("lang.gamename");
		VM.setValue("betrecord.mode",BetRecordBtnType.Game);
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.betrecord"));
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BetRecordCtrl");
	},

	start() {
		this.setSearChBtn();
		this.setNeedPage();
		this.setNoDataBtn();
		this.setGraphicsNode();
		this.setPreAndNextBtn();
		this.setDatePickEvent();
	},
	/***
	 * 設定遊戲商DropDown資料
	 * @param data
	 */
	setGameProviderDropDown(data){
		let dropdown= this.view["RecordSearch/content/banks/DropDown"].getComponent("DropDown");
		dropdown.clearOptionDatas();
		dropdown.addOptionDatas(data);
		dropdown.selected = 0;
	},
	/***
	 * 設定時間排序的資料
	 * @param data
	 * @returns {Promise<void>}
	 */
	async setRecordDateItem(data){
		let path = "BetRecordDate/ScrollView/view/content";
		await data.asyncForEach(async (value,index) =>{
			await this.creatFactory(path,"BetRecordDateItem",index,value,this.ShowByMakerView);
		});

		let node_parent = this.view[path];
		for(let i = data.length;i<node_parent.childrenCount;i++){
			node_parent.children[i].active = false;
		}
		this.CloseLoadView();
	},
	/***
	 * 設定廠商排序的資料
	 * @param data
	 * @returns {Promise<void>}
	 */
	async setRecordMakerItem(data){
		let path = "BetRecordGame/ScrollView/view/content";
		await data.asyncForEach(async (value,index) =>{
			await this.creatFactory(path,"BetRecordMakerAndGameItem",index,value,this.ShowByGameView,BetRecordBtnType.Maker);
		});

		let node_parent = this.view[path];
		for(let i = data.length;i<node_parent.childrenCount;i++){
			node_parent.children[i].active = false;
		}
		this.CloseLoadView();
	},
	/***
	 * 設定遊戲類別排序資料
	 * @param data
	 * @returns {Promise<void>}
	 */
	async setRecordGameItem(data){
		let path = "BetRecordGame/ScrollView/view/content";
		await data.asyncForEach(async (value,index) =>{
			await this.creatFactory(path,"BetRecordMakerAndGameItem",index,value,this.ShowByNumView,BetRecordBtnType.Game);
		});

		let node_parent = this.view[path];
		for(let i = data.length;i<node_parent.childrenCount;i++){
			node_parent.children[i].active = false;
		}
		this.CloseLoadView();
	},
	/***
	 * 設定注單號排序的資料
	 * @param data
	 * @returns {Promise<void>}
	 */
	async setRecordNumItem(data){
		let path = "BetRecordNum/ScrollView/view/content/itemlist";
		await data.asyncForEach(async (value,index) =>{
			await this.creatFactory(path,"BetRecordNumItem",index,value,this.ShowLoadView);
		});

		let node_parent = this.view[path];
		for(let i = data.length;i<node_parent.childrenCount;i++){
			node_parent.children[i].active = false;
		}
		this.CloseLoadView();
	},
	/***
	 * 設定目前page頁籤號
	 * @param count 總和頁
	 */
	setPreAndNextView(count){
		let path = "BetRecordNum/ScrollView/view/content/btns/";
		this.view[path+"total"].getComponent(cc.Label).string = VM.getValue("betrecord.nowpage")+"/"+ count;
	},
	/***
	 * 設定清單總和數字
	 * @param bets 下注總和
	 * @param profits 輸贏總和
	 */
	setSumViewValue(bets,profits){
		this.view["BetRecordSum/betsamount"].getComponent(cc.Label).string = bets;
		this.view["BetRecordSum/profitsamount"].getComponent(cc.RichText).string = profits;
		this.view["BetRecordSum/profitsamount"].color = VM.getValue(parseFloat(profits) < 0 ? "color.red" : "color.black");
	},
	/***
	 * 顯示提示UI
	 * @param type 提示總類
	 * @param key 提示文字
	 * @constructor
	 */
	ShowAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue(key));
	},
	/***
	 * 沒有資料的畫面
	 * @constructor
	 */
	ShowNoDataView(){
		PageManager.BetRecordNoDataPage();
	},
	/***
	 * 顯示時間排序的畫面
	 * @constructor
	 */
	ShowByDateView(){
		PageManager.BetRecordDate();
	},
	/***
	 * 顯示廠商排序的畫面
	 * @constructor
	 */
	ShowByMakerView(){
		PageManager.BetRecordMakerPage();
	},
	/***
	 * 顯示遊戲類別排序的畫面
	 * @constructor
	 */
	ShowByGameView(){
		PageManager.BetRecordGamePage();
	},
	/***
	 * 顯示注單號排序的畫面
	 * @constructor
	 */
	ShowByNumView(){
		PageManager.BetRecordNumPage();
	},
	/***
	 * 顯示注單號詳細畫面
	 * @param url 清單網址
	 * @constructor
	 */
	ShowBetInfoView(url){
		let vc = CtrlAssistant.main.getCtrl("MainCtrl");
		vc.AllScreenWebUrl(url);
		VM.setValue("core.lock",false);
		this.CloseLoadView();
	},
	/***
	 * 顯示讀取畫面
	 * @constructor
	 */
	ShowLoadView(){
		UIAssistant.main.SetPanelVisible("Load",true);
	},
	/***
	 * 關閉讀取畫面
	 * @constructor
	 */
	CloseLoadView(){
		UIAssistant.main.SetPanelVisible("Load",false);
	},
});