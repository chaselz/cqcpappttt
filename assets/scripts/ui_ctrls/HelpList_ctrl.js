var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');

cc.Class({
	extends: UI_ctrl,


	ctor(){
		this.runGraphics = function () {
			let x = this.view["onlineBtn"];
			let pen = x.getComponent(cc.Graphics);
			pen.clear();
			pen.roundRect((0-x.width/2),(0-x.height/2),x.width,x.height,44);
			pen.stroke();
			pen.fill();
		};
		this.setOnlineBtn = function(){
			UI_manager.add_button_listen(this.view["onlineBtn"],this,function () {
				console.log("聯繫客服");
				CtrlAssistant.main.getCtrl("MainCtrl").AllScreenWebUrl(VM.getValue("setting.serviceurl"));
			});
		};

		this.CreatItems = async function(path, itemname, data, func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname, index, value, func);
			});
			for(let i = data.length;i<this.view[path].childrenCount;i++){
				this.view[path].children[i].active = false;
			}
		};
	
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(index >= this.view[path].getComponentsInChildren(itemname).length) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent(itemname).setValue(data,onClickFunc);
				item.active = true;
			}else {
				item = this.view[path].getComponentsInChildren(itemname)[index];
				item.getComponent(itemname).setValue(data);
				item.node.active = true;
			}
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"HelpListCtrl");
	},

	start() {
		this.runGraphics();
		this.setOnlineBtn();
	},

	/**
	 * 設定幫助中心清單
	 */
	async setHelpList(){
		let data = VM.getValue("help.list");
		if(!data)
			return;
		await this.CreatItems("scroll/view/content","HelpItem",data,null);
	},

});