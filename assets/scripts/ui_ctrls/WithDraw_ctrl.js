import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.iserr = false;

		this.setShowPWBtn = function () {
			UI_manager.add_button_listen(this.view["content/pw/inputbox/eye"],this,function () {
				let isShow = this.view["content/pw/inputbox"].getComponent(cc.EditBox).inputFlag != cc.EditBox.InputFlag.PASSWORD;
				this.view["content/pw/inputbox"].getComponent(cc.EditBox).inputFlag = isShow ? cc.EditBox.InputFlag.PASSWORD : cc.EditBox.InputFlag.DEFAULT;
				let spritename = "universal/icon-show-pw"+(isShow ? "" : "-show");
				cc.log("name = ",spritename);
				UI_manager.setSprite(this.view["content/pw/inputbox/eye/Bg"], spritename);

			});
		};
		this.setEditBoxListener = function () {
			UI_manager.add_editorbox_text_changed_listen(this.view["content/amount/inputbox"],this,function (value) {
				if(this.iserr){
					let amount = parseFloat(value.string);
					let result = amount < parseFloat(VM.getValue("withdraw.min")) || amount > parseFloat(VM.getValue("withdraw.max").replace(/,/g,""));
					if(result)
						this.redMode("content/amount");
					else
						this.normalMode("content/amount");
				}
			});

			UI_manager.add_editorbox_text_changed_listen(this.view["content/pw/inputbox"],this,function (value) {
				if(this.iserr){
					if(value.string.length < 4 || value.string.length > 30)
						this.PwErrorMode();
					else
						this.normalMode("content/pw");

				}
			});
		};
		this.setWithDrawBtn = function () {
			UI_manager.add_button_listen(this.view["buttom/Btn"],this,function () {
				this.iserr = false;
				let amountEditorbox = this.view["content/amount/inputbox"].getComponent(cc.EditBox);
				let amount = parseFloat(amountEditorbox.string);

				if(amountEditorbox.string.isNull() || amount < parseFloat(VM.getValue("withdraw.min")) || amount > parseFloat(VM.getValue("withdraw.max").replace(/,/g,""))){
					this.iserr = true;
					this.redMode("content/amount");
				}
				let pwEditorbox = this.view["content/pw/inputbox"].getComponent(cc.EditBox);
				if(pwEditorbox.string.length < 4 || !pwEditorbox.string.hasNumber()){
					this.iserr = true;
					this.PwErrorMode();
				}

				if(this.iserr){
					this.showAlertErr();
					return;
				}
				VM.setValue("core.lock",true);
				ApiManager.WithDraw.get.Provider(VM.getValue("withdraw.bankid"),this.view["content/pw/inputbox"].getComponent(cc.EditBox).string,amount);
			});
		};
		/***
		 * 顯示錯誤的警告
		 */
		this.showAlertErr = function(){
			this.ShowAlert(TitleAlertType.Warn,"lang.inputErr");
		};
		/***
		 * EditorBox 警告模式
		 */
		this.redMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.red");
			this.view[path+"/top/warn"].color = VM.getValue("color.red");
			this.view[path+"/line"].color = VM.getValue("color.red");
		};
		/***
		 * EditorBox 一般狀態
		 * @param path
		 */
		this.normalMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.graylabel3");
			this.view[path+"/top/warn"].color = VM.getValue("color.graylabel3");
			this.view[path+"/line"].color = VM.getValue("color.linecolor");
		};
		this.init = function () {
			this.normalMode("content/pw");
			this.normalMode("content/amount");
			this.iserr = false;
			UI_manager.setLabelNodeVlaue(this.view["content/pw/inputbox"],"");
			UI_manager.setLabelNodeVlaue(this.view["content/amount/inputbox"],"");
		}
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.withdraw"));
		this.init();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"WithDrawCtrl");
	},

	start() {
		this.setShowPWBtn();
		this.setEditBoxListener();
		this.setWithDrawBtn();
	},

	ShowAddBankView(){
		PageManager.AddBankPage();
	},

	SetBankName(text){
		console.log(text);
		this.view["content/bank/Label"].getComponent(cc.Label).string = text;
	},

	PwErrorMode(){
		this.redMode("content/pw");
	},

	ShowAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue(key));
	}

});