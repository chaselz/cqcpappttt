import {VM} from "../modelView/ViewModel";
import AwaitLock from "../Extension/AwaitLock";
import Iitem from "../Item/Iitem";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");


cc.Class({
	extends: UI_ctrl,


	ctor(){
		this.lock = null;
		this.adlock = null;
		this.adTimer = null;
		this.winnerTimer = null;

		this.setMoreBtn = function () {

			UI_manager.add_button_listen(this.view["main/view/content/AdSpeace/moreBtn"],this,function () {
				PageManager.BulletinPage();
			});
		};

		this.CreatItems2 = async function(lockObj,data,path,itemname,countKey,func) {
			await lockObj.acquireAsync();
			try {
				await data.asyncForEach(async (value) =>{
					let index = VM.getValue("home."+countKey);
					await this.creatFactory(path,itemname,index,value,func);
					VM.setValue("home."+countKey,++index);
				});
			} finally {

				let pageview = this.view[path].getComponent(cc.PageView);
				let pages = pageview.getPages();
				let pagesNum = pages.length;
				for(let i = data.length;i<pagesNum;i++){
					pageview.removePage(pages[pages.length-1]);
				}
				lockObj.release();
			}
		};

		this.CreatItems = async function(data,path,itemname,func) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname,index,value,func);
			});

			let pageview = this.view[path].getComponent(cc.PageView);
			let pages = pageview.getPages();

			for(let i = data.length;i<pages.length;i++){
				pageview.removePage(pages[pages.length - 1]);
			}
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let pageview = this.view[path].getComponent(cc.PageView);
			let node_parent = this.view[path+"/view/content"];
			let item = null;
			if(index >= node_parent.childrenCount) {
				item = await UI_manager.create_item_Sync_NoParent(itemname);
				pageview.addPage(item);
			}else
				item = node_parent.children[index];
			item.active = true;
			if(type)
				item.getComponent(itemname).setValue(data,onClickFunc,type);
			else
				item.getComponent(itemname).setValue(data,onClickFunc);
		};

		this.bulletinItem = function (index,title,time) {
			let path = "main/view/content/BulletinPage/adText"+index+"/";
			this.view[path+"title"].getComponent(cc.Label).string = title;
			if(time)
				this.view[path+"time"].getComponent(cc.Label).string = time.GameTimeOnyDay();
			else
				this.view[path+"time"].getComponent(cc.Label).string = "";
		};

		this.startSlipAd = function () {
			let pageView = this.view["main/view/content/ADPage"].getComponent(cc.PageView);
			this.adTimer = setInterval(function () {
				let count = pageView.getPages().length;
				if(count < 2)
					return;

				let index = pageView.getCurrentPageIndex();
				index = ((index < count) && (index + 1 !== count)) ? (index + 1) : 0;
				pageView.scrollToPage(index, 1);
			},6000);
		};

		this.endSlipAd = function () {
			clearInterval(this.adTimer);
		};

		this.startSlipWinner = function () {
			let pageView = this.view["main/view/content/WinnerList/main"].getComponent(cc.PageView);
			this.winnerTimer = setInterval(function () {
				let count = pageView.getPages().length;
				if(count < 6)
					return;

				let index = pageView.getCurrentPageIndex();
				//當前的資料會跑到最上面，所以要多扣除後面 4 筆資料，讓輪播圖看起來比較流暢
				count -= 4;
				index = ((index < count) && (index + 1 !== count)) ? (index + 1) : 0;
				// cc.log("SlipWinner index = ",index);
				pageView.scrollToPage(index, 1);
			},6000);
		};

		this.endSlipWinner = function () {
			clearInterval(this.winnerTimer);
		};

		this.UIInit = function () {
			///移除 PageView的觸碰滑動功能
			let pageview = this.view["main/view/content/WinnerList/main"];
			pageview.off(cc.Node.EventType.TOUCH_START,  pageview._onTouchBegan,  pageview, true);
			pageview.off(cc.Node.EventType.TOUCH_MOVE,  pageview._onTouchMoved,  pageview, true);
			pageview.off(cc.Node.EventType.TOUCH_END,  pageview._onTouchEnded,  pageview, true);
			pageview.off(cc.Node.EventType.TOUCH_CANCEL,  pageview._onTouchCancelled,  pageview, true);

			this.bulletinItem(0,"","");
			this.bulletinItem(1,"","");
			this.bulletinItem(2,"","");
		};

		this.setBulletinBtns = function () {
			let path = "main/view/content/BulletinPage/adText";
			for(let i = 0;i<3;i++){
				UI_manager.add_button_listen(this.view[path+i],this,function () {
					if(VM.getValue("bulletin.bulletinlist")[i]) {
						VM.setValue("bulletin.bulletinindex", VM.getValue("bulletin.bulletinlist")[i]);
						PageManager.BulletinDetailPage();
					}
				});
			}
		};

		this.setHotActivity = function (data) {
			let node = this.view["main/view/content/Hot"];
			data.forEach((value,index)=>{
				node.children[index].getComponent(Iitem).setValue(value,null);
			});
		}
	},

	onEnable(){
		this.startSlipAd();
		this.startSlipWinner();
	},

	onDisable(){
		this.endSlipAd();
		this.endSlipWinner();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"HomeCtrl");
		this.adlock = new AwaitLock();
		this.lock = new AwaitLock();
	},

	start() {
		this.setMoreBtn();
		this.setBulletinBtns();
	},
	/***
	 * 設定活動廣告
	 * @param data
	 */
	async setADPage(data){
		await this.CreatItems2(this.adlock,data,"main/view/content/ADPage","AdPageItem","adcount",null);
		let gp = this.view["main/view/content/ADPage/bg"].getComponent(cc.Graphics);
		gp.node.width= this.view["main/view/content/ADPage/indicator"].width + 16;
		gp.clear();
		gp.roundRect(0-gp.node.width/2,0-gp.node.height /2 ,gp.node.width,gp.node.height,7);
		gp.fill();
	},
	/***
	 * 設定輸贏清單
	 * @param data
	 */
	setGameWinnerList(data){
		this.CreatItems2(this.lock,data,"main/view/content/WinnerList/main","WinnerItem","winnercount",null);
	},
	/***
	 * 設定火爆
	 * @param data
	 */
	setHotPage(data){
		this.setHotActivity(data);
	},
	/***
	 * 設定公告
	 * @param data
	 */
	setBulletinList(data){
		data.forEach((temp,index)=>{
			this.bulletinItem(index,temp.title,temp.published_at);
		});
	},

	Init(){
		VM.setValue("home.adcount",0);
		VM.setValue("home.winnercount",0);
		this.UIInit();
	},
});