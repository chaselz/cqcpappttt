import {VM} from "../modelView/ViewModel";
import ClassicItem from "../Item/ClassicItem";
import Awaiter from "../Extension/AwaitExtension";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var CtrlAssistant = require("CtrlAssistant");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.setGameListBtns = function () {
			this.setGameTypeBtn("casino",2);//電子
			this.setGameTypeBtn("sport",5);	//體育
			this.setGameTypeBtn("live",4);//真人
			this.setGameTypeBtn("fish",6);//捕魚
			this.setGameTypeBtn("chesscard",3);//棋牌
			this.setGameTypeBtn("lottery",1);//彩票
		};

		this.setGameTypeBtn = function (type,tagCode) {
			//							lottery, 1
			let path = "main/view/content/GameTypeList/";
			UI_manager.add_button_listen(this.view[path+type],this,function () {
				if(tagCode == 6){ ///先禁止捕魚功能
					this.ShowAlert(TitleAlertType.Warn,"newfuncnotopen");
					return;
				}
				this.ToGamesView(tagCode);
			});
		};

		this.ToGamesView = function (tagCode) {
			VM.setValue("core.tagCode",tagCode);
			PageManager.GamesPage();	//跳頁面
		};

		this.setPlatformBtn = function () {		//推薦平台
			UI_manager.add_button_listen(this.view["main/view/content/RecommendBtn"],this,async function () {
				if(VM.getValue("core.islogin")) {	//假如登入
					this.ShowLoad();
					VM.setValue("core.tagCode",2);		//電子					
					VM.setValue("hall.gameListLoading","GameHall");	
					await Awaiter.until(_ =>VM.getValue("gamehall.cq9code"));			//引用ts需要用class		//意思
					//目前存取到 -1 會表示電子類裡沒有CQ9
					if(VM.getValue("gamehall.cq9code") != -1) {		//如果有cq9遊戲		
						VM.setValue("hall.gameId", VM.getValue("gamehall.cq9code"));
						ApiManager.Game.get.GameList(VM.getValue("gamehall.cq9code"));		//取得站台遊戲清單、期數資訊
						return;
					}
					//沒有cq9才會往下執行
					this.CloseLoad();
					this.ShowAlert(TitleAlertType.Warn,"gameclose");
				}else
					this.ShowAlert(TitleAlertType.Warn,"hallwarn");
			});
		};

	},

	onLoad() {
		ApiManager.Game.get.GameProviderList(2);		//取得站台遊戲種類的廠商清單
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"GameHallCtrl");
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.gamehall"));
		VM.setValue("core.tagCode",-1);			//-1最好用enum列舉
	},

	start() {
		this.setGameListBtns();
		this.setPlatformBtn();
	},

	setRecommendData(data){
		let path = "main/view/content/ClassicList/ClassicItem";
		for(let i=0;i<4;i++){
			if(i < data.length){
				this.view[path+i].getComponent(ClassicItem).setValue(data[i],this.ShowLoad);
			}else{
				this.view[path+i].active = false;
			}
		}
	},

	ShowLoad(){
		UIAssistant.main.SetPanelVisible("Load",true);
	},

	CloseLoad(){
		UIAssistant.main.SetPanelVisible("Load",false);
	},

	ShowAlert(type,key){		//顯示警告
		TitleAlertView.main.show(type,VM.getValue("lang."+key));
	}

});