import BhvRollNumber from "../../Behavior/ui/BhvRollNumber";
import {VM} from "../modelView/ViewModel";
import ChooseViewManager from "../ManagerUI/ChooseViewManager";
import BalanceManager from "../ManagerUI/BalanceManager";
import Awaiter from "../Extension/AwaitExtension";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.isupdata = false;
	    this.mode = transferViewStatus.TransferView;
		this.Init = function () {
			VM.setValue("core.title",VM.getValue("lang.indoortransfer"));
			this.setTransFerBtnStatus("ToutBtn",false);
			this.setTransFerBtnStatus("TinBtn",false);
            this.mode = transferViewStatus.TransferView;
			this.view["chooseview"].getComponent(ChooseViewManager).Init();
			this.view["main/view/content/main/moneyInput"].getComponent(cc.EditBox).string = "";
			this.view["transferBtn"].getComponent(cc.Button).interactable = false;
			this.clearAmountInput();
		};

		this.setTransFerBtnStatus = function (btn_name,is_choose) {
			this.view["main/view/content/main/"+btn_name+"/choosed"].active = is_choose;
			this.view["main/view/content/main/"+btn_name+"/choose"].active = !is_choose;
			this.setChooseBtnLoadState("main/view/content/main/"+btn_name,false);
		};

		this.clearAmountInput = function () {
			this.view["main/view/content/main/moneyInput"].getComponent(cc.EditBox).string = "";
		};

		this.setAllBtns = function () {
			var path = "main/view/content/";
			UI_manager.add_button_listen(this.view[path+"main/moneyInput/clear"],this,function () {
				this.clearAmountInput();
			});

			UI_manager.add_button_listen(this.view[path+"main/ToutBtn"],this,function () {
				this.checkShowChoosePlatformView(path+"main/ToutBtn");
			});

			UI_manager.add_button_listen(this.view[path+"main/TinBtn"],this,function () {
				this.checkShowChoosePlatformView(path+"main/TinBtn");
			});

			UI_manager.add_button_listen(this.view[path+"main/title/loadNode/load"],this,function () {
				var anim = this.view[path+"main/title/loadNode/load/bg"].getComponent(cc.Animation);
				var animState = anim.getAnimationState("balanceloading");
				if(!animState.isPlaying){
					animState.play();
					ApiManager.User.get.UserBalance();
					setTimeout(()=>{
						anim.stop("balanceloading");
					},2000);
				}
			});

			UI_manager.add_button_listen(this.view["transferBtn"],this,function () {
			    if(this.mode == transferViewStatus.ChooseMakerView) {
                    this.view["chooseview"].getComponent(ChooseViewManager).Close();
                }else
                    this.isTransferMode();
			});

			UI_manager.add_button_listen(this.view[path+"main/moreBtn"],this,async function () {
				let active = UIAssistant.main.GetPageActive("BalanceArea");
				var load = this.view[path+"main/moreBtn/Load"];
				load.active = true;
				load.getComponent(cc.Animation).play("buttonload");
				await Awaiter.until(_=>BalanceManager.main.isReady) ;
				load.active = false;
				load.getComponent(cc.Animation).stop("buttonload");
				UI_manager.setSprite(this.view[path+"main/moreBtn/arrow"],"own/icon-"+(active ? "more":"close"));
				UIAssistant.main.SetPanelVisible("BalanceArea", !active);
			});
		};

		this.isTransferMode = function () {
            let amount = this.view["main/view/content/main/moneyInput"].getComponent(cc.EditBox).string;
            var fromData = VM.getValue("transfer.from");
            var toData = VM.getValue("transfer.to");
            if(amount.isNull() || !fromData || !toData){
                this.showAlert(TitleAlertType.Warn,"inputErr");
                return;
            }

            var err_510007;
            cc.log("from =",fromData);
            if(fromData.status == 1 && fromData.betPercent < 100 && parseFloat(fromData.amount) > 5)
                err_510007 = fromData;
            else if(!err_510007 && toData.status == 1 && toData.betPercent < 100 && parseFloat(toData.amount) > 5)
                err_510007 = toData;

            if(err_510007){
                let msgcontent = VM.getValue("lang.balancelockerr");
                this.showAlertWithstr(TitleAlertType.Warn,msgcontent.format(err_510007.mustBet,err_510007.validBet,err_510007.mustBet-err_510007.validBet));
                return;
            }
            this.LoadActive(true);
            ApiManager.User.set.ExchangeRedeem(VM.getValue("transfer.from").id,VM.getValue("transfer.to").id,parseFloat(amount));
        };
        /***
         * 檢查是否能開啟 ChoosePlatformView
         * @param path
         * @returns {Promise<void>}
         */
		this.checkShowChoosePlatformView = async function (path) {
			if(!this.view["chooseview"].getComponent(ChooseViewManager).active) {
				this.setChooseBtnLoadState(path,true);
			    await Awaiter.until(_=>this.view["chooseview"].getComponent(ChooseViewManager).isready || !this.node.active);
			    if(!this.node.active)
			    	return;
			    this.view["chooseview"].getComponent(ChooseViewManager).Show();
				this.setChooseBtnLoadState(path,false);
                if(!this.view["chooseview"].getComponent(ChooseViewManager).active)
                	this.checkShowChoosePlatformView(path);
            }

		};

		this.setChooseBtnLoadState = function (path,load) {
			let btn = this.view[path].getComponent(cc.Button);
			btn.interactable= !load;
			this.view[path+"/Load"].active = load;
			let anim = this.view[path+"/Load"].getComponent(cc.Animation);
			if (load)
				anim.play("buttonload");
			else
				anim.stop("buttonload");
		};

		this.setChooseViewCB = function () {
			this.view["chooseview"].getComponent(ChooseViewManager).setCallBack(this.chooseCallBack.bind(this));
		};

		this.chooseCallBack = function (value) {
			this.setTranferBtnValue("TinBtn",value[1]);
			this.setTranferBtnValue("ToutBtn",value[0]);
			this.checkTransferBtnStatus();
		};

		this.setTranferBtnValue = function (name,value) {
			var path = "main/view/content/main/"+name;
			this.view[path+"/choose"].active = !value;
			this.view[path+"/choosed"].active = value;
			if(!value) {
                VM.setValue("transfer."+(name == "ToutBtn" ? "from" : "to"),"");
                return;
            }
			this.view[path+"/choosed/money"].getComponent(cc.Label).string = VM.getValue("lang.balance")+ " " +value.amount;
			this.view[path+"/choosed/name"].getComponent(cc.RichText).string = value.name;
			if(value.sprite) {
				this.view[path + "/choosed/icon"].getComponent(cc.Sprite).spriteFrame = value.sprite;
				delete value["sprite"];
			}
			VM.setValue("transfer."+(name == "ToutBtn" ? "from" : "to"),value);
		};
		this.setBalanceViewToPage = function () {
			if(this.view["main/view/content/BalanceNode"].childrenCount == 1)
				return;
			BalanceManager.main.node.parent = this.view["main/view/content/BalanceNode"];
		};
		this.setChooseViewUI = function () {
			this.view["test"].getComponent(cc.Widget).updateAlignment();
			this.view["chooseview"].getComponent(ChooseViewManager).setHeightAndPosition(this.view["test"].height,this.view["test"].position.y);
		};
        /***
         * 監聽Money Editor 事件
         */
		this.setMoneyChangeEvent = function () {
            UI_manager.add_editorbox_text_changed_listen(this.view["main/view/content/main/moneyInput"],this,function (editor) {
            	editor.string =  parseInt(editor.string).toString();
            	if(editor.string == "NaN")
            		editor.string = 0;
            });

            UI_manager.add_editorbox_text_did_ended_listen(this.view["main/view/content/main/moneyInput"],this,function (editor) {
                this.checkTransferBtnStatus();
            });
        };
        /***
         * 改變TransferBtn 模式
         * @param type
         */
		this.changetTransferBtnStatus = function (type) {
		    let key = "lang.";
           if(type == transferViewStatus.ChooseMakerView){
               this.view["transferBtn"].getComponent(cc.Button).interactable = true;
               key += "determine";
           }else{
               this.checkTransferBtnStatus();
               key += "immediatelytransfer";
           }
           this.view["transferBtn/Bg/Label"].getComponent("VMLabel").setWatchArrValue([key]);
           this.mode = type;

        };
        /***
         * 檢查Btn狀態
         */
		this.checkTransferBtnStatus = function () {
		    if(this.mode == transferViewStatus.ChooseMakerView)
		        return;

		    let status = !this.view["main/view/content/main/moneyInput"].getComponent(cc.EditBox).string.isNull() && VM.getValue("transfer.from") && VM.getValue("transfer.to");
            this.view["transferBtn"].getComponent(cc.Button).interactable = status;
        };

		this.setChoosePlatFormViewActiveLisitener = function () {
            this.view["chooseview"].getComponent(ChooseViewManager).viewActiveLisitener((active)=>{
                this.changetTransferBtnStatus(active?transferViewStatus.ChooseMakerView:transferViewStatus.TransferView);
                this.checkTransferBtnStatus();
            });
        };

		this.setTransFerFromAndToBtn = function () {
			UI_manager.add_button_listen(this.view["main/view/content/main/transferBtn"],this,function () {
				this.view["chooseview"].getComponent(ChooseViewManager).changeTransferID();
			});
		};
		/***
		 * 更新選擇上的廠商資料
		 * @param data
		 */
		this.updateChoosePlatform = function (data) {
			var fromData = VM.getValue("transfer.from");
			if(fromData) {
				if (fromData.id == "center") {
					this.view["main/view/content/main/ToutBtn/choosed/money"].getComponent(cc.Label).string = VM.getValue("lang.balance") + " " + VM.getValue("member.cash");
				} else {
					let indexdata = data.find(x => x.providerID == VM.getValue("providerint."+fromData.id));
					if (indexdata) {
						fromData.amount = indexdata.balance.formatNumber(2);
						this.view["main/view/content/main/ToutBtn/choosed/money"].getComponent(cc.Label).string = VM.getValue("lang.balance") + " " + indexdata.balance.formatNumber(2);
					}
				}
			}
			var toData = VM.getValue("transfer.to");
			if(toData){
				if (toData.id == "center") {
					this.view["main/view/content/main/TinBtn/choosed/money"].getComponent(cc.Label).string = VM.getValue("lang.balance") + " " + VM.getValue("member.cash");
				} else {
					let indexdata = data.find(x => x.providerID == VM.getValue("providerint."+toData.id));
					if (indexdata) {
						toData.amount = indexdata.balance.formatNumber(2);
						this.view["main/view/content/main/TinBtn/choosed/money"].getComponent(cc.Label).string = VM.getValue("lang.balance") + " " + indexdata.balance.formatNumber(2);
					}
				}
			}
		};

	},

	onEnable(){
		this.setBalanceViewToPage();
		this.view["main/view/content/main/money/label"].getComponent(BhvRollNumber).value = 0;
		this.Init();
		this.setUserMoney(VM.getValue("member.cash"))
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"IndoorTransferCtrl");
	},

	start() {
		this.setAllBtns();
		this.setChooseViewCB();
		this.setChooseViewUI();
		this.setMoneyChangeEvent();
		this.setChoosePlatFormViewActiveLisitener();
		this.setTransFerFromAndToBtn();
	},

	onDisable(){

	},

	setUserMoney(amount){
		let usermoney = amount.toString().replaceAll("\,","");

		if( parseFloat(this.view["main/view/content/main/money/label"].getComponent(cc.RichText).string) == parseFloat(usermoney))
			return;

		this.view["main/view/content/main/money/label"].getComponent(BhvRollNumber).targetValue = parseFloat(usermoney);
	},

	setGameBalance(data){
		this.updateChoosePlatform(data);
		this.view["chooseview"].getComponent(ChooseViewManager).setValue(data);
		if(this.isupdata){
			this.isupdata = false;
			this.LoadActive(false);
		}

	},

	showAlert(type,key){
		this.showAlertWithstr(type,VM.getValue("lang."+key));
	},
	showAlertWithstr(type,str){
		TitleAlertView.main.show(type,str);
	},

	LoadActive(_active){
		UIAssistant.main.SetPanelVisible("Load",_active);
	},

	transferFinish(){
		this.view["main/view/content/main/moneyInput"].getComponent(cc.EditBox).string = "";
		this.isupdata = true;
		setTimeout(()=>{
			this.LoadActive(false);
		},3000);
	},

});

const transferViewStatus = Object.freeze({
    ChooseMakerView: Symbol(1),
    TransferView: Symbol(2),
});