import {VM} from "../modelView/ViewModel";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import PageManager from "../ManagerUI/PageManager";
var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.setBtns = function () {
			UI_manager.add_button_listen(this.view["main/helpBtn"],this,function () {
				PageManager.HelpCenterPage();
			});

			UI_manager.add_button_listen(this.view["main/joinBtn"],this,function () {
				PageManager.AgentJoinPage();
			});
		};

		this.FixLongViewInsetsDidChange = function(){

			if(VM.getValue("core.isLongScreen")){
				this.view["main"].getComponent(cc.Widget).top += 44;
			}
		};

		this.updateversion = function () {
			this.view["version"].getComponent(cc.Label).string = "ver."+MobileExtension.GetVersion() + " - " + cc.game.resver;
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		this.FixLongViewInsetsDidChange();
		this.updateversion();
	},

	start() {
		this.setBtns();
	},

});