import VMLabel from "../modelView/VMLabel";
import DWDetail from "../AbstractClass/Detail/DWDetail";
import {IDetail} from "../AbstractClass/Detail/IDetail";
import FundsDetail from "../AbstractClass/Detail/FundsDetail";
import BetDetail from "../AbstractClass/Detail/BetDetail";
import MenuInputBox from "../UI/MenuInputBox";
import {DetailType} from "../Enum/DetailType";
import CheckMenuInputBox from "../UI/CheckMenuInputBox";
var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.target_Detail = null;
		this.Details = [];
		this.tyep = DetailType.FOUNDS;
		this.Init = function () {
			this.view["timeNode/time"].getComponent(cc.Label).string = "";
		};
		/***
		 * 設定報表類別 UI 顯示
		 * @param type
		 */
		this.setToolBar = function (type) {
			this.view["toolbar"].children.forEach((value,index)=>{
				value.getComponentInChildren(VMLabel).setOriginLabelValue((index == type ? "<color=ebd19a><b>{{0}}</b></color>":"{{0}}"));
			});
			this.view["choose-line/choose"].runAction(cc.moveTo(0.2,(type-1)*213,0));
			switch (type) {
				case DetailType.BET:
					VM.setValue("core.title",VM.getValue("lang.betrecord"));
					break;
				case DetailType.FOUNDS:
					VM.setValue("core.title",VM.getValue("lang.founsdetails"));
					break;
				case DetailType.DW:
					VM.setValue("core.title",VM.getValue("lang.accessrecord"));
					this.Init();
					break;
			}

		};
		/***
		 * 設定報表類別Btn
		 */
		this.setToolBarBtn = function () {
			this.view["toolbar"].children.forEach((value,index)=>{
				UI_manager.add_button_listen(value,this,function () {
					this.changeMode(index);
				});
			});
		};

		this.changeMode = function (mode) {
			if(VM.getValue("core.lockinfunc"))
				return;
			this.unluckToolbarBtns();
			this.target_Detail = this.Details[mode];
			this.target_Detail.Init();
			this.setToolBar(mode);
			this.showRunView(mode);
			this.showDatePickerBtn(mode);
		};

		this.unluckToolbarBtns = function () {
			VM.setValue("core.lockinfunc",true);
			setTimeout(function () {
				VM.setValue("core.lockinfunc",false);
			},500);
		};

		this.showRunView = function (type) {
			this.tyep = type;
			this.view["DWView"].active = DetailType.DW == type;
			this.view["BetView"].active = DetailType.BET == type;
			this.view["FundsView"].active = DetailType.FOUNDS == type;
		};

		/***
		 * 繪製Graphics物件
		 */
		this.drawGraphics = function () {
			let gp = this.view["DWView/statustoolbar/statusBtns"];
			gp.getComponent(cc.Graphics).roundRect(0-gp.width/2,0-gp.height/2,gp.width,gp.height,16);
			gp.getComponent(cc.Graphics).fill();

			gp = this.view["DWView/statustoolbar/statusBtns/target"];
			gp.getComponent(cc.Graphics).roundRect(0-gp.width/2,0-gp.height/2,gp.width,gp.height,12);
			gp.getComponent(cc.Graphics).fill();
		};
		this.InitDetailMode = function () {
			this.Details.push(new FundsDetail(this.view));
			this.Details.push(new BetDetail(this.view));
			this.Details.push(new DWDetail(this.view));
		};

		this.showDatePickerBtn = function (type) {
			var vc = CtrlAssistant.main.getCtrl("MainCtrl");
			vc.setShowPicker(type != DetailType.DW);
		};
	},

	onEnable(){
		if(VM.getValue("core.defaultdetail")) {
			var mode = VM.getValue("core.defaultdetail");
			this.target_Detail = this.Details[mode];
			this.setToolBar(mode);
			this.showRunView(mode);
			this.showDatePickerBtn(mode);
		}else {
			if (this.target_Detail)
				this.target_Detail.onEnable();
			
			this.showDatePickerBtn(this.tyep);
		}

		cc.director.getCollisionManager().enabled = true;
		// cc.director.getCollisionManager().enabledDebugDraw = true;
		// cc.director.getCollisionManager().enabledDrawBoundingBox = true;
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"newDetailsCtrl");
		this.InitDetailMode();
		this.setToolBarBtn();
		this.drawGraphics();
	},

	start() {

	},

	onDisable(){
		cc.director.getCollisionManager().enabled = false;
		cc.director.getCollisionManager().enabledDebugDraw = false;
		VM.setValue("core.defaultdetail",undefined);
	},

	InitPage(){
		this.setToolBar(this.tyep);
		this.target_Detail = this.Details[this.tyep];
		this.showRunView(this.tyep);
		this.target_Detail.Init();
	},

	setDWData(data){
		if(this.target_Detail)
			this.target_Detail.setData(DetailType.DW,data);
	},

	setBetDate(data){
		if(this.target_Detail)
			this.target_Detail.setData(DetailType.BET,data);
	},

	setFundsDate(data){
		if(this.target_Detail)
			this.target_Detail.setData(DetailType.FOUNDS,data);
	},

	/***
	 * 設定遊戲商DropDown資料
	 * @param data
	 */
	setGameProvider(data){
		let inputbox= this.view["BetView/Input"].getComponent(MenuInputBox);
		inputbox.setData(data);
		inputbox.setInitSwtich(0);
	},

	/***
	 * 設定遊戲商DropDown資料
	 * @param data
	 */
	setFundsMenuData(data){
		let inputbox= this.view["FundsView/Input"].getComponent(CheckMenuInputBox);
		inputbox.setData(data);
		inputbox.setInitCheck();
	},

	setLoadActive(_active){
		this.target_Detail.setLoad(_active);
	},

	showNoDataView(_active){
		this.target_Detail.setNoDataActive(_active);
	},

	showAlert(type,str){
		TitleAlertView.main.show(type,str);
	}
});


