import {PageViewType} from "../Enum/PageViewType";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");
var CtrlAssistant = require("CtrlAssistant");
var Page = require("Page");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");
			
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		//設定遊戲類別項目按鈕
		this.setGameListBtns = function () {
			this.setGameTypeBtn("all",0);
			this.setGameTypeBtn("lottery",1);
			this.setGameTypeBtn("casino",2);
			this.setGameTypeBtn("chesscard",3);
			this.setGameTypeBtn("live",4);
			this.setGameTypeBtn("sport",5);
			this.setGameTypeBtn("fish",6);
		};
		this.setGameTypeBtn = function (type,tagCode) {				//設定分類下的內容
			let path = "RollerBar/view/content/ListNode/";
			this.view[path+type].selectedid = tagCode;				//將選擇分類的id設定成tagCode
			UI_manager.add_button_listen(this.view[path+type],this,function () {
				if(VM.getValue("core.lockinfunc"))		
					return;
				this.unluckToolbarBtns();							//給切換分類的時間限制
				
				if(tagCode == 6){ //先禁止捕魚功能     顯示其功能未開放（如果有捕魚分類）
					this.ShowAlert(TitleAlertType.Warn,"newfuncnotopen");		//顯示新功能尚未开放，敬请期待。
					return;
				}
				
				this.setNormalMode(VM.getValue("bound.selected"));
				this.setGoldMode(type);
				VM.setValue("bound.selected",type);
				VM.setValue("bound.selectedid",tagCode);
				let RollPath = "RollerBar/view";
                this.view[RollPath+"/Line"].runAction(cc.moveTo(0.2,(this.view[path+type].x+this.view[RollPath+"/content"].x),this.view[RollPath+"/Line"].y));	//控制分類下方金色＿線的移動   0.2=持續時間
				// self.view["ScrollView/view/content"].removeAllChildren();
				// ApiManager.get.BoundList(tagCode);
				this.newsetBoundData();							//選取分類後才會執行
				VM.setValue("bound.loading",false);
				UIAssistant.main.SetPanelVisible("Load",true,);			//疑問
			});
		};

		this.unluckToolbarBtns = function () {
			VM.setValue("core.lockinfunc",true);
			setTimeout(function () {
				VM.setValue("core.lockinfunc",false);
			},500);
		};

		this.CreatItems = async function(path, itemname, data, func = null) {
		//("ScrollView/view/content","BoundImageItem",boundlist.promo,this.setBoundBtnEvent)
			await data.asyncForEach(async (value,index) =>{			
				//index 第幾個資料  value object			
				await this.creatFactory(path,itemname, index, value, func);
				//await this.creatFactory(ScrollView/view/content","BoundImageItem",index,object,this.setBoundBtnEvent)
				/*
				if (index > data.length) 					
				this.view[path].children[i].active=false;		等於下面的for
				*/
			});
			for(let i = data.length;i<this.view[path].childrenCount;i++){			//超過的隱藏  切換不同分類的時候使用
				this.view[path].children[i].active = false;
			}
		};

		this.creatFactory = async  function(path,itemname,index,data,onClickFunc,type = null) {
			//								(ScrollView/view/content","BoundImageItem",index,object,this.setBoundBtnEvent)
			let item = null;
			if(index >= this.view[path].getComponentsInChildren(itemname).length) {		//查找所有子節點中指定類型的組件  第一次讀都是＝的
				item = await UI_manager.create_item_Sync(this.view[path], itemname);		//在這邊實例
				item.getComponent(itemname).setValue(data,onClickFunc);
				// item.active = true;
				
			
			}else {				//切分頁讓需要的資訊顯示出來（包含全部）
				//console.log("index:"+index);
				item = this.view[path].getComponentsInChildren(itemname)[index];
				//console.log("item：" +item);
				item.getComponent(itemname).setValue(data);		//好像是active = true  以及圖片順序
				//console.log(data);
				// item.node.active = true;
			}
		};

		this.setBoundBtnEvent = function () {
			if(VM.getValue("bound.loading"))
				return;
			PageManager.BoundDetailPage();		//進詳細資訊
		}
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.boundtitle"));			//標題
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BoundCtrl");
	},

	start() {
		this.setGameListBtns();
	},

	//初始化
	init(){
		this.view["RollerBar"].getComponent(cc.ScrollView).scrollToLeft(0);		//初步內容將在規定時間內滾動到視圖左側
		let type = VM.getValue("bound.selected");			
		let path = "RollerBar/view/content/ListNode/";							
		VM.setValue("bound.boundId",null);
		VM.setValue("bound.loading",false);
		this.setNormalModeAll();		
		this.setGoldMode(type);		//設定起始值為type, type為all
		let RollPath = "RollerBar/view";
		this.view[RollPath+"/Line"].setPosition(this.view[path+type].x+this.view[RollPath+"/content"].x,this.view[RollPath+"/Line"].y);
	},

	/**
	 * 設定優惠圖片(舊版 api)
	 */
	async setBoundData(){
		var data = VM.getValue("bound.list");
		if(!data){
			return;
		}
		await this.CreatItems("ScrollView/view/content","BoundImageItem",data,this.setBoundBtnEvent);

		VM.setValue("bound.loading",false);
		UIAssistant.main.SetPanelVisible("Load",false);
	},
	/**
	 * 設定優惠圖片(新版 api)
	 */
	async newsetBoundData(){
		var data = VM.getValue("bound.list");
		if(!data){
			return;
		}
		let boundlist = data.find(elemte=>elemte.tag == VM.getValue("bound.selectedid"));
		if(!boundlist){
			return;
		}
		await this.CreatItems("ScrollView/view/content","BoundImageItem",boundlist.promo,this.setBoundBtnEvent);		
		VM.setValue("bound.loading",false);
		UIAssistant.main.SetPanelVisible("Load",false);		//這個怎麼看
	},

	/**
	 * 設定被選取模式
	 * @param value 
	 */
	setGoldMode(value){
		let path = "RollerBar/view/content/ListNode/";
		this.view[path+value+"/RichText"].color = VM.getValue("color.gold");
		let str2 = this.view[path+value+"/RichText"].getComponent("VMLabel").watchPath;
		this.view[path+value+"/RichText"].getComponent(cc.RichText).string = "<b>"+VM.getValue(str2)+"</b>";		//加粗字體"<b>"+ ＋"</b>"
	},
	
	/** 
	 * 設定普通模式
	 * @param value 
	 */
	setNormalMode(value){		//設定類別位置 顏色 文字等等
		let path = "RollerBar/view/content/ListNode/";
		this.view[path+value+"/RichText"].color = VM.getValue("color.graylabel3");
		let str1 = this.view[path+value+"/RichText"].getComponent("VMLabel").watchPath;
		this.view[path+value+"/RichText"].getComponent(cc.RichText).string = VM.getValue(str1);
	},

	/**
	 * 設定全部類別為普通模式
	 * @param value 
	 */
	setNormalModeAll(){
		let nodes = this.view["RollerBar/view/content/ListNode"].children;		
		nodes.forEach(item => {
			this.setNormalMode(item.name);			
		});
	},

	ShowAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue("lang."+key));
		//TitleAlertType.Warn,"newfuncnotopen"
	},

	/**
	 * 設定沒有項目的類別隱藏
	 */
	setBoundClassHide(){
		let data = VM.getValue("bound.list");
		if(!data){
			return;
		}
		let boundClassNodes = this.view["RollerBar/view/content/ListNode"].children;
		boundClassNodes.forEach(node => {
			let boundClass = data.find(item=>item.tag == node.selectedid);
			if(boundClass == null || boundClass == undefined){			//陣列裡沒資料
				node.active = false;
			}else{
				node.active = boundClass.promo.length > 0 ? true : false;		//boundClass分類下的其中一種優惠資料
			}
		})
	},

});