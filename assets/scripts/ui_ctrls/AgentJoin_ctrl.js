var UI_ctrl = require("UI_ctrl");
const {VM} = require('ViewModel');

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){

	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.proxyjoin"));
		this.view["scroll"].getComponent(cc.ScrollView).scrollToTop(0);
	},

	start() {

	},

});