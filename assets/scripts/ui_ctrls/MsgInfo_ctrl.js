import MobileExtension from "../Extension/Nactive/MobileExtension";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.setCopyBtn = function () {
			UI_manager.add_button_listen(this.view["content/copy"],this,function () {
				let isCopy = MobileExtension.Copy(VM.getValue("msg.msgInfo").copy);
				if(isCopy){
					TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.copysuccess"));
				}
			});
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"MsgInfoCtrl");
		this.setCopyBtn();
	},

	start() {
	},

	/**
	 * 設定我的消息詳細內容
	 * @param data 訊息內容
	 */
	setMsgInfo(data){
		this.view["Top/RichText"].getComponent(cc.RichText).string = "<b>"+ data.title +"</b>";
		this.view["Top/Label"].getComponent(cc.Label).string = data.created_at.GameTimeOnyDay();
		this.view["content/msg"].getComponent(cc.Label).string = data.content;
		this.view["content/copy"].active = !data.copy.isNull();
	},

});