import NoClass from "../AbstractClass/GameList/NoClass";
import LotteryClass from "../AbstractClass/GameList/LotteryClass";
import GameClass from "../AbstractClass/GameList/GameClass";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');

var GameListType = require("GameListType");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.type = null;
		this.target_Class = null;
		this.Classes = [];
		let self = this;
		this.setWithdrawEvent = function () {
			UI_manager.add_button_listen(this.view["ScrollView/view/content/UserNode/Btn/withdrawBtn"],null,function () {
				PageManager.WithDrawPage();
			});
		};

		this.setRechargeEvent = function () {
			UI_manager.add_button_listen(this.view["ScrollView/view/content/UserNode/Btn/rechargeBtn"],null,function () {
				PageManager.RechargePage()
			});
		};
		
		this.setUserNodeBtn = function () {
			let withdraw = this.view["ScrollView/view/content/UserNode/Btn/withdrawBtn"];
			let withdrawPen = withdraw.getComponent(cc.Graphics);
			withdrawPen.roundRect(0,0,withdraw.width,withdraw.height,16);
			withdrawPen.stroke();
			withdrawPen.fill();
		};

		this.drawGraphics = function (temp) {
			let time = temp.getChildByName("time");
			let finalize = temp.getChildByName("finalize");
			let timePen = time.getComponent(cc.Graphics);
			let finPen = finalize.getComponent(cc.Graphics);
			timePen.roundRect((0-time.width),(0-time.height/2),time.width,time.height,18);
			timePen.stroke();
			timePen.fill();
			finPen.roundRect((0-finalize.width/2),(0-finalize.height/2),finalize.width,finalize.height,18);
			finPen.stroke();
			finPen.fill();
		};

		/*
		 * 搜尋結果取消的按鈕事件
		 */
		this.setCancelEvent = function () {
			UI_manager.add_button_listen(this.view["ScrollView/view/content/SearchNode/CancelBtn"],null,function () {
				if(VM.getValue("hall.search").length == 0)
					return;
				self.view["ScrollView/view/content/SearchNode/EditBox"].getComponent(cc.EditBox).string = "";
				self.view["ScrollView/view/content/SearchNo"].active = false;
				VM.setValue("hall.search","");
				self.target_Class.getAllData(self.type);
			});
		};

		/*
		 * 設定搜尋內容的 EditBox text-changed 監聽事件
		 */
		this.setEditBoxChangeValue = function () {
			this.view["ScrollView/view/content/SearchNode/EditBox"].on("text-changed",function (value) {
				let searchName = this.view["ScrollView/view/content/SearchNode/EditBox"].getComponent(cc.EditBox).string;
				VM.setValue("hall.search",searchName);
				this.getSearch();
			},this);
		};

		this.InitClassMode = function () {
			this.Classes.push(new NoClass(this.view));
			this.Classes.push(new GameClass(this.view));
			this.Classes.push(new LotteryClass(this.view));
		};

		this.checkIsPTJackPotMode = function () {
			cc.log("checkIsPTJackPotMode");
			cc.log("VM.getValue(\"hall.ptjackpot\")" ,VM.getValue("hall.ptjackpot"));
			this.view["ScrollView/view/content/PTJackPot"].active = VM.getValue("hall.ptjackpot");
			this.view["ScrollView/view/content/speace"].active = !VM.getValue("hall.ptjackpot");
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"GameListCtrl");
		this.setWithdrawEvent();
		this.setRechargeEvent();
		this.setCancelEvent();
		this.setEditBoxChangeValue();
		this.setUserNodeBtn();
		this.DefaultUserInfoLabel();
		this.InitClassMode();
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("hall.gameTitle"));
		this.checkIsPTJackPotMode();
	},

	start() {
	},

	init(){
		this.view["ScrollView"].getComponent(cc.ScrollView).scrollToTop(0);
		this.view["ScrollView/view/content/SearchNo"].active = false;
		this.view["ScrollView/view/content/SearchNode/EditBox"].getComponent(cc.EditBox).string = "";
	},

	/**
     * 預設玩家資料
     */
	DefaultUserInfoLabel(){
        this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = VM.getValue("lang.visitors");
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+"0.00";
    },

	/**
	 * 設定玩家名稱
	 * @param value 玩家名稱
	 */
	setUserName(value){
		if(this.target_Class)
			this.target_Class.setUserName(this.type,value);
	},

	/**
	 * 設定玩家錢包金額
	 * @param value 錢包金額
	 */
	setUserMoney(value){
		if(this.target_Class)
			this.target_Class.setUserMoney(this.type,value);
	},

	ShowInWebView(url) {
		CtrlAssistant.main.getCtrl("MainCtrl").setWebUrl(url);
	},

	/*
	 * 彩票項目的計時器
	 */
	setTimer(bol){
		let self = this;
		if(bol){
			this.timeCallback = function (dt) {
				if(self.target_Class)
					self.target_Class.setRefresh(GameListType.LotteryClass);
			}
			this.schedule(this.timeCallback, 1);
		}else{
			this.unschedule(this.timeCallback);
		}		
	},

	/**
	 * 初始化頁面
	 * @param type 頁面類型
	 */
	InitPage(type){
		this.type = type;
		this.target_Class = this.Classes[type];
		this.target_Class.Init();
	},

	/**
	 * 設定未分類頁面
	 * @param data 資料
	 */
	setNoClassData(data){
		if(this.target_Class)
			this.target_Class.setData(GameListType.NoClass,data);
	},

	/**
	 * 設定彩票頁面
	 */
	setLotteryClassData(){
		if(this.target_Class)
			this.target_Class.setClassData(GameListType.LotteryClass,VM.getValue("hall.lotteryClassName"));
	},

	/**
	 * 設定彩票項目
	 * @param data 資料
	 */
	setLotteryClassItems(data){
		if(this.target_Class)
			this.target_Class.setLotteryItems(GameListType.LotteryClass,data);
	},

	/**
	 * 設定遊戲頁面
	 * @param data 資料
	 */
	setGameClassData(data){
		if(this.target_Class)
			this.target_Class.setGameItems(GameListType.GameClass,data);
	},

	/**
	 * 設定 Load 狀態
	 * @param is_show 顯示狀態
	 */
	setLoad(is_show){
		this.target_Class.setLoad(is_show);
	},

	/**
	 * 取得搜尋結果
	 */
	getSearch(){
		if(this.target_Class)
			this.target_Class.searchItems(this.type,VM.getValue("hall.search"));
	},

});