import MenuItem from "../Item/MenuItem";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class MenuView extends cc.Component {

    @property(cc.Node)
    content: cc.Node = null;

    public static main: MenuView = null;

    private callback : void = null;

    protected onLoad(): void {
        MenuView.main = this;
        this.node.active = false;
    }

    /***
     * 製造及更新物件
     * @param path 父物件
     * @param itemname 物件名
     * @param data 資料
     * @param func 方法
     * @returns {Promise<void>}
     * @constructor
     */
    async CreatItems(itemname,data,initswitch,func = null,post = false) {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(itemname, index, value,initswitch, func,post);
        });

        for(let i = data.length;i<this.content.childrenCount;i++){
            this.content.children[i].active = false;
        }
    }
    /***
     * 物件工廠
     * @param path	路徑
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
     async creatFactory(itemname,index,data,initswitch,onClickFunc,post) {
        let item = null;
        if(index >= this.content.childrenCount) {
            item = await UI_manager.create_item_Sync(this.content, itemname);
            item.getComponent("Iitem").setValue(data,onClickFunc);
        }else {
            item = this.content.children[index];
            item.getComponent("Iitem").setValue(data);
        }
        if(index==initswitch && post) {
            ///ㄑ
            this.content.children.forEach(item=>{
                item.getComponent(MenuItem).setSwitch(data);
            });
        }
        item.active = true;

    }

    menuItemCB(key:any) {
        if(this.callback)
            this.callback(key);

        this.content.children.forEach(item=>{
            item.getComponent(MenuItem).setSwitch(key);
        });
    }


    setData(data,cb,initindex = -1,initpost = false){
        this.callback = cb;
        this.CreatItems("MenuItem",data,initindex,this.menuItemCB.bind(this),initpost);
    }
}
