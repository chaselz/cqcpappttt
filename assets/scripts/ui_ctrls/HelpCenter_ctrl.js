import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.runGraphics = function () {
			let x = this.view["onlineBtn"];
			let pen = x.getComponent(cc.Graphics);
			pen.clear();
			pen.roundRect((0-x.width/2),(0-x.height/2),x.width,x.height,44);
			pen.stroke();
			pen.fill();
		};
		/**
		 * 設定幫助頁的 button
		 */
		this.setHelpBtn = function(){
			UI_manager.add_button_listen(this.view["BtnNode/depositBtn"],this,function () {
				VM.setValue("core.title",VM.getValue("lang.recordhelp"));
				// VM.setValue("help.list",VM.getValue("help.helpList")[1]["detail"]);
				VM.setValue("help.list",this.getHelpData("存款帮助"));
				PageManager.HelpListPage();
			});
			UI_manager.add_button_listen(this.view["BtnNode/withdrawBtn"],this,function () {
				VM.setValue("core.title",VM.getValue("lang.withdrawhelp"));
				// VM.setValue("help.list",VM.getValue("help.helpList")[2]["detail"]);
				VM.setValue("help.list",this.getHelpData("取款帮助"));
				PageManager.HelpListPage();
			});
			UI_manager.add_button_listen(this.view["BtnNode/aboutBtn"],this,function () {
				VM.setValue("core.title",VM.getValue("lang.aboutus"));
				// VM.setValue("help.list",VM.getValue("help.helpList")[4]["detail"]);
				VM.setValue("help.list",this.getHelpData("关于我们"));
				PageManager.HelpListPage();
			});
			UI_manager.add_button_listen(this.view["onlineBtn"],this,function () {
				console.log("聯繫客服");
				CtrlAssistant.main.getCtrl("MainCtrl").AllScreenWebUrl(VM.getValue("setting.serviceurl"));
			});
		};

		this.CreatItems = async function(path, itemname, data, func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname, index, value, func);
			});
			for(let i = data.length;i<this.view[path].childrenCount;i++){
				this.view[path].children[i].active = false;
			}
		};
	
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(index >= this.view[path].getComponentsInChildren(itemname).length) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent(itemname).setValue(data,onClickFunc);
				item.active = true;
			}else {
				item = this.view[path].getComponentsInChildren(itemname)[index];
				item.getComponent(itemname).setValue(data);
				item.node.active = true;
			}
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"HelpCenterCtrl");
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.helpcenter"));
	},

	start() {
		this.runGraphics();
		this.setHelpBtn();
	},

	/**
	 * 設定幫助中心 常見問題清單
	 * @param data 清單資料
	 */
	async setCommonProblemList(data){
		if(!data)
			return;
		await this.CreatItems("scroll/view/content/ListNode","HelpItem",data,null);
	},

	/**
	 * 取得對應的幫助中心資料
	 * @param value 要取得的標題
	 */
	getHelpData(value){
		let res = VM.getValue("help.helpList");
		let detail = [];
		res.forEach(x => {
            if((x["main_title"] == "存款幫助" || x["main_title"] == "存款帮助") && value == "存款帮助"){
				detail = x["detail"];
			}else if((x["main_title"] == "取款幫助" || x["main_title"] == "取款帮助") && value == "取款帮助"){
				detail = x["detail"];
			}else if((x["main_title"] == "關於我們" || x["main_title"] == "关于我们") && value == "关于我们"){
				detail = x["detail"];
			}else if((x["main_title"] == "常見問題" || x["main_title"] == "常见问题") && value == "常见问题"){
				detail = x["detail"];
			}
		});
		return detail;
	},

});