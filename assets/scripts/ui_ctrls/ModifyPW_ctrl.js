import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.mode = ModifyPWType.Login;
		this.iserr = false;
		this.loginInit = function () {
			this.Init();
			this.view["content/newpw/top/warn"].getComponent("VMLabel").setWatchArrValue(["lang.input","lang.length","modifypw.minnum","lang.pw"]);
			this.view["content/oldpw/inputbox/PLACE_LABEL"].getComponent("VMLabel").setWatchPath("lang.originloginpw");
			VM.setValue("modifypw.minnum",6);
			this.view["content/newpw/inputbox/PLACE_LABEL"].getComponent("VMLabel").setOriginLabelValue("{{0}}qwe@567");
			this.playAnim(ModifyPWType.Login);
			this.setTitleLableRed(ModifyPWType.Login);
			this.closeAllWarnNode();
			this.setKeyBoard(cc.EditBox.InputMode.SINGLE_LINE);
		};

		this.withdrawInit = function () {
			if(VM.getValue("addbank.userbank")==0){
				PageManager.AddBankPage();
				return;
			}
			this.Init();
			this.view["content/newpw/top/warn"].getComponent("VMLabel").setWatchArrValue(["lang.input","lang.length","modifypw.minnum","lang.withdrawpw"]);
			this.view["content/oldpw/inputbox/PLACE_LABEL"].getComponent("VMLabel").setWatchPath("lang.originwithdrawpw");
			VM.setValue("modifypw.minnum",4);
			this.view["content/newpw/inputbox/PLACE_LABEL"].getComponent("VMLabel").setOriginLabelValue("{{0}}123456");
			this.playAnim(ModifyPWType.WithDraw);
			this.setTitleLableRed(ModifyPWType.WithDraw);
			this.closeAllWarnNode();
			this.setKeyBoard(cc.EditBox.InputMode.NUMERIC);
		};

		this.closeAllWarnNode = function () {
			this.activeNode("content/oldpw/Err1",false);
			this.activeNode("content/oldpw/Err2",false);
			this.activeNode("content/newpw/Err1",false);
			this.activeNode("content/newpw/Err2",false);
			this.activeNode("content/checkpw/Err1",false);
		};

		this.setKeyBoard = function (type) {
			UI_manager.setEditBoxKeyBoard(this.view["content/oldpw/inputbox"],type);
			UI_manager.setEditBoxKeyBoard(this.view["content/newpw/inputbox"],type);
			UI_manager.setEditBoxKeyBoard(this.view["content/checkpw/inputbox"],type);
		};

		this.normalMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.graylabel3");
			if(this.view[path+"/top/warn"])
				this.view[path+"/top/warn"].color = VM.getValue("color.graylabel3");
			this.view[path+"/line"].color = VM.getValue("color.linecolor");
		};

		this.ErrMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.red");
			if(this.view[path+"/top/warn"])
				this.view[path+"/top/warn"].color = VM.getValue("color.red");
			this.view[path+"/line"].color = VM.getValue("color.red");
		};

		this.setBtn = function () {
			UI_manager.add_button_listen(this.view["buttom/check"],this,function () {
				let oldpw = this.view["content/oldpw/inputbox"].getComponent(cc.EditBox).string;
				let newpw = this.view["content/newpw/inputbox"].getComponent(cc.EditBox).string;
				let checkpw = this.view["content/checkpw/inputbox"].getComponent(cc.EditBox).string;
				this.iserr = false;
				if(this.mode == ModifyPWType.Login)
					this.sentModifyLoginPwApi(oldpw,newpw,checkpw);
				else
					this.sentModifyWithDrawPwApi(oldpw,newpw,checkpw);
			});
		};

		this.sentModifyLoginPwApi = function (oldpw,newpw,checkpw) {
			if(!this.checkEditBoxValue(oldpw,newpw,checkpw))
				return;
			ApiManager.User.set.UpdatePassword(oldpw,newpw);
		};

		this.sentModifyWithDrawPwApi = function (oldpw,newpw,checkpw) {
			if(!this.checkEditBoxValue(oldpw,newpw,checkpw))
				return;
			ApiManager.User.set.UpdatePassWordWithDraw(oldpw,newpw);
		};

		this.checkEditBoxValue = function (oldpw,newpw,checkpw) {
			if(oldpw.length < VM.getValue("modifypw.minnum")){
				this.iserr = true;
				this.ErrMode("content/oldpw");
			}

			if(newpw.length < VM.getValue("modifypw.minnum")){
				this.iserr = true;
				this.ErrMode("content/newpw");
			}

			if(newpw != checkpw || checkpw.isNull()){
				this.iserr = true;
				this.ErrMode("content/checkpw");
			}

			if(this.iserr){
				this.ShowErrAlert(TitleAlertType.Warn,"lang.inputErr");
				return false;
			}
			VM.setValue("core.lock",true);
			return  true;
		};

		this.setChooseModeBtn = function () {
			UI_manager.add_button_listen(this.view["top/loginpwBtn"],this,function () {
				this.loginInit();
			});

			UI_manager.add_button_listen(this.view["top/withdrawBtn"],this,function () {
				this.withdrawInit();
			});
		};

		this.setTitleLableRed = function (type) {
			this.mode = type;
			this.view["top/loginpwBtn/bg/text"].color = VM.getValue(type == ModifyPWType.Login ? "color.gold":"color.graylabel3");
			this.view["top/withdrawBtn/bg/text"].color = VM.getValue(type == ModifyPWType.WithDraw ? "color.gold":"color.graylabel3");
		};

		this.setShowPWBtn = function (uiname) {
			UI_manager.add_button_listen(this.view["content/"+uiname+"/inputbox/eye"],this,function () {
				let editbox = this.view["content/"+uiname+"/inputbox"].getComponent(cc.EditBox);
				let isShow = editbox.inputFlag != cc.EditBox.InputFlag.PASSWORD;
				editbox.inputFlag = isShow ? cc.EditBox.InputFlag.PASSWORD : cc.EditBox.InputFlag.DEFAULT;
				let spritename = "universal/icon-show-pw"+(isShow ? "" : "-show");
				UI_manager.setSprite(this.view["content/"+uiname+"/inputbox/eye/Bg"], spritename);

			});
		};

		this.setShowPWBtns = function () {
			this.setShowPWBtn("oldpw");
			this.setShowPWBtn("newpw");
			this.setShowPWBtn("checkpw");
		};

		this.setEditBoxsChangeValue = function () {
			this.setEditBoxChangeValue("oldpw");
			this.setEditBoxChangeValue("newpw");
			UI_manager.add_editorbox_text_changed_listen(this.view["content/checkpw/inputbox"],this,function (editBox) {
				if(!this.iserr)
					return;
				let newpw = this.view["content/newpw/inputbox"].getComponent(cc.EditBox).string;
				this.activeNode("content/checkpw/Err1",editBox.string != newpw);
				if(editBox.string != newpw)
					this.ErrMode("content/checkpw");
				else
					this.normalMode("content/checkpw");
			});
		};

		this.setEditBoxChangeValue = function (editBoxUI) {

			UI_manager.add_editorbox_text_changed_listen(this.view["content/"+editBoxUI+"/inputbox"],this,function (editBox) {
				if(!this.iserr)
					return;
				let num = VM.getValue("modifypw.minnum");
				let conformlength = editBox.string.length < num;
				let onlyNum = this.mode == ModifyPWType.Login ? true : editBox.string.onlyNumber();
				this.activeNode("content/"+editBoxUI+"/Err1",conformlength);
				this.activeNode("content/"+editBoxUI+"/Err2",!onlyNum);
				if(conformlength || !onlyNum)
					this.ErrMode("content/"+editBoxUI);
				else
					this.normalMode("content/"+editBoxUI);
			});
		};

		this.activeNode = function (path,_active) {
			if(this.view[path].active != _active)
				this.view[path].active = _active;
		};

		this.playAnim = function (type) {
			if(this.mode != type)
				this.view["chooseline"].getComponent(cc.Animation).play(this.mode == ModifyPWType.Login ? "modifypwright" : "modifypwleft");
		}
	},

	onEnable(){
		this.loginInit();
		VM.setValue("core.lock",false);
		VM.setValue("core.title",VM.getValue("lang.changepw"));
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"ModifyPWCtrl");
	},

	start() {
		this.setChooseModeBtn();
		this.setBtn();
		this.setShowPWBtns();
		this.setEditBoxsChangeValue();
	},

	Init(){
		this.iserr = false;
		this.normalMode("content/oldpw");
		this.normalMode("content/newpw");
		this.normalMode("content/checkpw");
		UI_manager.setLabelNodeVlaue(this.view["content/oldpw/inputbox"],"");
		UI_manager.setLabelNodeVlaue(this.view["content/newpw/inputbox"],"");
		UI_manager.setLabelNodeVlaue(this.view["content/checkpw/inputbox"],"");
	},

	ShowErrAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue(key));
	}

});

var ModifyPWType = Object.freeze({
	Login:Symbol(1),
	WithDraw:Symbol(2)
});