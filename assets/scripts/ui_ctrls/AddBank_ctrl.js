import DropDown from "../UI/DropDown/DropDown";
import Alert from "../UI/Alert";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.iserr = false;

		this.setShowPWBtn = function () {
			UI_manager.add_button_listen(this.view["view/content/pw/inputbox/eye"],this,function () {
				let isShow = this.view["view/content/pw/inputbox"].getComponent(cc.EditBox).inputFlag != cc.EditBox.InputFlag.PASSWORD;
				this.view["view/content/pw/inputbox"].getComponent(cc.EditBox).inputFlag = isShow ? cc.EditBox.InputFlag.PASSWORD : cc.EditBox.InputFlag.DEFAULT;
				let spritename = isShow ? "icon-show-pw" : "icon-show-pw-show";
				cc.log("name = ",spritename);
				UI_manager.setSprite(this.view["view/content/pw/inputbox/eye/Bg"], spritename);

			});
		};

		this.setAddBankBtn = function () {
			UI_manager.add_button_listen(this.view["view/buttom/Btn"],this,function () {
				this.iserr = false;
				let seleted = VM.getValue("addbank.selectedIndex");
				let point = this.view["view/content/point/inputbox"].getComponent(cc.EditBox).string;
				let card = this.view["view/content/cardnum/inputbox"].getComponent(cc.EditBox).string;
				let pw = this.view["view/content/pw/inputbox"].getComponent(cc.EditBox).string;
				if(seleted == -1){
					this.iserr = true;
					this.redMode("view/content/banks");
				}

				if(point.isNull() || !point.isZH() || point.length < 2){
					this.iserr = true;
					this.redMode("view/content/point");
					this.activeNode("view/content/point/Err1",!point.isZH()&& point.length > 0);
					this.activeNode("view/content/point/Err2",point.length < 2 && point.length > 0);
				}

				if(card.isNull() || card.length < 2 || !card.onlyNumber()){
					this.iserr = true;
					this.redMode("view/content/cardnum");
					this.activeNode("view/content/cardnum/Err2",!card.onlyNumber());
				}

				if(this.view["view/content/pw"].active &&  (pw.length < 4 || !pw.onlyNumber())){
					this.iserr = true;
					this.redMode("view/content/pw");
					this.activeNode("view/content/pw/Err1",pw.length < 4);
					this.activeNode("view/content/pw/Err2",!pw.onlyNumber());
				}

				if(this.iserr){
					this.ShowErrAlert(TitleAlertType.Warn,"lang.inputErr");
					return;
				}

				let msg = VM.getValue("lang.bankcardcheckwarn");
				msg += "\n\n";
				msg += VM.getValue("lang.bank") +"：" + VM.getValue("addbank.banklist")[seleted].bank_name + "\n";
				msg += VM.getValue("lang.openaccountlocation") +"：" + point + "\n";
				msg += VM.getValue("lang.bankcardnum") +"：" + card + "\n";
				msg += VM.getValue("lang.bankaccountname") +"：" + VM.getValue("member.name") + "\n";

				Alert.main.Show("lang.bankcardcheck",msg,this.sentAddBankCardApi.bind(this));

			});
		};

		this.sentAddBankCardApi = function () {
			cc.log("sentAddBankCardApi");
			let seleted = VM.getValue("addbank.selectedIndex");
			let point = this.view["view/content/point/inputbox"].getComponent(cc.EditBox).string;
			let card = this.view["view/content/cardnum/inputbox"].getComponent(cc.EditBox).string;
			let pw = this.view["view/content/pw/inputbox"].getComponent(cc.EditBox).string;

			if(this.view["view/content/pw"].active)
				ApiManager.Deposit.set.SetUserBankPW(pw);

			let bankid = VM.getValue("addbank.banklist")[seleted].bank_id;
			ApiManager.Deposit.set.SetBankCard(bankid,VM.getValue("member.name"),card,point,0);
		};

		this.setEditBoxListener = function () {
			UI_manager.add_editorbox_text_changed_listen(this.view["view/content/point/inputbox"],this,function (value) {
				if(this.iserr){
					if(value.string.isZH() && value.string.length > 1)
						this.normalMode("view/content/point");
					else {
						this.redMode("view/content/point");
					}

					this.activeNode("view/content/point/Err1",!value.string.isZH()&& value.string.length > 0);
					this.activeNode("view/content/point/Err2",value.string.length < 2&& value.string.length > 0);
				}
			});

			UI_manager.add_editorbox_text_changed_listen(this.view["view/content/cardnum/inputbox"],this,function (value) {
				if(this.iserr){
					if(value.string.length < 2 || !value.string.onlyNumber())
						this.redMode("view/content/cardnum");
					else
						this.normalMode("view/content/cardnum");

					this.activeNode("view/content/cardnum/Err1",value.string.length < 2&& value.string.length > 0);
					this.activeNode("view/content/cardnum/Err2",!value.string.onlyNumber());
				}
			});

			UI_manager.add_editorbox_text_changed_listen(this.view["view/content/pw/inputbox"],this,function (value) {
				if(this.iserr){
					if(value.string.length < 4 || value.string.length > 30)
						this.redMode("view/content/pw");
					else
						this.normalMode("view/content/pw");

					this.activeNode("view/content/pw/Err1",value.string.length < 4 && value.string.length > 0);
					this.activeNode("view/content/pw/Err2",(!value.string.isNull()&&!value.string.onlyNumber()));
				}
			});
		};

		this.setDropDownCB = function () {
			let self = this;
			this.view["view/content/banks/DropDown"].getComponent("DropDown").SetSelectIndexCallBack(function (index) {
				VM.setValue("addbank.selectedIndex",index);
				self.normalMode("view/content/banks");
			});
		};

		/***
		 * EditorBox 警告模式
		 */
		this.redMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.red");
			if(this.view[path+"/inputbox/TEXT_LABEL"])
				this.view[path+"/inputbox/TEXT_LABEL"].color = VM.getValue("color.red");
			if(this.view[path+"/top/warn"]) {
				this.view[path + "/top/warn"].color = VM.getValue("color.red");
			}
			this.view[path+"/line"].color = VM.getValue("color.red");
		};
		/***
		 * EditorBox 一般狀態
		 * @param path
		 */
		this.normalMode = function (path) {
			this.view[path+"/top/Title"].color = VM.getValue("color.graylabel3");
			if(this.view[path+"/inputbox/TEXT_LABEL"])
				this.view[path+"/inputbox/TEXT_LABEL"].color = VM.getValue("color.graylabel3");
			if(this.view[path+"/top/warn"])
				this.view[path+"/top/warn"].color = VM.getValue("color.graylabel3");
			this.view[path+"/line"].color = VM.getValue("color.linecolor");
		};

		this.activeNode = function (path,isshow) {
			this.view[path].active = isshow;
		};
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.addbankaccount"));
		this.Init();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"AddBankCtrl");
	},

	start() {
		let dropdown = this.view["view/content/banks/DropDown"].getComponent(DropDown);
		dropdown.init(VM.getValue("lang.choosebank"));
		this.setAddBankBtn();
		this.setDropDownCB();
		this.setEditBoxListener();
		this.setShowPWBtn();
	},


	SetBankDropBoxValue(data){
		this.setDropBoxValue("view/content/banks/DropDown",data);
	},

	Init(){
		this.setDropDownInit("view/content/banks/DropDown");
		this.normalMode("view/content/cardnum");
		this.normalMode("view/content/point");
		this.normalMode("view/content/pw");
		this.normalMode("view/content/banks");
		UI_manager.setEditBoxNodeValue(this.view["view/content/pw/inputbox"],"");
		UI_manager.setEditBoxNodeValue(this.view["view/content/cardnum/inputbox"],"");
		UI_manager.setEditBoxNodeValue(this.view["view/content/point/inputbox"],"");
	},

	/**
	 * 設定DropDown資料
	 * @param path DropDown路徑
	 * @param data 資料
	 */
	setDropBoxValue(path,data){
		let _node = this.view[path];
		if(!_node)
			return;

		let dropdown = _node.getComponent("DropDown");
		dropdown.clearOptionDatas();
		dropdown.addOptionDatas(data,0);
	},
	/**
	 * DropDown 初始化
	 * @param path DropDown路徑
	 */
	setDropDownInit(path){
		let _node = this.view[path];
		if(!_node)
			return;

		let dropdown = _node.getComponent("DropDown");
		dropdown.clearOptionDatas();
		dropdown.init(VM.getValue("lang.choosebank"));
	},
	/***
	 * 設定密碼輸入是否顯示
	 * @param _active
	 */
	setPwEditorBoxActive(_active){
		this.view["view/content/pw"].active = !_active;
	},

	ShowErrAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue(key));
	},

});