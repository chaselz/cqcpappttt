import {VM} from "../modelView/ViewModel";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.setValue = function () {
			let data = VM.getValue("bulletin.bulletinindex");
			this.view["main/view/content/item/top/title"].getComponent(cc.RichText).string ="<b>"+ data.title+"</b>";
			this.view["main/view/content/item/top/title/time"].getComponent(cc.Label).string = data.published_at.GameTimeOnyDay();
			this.setContent(data.content);
		};

		this.setContent = function (value) {
			if(!cc.sys.isNative)
				return;

			let str = this.AnalyzeHtmlString(value);
			let contentlebel = this.view["content"].getComponent(cc.Label);
			contentlebel.string = str;
			//更新Label的數值;
			contentlebel._forceUpdateRenderData();
			let web = this.view["main/view/content/item/web"].getComponent(cc.WebView);
			web.node.height = contentlebel.node.height;
			let htmlstr = "<body bgcolor=\"#272727\"><span style=\"font-size:35px;\"><font color=\"#9F9F9F\">"+value+"</font></span><br>";
			this.view["main/view/content/item/web"].getComponent(cc.WebView).url = htmlstr;
		};

		this.AnalyzeHtmlString = function (str) {
			let temp = str.replaceAll("<p>","");
			temp = temp.replaceAll("</p>","\n");
			temp = temp.replaceAll("&nbsp;"," ");

			temp = temp.replaceAll("<br>","\n\n");

			temp = temp.replace(/<strong([^>]*)>[^<]*<\/strong>/g, (str, space) => {
				let s = '';
				for (const c of str.slice('<strong'.length + space.length +1, str.length - '<\/strong>'.length)) {
					s += c
				}
				return s
			});

			temp = temp.replace(/<span([^>]*)>[^<]*<\/span>/g, (str, space) => {
				let s = '';
				for (const c of str.slice('<span'.length + space.length +1, str.length - '<\/span>'.length)) {
					s += c;
				}
				return s
			});
			return temp;
		}
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.bulletin")+VM.getValue("lang.detail"));
		this.setValue();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		this.view["main/view/content/item/web"].getComponent(cc.WebView).url = "<body bgcolor=\"#272727\"></body>";
	},

	start() {

	},

	onDisable(){
		let contentlebel = this.view["content"].getComponent(cc.Label);
		contentlebel.string = "";
		this.view["main/view/content/item/web"].getComponent(cc.WebView).url = "<body bgcolor=\"#272727\"></body>";
	}


});