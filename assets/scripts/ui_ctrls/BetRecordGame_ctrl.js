import {BetRecordBtnType} from "../Enum/BetRecordBtnType";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
cc.Class({
	extends: UI_ctrl,

	ctor(){
		/***
		 * 製造及更新物件
		 * @param path 父物件
		 * @param itemname 物件名
		 * @param data 資料
		 * @param func 方法
		 * @returns {Promise<void>}
		 * @constructor
		 */
		this.CreatItems = async function(path,itemname,data,func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname, index, value, func,BetRecordBtnType.Game);
			});

			for(let i = data.length;i<this.view[path].childrenCount;i++){
				this.view[path].children[i].active = false;
			}
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(index >= this.view[path].childrenCount) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent("Iitem").setValue(data,onClickFunc,type);
			}else {
				item = this.view[path].children[index];
				item.getComponent("Iitem").setValue(data,null,type);
			}

			item.active = true;
		};

		this.showRecordGameView = function () {
			PageManager.BetRecordNumPage()
		}
	},

	onEnable(){
		this.setSumViewValue("","");
		let name = VM.getValue("betrecord.gameproviderlist").find(x=>x.code == VM.getValue("betrecord.targetprovider"));
		this.view["TagSpeace/time"].getComponent(cc.Label).string = name.name;
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BetRecordGameCtrl");
	},

	start() {
	},


	async setRecordGameItem(data){
		await this.CreatItems("List/view/content","BetRecordMakerAndGameItem",data,this.showRecordGameView);
		this.CloseLoadView();
	},


	InitGameView(){
		this.view["List/view/content"].children.forEach(node=>node.active = false);
	},
	//
	ShowLoadView() {
		this.view["Load"].active = true;
		this.view["Load/icon"].getComponent(cc.Animation).play("buttonload");
	},

	CloseLoadView(){
		this.view["Load"].active = false;
		this.view["Load/icon"].getComponent(cc.Animation).play("buttonload");
	},

	setSumViewValue(all,result){
		this.view["AllBet/result"].getComponent(cc.Label).string = all;
		this.view["AllResult/result"].getComponent(cc.Label).string = result;
		this.view["AllResult/result"].color =result.includes("-") ? VM.getValue("color.red"):VM.getValue("color.graylabel3");
	},

});