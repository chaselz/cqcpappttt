import {BetRecordBtnType} from "../Enum/BetRecordBtnType";
import {VM} from "../modelView/ViewModel";
import BetRecordNumItem from "../Item/BetRecordNumItem";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.prefabPool = new cc.NodePool();
		this.prefabArray = [];
		/***
		 * 製造及更新物件
		 * @param path 父物件
		 * @param itemname 物件名
		 * @param data 資料
		 * @param func 方法
		 * @returns {Promise<void>}
		 * @constructor
		 */
		this.CreatItems = async function(path,itemname,data,func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname, index, value, func,BetRecordBtnType.Game);
			});

			// for(let i = data.length;i<this.view[path].childrenCount;i++){
			// 	this.view[path].children[i].active = false;
			// }
			this.prefabArray[this.prefabArray.length - 1].getComponent(BetRecordNumItem).lastItemInit();
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(this.prefabPool.size() == 0) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent("Iitem").setValue(data,onClickFunc,type);
			}else {
				item = this.prefabPool.get();
				this.view[path].addChild(item);
				item.getComponent("Iitem").setValue(data,null,type);
			}
			this.prefabArray.push(item);
			item.active = true;
		};

		this.showRecordGameView = function () {
			PageManager.BetRecordGamePage();
		};

		/***
		 * 設定上下頁功能按鈕
		 */
		this.setPreAndNextBtn = function () {
			let path = "PageView/";
			UI_manager.add_button_listen(this.view[path+"first"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == 1)
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(1);
			});

			UI_manager.add_button_listen(this.view[path+"pre"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == 1)
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.nowpage")-1);
			});

			UI_manager.add_button_listen(this.view[path+"next"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == VM.getValue("betrecord.pagecount"))
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.nowpage") +1);
			});

			UI_manager.add_button_listen(this.view[path+"final"],this,function () {
				if(VM.getValue("betrecord.pagecount") == 1 || VM.getValue("betrecord.nowpage") == VM.getValue("betrecord.pagecount"))
					return;
				this.ShowLoadView();
				this.BetsSummaryDetailApi(VM.getValue("betrecord.pagecount"));
			});
		};

		this.drawGraphics = function () {
			let gps = this.view["PageView"].getComponentsInChildren(cc.Graphics);
			gps.forEach(gp=>{
				gp.roundRect(0-gp.node.width/2,0-gp.node.height/2,gp.node.width,gp.node.height,16);
				gp.stroke();
			});
		};
		/***
		 * 回收item
		 * @constructor
		 */
		this.RecycleItem = function () {
			// 删除节点
			this.view["List/view/content"].removeAllChildren();

			// 恢复坐标，回收预制，清空数组
			for(let i=0; i<this.prefabArray.length; i++) {
				this.prefabPool.put(this.prefabArray[i]);
			}

			this.prefabArray = [];
		}
	},

	onEnable(){
		this.setSumViewValue("","");
		VM.setValue("core.lock",false);
		VM.setValue("betrecord.nowpage",0);
		this.view["TagSpeace/time"].getComponent(cc.Label).string = VM.getValue("betrecord.gamename");
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BetRecordNumCtrl");
		this.setPreAndNextBtn();
		this.drawGraphics();
	},

	start() {
	},

	onDisable(){

	},

	InitView(){
		this.view["List/view/content"].children.forEach(node=>node.active = false);
	},

	async setRecordNumItem(data){
		this.RecycleItem();
		await this.CreatItems("List/view/content","BetRecordNumItem",data,null);
		this.CloseLoadView();
		this.view["List"].getComponent(cc.ScrollView).scrollToTop(0);
	},

	//
	ShowLoadView() {
		this.view["Load"].active = true;
		this.view["Load/icon"].getComponent(cc.Animation).play("buttonload");
	},

	CloseLoadView(){
		this.view["Load"].active = false;
		this.view["Load/icon"].getComponent(cc.Animation).play("buttonload");
	},

	setSumViewValue(all,result){
		this.view["AllBet/result"].getComponent(cc.Label).string = all;
		this.view["AllResult/result"].getComponent(cc.Label).string = result;
		this.view["AllResult/result"].color =result.includes("-") ? VM.getValue("color.red"):VM.getValue("color.graylabel3");
	},

	setPreAndNextView(count){
		this.view["PageView/title"].getComponent(cc.RichText).string = VM.getValue("betrecord.nowpage")+"／"+ count;
	},

	/***
	 * 發送注單號清單 api
	 * @param page
	 * @constructor
	 */
	BetsSummaryDetailApi(page){
		VM.setValue("betrecord.nowpage",page);
		ApiManager.Report.BetsSummaryDetailNew(VM.getValue("betrecord.targettime"),8,
			VM.getValue("betrecord.targetprovider"),VM.getValue("betrecord.targetgame"),page,10);
	},

	ShowBetInfoView(url){
		VM.setValue("core.tagCode",0);
		let vc = CtrlAssistant.main.getCtrl("MainCtrl");
		vc.AllScreenWebUrl(url);
		VM.setValue("core.lock",false);
	},

	showAlert(type,key){
		TitleAlertView.main.show(type,VM.getValue("lang."+key));
	},

});