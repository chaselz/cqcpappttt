import {VM} from "../modelView/ViewModel";
import AnalyticsHelper from "../Extension/Nactive/AnalyticsHelper";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.setValue = function () {
			let data = VM.getValue("member.data");
			this.setContent("Account",data.account);
			this.setContent("Name",data.name);
			this.setContent("Birthday",data.birthday);
			this.setContent("Currency","RMB");
			this.setContent("Mail",this.hidden(data.email,3,0));
			this.setContent("Phone",this.hidden(data.phone,3,0));
			this.setContent("QQ",data.qq);
			this.setContent("Webchat",data.wechat);
		};

		this.setContent = function (type,str) {
			this.view[type+"/content"].getComponent(cc.Label).string =str;
		};

		this.hidden = function(str,frontLen,endLen) {
			var len = str.length-frontLen-endLen;
			var xing = '';
			for (var i=0;i<len;i++) {
				xing+='*';
			}
			return str.substring(0,frontLen)+xing+str.substring(str.length-endLen);
		}
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.accountinfo"));
		this.setValue();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		UI_manager.add_button_listen(this.view["logoutBtn"],this,function () {
			ApiManager.Login.userlogout();
			AnalyticsHelper.logout();
		});
	},


});