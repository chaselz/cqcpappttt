import DropDown from "../UI/DropDown/DropDown";
import {PayType} from "../Enum/PayType";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var DropDownOptionData = require("DropDownOptionData");
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,



	properties: {
		choosepath: {
			get(){
				return this.path + "chooseway/content/";
			}
		},
		inputpath: {
			get(){
				return this.path + "inputbank/content/";
			}
		},
	},

	ctor(){
		this.path = "Scroll/view/content/";
		/**
		 * 新增UIAssisant的Page
		 */
		this.addPages = function () {
			var Page = require("Page");
			let page = new Page();
			page.NAME = "CheckDeposit";
			page.panels.push("PayView");
			page.panels.push("Top");
			page.panels.push("BelowContent");
			page.panels.push("checkview");
			UIAssistant.main.AddPage(page);

			let page2 = new Page();
			page2.panels.push("PayView");
			page2.NAME = "InputDeposit";
			page2.panels.push("Top");
			page2.panels.push("BelowContent");
			page2.panels.push("inputbank");
			UIAssistant.main.AddPage(page2);

			let page3 = new Page();
			page3.panels.push("PayView");
			page3.NAME = "QrCode";
			page3.panels.push("Top");
			page3.panels.push("BelowContent");
			page3.panels.push("QrCodeView");
			UIAssistant.main.AddPage(page3);

			let page4 = new Page();
			page4.panels.push("PayView");
			page4.NAME = "TransferInfo";
			page4.panels.push("Top");
			page4.panels.push("BelowContent");
			page4.panels.push("transferInfo");
			UIAssistant.main.AddPage(page4);
		};
		/**
		 * 設定選擇支付後的兩頁的下一步按鈕事件
		 */
		this.setNextBtn = function () {
			UI_manager.add_button_listen(this.view[this.path+"inputbank/buttom/Btn"],this,function (){
				let noerr = true;
				if(VM.getValue("recharge.depositway") == undefined) {
					noerr = false;
					this.ErrorMod(this.inputpath+"transferWay");
				}
				let namevalue =  this.view[this.inputpath+"name/inputbox"].getComponent(cc.EditBox).string;
				if(namevalue.isNull() || namevalue.length < 2 || !namevalue.isName()){
					noerr = false;
					this.ErrorMod(this.inputpath+"name");
				}
				let timevalue =  this.view[this.inputpath+"time/inputbox/label"].getComponent(cc.Label).string;
				if(timevalue.isNull()){
					noerr = false;
					this.ErrorMod(this.inputpath+"time");
				}
				let branchvalue =  this.view[this.inputpath+"branch/inputbox"].getComponent(cc.EditBox).string;
				if(this.view[this.inputpath+"branch"].active && (branchvalue.isNull() || branchvalue.length < 2 || !branchvalue.isName())){
					noerr = false;
					this.ErrorMod(this.inputpath+"branch");
				}
				VM.setValue("recharge.iserr",!noerr);
				if(noerr){
					PageManager.CheckDepositPage();
					VM.setValue("recharge.deposittime",this.GameNowDateString());
				}else{
					this.showAlertErr();
				}
			});
			UI_manager.add_button_listen(this.view[this.path+"checkview/buttom/Btn"],this,function () {
				if(VM.getValue("core.lock"))
					return;
				VM.setValue("core.lock",true);
				this.view[this.path+"checkview/buttom/Btn"].getComponent(cc.Button).interactable = false;
				this.activeObj(this.path+"checkview/buttom/load",true);
				ApiManager.Deposit.get.DepositProvider(VM.getValue("recharge.bankid"),VM.getValue("recharge.deposittime"),VM.getValue("recharge.depositname"),VM.getValue("recharge.depositway"),VM.getValue("recharge.depositbranch"),VM.getValue("recharge.money"));
			});
			UI_manager.add_button_listen(this.view[this.path+"QrCodeView/BackBtn"],this,function () {
				PageManager.RechargePage();
			});
		};

		this.GameNowDateString = function(){
			var now1 = new Date();
			return  "{0}-{1}-{2} {3}:{4}".format(now1.getFullYear(),(now1.getMonth()+1).preZeroFill(2),now1.getDate().preZeroFill(2),now1.getHours().preZeroFill(2),now1.getMinutes().preZeroFill(2))
		};

		/**
		 * 顯示錯誤的警告
		 */
		this.showAlertErr = function(){
			this.showAlert(TitleAlertType.Warn,VM.getValue("lang.inputErr"));
		};
		/***
		 * 顯示警告
		 * @param type
		 * @param key
		 */
		this.showAlertWithLang = function (type,key) {
			this.showAlert(type,VM.getValue("lang."+key));
		};
		/***
		 * 顯示警告
		 * @param type
		 * @param str
		 */
		this.showAlert = function (type,str) {
			TitleAlertView.main.show(type,str);
		};
		/**
		 * 設定存款方式的DropDown的值
		 */
		this.setTransFerDropDownData = function () {
			let temp = [];
			let arr = ["onlinetransfer","bankcounters","atmcashdeposit","atmcashmachine"];
			arr.forEach(x=>{
				let datatemp = new DropDownOptionData.default();
				datatemp.setValue(VM.getValue("lang."+x));
				temp.push(datatemp);
			});
			this.setDropBoxValue(this.inputpath+"transferWay/DropDown",temp);
		};
		/**
		 * 設定部分DropDown 的選擇回傳事件
		 */
		this.setDropDownCB = function () {
			this.setDropDownCallBack(this.inputpath+"transferWay/DropDown",function (index,value) {
				VM.setValue("recharge.depositway",value);
				VM.setValue("recharge.depositwayindex",index);
				this.NormalMod(this.inputpath+"transferWay");
				this.activeObj(this.inputpath+"branch",(index != 0));
				if(index == 0)
					VM.setValue("recharge.depositbranch",VM.getValue("lang.onlinebank"));
			}.bind(this));
		};
		/**
		 * 設定所以EditBox text-changed 事件
		 */
		this.setEditBoxChangeValue = function () {
			this.view[this.inputpath+"name/inputbox"].on("text-changed",function (value) {
				if(VM.getValue("recharge.iserr")){
					let iserr = value.string.length<2 || !value.string.isName();
					this.activeObj(this.inputpath + "name/nameErr1",value.string.length<2);
					this.activeObj(this.inputpath + "name/nameErr2",!value.string.isName());
					if(iserr)
						this.ErrorMod(this.inputpath+"name");
					else
						this.NormalMod(this.inputpath+"name");

				}
				VM.setValue("recharge.depositname",value.string);
			},this);
			this.view[this.inputpath+"branch/inputbox"].on("text-changed",function (value) {
				if(VM.getValue("recharge.iserr")){
					let iserr = value.string.length<2 || !value.string.isName();
					console.log("isnull = "+value.string.isNull());
					this.activeObj(this.inputpath + "branch/branchErr1",value.string.length<2 && !value.string.isNull());
					this.activeObj(this.inputpath + "branch/branchErr2",!value.string.isName() && !value.string.isNull());
					if(iserr)
						this.ErrorMod(this.inputpath+"branch");
					else
						this.NormalMod(this.inputpath+"branch");

				}
				VM.setValue("recharge.depositbranch",value.string);
			},this);
		};
		/**
		 * 初始化選擇支付的
		 * @constructor
		 */
		this.InitPayWayView = function () {
			this.setDropDownInit(this.choosepath+"PayWay/DropDown");
			let iscommon = VM.getValue("recharge.paytype") == "common";
			let key = iscommon ? "paybank":"payway";
			this.view[this.choosepath+"PayWay/top/Title"].getComponent("VMLabel").setWatchArrValue(["lang."+key]);
			// this.view[this.choosepath+"PayWay/DropDown"].getComponent(DropDown).init( VM.getValue("lang."+(iscommon?"choosepaybank":"choosepayway")));
		};
		/**
		 * 初始化所有DropDown的值
		 * @constructor
		 */
		this.InitDropDown = function () {
			let dropdown = this.view[this.choosepath + "PayWay/DropDown"].getComponent(DropDown);
			dropdown.init(VM.getValue("lang.choosepayway"));
			let dropdown1 = this.view[this.choosepath + "transferWay/DropDown"].getComponent(DropDown);
			dropdown1.init(VM.getValue("lang."+ (VM.getValue("recharge.paytype") == PayType.BankOnline ? "chooseonlinepay":"choosepaybank")));
			let dropdown2 = this.view[this.inputpath + "transferWay/DropDown"].getComponent(DropDown);
			dropdown2.init(VM.getValue("lang.choosedepositway"));
		};
		/**
		 * 設定TransferInfo 的 Btn 功能
		 */
		this.setTransferInfoBtn = function () {
			UI_manager.add_button_listen(this.view[this.path+"transferInfo/buttom/Btn"],this,function () {
				if(this.isUIActive(this.path+"transferInfo/warm")){
					VM.setValue("core.lock",true);
					setTimeout(function () {
						PageManager.MemberPage();
						VM.setValue("core.lock",false);
					},2000);
				}else{
					VM.setValue("core.lock",true);
					let self = this;
					setTimeout(function () {
						VM.setValue("core.lock",false);
						self.view[self.path+"transferInfo/buttom/Btn/Bg/Label"].getComponent(cc.Label).string = VM.getValue("lang.transferfinish");
						self.activeObj(self.path+"transferInfo/warm",true);
						TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.transationapplying"));
					},1000);
				}
			});
		};

		this.setCopyBtns = function () {
			var bankinfopath = "Scroll/view/content/checkview/bankinfo/";
			UI_manager.add_button_listen(this.view[bankinfopath+"bank/copyBtn"],this,function () {
				if(MobileExtension.Copy(this.view[bankinfopath+"bank/value"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[bankinfopath+"location/copyBtn"],this,function () {
				if(MobileExtension.Copy(this.view[bankinfopath+"location/value"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[bankinfopath+"name/copyBtn"],this,function () {
				if(MobileExtension.Copy(this.view[bankinfopath+"name/value"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[bankinfopath+"account/copyBtn"],this,function () {
				if(MobileExtension.Copy(this.view[bankinfopath+"account/value"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			
			var transferpath = "Scroll/view/content/transferInfo/";
			UI_manager.add_button_listen(this.view[transferpath+"payee/copy"],this,function () {
				if(MobileExtension.Copy(this.view[transferpath+"payee/msg"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[transferpath+"location/copy"],this,function () {
				if(MobileExtension.Copy(this.view[transferpath+"location/msg"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[transferpath+"name/copy"],this,function () {
				if(MobileExtension.Copy(this.view[transferpath+"name/msg"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[transferpath+"account/copy"],this,function () {
				if(MobileExtension.Copy(this.view[transferpath+"account/msg"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
			UI_manager.add_button_listen(this.view[transferpath+"amount/copy"],this,function () {
				if(MobileExtension.Copy(this.view[transferpath+"amount/msg"].getComponent(cc.Label).string)){
					this.showAlertWithLang(TitleAlertType.Fulfill,"copysuccess");
				}
			});
		};
	},
	/**
	 * 初始化
	 */
	init() {
		this.view[this.path+"checkview/buttom/Btn"].getComponent(cc.Button).interactable = true;
		this.activeObj(this.path+"checkview/buttom/load",false);
		this.ChooseWayInit();
		this.InitPayWayView();
		this.InitDropDown();
		this.NormalMod(this.choosepath+"PayWay");
	},

	onEnable(){
		VM.setValue("core.lock",false);
		this.init();
	},
	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"PayViewCtrl");
	},

	start() {
		this.addPages();
		this.setNextBtn();
		this.setTransFerDropDownData();
		this.setDropDownCB();
		this.setEditBoxChangeValue();
		this.setTransferInfoBtn();
		this.setCopyBtns();
	},
	/**
	 * 設定UI的顯示狀態
	 * @param path UI路境
	 * @param active 是否顯示
	 */
	activeObj(path,active) {
		this.view[path].active = active;
	},
	/**
	 * 設定UI Array 的顯示狀態
	 * @param arr UI們
	 * @param active 是否顯示
	 */
	activeArrObj(arr,active) {
		arr.forEach(x=>this.activeObj(x,active));
	},
	/**
	 * 選擇支付頁UI初始化
	 * @constructor
	 */
	ChooseWayInit(){
		let _path = this.path + "chooseway/content/";
		this.activeArrObj([_path+"warnview",_path+"transferWay",_path+"cardInfo",_path+"money",_path+"BankIconlayout"],false);
		this.view[this.path+"chooseway/buttom/Btn/Bg/Label"].getComponent("VMLabel").onValueInit();
	},
	/**
	 * 設定DropDown資料
	 * @param path DropDown路徑
	 * @param data 資料
	 */
	setDropBoxValue(path,data){
		let _node = this.view[path];
		if(!_node)
			return;

		let dropdown = _node.getComponent("DropDown");
		dropdown.clearOptionDatas();
		dropdown.addOptionDatas(data);
	},
	TransferDropDownInit(){
		let dropdown = this.view[this.choosepath + "transferWay/DropDown"].getComponent(DropDown);
		dropdown.clearOptionDatas();
		dropdown.init(VM.getValue("lang."+ (VM.getValue("recharge.paytype") == PayType.BankOnline ? "chooseonlinepay":"choosepaybank")));

	},
	/**
	 * DropDown 初始化
	 * @param path DropDown路徑
	 */
	setDropDownInit(path){
		let _node = this.view[path];
		if(!_node)
			return;

		let dropdown = _node.getComponent("DropDown");
		dropdown.clearOptionDatas();
		dropdown.init();
	},
	/**
	 * 設定轉帳銀行卡資料
	 * @param data 銀行卡資料
	 */
	setCardInfo(data){
		this.view[this.choosepath+"cardInfo/content/bank/info"].getComponent(cc.Label).string = data.bank_name;
		this.view[this.choosepath+"cardInfo/content/payee/info"].getComponent(cc.Label).string = data.name;
		this.view[this.choosepath+"cardInfo/content/location/info"].getComponent(cc.Label).string = data.account_point;
		this.view[this.choosepath+"cardInfo/content/account/info"].getComponent(cc.Label).string = data.account;
	},
	/**
	 * 設定DopDown的選擇回傳事件
	 * @param path UI路徑
	 * @param callback 回傳事件
	 */
	setDropDownCallBack(path,callback){
		let dropdown = this.view[path].getComponent("DropDown");
		dropdown.SetSelectIndexCallBack(callback);
	},
	/**
	 * 輸入框錯誤狀態
	 * @param path
	 * @constructor
	 */
	ErrorMod(path) {
		if(this.view[path+"/line"].color == VM.getValue("color.red"))
			return;
		this.view[path+"/line"].color = VM.getValue("color.red");
		this.view[path+"/top/Title"].color = VM.getValue("color.red");
	},
	/**
	 * 輸入框一般狀態
	 * @param path
	 * @constructor
	 */
	NormalMod(path) {
		if(this.view[path+"/line"].color == VM.getValue("color.linecolor"))
			return;
		this.view[path+"/line"].color = VM.getValue("color.linecolor");
		this.view[path+"/top/Title"].color = VM.getValue("color.graylabel3");
	},
	/**
	 * 設定選擇支付確定按鈕事件
	 * @param func onClick 事件
	 * @param caller 觸發者
	 * @constructor
	 */
	SetChooseNextBtnEvent(func,caller) {
		UI_manager.add_button_listen(this.view[this.path+"chooseway/buttom/Btn"],caller,func);
	},
	/**
	 * 清除選擇支付確定按鈕事件
	 * @constructor
	 */
	ClearChooseNextBtnEvent(){
		UI_manager.clear_button_listen(this.view[this.path+"chooseway/buttom/Btn"]);
	},
	/**
	 * UI是否有顯示
	 * @param path 路徑
	 * @returns {是否}
	 */
	isUIActive(path){
		return this.view[path].active;
	},
	/**
	 * 改变二维码显示内容
	 * @param str
	 */
	changeQRCodeUI(str){
		let qrcode = this.view[this.path+"QrCodeView/QrCodeImg"].getComponent(cc.Sprite);
		let imgData = str;
		let image = new Image();

		image.onload = function() {
			let texture = new cc.Texture2D();
			texture.initWithElement(image);
			texture.handleLoadedTexture();
			qrcode.spriteFrame = new cc.SpriteFrame(texture);
		};
		image.src = imgData;
	},
	/**
	 * 設定QRCode
	 * @param url qrcode圖片網址
	 */
	setQRCodeImg(url){
		cc.loader.load(url, function (err, texture) {
			var sprite  = new cc.SpriteFrame(texture);
			this.view[this.path+"QrCodeView/QrCodeImg"].getComponent(cc.Sprite).spriteFrame = sprite;
		});
	},
	/**
	 * 設定選擇支付畫面的按鈕文字
	 * @param value 按鈕文字
	 */
	setChooseBtnName(value){
		this.view[this.path+"chooseway/buttom/Btn/Bg/Label"].getComponent("VMLabel").setLabelValue(value);
	},
	/**
	 * 設定網銀轉帳圖片
	 * @param url 圖片網址
	 */
	setBankIcon(url){
		if(url == null || url == undefined || !url.isURL()) {
			console.log("not url");
			return;
		}

		this.activeObj(this.choosepath+"BankIconlayout",true);
		UI_manager.setSpriteFormUrl(this.view[this.choosepath+"BankIconlayout/bankicon"],url);
	},
	/**
	 * 設定匯款人資料
	 * @param data
	 */
	setTransferAccountInfo(data){
		let _path = this.path + "transferInfo/";
		this.view[_path+"name/msg"].getComponent(cc.Label).string = data.bank_name;
		this.view[_path+"payee/msg"].getComponent(cc.Label).string = data.payee_name;
		this.view[_path+"account/msg"].getComponent(cc.Label).string = data.card_number;
		this.view[_path+"location/msg"].getComponent(cc.Label).string = data.location;
		this.view[_path+"amount/msg"].getComponent(cc.Label).string = data.amount.toFixed(2);
	},
	/**
	 * 顯示金額旁的上線下提示
	 * @param active
	 */
	activeMoneyWarn(active){
		this.activeObj(this.choosepath+"money/top/warn",active);
	},

	setBirthDay(){
		this.view[this.inputpath+"time/inputbox/label"].getComponent(cc.Label).string = this.GameNowDateString();
	}
});