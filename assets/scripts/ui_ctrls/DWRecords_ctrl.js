import Awaiter from "../Extension/AwaitExtension";
import RecordItem from "../Item/RecordItem";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.mode = RecordMode.Deposit;

		this.setNoDataBtn = function () {
			UI_manager.add_button_listen(this.view["content/DWRecordNoData/Btn"],this,function () {
				PageManager.RechargePage();
			});
		};

		this.setChangeModeBtns = function () {
			UI_manager.add_button_listen(this.view["top/depositBtn"],this,this.depositBtnFunc);
			UI_manager.add_button_listen(this.view["top/withdrawBtn"],this,this.withDrawBtnFunc);
		};

		this.depositBtnFunc = function () {
			if(VM.getValue("core.lock"))
				return;
			this.openLoad();
			this.playAnim(RecordMode.Deposit);
			this.setTitleLableRed(RecordMode.Deposit);
			this.depositApi(0);
			this.drawGraphicsNodes("apply");
			this.setNoPassView();
		};

		this.withDrawBtnFunc = function () {
			if(VM.getValue("core.lock"))
				return;
			this.openLoad();
			this.playAnim(RecordMode.WithDraw);
			this.setTitleLableRed(RecordMode.WithDraw);
			this.withDrawApi(0);
			this.drawGraphicsNodes("apply");
			this.setNoPassView();
		};

		this.drawGraphicsNodes = function (key) {
			this.waitToDrawAsync("apply",key);
			this.waitToDrawAsync("transation",key);
			this.waitToDrawAsync("nopass",key);
		};

		this.drawGraphics = function (path,mode) {
			let node = this.view[path];
			if(!node.getComponent(cc.Graphics))
				return;

			let graphics = node.getComponent(cc.Graphics);
			graphics.clear();
			graphics.roundRect(0,0,node.width,node.height,25);
			let isred = mode == StatusMode.RED;
			this.view[path+"/msg"].color = isred ? cc.Color.WHITE : VM.getValue("color.black");
			graphics.fillColor = (isred ? VM.getValue("color.red") : cc.Color.WHITE);
			graphics.fill();
			if(isred)
				return;

			graphics.stroke();
		};

		this.waitToDrawAsync = async function (path,key) {
			let uipath = "content/top/"+path;
			await Awaiter.until(_=> !this.view[uipath+"/msg"].getComponent(cc.Label).string.isNull());
			this.drawGraphics(uipath,path == key ? StatusMode.RED : StatusMode.NORMAL);
		};

		this.setStatusBtn = function () {
			this.statusBtnFunc("apply",0);
			this.statusBtnFunc("transation",1);
			this.statusBtnFunc("nopass",2);
		};

		this.statusBtnFunc = function (ui,mode) {
			UI_manager.add_button_listen(this.view["content/top/"+ui],this,function () {
				if(VM.getValue("core.lock"))
					return;
				this.openLoad();
				this.drawGraphicsNodes(ui);
				if(this.mode == RecordMode.Deposit)
					this.depositApi(mode);
				else
					this.withDrawApi(mode);
			});
		};

		this.depositApi = function (mode) {
			ApiManager.Report.DepositReport(mode);
		};

		this.withDrawApi = function (mode) {
			ApiManager.Report.WithDrawReport(mode);
		};

		this.setTitleLableRed = function (type) {
			this.mode = type;
			this.view["top/depositBtn/bg/text"].color = VM.getValue(type == RecordMode.Deposit ? "color.red":"color.black");
			this.view["top/withdrawBtn/bg/text"].color = VM.getValue(type == RecordMode.WithDraw ? "color.red":"color.black");
		};

		this.playAnim = function (type) {
			if(this.mode != type)
				this.view["chooseline"].getComponent(cc.Animation).play(this.mode == RecordMode.Deposit ? "modifypwright" : "modifypwleft");

		};

		this.setNoPassView = function () {
			let isdepositmode = this.mode == RecordMode.Deposit;
			this.activeNode("content/DWRecordNoData/Btn",isdepositmode);
			UI_manager.setSprite(this.view["content/DWRecordNoData/icon"],isdepositmode?"bg-no-deposit":"bg-no-withdraw");
			this.view["content/DWRecordNoData/msg"].getComponent("VMLabel").setWatchPath(isdepositmode?"lang.nodepositrecord":"lang.nowithdrawrecord");
		};

		this.activeNode = function (path,_active) {
			if(this.view[path].active != _active)
				this.view[path].active = _active;
		};
	},

	onEnable(){
		this.mode = RecordMode.Deposit;
		VM.setValue("core.lock",false);
		VM.setValue("core.title",VM.getValue("lang.accessrecord"));
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"DWRecordsCtrl");
	},

	start() {
		this.setStatusBtn();
		this.setChangeModeBtns();
		this.setNoDataBtn();
	},

	async setRecordList(data){
		let hasData = data.length > 0;
		UIAssistant.main.SetPanelVisible("DWRecordList",hasData);
		UIAssistant.main.SetPanelVisible("DWRecordNoData",!hasData);
		if(!hasData) {
			this.closeLoad();
			return;
		}

		await data.asyncForEach(async (value,index) =>{
			await this.creatFactory(index,value);
		});

		let node_parent = this.view["content/DWRecordList/view/content"];
		for(let i = data.length;i<node_parent.childrenCount;i++){
			node_parent.children[i].active = false;
		}
		this.closeLoad();
	},

	async creatFactory(index,data) {
		let node_parent = this.view["content/DWRecordList/view/content"];
		let item = null;
		if(index >= node_parent.childrenCount)
			item =  await UI_manager.create_prefab_atSync(node_parent, "RecordItem");
		else
			item =  node_parent.children[index];

		item.active = true;
		item.getComponent(RecordItem).setValue(data);
		item.getComponent(RecordItem).ActiveBtn(this.mode == RecordMode.Deposit);
	},

	openLoad(){
		VM.setValue("core.lock",true);
		UIAssistant.main.SetPanelVisible("Load",true);
	},

	closeLoad(){
		VM.setValue("core.lock",false);
		UIAssistant.main.SetPanelVisible("Load",false);
	},

	Init(){
		this.depositBtnFunc();
	}
});

var RecordMode = Object.freeze({
	Deposit:Symbol(1),
	WithDraw:Symbol(2)
});

var StatusMode = Object.freeze({
	RED:Symbol(1),
	NORMAL:Symbol(2)
});