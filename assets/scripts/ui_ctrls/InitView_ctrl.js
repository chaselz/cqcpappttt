import {PageViewType} from "../Enum/PageViewType";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,
	ctor(){
		this.value = 0;
		this.lerp = 0.05;
		this.targetValue = 0;

		this.updateProgressBar = function(){
			var num = this.formatFloat(this.value,3);
			this.finish = num == 1;
			this.view["ProgressBar"].getComponent(cc.ProgressBar).progress = num;
		};

		this.formatFloat = function(num, pos)
		{
			var size = Math.pow(10, pos);
			return Math.round(num * size) / size;
		};
	},

	properties: {
		// 声明 player 属性
		finish: {
			default: [],
			type: cc.Boolean
		}
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"InitViewCtrl");
		this.node.active = false;
		this.view["version"].getComponent(cc.Label).string = MobileExtension.GetVersion() + " - " + cc.game.resver;
	},

	update(){
		if(!this.finish && this.targetValue > 0 && this.value != 1) {
			this.value = cc.misc.lerp(this.value, this.targetValue, this.lerp);
			this.updateProgressBar();
		}
	},

	initProgress(){
		this.finish = false;
		this.value = 0;
		this.targetValue = 0;
		this.updateProgressBar();
	},

	setState(progress,title = ""){
		if(UIAssistant.main.GetCurrentPage() != PageViewType.InitView)
			PageManager.InitViewPage();

		this.targetValue = progress;
		if(!title.isNull())
			this.view["StateLabel"].getComponent(cc.Label).string = title;
	}

});