import {RechargeType} from "../Enum/RechargeType";
import RechargeList from "../AbstractClass/Recharge/RechargeList";
import ChooseRecharge from "../AbstractClass/Recharge/ChooseRecharge";
import TransferRecharge from "../AbstractClass/Recharge/TransferRecharge";
import QRRecharge from "../AbstractClass/Recharge/QRRecharge";
import JXPayRecharge from "../AbstractClass/Recharge/JXPayRecharge";
import FourthRecharge from "../AbstractClass/Recharge/FourthRecharge";
import {PageViewType} from "../Enum/PageViewType";
import {DetailType} from "../Enum/DetailType";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var Page = require("Page");
var ApiType = require("ApiType");

cc.Class({
	extends: UI_ctrl,

	ctor(){
		this.IRechargeList = {};
		this.targetMode = null;

		this.InitIRecharge = function () {
			this.IRechargeList[RechargeType.List] = new RechargeList(this,this.view);
			this.IRechargeList[RechargeType.Choose] = new ChooseRecharge(this,this.view);
			this.IRechargeList[RechargeType.Transfer] = new TransferRecharge(this,this.view);
			this.IRechargeList[RechargeType.JXPay] = new JXPayRecharge(this,this.view);
			this.IRechargeList[RechargeType.QR] = new QRRecharge(this,this.view);
			this.IRechargeList[RechargeType.FourthPay] = new FourthRecharge(this,this.view);
		};

		this.setModeView = function (indexmode) {
			if(indexmode == RechargeType.List)
				return;
			//this.view["RechargeMain/view/content/RechargeNextBtn"].active = indexmode != RechargeType.List;
			UIAssistant.main.ShowPage("Recharge"+indexmode);
		};

		this.addRechargePage = function () {
			let temp = new Page();
			temp.NAME = "RechargeList";
			temp.panels.push("Top");
			temp.panels.push("BelowContent");
			temp.panels.push("RechargeV2");
			temp.panels.push("RechargePaylist");
			UIAssistant.main.AddPage(temp);

			let temp2 = new Page();
			temp2.NAME = "RechargeChoose";
			temp2.panels.push("Top");
			temp2.panels.push("BelowContent");
			temp2.panels.push("RechargeV2");
			temp2.panels.push("RechargeChooseView");
			temp2.panels.push("RechargeNextBtn");
			UIAssistant.main.AddPage(temp2);

			let temp3 = new Page();
			temp3.NAME = "RechargeTransfer";
			temp3.panels.push("Top");
			temp3.panels.push("BelowContent");
			temp3.panels.push("RechargeV2");
			temp3.panels.push("TransferInfoView");
			temp3.panels.push("TransferNextBtn");
			UIAssistant.main.AddPage(temp3);

			let temp4 = new Page();
			temp4.NAME = "RechargeJXPay";
			temp4.panels.push("Top");
			temp4.panels.push("BelowContent");
			temp4.panels.push("RechargeV2");
			temp4.panels.push("JXPayView");
			temp4.panels.push("JXPayNextBtn");
			UIAssistant.main.AddPage(temp4);

			let temp5 = new Page();
			temp5.NAME = "RechargeQR";
			temp5.panels.push("Top");
			temp5.panels.push("BelowContent");
			temp5.panels.push("RechargeV2");
			temp5.panels.push("QRView");
			temp5.panels.push("QRNextBtn");
			UIAssistant.main.AddPage(temp5);

			let temp6 = new Page();
			temp6.NAME = "RechargeFourthPay";
			temp6.panels.push("Top");
			temp6.panels.push("BelowContent");
			temp6.panels.push("RechargeV2");
			temp6.panels.push("FourthPayView");
			temp6.panels.push("QRNextBtn");
			UIAssistant.main.AddPage(temp6);
		};

		this.InitBtn = function () {
			UI_manager.add_button_listen(this.view["RechargeMain/view/content/QRNextBtn"],this,function () {
				VM.setValue("core.defaultdetail",DetailType.DW);
				PageManager.DetailsPage();
			});
		}
	},

	Init(){
		this.setMode(RechargeType.List);
	},

	onEnable(){

	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"RechargeV2Ctrl");
		this.addRechargePage();
		this.InitIRecharge();
		this.InitBtn();
	},

	start() {

	},


	setRechargeMenuData(data){
		this.targetMode.setData(data);
	},

	setModeMenu(data){
		this.targetMode.setData(data,ApiType.depositModeAllMenuType);
	},

	setModeBankMenu(data){
		this.targetMode.setData(data,ApiType.depositModeBankMenuAllType);
	},

	setMode(indexmode){
		this.targetMode = this.IRechargeList[indexmode];
		this.setModeView(indexmode);
		if(this.targetMode)
			this.targetMode.Init();
	},

	setBankList(data){
		this.targetMode.setData(data,ApiType.bankListType);
	},

	sentDepositProviderResutl(data){
		cc.log("sentDepositProviderResutl");
		this.targetMode.setData(data,ApiType.depositProviderType);
	},

	sentDepositFourthResult(data){
		this.targetMode.setData(data,ApiType.depositFourthType);
	}
});