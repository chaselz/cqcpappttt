var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
const {VM} = require('ViewModel');
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");
var Page = require("Page");
var LoginType = require("LoginType");
cc.Class({
	extends: UI_ctrl,

	ctor(){
		/*
		 * 設定顯示與隱藏密碼
		 */
		this.setShowPWBtn = function(){
			let self = this;
			UI_manager.add_button_listen(this.view["PWInput/ShowPWBtn"], null, function () {
				let inputbox = self.view["PWInput"].getComponent("Inputbox");
				let status = inputbox.getKeyWordType() == cc.EditBox.InputFlag.DEFAULT;
				inputbox.setKeyWordType(status ? cc.EditBox.InputFlag.PASSWORD : cc.EditBox.InputFlag.DEFAULT);
				UI_manager.setSprite(self.view["PWInput/ShowPWBtn/Bg"], "universal/icon-show-pw"+(status ? "":"-show"));
			});
		};
		/*
		 * 設定InputBox 事件
		 */
		this.setInputBoxsEvent = function(){
			this.setInputBoxEvent(this.view["AccountInput"],4);
			this.setInputBoxEvent(this.view["PWInput"],6);
			// let self = this;
			// let Input = this.view["AccountInput"].getComponent("Inputbox");
			// let Input2 = this.view["PWInput"].getComponent("Inputbox");
			// Input.add_EditorChanged_listen(null,function (value) {
			//
			// 	if(!VM.getValue("login.hasErr"))
			// 		return;
			// 	if(value.string.length < 4) {
			// 		Input.setRed();
			// 		self.showAccountError();
			// 	}else {
			// 		Input.setFocus();
			// 		self.hideAccountError();
			// 	}
			// });
			// Input2.add_EditorChanged_listen(null,function (value) {
			//
			// 	if(!VM.getValue("login.hasErr"))
			// 		return;
			// 	if(value.string.length < 6) {
			// 		Input2.setRed();
			// 		self.showPwError();
			// 	}else{
			// 		Input2.setFocus();
			// 		self.hidePwError();
			// 	}
			// });
		};
		/*
		 * 設定輸入Inputbox 開始與結束事件
		 *  @param InputBox 輸入UI
		 *  @param mix 最少輸入的數值
		 */
		this.setInputBoxEvent = function(InputBox,mix){
			let Input = InputBox.getComponent("Inputbox");
			Input.add_BeginEditor_listen(null,function (value) {
				if(!VM.getValue("login.hasErr") ||  value.string.length >= mix)
					Input.setFocus();
			});
			Input.add_DidEndEditor_listen(null,function (value) {
				if(!VM.getValue("login.hasErr") || value.string.length >= mix)
					Input.Focus();
			});

		};
		/*
		 * 設定InputBox Id
		 */
		this.setInputId = function () {
			this.view["AccountInput"].getComponent("Inputbox").id = "id";
			this.view["PWInput"].getComponent("Inputbox").id = "password";
		};

		this.ForgetPWBtn = function () {
			UI_manager.add_button_listen(this.view["ButtomPage/ForgetPWBtn"],this,function () {
				CtrlAssistant.main.getCtrl("MainCtrl").AllScreenWebUrl(VM.getValue("setting.serviceurl"));
			});
		};
	},

	onEnable(){
		VM.setValue("login.hasErr",false);
		VM.setValue("core.title",VM.getValue("lang.login"));
		CtrlAssistant.main.getCtrl("LoginCtrl").setToggleBtn(LoginType.SignIn);
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		//CtrlAssistant.main.saveCtrl(this,"SigninCtrl");
	},

	start() {
		this.setInputBoxsEvent();
		this.setShowPWBtn();
		this.setInputId();
		this.ForgetPWBtn();
	},
	/*
	 * 設定登入事件
	 * @param func 觸發事件
	 * @param caller 呼叫者
	 */
	setLoginEvent(func,caller){
		UI_manager.add_button_listen(this.view["ButtomPage/LoginBtn"],caller,func);
	},

	/*
	 * 清除登入事件
	 * @param target 原本登入事件是的物件
	 * @param caller 呼叫者
	 */
	ClearLoginEvent(target,caller){
		this.view["ButtomPage/LoginBtn"].targetOff(target);
		UI_manager.clear_button_listen(this.view["ButtomPage/LoginBtn"],caller,target);
	},
	Init(){
		this.InitEditBoxValue();
		this.hideAccountError();
		this.hidePwError();
	},
	/*
     * 初始化EditBox
     */
	InitEditBoxValue(){
		let aInput = this.view["AccountInput"].getComponent("Inputbox");
		let pInput = this.view["PWInput"].getComponent("Inputbox");
		aInput.setString("");
		pInput.setString("");
		aInput.Focus();
		pInput.Focus();
	},
	/*
     * 取的帳號數值
     */
	getAccount(){
		return this.view["AccountInput"].getComponent("Inputbox").getString();
	},
	/*
     * 取的密碼數值
     */
	getPassword(){
		return this.view["PWInput"].getComponent("Inputbox").getString();
	},
	setAccount(value){
		this.view["AccountInput"].getComponent("Inputbox").setString(value);
	},
	/*
     * 顯示帳號錯誤
     */
	showAccountError(){
		this.view["AccountInput"].getComponent("Inputbox").setRed();
		this.view["AccountErr"].active = true;
	},
	/*
     * 顯示密碼錯誤
     */
	showPwError(){
		this.view["PWInput"].getComponent("Inputbox").setRed();
		// this.view["PWErr"].active = true;
	},
	/*
     * 隱藏帳號錯誤提示
     */
	hideAccountError(){
		this.view["AccountErr"].active = false;
	},
	/*
     * 隱藏密碼錯誤提示
     */
	hidePwError(){
		this.view["PWErr"].active = false;
	},
});