import MobileExtension, {UIInterfaceOrientation} from "../Extension/Nactive/MobileExtension";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var CtrlAssistant = require("CtrlAssistant");
var CtrlAssistant = require("CtrlAssistant");
@ccclass
export default class WebView_ctrl extends cc.Component {


    protected onEnable(): void {
        VM.setValue("core.isweb",true);
        //鎖定限制轉屏的遊戲類型
        if(VM.getValue("setting.webviewblacklist").includes(VM.getValue("core.tagCode")))
            return;
        MobileExtension.ChangeUIInterfaceOrientation(UIInterfaceOrientation.All);
    }

    start () {

    }

    protected onDisable(): void {
        VM.setValue("core.isweb",false);
        VM.setValue("core.isallscreenwebview",false);
        MobileExtension.ChangeUIInterfaceOrientation(UIInterfaceOrientation.Portrait);
        if(cc.sys.os == cc.sys.OS_ANDROID)
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', 'reloadWebView', '(I)V', VM.getValue("core.webIndex"));

        CtrlAssistant.main.getCtrl("MainCtrl").webviewloadBlank();
        CtrlAssistant.main.getCtrl("MainCtrl").CloseFloatViewBtn();
    }

}
