var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var UIAssistant = require("UIAssistant");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.CreatItems = async function(path, itemname, data, func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname, index, value, func);
			});
			for(let i = data.length;i<this.view[path].childrenCount;i++){
				this.view[path].children[i].active = false;
			}
		};
	
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let item = null;
			if(index >= this.view[path].getComponentsInChildren(itemname).length) {
				item = await UI_manager.create_item_Sync(this.view[path], itemname);
				item.getComponent(itemname).setValue(data,onClickFunc);
				item.active = true;
			}else {
				item = this.view[path].getComponentsInChildren(itemname)[index];
				item.getComponent(itemname).setValue(data);
				item.node.active = true;
			}
		};
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.mymsg"));
		this.init();
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"MyMessageCtrl");
	},

	start() {
	},

	init(){
		this.showMsgNode("MsgList");
	},

	/**
	 * 設定我的消息清單
	 * @param data 我的消息清單資料
	 */
	async setMessageList(data){
		if(!data)
			return;
		
		await this.CreatItems("MsgList/view/content","MsgItem",data,null);
		UIAssistant.main.SetPanelVisible("Load",false);
	},

	/**
	 * 顯示對應的我的消息節點
	 * @param name 節點名稱
	 */
	showMsgNode(name){
		let msgNode = this.node.children;
		msgNode.forEach((x) => {
			x.active = x.name == name ;
		});
	},

	showAlert(type,str){
		TitleAlertView.main.show(type,str);
	},
});