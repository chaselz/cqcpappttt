import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){

		this.CreatItems = async function(data,path,itemname,func = null) {
			await data.asyncForEach(async (value,index) =>{
				await this.creatFactory(path,itemname,index,value,func);
			});

			let node_parent = this.view[path];
			for(let i = data.length;i<node_parent.childrenCount;i++){
				node_parent.children[i].active = false;
			}
		};
		/***
		 * 物件工廠
		 * @param path	路徑
		 * @param itemname	物件名
		 * @param index	第幾個
		 * @param data	資料
		 * @param onClickFunc	點擊事件
		 * @param type	物件類別
		 * @returns {Promise<void>}
		 */
		this.creatFactory = async function(path,itemname,index,data,onClickFunc,type = null) {
			let node_parent = this.view[path];
			let item = null;

			if(index >= node_parent.childrenCount)
				item =  await UI_manager.create_item_Sync(node_parent, itemname);
			else
				item =  node_parent.children[index];

			item.active = true;
			if(type)
				item.getComponent(itemname).setValue(data,onClickFunc,type);
			else
				item.getComponent(itemname).setValue(data,onClickFunc);
		};

		this.setData = function () {
			let data = VM.getValue("bulletin.bulletinlist");
			this.CreatItems(data,"main/view/content","BulletinItem",this.ToBulletinDetails);
		};

		this.ToBulletinDetails = function () {
			PageManager.BulletinDetailPage();
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
	},

	onEnable(){
		VM.setValue("core.title",VM.getValue("lang.bulletin"));
		this.setData();
	},

	start() {
	},



});