import LotteryType from "../AbstractClass/Hall/LotteryType";
import CasinoType from "../AbstractClass/Hall/CasinoType";
import ChesscardType from "../AbstractClass/Hall/ChesscardType";
import LiveType from "../AbstractClass/Hall/LiveType";
import SportType from "../AbstractClass/Hall/SportType";
import PageManager from "../ManagerUI/PageManager";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.type = null;
		this.target_Hall = null;
		this.Halls = [];
		this.setWithdrawEvent = function () {
			UI_manager.add_button_listen(this.view["ScrollView/view/content/UserNode/Btn/withdrawBtn"],null,function () {
				PageManager.WithDrawPage();
			});
		};

		this.setRechargeEvent = function () {
			UI_manager.add_button_listen(this.view["ScrollView/view/content/UserNode/Btn/rechargeBtn"],null,function () {
				PageManager.RechargePage();
			});
		};

		this.setUserNodeBtn = function () {
			let withdraw = this.view["ScrollView/view/content/UserNode/Btn/withdrawBtn"];
			let withdrawPen = withdraw.getComponent(cc.Graphics);
			withdrawPen.roundRect(0,0,withdraw.width,withdraw.height,16);
			withdrawPen.stroke();
			withdrawPen.fill();
		};
		this.seq = null;

		this.InitHallMode = function () {
			this.Halls.push(new LotteryType(this.view));
			this.Halls.push(new CasinoType(this.view));
			this.Halls.push(new ChesscardType(this.view));
			this.Halls.push(new LiveType(this.view));
			this.Halls.push(new SportType(this.view));
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"HallCtrl");
		this.setWithdrawEvent();
		this.setRechargeEvent();
		this.setUserNodeBtn();
		this.DefaultUserInfoLabel();
		this.InitHallMode();
	},

	onEnable(){
		VM.setValue("hall.gameTitle",null);
		VM.setValue("core.title",VM.getValue(VM.getValue("core.tagCode") == 1 ? "lang.lottery" : "lang.gamehall"));
	},

	start() {

	},

	/**
     * 預設玩家資料
     */
	DefaultUserInfoLabel(){
        this.view["ScrollView/view/content/UserNode/UserInfo/user"].getComponent(cc.Label).string = VM.getValue("lang.visitors");
        this.view["ScrollView/view/content/UserNode/UserInfo/money"].getComponent(cc.Label).string = VM.getValue("lang.TWDollar")+"0.00";
    },

	/**
	 * 設定玩家名稱
	 * @param value 玩家名稱
	 */
	setUserName(value){
		if(this.target_Hall)
			this.target_Hall.setUserName(this.type,value);
	},

	/**
	 * 設定玩家錢包金額
	 * @param value 錢包金額
	 */
	setUserMoney(value){
		if(this.target_Hall)
			this.target_Hall.setUserMoney(this.type,value);
	},

	/**
	 * 初始化頁面
	 * @param type 頁面類型
	 */
	InitPage(type){
		this.type = type;
		this.target_Hall = this.Halls[type-1];
		this.target_Hall.Init();
	},

	/**
	 * 設定廠商清單的頁面
	 * @param type 遊戲類型
	 * @param data 資料
	 */
	setHallData(type,data){
		if(this.target_Hall)
			this.target_Hall.setData(type,data);
	},

	/**
	 * 設定遊戲清單的資料
	 * @param type 遊戲類型
	 * @param data 資料
	 */
	setGameListData(type,data){
		if(this.target_Hall)
			this.target_Hall.setGameListData(type,data);
	},

	/**
	 * 設定 WebView 的 url
	 * @param type 遊戲類型
	 * @param data url 資料
	 */
	setWebUrl(type,data){
		if(this.target_Hall)
			this.target_Hall.setWebUrl(type,data);
	},

	/**
	 * 設定 Load 狀態
	 * @param is_show 顯示狀態
	 */
	setLoad(is_show){
		this.target_Hall.setLoad(is_show);
	},

});