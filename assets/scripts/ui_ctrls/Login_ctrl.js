import MobileExtension from "../Extension/Nactive/MobileExtension";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
const {VM} = require('ViewModel');
var LoginType = require("LoginType");
var CtrlAssistant = require("CtrlAssistant");
var Page = require("Page");
var UIAssistant = require("UIAssistant");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		/*
         * 設定選擇按鈕文字的顏色
         */
		this.setTitleColor = function(type,red,black){
			this.view["logincontent/view/content/below/Toggle/Top/SignInToggle/title"].getComponent("VMLabel").setOriginLabelValue(type == LoginType.SignIn ? "<b>{{0}}</b>":"{{0}}");
			this.view["logincontent/view/content/below/Toggle/Top/SignUpToggle/title"].getComponent("VMLabel").setOriginLabelValue(type == LoginType.SignUp ? "<b>{{0}}</b>":"{{0}}");
			this.view["logincontent/view/content/below/Toggle/Top/SignInToggle/title"].color = type == LoginType.SignIn ? red:black;
			this.view["logincontent/view/content/below/Toggle/Top/SignUpToggle/title"].color = type == LoginType.SignIn ? black:red;
		};
		/*
         * 設定選擇按鈕底下紅線
         */
		this.setStatBtnColor= function(type){
			this.view["logincontent/view/content/below/Toggle/Top/SignInToggle/bottomRed"].color = type == LoginType.SignIn ? VM.getValue("color.gold") : VM.getValue("color.linecolor");
			this.view["logincontent/view/content/below/Toggle/Top/SignUpToggle/bottomRed"].color = type == LoginType.SignUp ? VM.getValue("color.gold") : VM.getValue("color.linecolor");
		};

		/*
		 * 設定選擇登入／註冊按鈕
		 */
		this.setChooseStatBtn = function(){
			UI_manager.add_button_listen(this.view["logincontent/view/content/below/Toggle/Top/SignInToggle"],this,function () {
				this.showPage(LoginType.SignIn);
			});
			UI_manager.add_button_listen(this.view["logincontent/view/content/below/Toggle/Top/SignUpToggle"],this,function () {
				this.showPage(LoginType.SignUp);
			});
		};
		/*
		 * 設定UIAssistant 值
		 */
		this.setUIAssistantPages = function(){
			let temp = new Page();
			temp.NAME = "SignIn";
			temp.panels.push("logincontent");
			temp.panels.push("Sigin");
			temp.panels.push("Top");
			UIAssistant.main.AddPage(temp);

			let temp2 = new Page();
			temp2.NAME = "SignUp";
			temp2.panels.push("logincontent");
			temp2.panels.push("Top");
			temp2.panels.push("SignUp");
			UIAssistant.main.AddPage(temp2);
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"LoginCtrl");
		CtrlAssistant.main.saveCtrl(this.view["logincontent/view/content/below/Toggle/content/Sigin"].getComponent("Sigin_ctrl"),"SigninCtrl");
		CtrlAssistant.main.saveCtrl(this.view["logincontent/view/content/below/Toggle/content/SignUp"].getComponent("SignUp_ctrl"),"SignUpCtrl");
		this.setUIAssistantPages();
		this.view["logincontent/view/version"].getComponent(cc.Label).string = MobileExtension.GetVersion() + " - " + cc.game.resver;
	},

	start() {
        this.setChooseStatBtn();
		this.FixLongViewInsetsDidChange();
		this.setToggleBtn(VM.getValue("login.status"));
	},

	onDisable(){
		this.view["logincontent/view/version"].active = false;
	},
 	/*
 	 * 調整長板畫面
 	 */
	FixLongViewInsetsDidChange(){
		if(VM.getValue("core.isLongScreen")){
			this.view["logincontent"].getComponent(cc.Widget).top += 44;
		}
	},
  	/*
  	 * 設定選擇按鈕
  	 */
	setToggleBtn(type){
		this.setStatBtnColor(type);
		this.setTitleColor(type,VM.getValue("color.gold"),VM.getValue("color.graylabel3"));
		this.view["logincontent/view/version"].active = type == LoginType.SignIn;
	},
	/*
	 * 設定顯示的Page
	 */
	showPage(type){
		UIAssistant.main.ShowPage(type == LoginType.SignIn ?"SignIn":"SignUp");
		this.setToggleBtn(type);
	},

	setRegister(m_active){
		this.view["logincontent/view/content/below/Toggle/Top/SignUpToggle"].active = m_active;
		let width =  this.view["logincontent/view/content/below/Toggle/Top"].width;

		this.view["logincontent/view/content/below/Toggle/Top/SignInToggle"].width = m_active ? width / 2 : width;

	},

});
