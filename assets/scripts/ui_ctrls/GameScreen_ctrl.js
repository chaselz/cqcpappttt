var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");
var Page = require("Page");
var CtrlAssistant = require("CtrlAssistant");
const {VM} = require('ViewModel');
var ApiManager = require("ApiManager");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"GameScreenCtrl");
	},

	start() {
	},

});