var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var UIAssistant = require("UIAssistant");
var CtrlAssistant = require("CtrlAssistant");
var Page = require("Page");
const {VM} = require('ViewModel');
cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	onEnable(){
		this.picNode.getComponent(cc.Sprite).spriteFrame = null;
		VM.setValue("core.title",VM.getValue("lang.boundtitle"));
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"BoundDetailCtrl");
		this.maskNode = this.view["ScrollView/view/content/Mask"];
		this.picNode = this.view["ScrollView/view/content/Mask/BoundImage"];
		this.maskNode.on('touchmove', this.onTouchMove, this);
	},

	start() {
	},

	//初始化
	init(){
		this.maskNode.setScale(1,1);
		this.picNode.getComponent(cc.Sprite).spriteFrame = null;
		this.picNode.setScale(1, 1);
		this.picNode.setPosition(cc.v2(0, 0));
		this.view["ScrollView"].getComponent(cc.ScrollView).vertical = true;
		this.isScrollOpen = false;
		this.isImageTop = true;
	},

	//設定優惠活動詳細頁資料
	setBoundDetail(){
		let self = this;
		let data = VM.getValue("bound.detail");	//取得 api 回傳的資料
		let url = data.mobile_content_img;		//取得圖片位址
		cc.loader.load(url,function (err, texture) {			//讀取圖片
			cc.log("test");
			if(texture){ 	//取得加载的图片
				UIAssistant.main.SetPanelVisible("Load",false);
				var spriteFrame  = new cc.SpriteFrame(texture);	//建立一個 SpriteFrame 放取得的圖片
				self.picNode.getComponent(cc.Sprite).spriteFrame = spriteFrame;
				self.maskNode.width = 576;
				self.maskNode.height = 576 / texture.width * texture.height;
				self.picNode.width = 576;
				self.picNode.height = 576 / texture.width * texture.height;
			}
			if(err){
				console.log("圖片加載失敗");
				// console.log(err)
			}
		});
	},

	/**
	 * 觸碰事件
	 * @param event 觸碰事件
	 */
	onTouchMove (event) {
		let touches = event.getTouches();
		if (touches.length == 1) {						// 一根手指是移动
			let delta = event.getDelta();
			let prePos = this.picNode.getPosition();
			this.picNode.setPosition(this.picNode.position.add(delta));
			let newPos = this.picNode.getPosition();
			let disY = newPos.y - prePos.y;
			this.restrictPic();
			if (this.picNode.scale <= 1) {
				this.view["ScrollView"].getComponent(cc.ScrollView).vertical = true;
			}else{
				if(this.isImageTop && disY > 0){		//圖片內容已滑到置頂, 往下滑
					this.view["ScrollView"].getComponent(cc.ScrollView).vertical = false;
					if(this.isScrollOpen){
						this.isScrollOpen = false;
						this.view["ScrollView"].getComponent(cc.ScrollView).scrollToTop(0.75);
					}
				}else if(!this.isImageTop && disY < 0){	//圖片內容已滑到置底, 往上滑
					this.view["ScrollView"].getComponent(cc.ScrollView).vertical = false;
					if(this.isScrollOpen){
						this.isScrollOpen = false;
						this.view["ScrollView"].getComponent(cc.ScrollView).scrollToBottom(0.75);
					}
				}else{
					this.view["ScrollView"].getComponent(cc.ScrollView).vertical = this.isScrollOpen;
				}
			}
		}else if (touches.length == 2) {				// 两根手指是缩放
			this.isScrollOpen = false;
			this.view["ScrollView"].getComponent(cc.ScrollView).vertical = false;
			let prePoint1 = this.node.convertToNodeSpaceAR(touches[0].getPreviousLocation());
			let prePoint2 = this.node.convertToNodeSpaceAR(touches[1].getPreviousLocation());
			let touchPoint1 = this.node.convertToNodeSpaceAR(touches[0].getLocation());
			let touchPoint2 = this.node.convertToNodeSpaceAR(touches[1].getLocation());
			let newPointsDis = touchPoint1.sub(touchPoint2).mag();
			let prePointsDis = prePoint1.sub(prePoint2).mag();
			if (newPointsDis > prePointsDis) {			// 两根手指在往外滑
				this.picNode.scale += 0.05;
			}else if (newPointsDis < prePointsDis) {	// 两根手指在往内滑
				if (this.picNode.scale <= 1) {
					this.view["ScrollView"].getComponent(cc.ScrollView).vertical = true;
					this.picNode.scale = 1;
					return;
				}
				this.picNode.scale -= 0.05;
			}
			
			this.restrictPic();
		}
	},

	/**
	 * 限制不滑超過邊界
	 */
	restrictPic () {
		let picWidth = this.picNode.getBoundingBox().width;
		let picHeight = this.picNode.getBoundingBox().height;
		if (this.picNode.x>0 && this.picNode.x-0>picWidth/2-this.maskNode.width/2)
			this.picNode.x = picWidth/2-this.maskNode.width/2;
		if (this.picNode.x<0 && this.picNode.x-0<this.maskNode.width/2-picWidth/2)
			this.picNode.x = this.maskNode.width/2-picWidth/2;
		if (this.picNode.y>0 && this.picNode.y-0>picHeight/2-this.maskNode.height/2){
			this.picNode.y = picHeight/2-this.maskNode.height/2;
			this.isImageTop = false;
			this.isScrollOpen = true;
			this.view["ScrollView"].getComponent(cc.ScrollView).vertical = true;
		}
		if (this.picNode.y<0 && this.picNode.y-0<this.maskNode.height/2-picHeight/2){
			this.picNode.y = this.maskNode.height/2-picHeight/2;
			this.isImageTop = true;
			this.isScrollOpen = true;
			this.view["ScrollView"].getComponent(cc.ScrollView).vertical = true;
		}
	},

});