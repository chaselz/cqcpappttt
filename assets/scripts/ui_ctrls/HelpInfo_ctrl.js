import {VM} from "../modelView/ViewModel";

var UI_ctrl = require("UI_ctrl");
var UI_manager = require("UI_manager");
var CtrlAssistant = require("CtrlAssistant");

cc.Class({
	extends: UI_ctrl,

	properties: {
	},

	ctor(){
		this.runGraphics = function () {
			let x = this.view["onlineBtn"];
			let pen = x.getComponent(cc.Graphics);
			pen.clear();
			pen.roundRect((0-x.width/2),(0-x.height/2),x.width,x.height,44);
			pen.stroke();
			pen.fill();
		};
		/**
		 * 設定在線客服 button
		 */
		this.setOnlineBtn = function(){
			UI_manager.add_button_listen(this.view["onlineBtn"],this,function () {
				console.log("聯繫客服");
				CtrlAssistant.main.getCtrl("MainCtrl").AllScreenWebUrl(VM.getValue("setting.serviceurl"));
			});
		};
	},

	onLoad() {
		UI_ctrl.prototype.onLoad.call(this);
		CtrlAssistant.main.saveCtrl(this,"HelpInfoCtrl");
	},

	start() {
		this.runGraphics();
		this.setOnlineBtn();
	},

	/**
	 * 清除幫助中心原本內容
	 */
	clearHelpInfo(){
		this.view["scroll/view/content/title"].getComponent(cc.Label).string = "";
		this.view["scroll/view/content/info"].getComponent(cc.Label).string = "";
	},

	/**
	 * 設定幫助中心內容
	 * @param data 資料
	 */
	setHelpInfo(data){
		this.view["scroll/view/content/title"].getComponent(cc.Label).string = data.sub_title;
		this.view["scroll/view/content/info"].getComponent(cc.Label).string = this.AnalyzeHtmlString(data.content);
	},
	/***
	 * 正規化移除
	 * @param str
	 * @returns {string}
	 * @constructor
	 */
	AnalyzeHtmlString(str) {
		let temp = str;//str.replaceAll("<p>","");
		// temp = temp.replaceAll("</p>","");
		temp = temp.replaceAll("&nbsp;"," ");

		temp = temp.replaceAll("<br>","\n\n");

		// temp = temp.replace(/<strong([^>]*)>[^<]*<\/strong>/g, (str, space) => {
		// 	let s = '';
		// 	for (const c of str.slice('<strong'.length + space.length +1, str.length - '<\/strong>'.length)) {
		// 		s += c
		// 	}
		// 	return s
		// });
		//
		// temp = temp.replace(/<span([^>]*)>[^<]*<\/span>/g, (str, space) => {
		// 	let s = '';
		// 	for (const c of str.slice('<span'.length + space.length +1, str.length - '<\/span>'.length)) {
		// 		s += c;
		// 	}
		// 	return s
		// });

		temp = temp.replace(/<(.[^>]*)>/g, (str, space) => {
			let s = '';
			for (const c of str.slice('<h2>'.length + space.length +1, str.length - '<\/h2>'.length)) {
				s += c;
			}
			return s
		});
		return temp;
	}
});