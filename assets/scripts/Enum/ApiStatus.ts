
export const enum ApiStatus {
    Init = 0,
    Normal = 1,
    Limit = 2,
    Err = 3,
    NoIp = 4,
}
