
export const enum PayType {
    Common = "common",
    AliPay = "alipay",
    Wechat = "wechat",
    BankOnline = "bankOnline",
    QQ = "qq",
    UnionPay = "unionPay",
    JingDong = "jingDong",
    CGP = "cgp",
    AlipayCard = "alipay2card",
    GP = "gp",
    WechatCard = "wechat2card",
}
