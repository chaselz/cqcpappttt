
export const enum RechargeType {
    List = "List",
    Choose = "Choose",
    Transfer = "Transfer",
    Result = "Result",
    JXPay = "JXPay",
    QR = "QR",
    FourthPay = "FourthPay",
}
