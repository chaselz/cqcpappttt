export const enum PageViewType {
    Home = "Home",
    SignIn = "SignIn",
    SignUp = "SignUp",
    Games = "Hall",
    GameScreen = "GameScreen",
    GameList = "GameList",
    Bound = "Bound",
    BoundDetail = "BoundDetail",
    Member = "Member",
    Recharge = "Recharge",
    PayView = "PayView",
    WithDraw = "WithDraw",
    AddBank = "AddBank",
    Banklist = "Banklist",
    ModifyPW = "ModifyPW",
    MyMessage = "MyMessage",
    MsgInfo = "MsgInfo",
    DWRecords = "DWRecords",
    BetRecord = "BetRecord",
    BetRecordDate = "BetRecordDate",
    BetRecordMaker = "BetRecordMaker",
    BetRecordGame = "BetRecordGame",
    BetRecordNum = "BetRecordNum",
    GameHall = "GameHall",
    MoreView = "MoreView",
    Bulletin = "Bulletin",
    BulletinDetail = "BulletinDetail",
    newMember = "newMember",
    IndoorTransfer = "IndoorTransfer",
    Web = "Web",
    Web2 = "Web2",
    Voucher = "Voucher",
    newDetails = "newDetails",
    MenuView = "MenuView",
    CheckMenuView = "CheckMenuView",
    MemberInfo = "MemberInfo",
    RechargeV2 = "RechargeV2",
    InitView = "InitView",
    RechargeQR = "RechargeQR",
    RechargeJXPay = "RechargeJXPay",
    RechargeTransfer = "RechargeTransfer",
    RechargeChoose = "RechargeChoose",
    RechargeFourthPay = "RechargeFourthPay",
    HelpCenter = "HelpCenter",
    HelpList = "HelpList",
    HelpInfo = "HelpInfo",
    AgentJoin = "AgentJoin",
    InputDeposit = "InputDeposit",
    BetRecordNoData = "BetRecordNoData",
    CheckDeposit = "CheckDeposit",
    TransferInfo = "TransferInfo",
    QrCode = "QrCode",
}
