
export const enum PlatFormType {
    Develop = 1,
    Integration = 2,
    Official = 3,
    VIP = 4,
}
