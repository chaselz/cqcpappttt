export const enum LophiiformesError {
//廳主代碼錯誤HALL_CODE_ERROR_= 160101,
//傳入參數錯誤		
INPUT_PARAMS_ERROR_= 160102,
//站台代碼錯誤			
SITE_CODE_ERROR= 160104,
//沒有上傳檔案							
UPLOAD_FILE_EMPTY= 160105,
//上傳檔案格式不符						
UPLOAD_FILE_TYPE_ERROR= 160106,
//上傳檔案過大						
UPLOAD_FILE_OVER_SIZE= 160107,
//上傳檔案失敗						
UPLOAD_FILE_FAIL= 160108,
//維護中						
UNDER_MAINTAIN= 160120,
//無此權限錯誤						
AUTHORITY_ERROR= 160201,
//此會員銀行卡出款處理中，無法被刪除					
USER_BANK_DELETE_ERROR= 160301,
//此銀行卡已被註冊						
USER_BANK_ALREADY_REGISTER= 160302,
//超過會員銀行卡限制張數					
USER_BANK_NUMBER_LIMIT= 160303,
//新增失敗					
INSERT_DATA_ERROR= 161301,
//修改失敗					
UPDATE_DATA_ERROR= 161302,
//刪除失敗					
DELETE_DATA_ERROR= 161303,
//此四方支付已被使用，無法刪除					
FOURTH_ALLOW_HAS_FOURTH= 162401,
//缺乏秘鑰資料					
MISSED_SECRET_ERROR= 162402,
//缺乏成對公私鑰資料					
MISSED_KEYS_ERROR= 162403,
//缺乏機構號資料					
MISSED_ORG_ERROR= 162404,
//此四方支付已綁定會員層級支付					
FOURTH_EXISTS_USER_LEVEL= 162405,
//此公司銀行卡已綁定會員層級支付						
COMPANY_BANK_CARD_USED= 162405,
//此使用者憑證已失效						
USER_TOKEN_FAIL= 163101,
//呼叫其他服務錯誤					
OTHER_SERVICE_ERROR= 163301,
//例外錯誤				
EXCEPTION_ERROR= 169901,
}