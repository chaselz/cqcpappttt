export const enum RouterError {

//呼叫服務失敗	
CALL_SERVICE_API_FAIL = 110001,
//连线错误(Redis 連線錯誤)	
CALL_REDIS_FAIL = 110002,
//RSA KEY 失效	
RSA_KEY_ERROR = 110101,
//網域無對應Code值	
DOMAIN_NO_MATCH_CODE = 110102,
//找不到頁面	
PAGE_NOT_FOUND_CODE = 110103,
//重複發送相同請求	
REQUEST_SENT_REPEATEDLY = 110104,
//維護中	
UNDER_MAINTAIN = 110120,
//此使用者凭证已失效(Token 失效錯誤)	
TOKEN_INVALID_ERROR = 110121,
//使用者凭证错误(Token 必傳錯誤)	
TOKEN_REQUIRED_ERROR = 110122,
//無此權限錯誤	
UNAUTHORIZED = 110201,

}