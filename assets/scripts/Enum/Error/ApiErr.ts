export const enum ApiErr {
//查無此代碼
    CouponNotFound=510001,
    //優惠卷已被派發
    CouponHasBeenDispatched=510002,
    //優惠卷已被使用
	CouponHasBeenUsed=510003,
    //優惠卷已過期
	CouponHasExpired =510004,
    //檢查CD時間未到
    CheckCDTimeError=510005,
    //不符合領取限制：轉入金額小於存送金額限制
    NotAllowMinimumAmount=510006,
    //平台已被鎖定
    ProviderLocked=510007,
    //發生異常平台已被鎖定
    ProviderPendingLocked=510008,
    //領取優惠卷錯誤系統無
    CouponReceiveError=510009,
    //找不到會員資訊
    UserNotFound=510010,
    //尚有注單未結算
    IncompleteOrder=510011,
    //輸入的會員帳號錯誤
    AccountNotFound=510012,
    //沒有異動任何資料
    UpdateNull=510013,
    //此錢包狀態不能進行強制解鎖
    CanNotForceUnlock=510014,
    //中心錢包餘額不足
    InsufficientBalance=510015,
    //有效期限內才能派發
    DispatchMustInValidDate=510016,
    //優惠卷無效
    CouponInvalid=510017,
    //不支援此遊戲平台
    ProviderNotSupport=510019,
    //輸出檔案失敗
    ExportFail=510020,
    //該遊戲平台無啟用
    ProviderDisable=510021,
    //mega 遊戲平台建立會員帳號失敗
    MegaSetAccountFail=510022,
    //CQ9 遊戲平台建立會員帳號失敗
    CQ9SetAccountFail=510023,
    //KY 遊戲平台建立會員帳號失敗
    KYSetAccountFail=510024,
    //AG 遊戲平台建立會員帳號失敗
    AGSetAccountFail=510025,
    //YG 遊戲平台建立會員帳號失敗
    YGSetAccountFail=510026,
    //BG 遊戲平台建立會員帳號失敗
    BGSetAccountFail=510027,
    //VR 遊戲平台建立會員帳號失敗
    VRSetAccountFail=510028,
    //MG 遊戲平台建立會員帳號失敗
    MGSetAccountFail=510029,
    //BBIN 遊戲平台建立會員帳號失敗
    BBINSetAccountFail=510030,
    //JDB 遊戲平台建立會員帳號失敗
    JDBSetAccountFail=510031,
    //SB 遊戲平台建立會員帳號失敗
    SBSetAccountFail=510032,
    //eBET 遊戲平台建立會員帳號失敗
    EBETSetAccountFail=510033,
    //LEG 遊戲平台建立會員帳號失敗
    LEGSetAccountFail=510034,
    //PT 遊戲平台建立會員帳號失敗
    PTSetAccountFail=510035,
    //BC 遊戲平台建立會員帳號失敗
    BCSetAccountFail=510036,
    //遊戲錢包餘額超出限制
    GameBalanceOverLimit=510037,
    //不符合領取限制：轉入金額大於最高轉入金額限制最高仅能转入__元！
    NotAllowMaximumAmount=510038,
    //重複建立會員帳號
    DuplicateCreateAccount=510039,
}