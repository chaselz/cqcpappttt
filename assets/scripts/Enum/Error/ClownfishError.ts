export const enum ClownfishError {

//廳主代碼錯誤		
HALL_CODE_ERROR_= 200101,
//傳入參數錯誤		
INPUT_PARAMS_ERROR_= 200102,
//站台代碼錯誤			
SITE_CODE_ERROR= 200104,
//沒有上傳檔案							
UPLOAD_FILE_EMPTY= 200105,
//上傳檔案格式不符						
UPLOAD_FILE_TYPE_ERROR= 200106,
//上傳檔案過大						
UPLOAD_FILE_OVER_SIZE= 200107,
//上傳檔案失敗						
UPLOAD_FILE_FAIL= 200108,
//非法網址							
URL_ILLEGAL= 200109,
//遊戲未啟用							
GAME_NOT_ENABLED= 200110,
//預關時間不可大於每期時間						
PRE_CLOSE_OVER_PERIOD= 200111,
//站台無此大類						
SITE_GAME_TAG_ERROR= 200112,
//超過可查詢的時間區間							
TIME_OUT_OF_RANGE= 200113,
//僅支援自開彩彩種遊戲							
ONLY_SUPPORT_VISION_CUSTOM= 200114,
//自開彩賽果格式錯誤						
VISION_CUSTOM_RESULT_ERROR= 200115,
//熱門遊戲單一類別限制最高三筆						
HOT_GAME_LIMIT_THREE= 200116,
//維護中						
UNDER_MAINTAIN= 200120,
//此遊戲已不存在						
GAME_IS_GONE= 200121,
//此遊戲已設定						
GAME_IS_HERE= 200122,
//熱門遊戲內容錯誤						
CONTENT_IS_WRONG= 200123,
//點擊過於頻繁，請稍後再試						
FAST_LINK_ERROR= 200124,
//帳號錯誤						
ACCOUNT_ERROR= 200125,
//無此權限錯誤						
AUTHORITY_ERROR= 200201,
//新增失敗							
INSERT_DATA_ERROR= 201301,
//修改失敗							
UPDATE_DATA_ERROR= 201302,
//刪除失敗						
DELETE_DATA_ERROR= 201303,
//無資料異動						
DATA_UNMODIFIED= 201304,
//此使用者憑證已失效						
USER_TOKEN_FAIL= 203101,
//遊戲端登入失敗						
LOGIN_GAME_ERROR= 203102,
//遊戲商餘額不足						
GAME_INSUFFICIENT_BALANCE= 203103,
//遊戲商重複轉出						
GAME_DUPLICATE_EXCHANGE= 203104,
//遊戲商轉入錯誤						
GAME_REDEEM_AMOUNT_ERROR= 203105,
//不需手動轉入						
DO_NOT_NEED_REDEEM= 203106,
//遊戲商額度不足							
GAME_INSUFFICIENT_AMOUNT= 203107,
//遊戲商轉出錯誤							
GAME_EXCHANGE_AMOUNT_ERROR= 203108,
//會員錢包餘額不足						
USER_INSUFFICIENT_BALANCE= 203109,
//更新遊戲設定失敗						
UPDATE_GAME_SETTING_ERROR= 203201,
//重置遊戲設定失敗						
RESET_GAME_SETTING_ERROR= 203202,
//建立自開彩遊戲失敗						
CREATE_CUSTOM_GAME_ERROR= 203203,
//更新自開彩遊戲殺率失敗						
UPDATE_GAME_RTP_ERROR= 203204,
//重置自開彩遊戲累計返還率失敗						
RESET_GAME_GRAND_RTP_ERROR= 203205,
//新增自開彩賽果失敗						
CREATE_CUSTOM_RESULT_ERROR= 203206,
//刪除自開彩賽果失敗							
DELETE_CUSTOM_RESULT_ERROR= 203207,
//呼叫其他服務錯誤							
OTHER_SERVICE_ERROR= 203301,
//呼叫遊戲服務錯誤						
GAME_SERVICE_ERROR= 203302,
//例外錯誤				
EXCEPTION_ERROR= 209901,

}