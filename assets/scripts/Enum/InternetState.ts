export const enum InternetState {
    NotReachable = -1,
    ReachableViaWiFi = 1,
    ReachableViaWWAN = 2,
    CMNET = 3,
}