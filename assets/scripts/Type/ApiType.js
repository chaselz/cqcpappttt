const ApiType = Object.freeze({
    loginType: 1,
    gameEnableType: 2,
    userBalanceType: 3,
    gameBalanceType: 4,
    msgListType: 5,
    gameProviderType: 6,
    frontDocListType: 7,
    bulletinListType: 8,
    gameHotType: 9,
    gameWinnerType: 10,
    userDataType: 11,
    bannerListType: 12,
    favoriteListType: 13,
    gameListType: 14,
    depositMenuType: 15,
    depositModeMenuType: 16,
    depositModeBankMenuType: 17,
    depositFourthType: 18,
    betsSummaryType: 19,
    gameLoginV2Type: 20,
    logoutType: 21,
    bankListType: 22,
    savebankcardType: 23,
    setfavoriteListType: 24,
    reportListType: 25,
    rewardInfoType: 26,
    bankDefaultType: 27,
    maintainCheckType: 28,
    rewardStatisticsType: 29,
    readedMsgType: 30,
    updateUserPWType: 31,
    checkIsSetPWType: 32,
    addPwWithDraw: 33,
    updatePwWithDraw: 34,
    userregister: 35,
    registerType: 36,
    boundListType: 37,
    boundDetailType: 38,
    depositMenuTypeOne: 39,
    depositProviderType: 40,
    gameVisionCategory:41,
    providerLimitType:42,
    providerType:43,
    addbankPWType:44,
    transWithDrawReportsType:45,
    transDepositReportsType:46,
    betsSummaryDetailType: 47,
    betsSummaryGameType: 48,
    betsSummaryDateType: 49,
    gameProviderItemType: 50,
    gameReportLinkType: 51,
    userBankListType: 52,
    siteInfoType: 53,
    gameTagType: 54,
    gameBalanceProgressType: 55,
    gameRedeemType: 56,
    changeRedeemType: 57,
    verifycodeType: 58,
    receivevoucherType: 59,
    luckymoneyType: 60,
    receiveluckymoneyType: 61,
    frontDocContentType: 62,
    hotactivitylistType: 63,
    clickhotactivityType: 64,
    connectErrType: 65,
    frontNewTransType: 66,
    NewPromotionListType: 67,
    depositModeAllMenuType: 68,
    depositModeBankMenuAllType: 69,
    GamePlayWayType: 70,
    agPlayFrontendType: 71,
});

module.exports = ApiType;