const LangType = Object.freeze({
    LangCN: "zh-cn",
    LangTW: "zh-tw",
    LangEN: "en",
});

module.exports = LangType;