const GameListType = Object.freeze({
    NoClass: 0,
    GameClass: 1,
    LotteryClass: 2,
});

module.exports = GameListType;