const HallType = Object.freeze({
    lottery:1,
    casino: 2,
    chesscard:3,
    live: 4,
    sport:5,
    fish: 6,
});

module.exports = HallType;