const TitleAlertType = Object.freeze({
    Error: Symbol(1),
    Warn: Symbol(2),
    Fulfill: Symbol(3),
});

module.exports = TitleAlertType;