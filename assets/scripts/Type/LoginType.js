const LoginType = Object.freeze({
    SignIn: Symbol(1),
    SignUp: Symbol(2),
});

module.exports = LoginType;