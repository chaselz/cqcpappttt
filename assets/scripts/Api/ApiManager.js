import {ApiStatus} from "../Enum/ApiStatus";
import {PlatFormType} from "../Enum/PlatFormType";
import AnalyticsHelper from "../Extension/Nactive/AnalyticsHelper";

var ApiType = require("ApiType");
var base64 = require("base64");
const {VM} = require('ViewModel');
var TitleAlertType = require("TitleAlertType");
var TitleAlertView = require("TitleAlertView");

//RSA公鑰
var _publicKey;
var _APIToken;
var ApiManager = {
    // 黑名單
    blackList : ["jarint01.gakkixmasami.cc","test.q.q.com","127.0.0.1:8082"],
    // //api 網址
    server : "",
    //站台代碼
    agentCode : "",
    //觀察者模式的監聽容器
    m_callback : [],
    //自身ＩＰ
    ip:"",
    //可信任的 domain 網址
    domain :"",
    //裝置類型代碼
    deviceType:{},
    //RSA +mi
    encrypt:new JSEncrypt(),
    header : {},

    platform : PlatFormType.Develop,

    status: ApiStatus.Init,

    subindex : -1,

    functionLock : false,
    //App 註冊網址
    registerURL:"http://appengine.JumBoApp.com",

    get APIToken(){
        return _APIToken;
    },
    set APIToken(value){
        _APIToken = value;
        this.header = {
            "Api-Token":value
        };
    },

    //RSA公鑰
    get publicKey(){
        return _publicKey;
    },
    set publicKey(value) {
        _publicKey = value;
        this.encrypt.setPublicKey(atob(value));
    },

    /*
     * 初始化
     * @param login 是否要登入
     */
    init(login,_platform){
        this.platform = _platform;
        this.ininConnctServer();
        this.Login.getMyIP3()
            .then(this.getIPResult.bind(this))
            .then((result)=>{
                if(result){
                    this.status = ApiStatus.Normal;
                    this.get.Plat();
                    if(login)
                        this.Login.userlogin("angelll","qwe123");
                }else
                    this.status = ApiStatus.Limit;
            });
    },

    async getIPResult(result){
        if(!result){
            result = await this.Login.getMyIP2();
            result = await this.Login.getMyIP();
        }

        if(result)
            return this.get.Origin();
        esle
        this.status = ApiStatus.NoIp;
    },

    ininConnctServer(){
        switch (this.platform) {
            case PlatFormType.Develop:
                this.server = "http://jdengine.gakkixmasami.com";
                this.agentCode = "jar01";
                break;
            case PlatFormType.Integration:
                this.server = "http://jiengine.gakkixmasami.com";
                this.agentCode = "jar02";
                break;
            case PlatFormType.VIP:
            case PlatFormType.Official:
                this.server = "https://engine.1485745.com";
                this.agentCode = "jb";
                break;
        }
    },
    /*
     * 新增監聽者
     * @param callback 監聽事件
     * @param return indexkey
     */
    addListenter(callback){
        this.m_callback.push(callback);
        return ++this.subindex;
    },
    /*
     * 清除監聽者
     * @param key 監聽事件
     */
    removeListenter(key){
        this.arrayRemove(key);
    },
    arrayRemove(value){

        if(value > this.m_callback.length)
            return;

        this.m_callback.splice(value, 1);
        this.subindex--;
    },
    /*
     * 傳訊息給監聽者
     * @param type API類型
     * @param responseJson response資料
     */
    pushCallBack(type,responseJson){
        this.m_callback.forEach(callback => {
            callback(type,responseJson)
        });
    },
    /**
     * Get請求
     * @param type API類型
     * @param url API URL
     * @param headerJson header資料
     */
    GetRequest(type,url,headers,logntime = false){
        this._sendRequest(type,"GET",url,null,headers);
    },
    /*
     * Post 請求
     * @param type API類型
     * @param url API URL
     * @param data資料
     * @param headerJson header資料
     */
    PostRequest(type,url,data,headerJson,isjson,longtime = false ,origin_json){
        this._sendRequest(type,"POST",url,isjson ? JSON.stringify(data) : data,headerJson,longtime,origin_json);
    },

    PutRequest(type,url,data,headers,longtime = false){
        this._sendRequest(type,"PUT",url,data,headers,longtime);
    },

    _sendRequest(apitype,sendtype,url,data,headerdata,longtime = false,origin_json= null){
        let xhr = new XMLHttpRequest();
        let self = this;
        if(!longtime)
            xhr.timeout = 16000;
        xhr.onreadystatechange = async function () {
            if (xhr.readyState == 4 && (xhr.status >= 200)) {
                var response = xhr.responseText;
                var json = JSON.parse(response);
                if(CC_DEBUG)
                    console.log(json);
                self.pushCallBack(apitype,json);
                if(json.code != 1) {

                    if(!VM.getValue("core.initapp")){
                        let errString = VM.getValue("err.err_"+json.code);
                        if(errString)
                            TitleAlertView.main.show(TitleAlertType.Error,errString);
                        else
                            TitleAlertView.main.show(TitleAlertType.Error,`${VM.getValue("lang.errcode")}：${json.code}`);
                    }
                    AnalyticsHelper.logeEvent(AnalyticsHelper.ApiCodeErr, {type:apitype,code:json.code,request:origin_json ? origin_json : data,response:json.result});
                }
            }
        };


        xhr.onerror = async  function(e){
            self.pushCallBack(apitype,{"code":"784444"});
            TitleAlertView.main.show(TitleAlertType.Error,VM.getValue("lang.connecterr"));
            AnalyticsHelper.logError(AnalyticsHelper.ApiErr,{type:apitype,request:origin_json ? origin_json : data,Description:"err"});
        };

        xhr.ontimeout = async function(e){
            self.pushCallBack(apitype,{"code":"781234"});
            TitleAlertView.main.show(TitleAlertType.Error,VM.getValue("err.err_781234"));
            AnalyticsHelper.logError(AnalyticsHelper.ApiErr,{type:apitype,Description:"timeOut"});
        };
        xhr.onloadend = async function(e){
            // AnalyticsHelper.logError(AnalyticsHelper.ApiErr,"{type:{0},Description:{1}}".format(apitype.toString(),"onloadend"));
        };
        xhr.onabort = async function(){
            AnalyticsHelper.logError(AnalyticsHelper.ApiErr,{type:apitype,Description:"Abort"});
        };
        let targeturl = this.server+"/"+url;
        xhr.open(sendtype,encodeURI(targeturl), true);
        for(let header in headerdata){
            xhr.setRequestHeader(header,headerdata[header]);
        }
        xhr.setRequestHeader("App-Origin", this.domain);
        if(data != null) {
            xhr.send(data);
        }else{
            xhr.send();
        }
    },
    MaintainCheck(){
        this.GetRequest(ApiType.maintainCheckType,"maintain/check/"+this.ip,null);
    },

    get:{
        /*
         * 取得可信任的domain
         */
        Origin(){
            return new Promise((resolve,reject)=>{
                var xhr = new XMLHttpRequest();
                xhr.open("GET", ApiManager.server+"/getOrigin?code="+ApiManager.agentCode,true);
                // xhr.timeout = 8000;
                xhr.onload = function(){
                    if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 400)) {
                        var json = JSON.parse(xhr.responseText);
                        if(json.code == 1){
                            let whitelist = json.result.filter(x=> !ApiManager.blackList.includes(x));
                            if(whitelist.length > 0) {
                                ApiManager.domain = (ApiManager.platform == PlatFormType.Official || ApiManager.platform == PlatFormType.VIP ? "https://" : "http://")+ whitelist[0];
                                resolve(true);
                            }
                        }
                    }
                    resolve(false);
                };
                xhr.onloadend = function(){
                    if(ApiManager.status == ApiStatus.Init){
                        ApiManager.status = ApiStatus.Limit;
                        resolve(false);
                    }
                };
                xhr.onerror = function(e){
                    cc.log(e);
                    ApiManager.status = ApiStatus.Err;
                    resolve(false);
                };

                xhr.ontimeout = function(e){
                    cc.log(e);
                    ApiManager.status = ApiStatus.Limit;
                    resolve(false);
                };

                xhr.onabort = function(){
                    resolve(false);
                };
                xhr.send();
            });
        },
        //判別裝置
        Platform(){
            let key = "";
            if(cc.sys.platform == cc.sys.ANDROID)
                key = "Android App";
            else if(cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD)
                key = "iOS App";
            else
                key = "PC";

            return ApiManager.deviceType[key];
        },
        //取的裝置類型代碼
        Plat(){
            let xhr = new XMLHttpRequest();
            let self = ApiManager;
            xhr.open("GET", ApiManager.server+"/user/plat",true);

            xhr.onload = function(){
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var json = JSON.parse(xhr.responseText);
                    console.log(json);
                    if(json.code    ==  1)
                        self.deviceType = json.result;
                }
            };
            xhr.onerror = function(e){
                ApiManager.status = ApiStatus.Err;
            };
            xhr.ontimeout = function(){
                ApiManager.status = ApiStatus.Limit;
            };
            xhr.onabort = function(){
                ApiManager.status = ApiStatus.Limit;
            };
            xhr.setRequestHeader("App-Origin", ApiManager.domain);
            xhr.send();
        },
        //站台資料 （站台icon 跟 最愛 icon）
        SiteInfo(){
            ApiManager.GetRequest(ApiType.siteInfoType,"site/info",null);
        },

        /*
         * 取得文案清單
         * @param code 文案頁面(1：幫助中心、2：推廣說明、3：開戶協議、4：代理加盟)
         */
        FrontDocList(code){
            //let type = code == 1 ? ApiType.frontDocListType1: code == 2 ? ApiType.frontDocListType2 : code == 3 ? ApiType.frontDocListType3 : ApiType.frontDocListType4
            ApiManager.GetRequest(ApiType.frontDocListType,"doc/getFrontDocList/"+code,null);
        },
        /*
         * 取得單筆文案內文資料資料
         * @param code 副文案id
         */
        FrontDocContent(code){
            ApiManager.GetRequest(ApiType.frontDocContentType,"doc/getFrontDocContent/"+code,null);
        },
        //公告清單
        BulletinList(tag){
            ApiManager.GetRequest(ApiType.bulletinListType,"bulletin/list/?Tag="+tag,ApiManager.header);
        },
        //所有公告清單
        AllBulletinList(){
            ApiManager.GetRequest(ApiType.bulletinListType,"bulletin/listAllTag",ApiManager.header);
        },
        //取得新版中獎名單
        BannerList(tag){
            ApiManager.GetRequest(ApiType.bannerListType,"banner/list/",null);
        },

        //取得優惠清單
        BoundList(tag){
            ApiManager.GetRequest(ApiType.boundListType,"promotion/list/?Tag="+tag,null);
        },

        //取得優惠活動內容
        BoundDetail(Id){
            ApiManager.GetRequest(ApiType.boundDetailType,"promotion/info/"+Id,null);
        },

    },
    //*************************************************************************************************
    /*
     *RSA 加密
     * @param data 要加密的JSON
     */
    rsaEncryptFromJson(dataJson){
        return this.rsaencryptBySplit(JSON.stringify(dataJson));
    },
    /*
     *RSA 加密
     * @param data 要加密的data
     */
    rsaEncrypt(data){
        return this.rsaencryptBySplit(data);
    },
    rsaencryptBySplit ( param) {
        let encrypted = '';
        let partLen = this.encrypt.getPublicKey().length / 8 - 11;
        let parts = this.strSplit(param, partLen);
        parts.forEach((value) => {
            let tmpStr = '';
            //檢查加密後的長度
            while (tmpStr.length !== 256) {
                tmpStr = this.encrypt.getKey().encrypt(value);
            }
            encrypted += tmpStr
        });
        return hex2b64(encrypted);
    },

    strSplit (string, splitLength) {
        if (splitLength === null) {
            splitLength = 1
        }
        if (string === null || splitLength < 1) {
            return false
        }

        string += ''
        let chunks = []
        let pos = 0
        let len = string.length

        while (pos < len) {
            chunks.push(string.slice(pos, pos += splitLength))
        }
        return chunks
    }
    //*************************************************************************************************
};

var b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
var b64pad = "=";

function hex2b64(h) {
    var i;
    var c;
    var ret = "";
    for (i = 0; i + 3 <= h.length; i += 3) {
        c = parseInt(h.substring(i, i + 3), 16);
        ret += b64map.charAt(c >> 6) + b64map.charAt(c & 63);
    }
    if (i + 1 == h.length) {
        c = parseInt(h.substring(i, i + 1), 16);
        ret += b64map.charAt(c << 2);
    }
    else if (i + 2 == h.length) {
        c = parseInt(h.substring(i, i + 2), 16);
        ret += b64map.charAt(c >> 2) + b64map.charAt((c & 3) << 4);
    }
    while ((ret.length & 3) > 0) {
        ret += b64pad;
    }
    return ret;
}
// convert a base64 string to hex
function b64tohex(s) {
    var ret = "";
    var i;
    var k = 0; // b64 state, 0-3
    var slop = 0;
    for (i = 0; i < s.length; ++i) {
        if (s.charAt(i) == b64pad) {
            break;
        }
        var v = b64map.indexOf(s.charAt(i));
        if (v < 0) {
            continue;
        }
        if (k == 0) {
            ret += int2char(v >> 2);
            slop = v & 3;
            k = 1;
        }
        else if (k == 1) {
            ret += int2char((slop << 2) | (v >> 4));
            slop = v & 0xf;
            k = 2;
        }
        else if (k == 2) {
            ret += int2char(slop);
            ret += int2char(v >> 2);
            slop = v & 3;
            k = 3;
        }
        else {
            ret += int2char((slop << 2) | (v >> 4));
            ret += int2char(v & 0xf);
            k = 0;
        }
    }
    if (k == 1) {
        ret += int2char(slop << 2);
    }
    return ret;
}

ApiManager.User = {
    get:{
        //取的錢包餘額
        UserBalance(){
            ApiManager.GetRequest(ApiType.userBalanceType,"user/balance",ApiManager.header);
        },
        //取得各遊戲商餘額
        GameBalance(){
            ApiManager.GetRequest(ApiType.gameBalanceType,"user/gameBalance",ApiManager.header);
        },
        //取得玩家訊息
        UserMsg(){
            ApiManager.GetRequest(ApiType.msgListType,"message/list",ApiManager.header);
        },
        /*
         * 已讀訊息
         */
        ReadedMsg(id){
            var json = {
                "msg_id":id
            };
            ApiManager.PostRequest(ApiType.readedMsgType,"message/read",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        },
        //取得會員資料
        UserData(){
            ApiManager.GetRequest(ApiType.userDataType,"user/data",ApiManager.header);
        },
        /*
         * 取得會員推廣資訊
         */
        RewardInfo(){
            ApiManager.GetRequest(ApiType.rewardInfoType,"reward/info",ApiManager.header);
        },
        /*
         * 推廣代理報表
         * @param start 開始時間 *範例:2019-07-24
         * @param end 結束時間 *範例:2019-07-24
         */
        RewardStatistics(start,end){
            ApiManager.GetRequest(ApiType.rewardStatisticsType,"reward/statistics/"+start+"/" +end,ApiManager.header);
        },
        /*
         * 確認是否設定取款密碼
         */
        CheckIsSetPasswordWithDraw(){
            ApiManager.GetRequest(ApiType.checkIsSetPWType,"user/passwordWithdrawCheck",ApiManager.header);
        },
        /***
         * 取得會員各遊戲平台餘額及打碼量
         * @constructor
         */
        UserGameBalanceProgress(){
            ApiManager.PostRequest(ApiType.gameBalanceProgressType,"v1/front/user/game/balance",null,ApiManager.header,false,true);
        },

    },
    set:{
        /*
         * 更新使用者密碼
         * @param oldPW 舊密碼
         * @param newPW 新密碼
         */
        UpdatePassword(oldPW,newPW){
            var json = {
                "password":oldPW,
                "new_password":newPW
            };
            ApiManager.PutRequest(ApiType.updateUserPWType,"user/password",ApiManager.rsaEncryptFromJson(json),ApiManager.header);
        },
        /*
         * 新增取款密碼
         */
        AddPasswordWithDraw(PW){
            var json = {
                "password":PW,
            };
            ApiManager.PostRequest(ApiType.addPwWithDraw,"user/passwordWithdraw",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        },
        /*
         * 更新取款密碼
         * @param oldPW 舊密碼
         * @param newPW 新密碼
         */
        UpdatePassWordWithDraw(oldPW,newPW){
            var json = {
                "password":oldPW,
                "new_password":newPW
            };
            ApiManager.PutRequest(ApiType.updatePwWithDraw,"user/passwordWithdraw",ApiManager.rsaEncryptFromJson(json),ApiManager.header);
        },
        /***
         *
         * @param provider_code
         * @constructor
         */
        GameRedeem(provider_code){
            var json = {
                "provider_code":provider_code.toString(),
            };
            ApiManager.PostRequest(ApiType.gameRedeemType,"game/redeem",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        },
        /***
         * 玩家手動上下分 //中心錢包為 center
         * @param form
         * @param to
         * @param amount
         * @constructor
         */
        ExchangeRedeem(form,to,amount){
            var json = {
                "from_provider_code":form,
                "to_provider_code":to,
                "amount":amount,
            };
            cc.log("json =",json);
            ApiManager.PostRequest(ApiType.changeRedeemType,"manual/exchangeRedeem",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);

        }
    }
};

ApiManager.Game = {
    get:{
        //取的ag的playfrontend
        GamePlayWay(gameid,lang){
            ApiManager.GetRequest(ApiType.GamePlayWayType,"game/playway/"+gameid+"/"+lang,ApiManager.header);
        },
        //取的ag的playfrontend
        AgPlayFrontend(lang){
            ApiManager.GetRequest(ApiType.agPlayFrontendType,"game/agPlayFrontend/"+lang+"/",ApiManager.header);
        },
        //取得最愛遊戲清單
        FavoriteList(tag){
            ApiManager.GetRequest(ApiType.favoriteListType,"favorite/listV2/"+tag,ApiManager.header);
        },
        //取得站台遊戲清單、期數資訊
        GameList(tag){
            ApiManager.GetRequest(ApiType.gameListType,"game/list/"+tag,null);
        },
        //取得站台遊戲商選單
        GameProviderItem(tag){
            ApiManager.GetRequest(ApiType.gameProviderItemType,"game/providerItem/"+tag,ApiManager.header);
        },
        //取得熱門遊戲
        HotGame(lang,tag){
            ApiManager.GetRequest(ApiType.gameHotType,"game/hot/"+lang+"/"+tag,ApiManager.header);
        },
        //取得新版中獎名單
        GameWinner(tag){
            ApiManager.GetRequest(ApiType.gameWinnerType,"game/winner/"+tag,null);
        },
        //取得新版全部中獎名單
        AllGameWinner(){
            ApiManager.GetRequest(ApiType.gameWinnerType,"game/winner/",null);
        },
        //取得站台遊戲種類
        GameTag(){
            ApiManager.GetRequest(ApiType.gameTagType,"game/tag",null);
        },
        //取得站台遊戲廠商清單
        CompanyList(){
            ApiManager.GetRequest(ApiType.gameEnableType,"game/enabled");
        },
        //取得站台遊戲種類的廠商清單
        GameProviderList(code){
            ApiManager.GetRequest(ApiType.gameProviderType,"game/tagProvider/"+code,ApiManager.header);
        },
        //取得mega彩票頁籤類型與翻譯
        GameVisionCategory(code){
            ApiManager.GetRequest(ApiType.gameVisionCategory,"game/visionCategoryTypeFrontend/"+code,ApiManager.header);
        },
        /***
         * 取得細單連結
         * @param Provider_Code 遊戲商代碼
         * @param Detail_Json 	json，參考備註
         * @constructor
         */
        GameReportLink(Provider_Code,Detail_Json){
            ApiManager.GetRequest(ApiType.gameReportLinkType,"game/ReportLink/"+Provider_Code+"/"+JSON.stringify(Detail_Json),ApiManager.header);
        },
        /***
         * 取得火到活動
         * 相關API網址：https://documenter.getpostman.com/view/5162913/SzKR11Wa?version=latest
         * @constructor
         */
        HotActivityList(){
            var json = {
                "siteCode":ApiManager.agentCode,
            };
            ApiManager.PostRequest(ApiType.hotactivitylistType,"v1/front/promotion/hot/list",json,ApiManager.header,true);

        },
        /***
         * 取得前台優惠活動清單
         * @constructor
         */
        NewPromotionList(){
            ApiManager.PostRequest(ApiType.NewPromotionListType,"v1/front/promotion/list",null,ApiManager.header,false);
        },
    },
    set:{
        /*
         * 異動我的最愛清單
         * @param tag 遊戲種類id
         * @param data 清單內容。格式
         * id 廠商遊戲代碼  code 遊戲代碼 [{"id": 553,"code": "586CCC-PK10"},{"id": 553,"code": "2F7F08-PK10"}]
         */
        FavoriteGame(tag,data){
            var json = {
                "tag":tag,
                "data":JSON.stringify(data)
            };
            ApiManager.PutRequest(ApiType.setfavoriteListType, "favorite/updateV2", ApiManager.rsaEncryptFromJson(json), ApiManager.header);
        },
        /***
         * 點擊火爆活動時計算人數
         * 相關API網址：https://documenter.getpostman.com/view/5162913/SzKR11Wa?version=latest
         * @constructor
         */
        ClickHotActivity(id){
            var json = {
                "id":id,
            };
            ApiManager.PostRequest(ApiType.clickhotactivityType,"v1/front/promotion/hot/click",json,ApiManager.header,true);

        },
    },
    //登入遊戲v2
    LoginV2(id,gameCode){
        var json = {
            "id":id,
            "game_code":gameCode,
            "home_url":ApiManager.domain,
            "hall_url":ApiManager.domain,
            "platform":ApiManager.get.Platform()
        };

        ApiManager.PostRequest(ApiType.gameLoginV2Type,"game/loginV2User",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
    },
};

ApiManager.Login = {
    /*
    取得自身ＩＰ
     */
    getMyIP(){
        return new Promise((resolve,reject)=>{
            let url = "http://ip-api.com/json/?lang=zh-CN";
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url,true);

            xhr.onload = function(){
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var json = JSON.parse(xhr.responseText);
                    ApiManager.ip = json["query"];
                    if(ApiManager.ip.isNull())
                        AnalyticsHelper.logError(AnalyticsHelper.getIp1FuncErr,{Description: "No Get Ip in response"});
                    resolve(!ApiManager.ip.isNull());
                }else{
                    AnalyticsHelper.logError(AnalyticsHelper.getIp1FuncErr, {Description: "{0} - {1}".format(xhr.status,xhr.readyState)});
                    resolve(false);
                }
            };

            xhr.onerror = function(e){
                AnalyticsHelper.logError(AnalyticsHelper.getIp1FuncErr,{Description: "err"});
                resolve(false);
            };

            xhr.ontimeout = function(){
                AnalyticsHelper.logError(AnalyticsHelper.getIp1FuncErr,{Description: "timeOut"});
                resolve(false);
            };

            xhr.send();
        });
    },
    /*
    取得自身ＩＰ
     */
    getMyIP2(){
        return new Promise((resolve,reject)=>{
            let url = "https://ping.1485745.com/";
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url,true);

            xhr.onload = function(){
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var json = JSON.parse(xhr.responseText);
                    ApiManager.ip = json["result"]["ip"];
                    if(ApiManager.ip.isNull())
                        AnalyticsHelper.logError(AnalyticsHelper.getIp2FuncErr,{Description: "No Get Ip in response"});
                    resolve(!ApiManager.ip.isNull());
                }else{
                    AnalyticsHelper.logError(AnalyticsHelper.getIp2FuncErr, {Description: "{0} - {1}".format(xhr.status,xhr.readyState)});
                    resolve(false);
                }
            };

            xhr.onerror = function(e){
                AnalyticsHelper.logError(AnalyticsHelper.getIp2FuncErr,{Description: "err"});
                resolve(false);
            };

            xhr.ontimeout = function(){
                AnalyticsHelper.logError(AnalyticsHelper.getIp2FuncErr,{Description: "timeOut"});
                resolve(false);
            };


            xhr.send();
        });
    },
    /*
    取得自身ＩＰ
     */
    getMyIP3(){
        return new Promise((resolve,reject)=>{
            let url = ApiManager.server+"/ping";
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url,true);

            xhr.onload = function(){
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var json = JSON.parse(xhr.responseText);
                    ApiManager.ip = json["result"]["ip"];
                    if(ApiManager.ip.isNull())
                        AnalyticsHelper.logError(AnalyticsHelper.getIp3FuncErr,{Description: "No Get Ip in response"});
                    resolve(!ApiManager.ip.isNull());
                }else{
                    AnalyticsHelper.logError(AnalyticsHelper.getIp3FuncErr, {Description: "{0} - {1}".format(xhr.status,xhr.readyState)});
                    resolve(false);
                }
            };

            xhr.onerror = function(e){
                AnalyticsHelper.logError(AnalyticsHelper.getIp3FuncErr,{Description: "err"});
                resolve(false);
            };

            xhr.ontimeout = function(){
                AnalyticsHelper.logError(AnalyticsHelper.getIp3FuncErr,{Description: "timeOut"});
                resolve(false);
            };

            xhr.send();
        });
    },
    /***
     * 取得註冊設定
     */
    userregister(){
        ApiManager.GetRequest(ApiType.userregister,"user/register",null);

    },
    /*
     * 會員登入
     * @param user 帳號
     * @param pw 密碼
     */
    userlogin(user,pw){
        var json = {
            "account":user,
            "password":pw,
            "plat_id":ApiManager.get.Platform(),
            "login_ip":ApiManager.ip
        };
        console.log("userlogin =",JSON.stringify(json));
        ApiManager.PostRequest(ApiType.loginType,"user/login",json,null,true);
    },
    /**
     * 新版註冊
     */
    Register(promotion_code,account,pw,nickname,name,phone,email,birthday,qq,wechat){
        var json =
            {
                //"site_code": "{{SiteCode}}",
                "promoCode":promotion_code,
                //"promotion_type":"user",
                "account":account,
                "password":pw,
                "nickname":nickname,
                "name":name,
                "phone":phone,
                "email":email,
                "birthday":birthday,
                "qq":qq,
                "wechat":wechat,
                "registerIP": ApiManager.ip,
                "registerURL": ApiManager.registerURL

            };
        if(promotion_code.isNull())
            delete json.promotion_code;
        console.log("regisger json = \n"+JSON.stringify(json));
        ApiManager.PostRequest(ApiType.registerType,"v1/front/member/register",json,null,true);
    },

    guestLogin(){
        var json = {
            "plat_id":3,
        };
        ApiManager.PostRequest(ApiType.loginType,"user/login",json,null,true);
    },

    //會員登出
    userlogout(){
        ApiManager.PostRequest(ApiType.logoutType,"user/logout",null,ApiManager.header,false);
    },
};

ApiManager.Deposit = {

    set :{
        /*
         * 新增銀行卡
         * @param bank_id 轉帳銀行 id
         * @param name 戶名（最大長度30）
         * @param account 帳號（最大長度50）
         * @param account_point 開戶網點 （最大長度100
         * @param bank_default 預設提款銀行卡（0:非預設 1.預設）
         *
        */
        SetBankCard(bank_id,name,account,account_point,bank_default){
            var json = {
                "bank_id":bank_id,
                "name":name,
                "account":account,
                "account_point":account_point,
                "bank_default":bank_default
            };

            ApiManager.PostRequest(ApiType.savebankcardType,"user_bank",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        },
        /*
         * 更新會員預設提款銀行卡
         * @param cardid 會員銀行卡id
         */
        BankDefault(cardid){
            var json = {
                "id":cardid,
                "bank_default":1
            };

            ApiManager.PutRequest(ApiType.bankDefaultType,"user_bank/default",ApiManager.rsaEncryptFromJson(json),ApiManager.header);
        },
        /***
         * 新增取款密碼
         * @param pw
         * @constructor
         */
        SetUserBankPW(pw){
            var json = {
                "password":pw,
            };

            ApiManager.PostRequest(ApiType.addbankPWType,"user/passwordWithdraw",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        }
    },
    get:{
        //取得會員銀行卡清單
        UserBankList(){
            ApiManager.GetRequest(ApiType.userBankListType,"user_bank/list",ApiManager.header);
        },
        //取得出款銀行設定
        BankList(){
            ApiManager.GetRequest(ApiType.bankListType,"bank_list/site",ApiManager.header);
        },
        //取得入款通道清單(不限金額)
        DepositMenu(){
            ApiManager.GetRequest(ApiType.depositMenuType,"deposit/menuzero",ApiManager.header);
        },
        /*
         * 取得入款通道清單
         * @param amount 入款金額
         *
        */
        DepositMenuOne(amount){
            ApiManager.GetRequest(ApiType.depositMenuTypeOne,"deposit/menuone/"+amount*100,ApiManager.header);
        },
        /*
         * 取得支付方式清單f
         * @param mode 取得入款通道清單取得的 mode
         * @param amount 入款金額
         *
        */
        DepositModeMenu(mode,amount){
            ApiManager.GetRequest(ApiType.depositModeMenuType,"deposit/menutwo/"+mode+"/"+amount*100,ApiManager.header);
        },
        /*
         * 取得支付銀行清單
         * @param mode 取得入款通道清單取得的 mode
         * @param type 取得支付方式清單取得的 type
         * @param amount 入款金額
         *
        */
        DepositModeBankMenu(mode,type,amount){
            ApiManager.GetRequest(ApiType.depositModeBankMenuType,"deposit/menuthree/"+mode+"/"+type+"/"+amount*100,ApiManager.header);
        },
        /*
         * 申請轉帳入款
         * @param bankid 銀行卡id
         * @param time 時間
         * @param username 姓名
         * @param method 匯款方式
         * @param point 所屬支行
         * @param amount 入款金額
         */
        DepositProvider(bankid,time,username,method,point,amount){
            var json = {
                "company_bank_id":bankid,
                "deposit_time":time,
                "user_name":username,
                "method":method,
                "point":point,
                "amount":amount*100
            };
            ApiManager.PostRequest(ApiType.depositProviderType,"deposit/provider",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
        },
        /*
         * 申請四方支付入款
         * @param _fourth_id 第四方商家
         * @param _bank_id 第三方通道
         * @param _amount 入款金額
         *
         */
        FourthPay(_fourth_id,_bank_id,_amount,_point){
            var json = {
                "fourth_id":_fourth_id,
                "bank_id":_bank_id,
                "amount":_amount*100,
                "point":_point,
                "return_url":ApiManager.domain
            };
            ApiManager.PostRequest(ApiType.depositFourthType,"deposit/fourth",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false);
        },
        /**
         * 申請網銀入款
         * @param _fourth_id 第四方商家
         * @param _bank_id 第三方通道
         * @param _amount 入款金額
         * @constructor
         */
        BankPay(_fourth_id,_bank_id,_amount,_point){
            var json = {
                "fourth_id":_fourth_id,
                "bank_id":_bank_id,
                "amount":_amount*100,
                "point": _point,
                "return_url":ApiManager.domain
            };
            ApiManager.PostRequest(ApiType.depositFourthType,"deposit/bank",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);

        },

        /******************************************
         * 新的支付街口
         */

        /***
         * 取得支付方式清單f
         * @param mode 取得入款通道清單取得的 mode
         * @param amount 入款金額
         *
         */
        DepositModeMenuAll(mode){
            ApiManager.GetRequest(ApiType.depositModeAllMenuType,"deposit/menutwoall/"+mode+"?Mode="+mode,ApiManager.header);
        },

        /***
         * 取得支付方式清單f
         * @param mode 取得入款通道清單取得的 mode
         * @param amount 入款金額
         *
         */
        DepositModeBankMenuAll(mode,type){
            var url = "deposit/menuthreeall/{0}/{1}?Mode={2}&Type={3}".format(mode,type,mode,type);
            ApiManager.GetRequest(ApiType.depositModeBankMenuAllType,url,ApiManager.header);
        },
    },

};

ApiManager.WithDraw = {
    get:{
        ProviderLimit(){
            ApiManager.GetRequest(ApiType.providerLimitType,"withdraw/providerLimit",ApiManager.header);
        },

        Provider(bankid,pw,_amount){
            var json = {
                "user_bank_id":bankid,
                "password":pw,
                "amount":_amount*100,
            };
            console.log("json = "+JSON.stringify(json));
            ApiManager.PostRequest(ApiType.providerType,"withdraw/provider",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);

        },
    },
};

ApiManager.Report = {
    /***
     * 存取紀錄
     * @param mode
     * @constructor
     */
    DepositReport(mode){
        ApiManager.GetRequest(ApiType.transDepositReportsType,"transReports/deposit/?Mode="+mode,ApiManager.header);
    },
    /***
     * 取款紀錄
     * @param mode
     * @constructor
     */
    WithDrawReport(mode){
        ApiManager.GetRequest(ApiType.transWithDrawReportsType,"transReports/withdrawList/?Mode="+mode,ApiManager.header);
    },
    /***
     * 下注紀錄總計-依日期區分
     * @param start 開始日期(Y-m-d)
     * @param end 結束日期(Y-m-d)
     * @param time_zone 時區(-4,0,8)
     * @param game_provider 遊戲商
     */
    BetsSummaryNew(start,end,time_zone,game_provider){
        ApiManager.GetRequest(ApiType.betsSummaryType,"transReports/betsSummaryNew/"+start+"/"+end+"/"+time_zone+"/?Game_Provider="+game_provider,ApiManager.header);
    },
    /***
     * 下注紀錄總計-單一日期
     * @param date 開始日期(Y-m-d)
     * @param time_zone 時區(-4,0,8)
     * @param game_provider 遊戲商
     */
    BetsSummaryDateNew(date,time_zone,game_provider){
        ApiManager.GetRequest(ApiType.betsSummaryDateType,"transReports/betsSummaryDateNew/"+date+"/"+time_zone+"/?Game_Provider="+game_provider,ApiManager.header);
    },
    /***
     * 下注紀錄總計-依遊戲商、遊戲代碼區分
     * @param date 時間
     * @param time_zone 時區
     * @param game_provider 遊戲商
     * @param lang 語言
     * @constructor
     */
    BetsSummaryGameNew(date,time_zone,game_provider,lang){
        ApiManager.GetRequest(ApiType.betsSummaryGameType,"transReports/betsSummaryGameNew/"+date+"/"+time_zone+"/"+game_provider+"/"+lang,ApiManager.header);

    },
    /***
     * 下注紀錄細單
     * @param date 查詢單一日期(Y-m-d)
     * @param time_zone 時區(-4,0,8)
     * @param game_provider 遊戲商 請帶game_provider_code
     * @param gamecode 遊戲代碼
     * @param page 頁數 最小為1
     * @param count 筆數 最小為1、最大為2000
     * @constructor
     */
    BetsSummaryDetailNew(date,time_zone,game_provider,gamecode,page,count){
        ApiManager.GetRequest(ApiType.betsSummaryDetailType,"transReports/betsGameDetailNew/"+date+"/"+time_zone+"/"+game_provider+"/"+gamecode+"/"+page+"/"+count,ApiManager.header);

    },
    /***
     * 取得新版前台資金明細類別
     * @constructor
     */
    FrontNewTrans(){
        ApiManager.GetRequest(ApiType.frontNewTransType,"report/frontNewTransType",ApiManager.header);
    },

    /***
     * 取得資金明細
     * @param start 開始日期(Y-m-d h)
     * @param end 結束日期(Y-m-d h)
     * @param count 筆數
     * @param page 頁數
     * @param kind 類別(參考:現金版代碼)
     * @param mid_kind 中類別(參考:現金版代碼)
     * @param item 細項(參考:現金版代碼)
     */
    ReportList(start,end,count,page,kind,mid_kind,item){
        ApiManager.GetRequest(ApiType.reportListType,"report/txnList/"+start+"/"+end+"/"+count+"/"+page+"/?Kind="+JSON.stringify(kind) +"&Mid_Kind="+JSON.stringify(mid_kind)+"&Item="+JSON.stringify(item),ApiManager.header);
    },
};

ApiManager.Voucher = {
    /***
     * 驗證優惠券
     * @param _code
     * @constructor
     */
    CheckVoucherCode(_code){
        var json = {
            "CouponCode":_code,
        };
        console.log("json = "+JSON.stringify(json));
        ApiManager.PostRequest(ApiType.verifycodeType,"v1/front/coupon/verify/code",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
    },
    /***
     * 領取優惠券 - 存送
     * @param _code
     * @param _amount
     * @param _id
     * @constructor
     */
    ReceiveVoucher(_code,_amount,_id){
        var json = {
            "CouponCode":_code,
            "InAmount":parseFloat(_amount),
            "ProviderID":_id,
        };
        console.log("json = "+JSON.stringify(json));
        ApiManager.PostRequest(ApiType.receivevoucherType,"v1/front/coupon/receive/deposit",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
    },
    /***
     * 查詢會員紅包列表
     * @param _status
     * @param _page
     * @param _count
     * @constructor
     */
    GetLuckyMoneyList(_status,_page,_count){
        var json = {
            "Status":_status,
            "Page":_page,
            "Count":_count,
        };
        console.log("json = "+JSON.stringify(json));
        ApiManager.PostRequest(ApiType.luckymoneyType,"v1/front/coupon/user/red_packets/list",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
    },
    /***
     * 領取優惠券 - 紅包
     * @param _code
     * @param _id
     * @constructor
     */
    ReceiveLuckyMoney(_code,_id){
        var json = {
            "CouponCode":_code,
            "ProviderID":_id,
        };
        console.log("json = "+JSON.stringify(json));
        ApiManager.PostRequest(ApiType.receiveluckymoneyType,"v1/front/coupon/receive/red_packets",ApiManager.rsaEncryptFromJson(json),ApiManager.header,false,false,json);
    }
};

module.exports = ApiManager;