
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import Awaiter from "../Extension/AwaitExtension";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class LotteryItem extends Iitem {

    @property(cc.Sprite)
    lotterySprite: cc.Sprite = null;

    @property(cc.RichText)
    nameRichText: cc.RichText = null;

    @property(cc.Label)
    periodLable: cc.Label = null;

    @property(cc.Label)
    timeLable: cc.Label = null;

    @property(cc.Label)
    finalizeLable: cc.Label = null;

    @property(cc.Node)
    timeNode: cc.Node = null;

    @property(cc.Node)
    finalizeNode: cc.Node = null;

    private id:number = 0;
    private code:String = "";
    private old_width:number = 0;
    private timeString:String = "";
    private isfinalize:boolean = false;

    protected onLoad(): void {
        this.drawFinalizeGraphics();
        this.old_width = this.timeNode.width;
        this.node.on("click",function () {
            VM.setValue("hall.gameCode",this.code);
            VM.setValue("hall.gameId",this.id);
            this.onclik();
        },this);
    }

    private async drawTimeGraphics(){
        let time = this.timeNode;
        await Awaiter.until(_=>this.timeNode.width != this.old_width);
        let timePen = this.timeNode.getComponent(cc.Graphics);
        timePen.clear();
        timePen.roundRect((0-time.width),(0-time.height/2),time.width,time.height,18);
        timePen.stroke();
        timePen.fill();
        this.old_width = time.width;
    }

    private async drawFinalizeGraphics(){
        let finalize = this.finalizeNode;
            let finPen = finalize.getComponent(cc.Graphics);
            finPen.roundRect((0-finalize.width/2),(0-finalize.height/2),finalize.width,finalize.height,18);
            finPen.stroke();
            finPen.fill();
    }

    private async sleep(ms = 0){
        return new Promise(r => setTimeout(r, ms));
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.id = data.id;
        this.code = data.code;
        this.setLotteryCode(data.code);
        this.setLotteryName(data.name);
        this.setIcon(data.image);
        this.setPeriod(data.num);
    }

    refreshData(data){
        this.setActive(true);
        this.setPeriod(data.num);
    }
    
    private setLotteryCode(value){
        this.node.name = value;
    }

    private setLotteryName(value){
        this.nameRichText.string = "<b>"+value+"</b>";
    }

    private setPeriod(value){
        this.periodLable.string = VM.getValue("lang.lotteryPeriod1")+" "+value+" "+VM.getValue("lang.lotteryPeriod2");
    }

    private setIcon(url){
        let self = this;
        UI_manager.setSprite(this.lotterySprite.node,"universal/icon-default-logo");
        if(url == "" || url.length == 0 || url.toLowerCase().indexOf(".gif") != -1 || url.toLowerCase().indexOf(".svg") != -1 || url.toLowerCase().indexOf(".jfif") != -1)
            return;
        cc.loader.load(url, function (err, texture) {
            if(err) {
                UI_manager.setSprite(self.lotterySprite.node,"universal/icon-default-logo");
                return;
            }
            let sp  = new cc.SpriteFrame(texture);
            self.lotterySprite.spriteFrame = sp;
        });
    }

    setTimeType(value){
        this.isfinalize = false;
        this.finalizeNode.active = false;
        this.timeNode.active = true;
        let timelabel = this.timeLable;
        this.timeString = ""+value.h+":"+value.m+":"+value.s;
        let oldString = timelabel.string;
        timelabel.string = this.timeString;
        if(oldString.length != this.timeString.length) {
            this.drawTimeGraphics();
        }
    }

    setFinalize(){
        this.timeNode.active = false;
        this.finalizeNode.active = true;
        this.finalizeLable.getComponent("VMLabel").setLabelValue(VM.getValue("lang.finalize"));
    }

    getActive(){
        return this.node.active;
    }

    setActive(value){
        this.node.active = value;
    }

}
