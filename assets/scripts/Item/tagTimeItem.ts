
import Iitem from "./Iitem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class tagTimeitem extends Iitem {

    @property(cc.RichText)
    timelabel: cc.RichText = null;
    private key : string = "";

    public get Key(){
        return this.key;
    }
    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.key = data;
        this.timelabel.string = "<b>{0}</b>".format(data);
    }
}
