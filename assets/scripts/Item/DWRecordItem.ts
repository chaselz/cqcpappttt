
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class DWRecordItem extends Iitem {

    @property(cc.Label)
    timelabel: cc.Label = null;

    @property(cc.Label)
    amountlabel: cc.Label = null;

    @property(cc.Label)
    contenlabel: cc.Label = null;

    @property(cc.Node)
    topNode: cc.Node = null;

    protected onLostFocusInEditor(): void {
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.timelabel.string = data.application_time.GameTimeOnyTime();
        this.amountlabel.string = data.balance;
        // this.setStatusLabel(data.status);
        this.setContent(data.front_remark);

    }

    private setContent(str:string){
        this.contenlabel.string = str ? str : VM.getValue("lang.webbankwithdraw");
        this.contenlabel._forceUpdateRenderData();
        if(this.contenlabel.node.height > 48){
            this.topNode.height = this.contenlabel.node.height + 12;
            this.getComponent(cc.Layout).updateLayout();
        }
    }
}
