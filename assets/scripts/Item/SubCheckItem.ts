import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import {ChecckStatus} from "../Enum/ChecckStatus";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
@ccclass
export default class SubCheckItem extends Iitem {

    @property(cc.Sprite)
    checkstatusImg: cc.Sprite = null;

    @property(cc.Label)
    titleLable: cc.Label = null;

    private state : ChecckStatus = ChecckStatus.NO;
    // LIFE-CYCLE CALLBACKS:
    private key: number = 0;

    public get Key(){
        return this.key;
    }

    protected onLoad(): void {
        this.node.on("click",function () {
            this.onclik(this.key);
            if(this.state == ChecckStatus.NO)
                this.setClickState();
            else
                this.setCancelState();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.key = data;
        this.titleLable.string = VM.getValue("fundsname.id"+data);
    }

    public setClickState(){
        if(this.checkstatusImg)
            UI_manager.setSprite(this.checkstatusImg.node,"detail/icon-selectbox-s");
        this.state = ChecckStatus.All;
    }

    public setCancelState(){
        if(this.checkstatusImg)
            UI_manager.setSprite(this.checkstatusImg.node,"detail/icon-selectbox-n");
        this.state = ChecckStatus.NO;
    }


}
