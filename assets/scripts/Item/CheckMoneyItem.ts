

import CheckItem from "./CheckItem";
import {ChecckStatus} from "../Enum/ChecckStatus";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CheckMoneyItem extends CheckItem {

    protected updateChooseLabel() {
        let str = "";
        if(this.state == ChecckStatus.All){
            str = VM.getValue("fundsname.id0");
        }else if(this.state == ChecckStatus.NO){
        }else{
            str = VM.getValue("fundsname.id"+this.sub_key[0]);
        }
        this.chooseLable.string = str;
        this.updateNumAlert(this.sub_key.length > 1 && this.state == ChecckStatus.Section);
    }

    protected updateNumAlert(show: boolean) {
        this.nunmoreNode.active = show;
        if (show) {
            this.numLabel.string = (this.sub_key.length - 1) + "+";
            //更新Label的數值;
            this.numLabel._forceUpdateRenderData();
            this.nunmoreNode.getComponent(cc.Layout).updateLayout();
            this.DrawGPNumNode();
        }
    }
}
