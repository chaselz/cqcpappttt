import Iitem from "./Iitem";
import DropDown from "../UI/DropDown/DropDown";
import array = cc.js.array;
import DropDownOptionData from "../UI/DropDown/DropDownOptionData";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
@ccclass
export default class LuckyMoneyItem extends Iitem {

    @property(cc.RichText)
    activityNameLabel: cc.RichText = null;

    @property(cc.RichText)
    amountLabel: cc.RichText = null;

    @property(cc.Label)
    postTimeLabel: cc.Label = null;

    @property(cc.Label)
    receiveTimeLabel: cc.Label = null;

    @property(DropDown)
    dropDown: DropDown = null;

    @property(cc.Button)
    receiveBtn: cc.Button = null;

    @property(cc.Node)
    receiveNode: cc.Node = null;

    private status:number = -1;
    private code:string = "";
    private platformindex = -1;
    private platformList = null;

    protected onLoad(): void {
        this.OperationGraphics();
        this.setBtn();
        this.setDropDownCB();
    }

    private OperationGraphics(){
        let gp = this.dropDown.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0,0,gp.node.width,gp.node.height,8);
        gp.stroke();
    }

    private CloseGraphics(){
        let gp = this.dropDown.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0,0,gp.node.width,gp.node.height,8);
        gp.fill();
    }

    private setBtn(){
        UI_manager.add_button_listen(this.receiveBtn.node,this,function () {
            if(this.platformindex < 0){
                TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.input")+VM.getValue("lang.tinplatform"));
                return;
            }
            this.onclik();
            ApiManager.Voucher.ReceiveLuckyMoney(this.code,this.platformList[this.platformindex]);
        });
    }

    private setDropDownCB(){
        this.dropDown.SetSelectIndexCallBack((index)=>{
            this.platformindex = index;
        });
    }

    private NeverReceiveMode(){
        //UI_manager.setSprite(this.dropDown.arrowCaption.node,"universal/icon-dropdown-show");
        this.dropDown.interactable = true;
        this.receiveBtn.interactable = true;
        this.OperationGraphics();
    }

    private ReceivedMod(){
        this.dropDown.interactable = false;
        this.receiveBtn.interactable = false;
        this.CloseGraphics();
        //UI_manager.setSprite(this.dropDown.arrowCaption.node,"universal/icon-dropdown-show-d");
    }

    private setStatus(_status){
        this.receiveNode.active = _status == 3; //2:未領取 3:已領取
        if(this.receiveNode.active)
            this.ReceivedMod();
        else
            this.NeverReceiveMode();
    }


    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.code = data.couponCode;
        this.status =data.status;
        this.platformindex = -1;
        this.platformList = data.providerIDs;
        this.setStatus(data.status);
        this.activityNameLabel.string = "<b>{0}</b>".format(data.couponName);
        this.amountLabel.string = "<b>{0}</b>".format(data.promoAmount.formatNumber(2));
        this.postTimeLabel.string = data.sendTime.GameTimeWithSecond();
        // cc.warn(data.sendTime.GameTimeWithSecond());
        if(data.receiveTime) {
            this.receiveTimeLabel.string = data.receiveTime.GameTimeWithSecond();
            this.dropDown.clearOptionDatas();
            this.dropDown.labelCaption.string = VM.getValue("providername.id"+data.providerID);
        }else
            this.setDropDownValue(data.providerIDs);
    }

    public showReceive(){
        this.node.active = this.status == 3;
    }

    public showNeverReceive() {
        this.node.active = this.status == 2;
    }

    private setDropDownValue(ids:any){
        this.dropDown.clearOptionDatas();
        this.dropDown.init(VM.getValue("lang.tinplatform"));
        let gameenable = VM.getValue("core.gameenable");
        ids = ids.filter((e) => {
            return gameenable.find(x=>x.provider.code == VM.getValue("provider.id"+e)) != undefined;
        });
        var temp = [];
        ids.forEach(x=>{
            let datatemp = new DropDownOptionData();
            datatemp.setValue(VM.getValue("providername.id"+x));
            temp.push(datatemp);
        });
        this.dropDown.addOptionDatas(temp);
    }
}
