
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class GameClassItem extends Iitem {

    @property(cc.Label)
    classLabel: cc.Label = null;

    @property(cc.Node)
    lineNode: cc.Node = null;

    private id:number = 0;
    private code:String = "";

    start() {
        this.node.on("click",function () {
            VM.setValue("hall.className",this.code);
            let isReturn = this.onclik();
            if(isReturn)
                return;
            
			if(VM.getValue("core.tagCode") == 1){
                ApiManager.Game.get.GameList(VM.getValue("hall.gameId"));
            }else{
                VM.setValue("hall.gameId",this.id);
                ApiManager.Game.get.GameList(this.id);
            }
            this.setSelected();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.code = data.code;
        this.id = data.id;
        if(data.code == VM.getValue("hall.className"))
            this.setSelected();
        else
            this.Init();
        this.setClassName(data.code);
        this.setLabel(data.name);
    }

    Init(){
        this.lineNode.active = false;
        this.classLabel.node.color = VM.getValue("color.graylabel3");
    }

    setSelected(){
        this.lineNode.active = true;
        this.classLabel.node.color = VM.getValue("color.gold");
    }

    private setClassName(value){
        this.node.name = value;
    }

    private setLabel(value){
        this.classLabel.string = value;
    }
}
