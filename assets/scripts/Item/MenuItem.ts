
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
@ccclass
export default class MenuItem extends Iitem {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Node)
    switch:cc.Node = null;

    private key :string = "";

    start(){

        UI_manager.add_button_listen(this.node,this,function () {
            this.onclik(this.key);
        });

    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.key = data;
        this.label.string = data;
    }

    setSwitch(_key:any){
        this.switch.active = this.key == _key;
    }

    o
}
