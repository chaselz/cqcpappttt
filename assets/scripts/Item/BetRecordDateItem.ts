import {VM} from "../modelView/ViewModel";
import Iitem from "./Iitem";
const {ccclass, property} = cc._decorator;

@ccclass
export default class BetRecordDateItem extends Iitem {

    @property(cc.Label)
    timelabel: cc.Label = null;

    @property(cc.Label)
    countlabel: cc.Label = null;

    @property(cc.RichText)
    resultlabel: cc.RichText = null;
    private itme : string = "";
    start () {
        this.node.on("click",()=>{
            this.saveClickItemDate();
            if(this.onclik)
                this.onclik();
        },this);
    }

    /***
     * 設定Item
     * @param data
     * @param onClickFunc
     */
    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.itme = data.date;
        var times = data.date.split("-");
        this.timelabel.string = "{0}/{1}".format(times[1],times[2]);
        this.setLossWinLabel(data.profit);
        this.countlabel.string = data.count;
    }

    /***
     * 設定輸贏Label
     * @param _count
     */
    private setLossWinLabel(_count){
        this.resultlabel.node.color =  VM.getValue(parseFloat(_count)>=0 ? "color.graylabel3":"color.red");
        this.resultlabel.string = _count;
    }

    /***
     * 儲存點選的時間
     */
    public saveClickItemDate(){
        VM.setValue("betrecord.targettime",this.itme);
    }
    // update (dt) {}
}
