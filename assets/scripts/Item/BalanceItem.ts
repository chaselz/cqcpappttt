
import Iitem from "./Iitem";
import BhvRollNumber from "../../Behavior/ui/BhvRollNumber";
import {VM} from "../modelView/ViewModel";
import Color = cc.Color;
import constructor = cc.constructor;
import BalanceManager from "../UI/BalanceManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

@ccclass
export default class BalanceItem extends Iitem {

    @property(cc.RichText)
    nameLabel: cc.RichText = null;
    @property(BhvRollNumber)
    money: BhvRollNumber = null;
    @property(cc.Sprite)
    icon: cc.Sprite = null;
    @property(cc.Node)
    betprogresstitle : cc.Node = null;
    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;
    @property(cc.Label)
    betprogressLable : cc.Label = null;
    @property(cc.Node)
    lockNode: cc.Node = null;
    @property(cc.Animation)
    loadAnim:cc.Animation = null;
    @property(cc.Button)
    btn:cc.Button = null;

    private name : string = "";
    private providerID: string = "";
    private mustBet:string = "";
    private validBet:string = "";
    private bankruptcyAmount:string = "";
    private manager:BalanceManager = null;

    /***
     * BalanceItem點擊事件
     * @param event
     * @param customEventData
     * @constructor
     * https://docs.google.com/document/d/1bKb4q26O0uPNlR02412MWiAvXTmTYZW60iZC7x0GSYE/edit
     * 打碼量100% 、錢少於五塊 能解鎖
     */
    public BalanceClick(event, customEventData){
        if(this.lockNode.active && this.progressBar.progress != 1 && this.money.targetValue > 5){
           let msgcontent = VM.getValue("lang.balancelockerr");
           TitleAlertView.main.show(TitleAlertType.Warn,msgcontent.format(this.mustBet,this.validBet,(this.mustBet-this.validBet)));
        }else {
            ApiManager.User.set.GameRedeem(this.providerID);
            if(this.onclik)
            this.onclik(this.nameLabel.string);
            this.loadAnim.node.active = true;
            this.loadAnim.play();
            this.btn.interactable = false;
            this.icon.node.color = new Color(88, 88, 88);
            this.icon.node.opacity = 100;
        }
    }

    /***
     * 設定值
     * @param data
     * @param onclickfunc
     * @param type
     */
    public setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.setNameAndIcon(data.providerID);
        this.mustBet = data.mustBet.toString();
        this.validBet = data.validBet.toString();
        this.bankruptcyAmount = data.bankruptcyAmount.toString();

        this.icon.node.color = data.status == 1 ? new Color(88,88,88) : cc.Color.WHITE;
        this.icon.node.opacity = data.status == 1 ? 100 : 255;
        this.lockNode.active = data.status == 1;
        this.money.targetValue = data.balance;
        this.progressBar.progress = data.betPercent/100;
        this.betprogressLable.string = data.betPercent+"%";
        this.betprogressLable.node.color =  VM.getValue("color."+(data.betPercent == 100 ?"gold":"graylabel")) ;
    }

    /***
     * 設定鎖住
     * @param targetname
     */
    public lockitem(targetname: string){
        if(targetname == this.nameLabel.string)
            return;
        let gray = VM.getValue("color.graylabel2");
        this.nameLabel.string = "<color=484848>"+this.nameLabel.string+"</color>";
        this.money.node.color = gray;
        if(this.betprogressLable.string == "100%")
            this.betprogressLable.node.opacity = 127;
        else
            this.betprogressLable.node.color = gray;

        this.betprogresstitle.color = gray;
        this.progressBar.barSprite.node.opacity = 127;
        this.icon.node.opacity = 127;
    }

    /***
     * 解鎖
     */
    public removeLock(){
        this.btn.interactable = true;

        if(!this.lockNode.active)
            this.icon.node.color = cc.Color.WHITE;
        this.loadAnim.stop();
        this.loadAnim.node.active = false;

        this.nameLabel.string = this.name;
        this.money.node.color = VM.getValue("color.graylabel3");
        if(this.betprogressLable.string == "100%")
            this.betprogressLable.node.opacity = 255;
        else
            this.betprogressLable.node.color = VM.getValue("color.graylabel");

        this.betprogresstitle.color = VM.getValue("color.graylabel");
        this.progressBar.barSprite.node.opacity = 255;
        this.icon.node.opacity = 255;
    }

    /***
     * 設定Manager
     * @param _manager
     */
    public setManager(_manager:BalanceManager){
        this.manager = _manager;
    }

    /***
     * 設定圖片與名字
     * @param id
     */
    private setNameAndIcon(id:number){
        let data = VM.getValue("core.gameenable").find(x=>x.provider.code == VM.getValue("provider.id"+id));
        if(data) {
            if(data.provider.code == this.providerID)
                return;


            this.setName(VM.getValue("providername.id"+id));
            this.setIcon(data.provider.code);
            this.providerID = data.provider.code;
        }
    }

    private setName(name:string){
        this.name = name;
        this.nameLabel.string = "<b>"+name+"</b>";
    }

    private setIcon(icon_name:string){
        UI_manager.setSprite(this.icon.node,"provider/logo-"+icon_name);
    }

}
