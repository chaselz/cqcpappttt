import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import Color = cc.Color;

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class platformItem extends Iitem {

    @property(cc.Sprite)
    iconImg: cc.Sprite = null;

    @property(cc.RichText)
    nameLabel: cc.RichText = null;

    @property(cc.Label)
    amountLabel : cc.Label = null;


    @property(cc.Node)
    mainNode : cc.Node = null;

    @property(cc.Node)
    LabelNode : cc.Node = null;

    @property(cc.Label)
    LabelTitle : cc.Label = null;

    @property(cc.Node)
    LockImg:cc.Node = null;

    public get ID(){
        return this.providerID;
    }

    private providerID:any = "center";
    private name:string = "";
    public mustBet:string = "";
    public validBet:string = "";
    public bankruptcyAmount:string = "";
    public betPercent:number = 0;
    public status:number = 0;
    protected start(): void {
        UI_manager.add_button_listen(this.node,this,function () {
            //被鎖住的也可以選擇
            // if(this.status == 0)
                this.onclik(this.providerID,this);
        });
    }
    public updataIconState(){
        this.iconImg.node.color = this.status == 1 ? new Color(88,88,88) : cc.Color.WHITE;
        this.iconImg.node.opacity = this.status == 1 ? 100 : 255;
        if(this.LockImg)
            this.LockImg.active = this.status == 1;
    }

    public async setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.setNameAndIcon(data.providerID);
        this.status = data.status;
        this.betPercent = data.betPercent;
        this.amountLabel.string = data.balance.formatNumber(2);
        this.mustBet = data.mustBet.toString();
        this.validBet = data.validBet.toString();
        this.bankruptcyAmount = data.bankruptcyAmount.toString();
    }

    public setCallBack(_cb:void){
        this.onclik = _cb;
    }

    public async Init(){
        this.LabelNode.active = false;
        this.setMainNodeColor(cc.Color.WHITE);
        this.nameLabel.string = this.nameLabel.string;
    }

    public ToutMode(){
        this.LabelNode.active = true;
        this.LabelNode.color = VM.getValue("color.red");
        this.LabelTitle.string = VM.getValue("lang.tout");
        this.setMainNodeColor(new cc.Color(120,120,120),200);
    }

    public TinMode(){
        this.LabelNode.active = true;
        this.LabelNode.color = VM.getValue("color.green");
        this.LabelTitle.string = VM.getValue("lang.tin");
        this.setMainNodeColor(new cc.Color(120,120,120),200);
    }

    public changeStatus(id){
        if(id[0]==this.providerID){
            this.ToutMode();
        }else if(id[1]==this.providerID){
            this.TinMode();
        }else{
            this.Init();
        }
    }

    private setMainNodeColor(color:cc.Color,opacity=255){
        this.mainNode.children.forEach(node=>{
            if(node.name == "icon" && this.status == 1)
                return;
            node.color = color
        });
        this.mainNode.children.forEach(node=>{
            if(node.name == "icon" && this.status == 1)
                return;
            node.opacity = opacity
        });
        this.nameLabel.string = this.nameLabel.string;
    }

    private setNameAndIcon(id:number){
        let data = VM.getValue("core.gameenable").find(x=>x.provider.code == VM.getValue("provider.id"+id));
        if(data) {
            if(data.provider.code == this.providerID)
                return;

            this.setName(VM.getValue("providername.id"+id));
            this.setIcon(data.provider.code);
            this.providerID = data.provider.code;
        }
    }

    private setName(name:string){
        this.name = name;
        this.nameLabel.string = "<b>"+name+"</b>";
    }

    private setIcon(icon_name:string){
        UI_manager.setSprite(this.iconImg.node,"provider/logo-"+icon_name);
    }
}
