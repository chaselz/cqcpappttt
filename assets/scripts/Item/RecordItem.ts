import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class RecordItem extends cc.Component {

    @property(cc.Label)
    timeLabel: cc.Label = null;

    @property(cc.Label)
    amountLabel: cc.Label = null;

    @property(cc.Label)
    statusLabel: cc.Label = null;

    @property(cc.Button)
    showBtn: cc.Button = null;

    @property(cc.Node)
    msgNode: cc.Node = null;

    @property(cc.Node)
    lineNode: cc.Node = null;

    @property(cc.Label)
    msgLabel: cc.Label = null;

    @property(cc.Sprite)
    BtnBgImg: cc.Sprite = null;

    start () {
        UI_manager.add_button_listen(this.showBtn.node,this,this.setBtn);
    }

    setValue(data:any){
        this.timeLabel.string = data.application_time.slice(0,-3);
        this.amountLabel.string = data.balance;
        this.setStatusLabel(data.status);
        this.msgLabel.string = data.front_remark;
        this.Init();
    }

    private setStatusLabel(status){
        this.statusLabel.string = status == "0" ? VM.getValue("lang.apply") : status == "1" ? VM.getValue("lang.transactionfinish") : VM.getValue("lang.nopass");
    }

    private setBtn(){
        let status = this.msgNode.active;
        this.msgNode.active = !status;
        this.lineNode.active = status;
        UI_manager.setSprite(this.BtnBgImg.node,status ? "icon-dropdown-show":"icon-dropdown-hide");
    }

    private Init(){
        this.msgNode.active = false;
        UI_manager.setSprite(this.BtnBgImg.node,"icon-dropdown-show");
    }

    public ActiveBtn(IsActive:boolean){
        this.BtnBgImg.node.active = IsActive;
    }
}
