
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");

@ccclass
export default class ClassicItem extends Iitem {

    @property(cc.Label)
    namelabel: cc.Label = null;

    @property(cc.Sprite)
    gameSprite: cc.Sprite = null;

    private gamecode:string = "";
    private site_id:number = 0;

    start () {
        UI_manager.add_button_listen(this.node,this,function () {
            if(VM.getValue("core.islogin")) {
                this.onclik();
                ApiManager.Game.LoginV2(this.site_id, this.gamecode);
            }else
                TitleAlertView.main.show(TitleAlertType.Warn,VM.getValue("lang.hallwarn"));
        });
    }

    public setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.namelabel.string = data.name;
        if(this.site_id != data.gamelist_bysite_id && this.gamecode != data.game_code) {
            this.site_id = data.gamelist_bysite_id;
            this.gamecode = data.game_code;
            UI_manager.setSpriteFormUrl(this.gameSprite.node, data.pic,null,"universal/icon-default-logo");
        }
        this.node.active = true;
    }
}
