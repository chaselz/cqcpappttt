import {VM} from "../modelView/ViewModel";
import {BetRecordBtnType} from "../Enum/BetRecordBtnType";
import Iitem from "./Iitem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BetRecordMakerAndGameItem extends Iitem {

    @property(cc.Label)
    namelabel: cc.Label = null;

    @property(cc.Label)
    countlabel: cc.Label = null;

    @property(cc.Label)
    amountLabel: cc.Label = null;

    @property(cc.Label)
    resultlabel: cc.Label = null;

    private provider: string = "";
    private onclik: void = null;
    private mode: BetRecordBtnType = BetRecordBtnType.Date;
    start () {
        this.node.on("click",()=>{
            this.saveClickItemDate();
            if(this.onclik)
                this.onclik();
        },this);
    }

    /***
     * 設定Item
     * @param data
     * @param onclickfunc
     * @param type Item類型
     */
    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.mode = type;
        this.namelabel.string = type == BetRecordBtnType.Maker ? data.name : data.game_name;
        this.provider = type == BetRecordBtnType.Maker ? data.game_provider : data.game_code;
        this.amountLabel.string = data.bet;
        this.setLossWinLabel(data.profit);
        this.countlabel.string = data.count;
    }

    /***
     * 設定輸贏Label
     * @param _count
     */
    private setLossWinLabel(_count){
        this.resultlabel.node.color =  VM.getValue(parseFloat(_count)>=0 ? "color.graylabel3":"color.red");
        this.resultlabel.string = _count;
    }

    /***
     * 儲存點選的Maker or Game
     */
    public saveClickItemDate(){
        let key = "betrecord."+(this.mode == BetRecordBtnType.Maker ? "targetprovider" : "targetgame");
        VM.setValue(key,this.provider);
        if(this.mode == BetRecordBtnType.Game)
            VM.setValue("betrecord.gamename",this.namelabel.string);
    }
    // update (dt) {}
}
