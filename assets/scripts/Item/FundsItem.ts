
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import {ChecckStatus} from "../Enum/ChecckStatus";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class FundsItem extends Iitem {

    @property(cc.Label)
    timelabel: cc.Label = null;

    @property(cc.Label)
    typelabel: cc.Label = null;

    @property(cc.Label)
    amountabel: cc.Label = null;

    @property(cc.Label)
    balancebel: cc.Label = null;

    @property(cc.Button)
    contentBtn: cc.Button = null;

    @property(cc.Node)
    contentNode: cc.Node = null;

    @property(cc.Node)
    topNode: cc.Node = null;

    @property(cc.Label)
    contentLabel: cc.Label = null;

    @property(cc.Node)
    lineNode: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    private reDraw: boolean = false;

    onLoad () {
        this.contentBtn.node.on("click",function () {
            if(this.contentLabel.string.isNull())
                return;

            this.contentNode.active = !this.contentNode.active;
            this.lineNode.active = !this.lineNode.active;
            if(this.reDraw)
                this.drawGP();

            this.reDraw = false;
            this.changeContentBtnSprite();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.reDraw = true;
        this.timelabel.string = data.OperatingTime.GameTimeOnyTime();
        this.amountabel.string = data.Amount;
        this.amountabel.node.color = VM.getValue("color."+(parseInt(data.Amount.replaceAll(",","")) < 0 ? "red" : "graylabel3"));
        this.balancebel.string = data.AfterAmount;
        this.contentLabel.string = data.Remark;
        this.contentBtn.node.active = !data.Remark.isNull();
        this.typelabel.string = VM.getValue("fundsname.id"+data.Type);
        this.contentNode.active = false;
        this.changeContentBtnSprite();
        this.reSetHeight();
    }
    
    private changeContentBtnSprite(){
        UI_manager.setSprite(this.contentBtn.target,"own/icon-"+(this.contentNode.active?"reduce":"extend"));
    }
    ///重新設定topNode的高度
    private reSetHeight(){
        this.typelabel._forceUpdateRenderData();
        this.amountabel._forceUpdateRenderData();
        this.balancebel._forceUpdateRenderData();
        let nodeHeight = this.typelabel.node.height;
        nodeHeight = this.amountabel.node.height > nodeHeight ? this.amountabel.node.height : nodeHeight;
        nodeHeight = this.balancebel.node.height > nodeHeight ? this.balancebel.node.height : nodeHeight;
        if(nodeHeight > 40){
            this.topNode.height = nodeHeight+29.1*2;
        }
    }

    private drawGP(){
        var gp = this.contentNode.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0-gp.node.width/2,0-gp.node.height/2,gp.node.width,gp.node.height,16);
        gp.fill();
    }
}
