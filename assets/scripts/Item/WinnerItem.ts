import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class WinnerItem extends Iitem {

    @property(cc.Sprite)
    iconImg: cc.Sprite = null;

    @property(cc.RichText)
    topmsgLabel: cc.RichText = null;

    @property(cc.RichText)
    buttommsgLabel: cc.RichText = null;

    public setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        if(!data.image.isNull() && !data.image.includes(".svg") && !data.image.includes(".gif"))
            UI_manager.setSpriteFormUrl(this.iconImg.node,data.image,null,"universal/icon-default-logo");
        else
            UI_manager.setSprite(this.iconImg.node,"universal/icon-default-logo");
        let msg = data.account +" "+ VM.getValue("lang.play") +" <b> "+ data.name + "</b>";
        this.topmsgLabel.string = msg;
        this.buttommsgLabel.string = VM.getValue("lang.win") +" <color=ebd19a>" +data.pay+"</color> " +VM.getValue("lang.dollar")+"！";
    }
}
