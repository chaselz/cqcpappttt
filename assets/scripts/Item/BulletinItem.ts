import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
const UI_manager = require("UI_manager");
const {ccclass, property} = cc._decorator;
var CtrlAssistant = require("CtrlAssistant");
var UIAssistant = require("UIAssistant");

@ccclass
export default class BulletinItem extends Iitem {

    @property(cc.RichText)
    titleLabe: cc.RichText = null;

    @property(cc.Label)
    contentLabel: cc.Label = null;

    @property(cc.Label)
    timeLabel: cc.Label = null;

    private data : any = null;

    protected start(): void {
        UI_manager.add_button_listen(this.node,this,function () {
            VM.setValue("bulletin.bulletinindex",this.data);
            this.onclik();
        });
    }

    public setValue(data: any, onclickfunc: void, type: any = null) {
        this.data = data;
        super.setValue(data, onclickfunc, type);
        this.setTitle(data.title);
        this.setContent(data.content);
        this.timeLabel.string = data.published_at.GameTimeOnyDay();
    }

    private setTitle(value:string){
        // if(value.length > 12)
        //     value = value.substr(0,12) + "...";
        this.titleLabe.string = "<b>"+ value + "</b>";
    }

    private setContent(value:string){
        let temp = value.replaceAll("&nbsp;","");
        temp = temp.replaceAll("<p>","");
        temp = temp.replaceAll("</p>","");
        temp = temp.replaceAll("<br>"," ");

        temp = temp.replace(/<strong([^>]*)>[^<]*<\/strong>/g, (str, space) => {
            let s = '';
            for (const c of str.slice('<strong'.length + space.length +1, str.length - '<\/strong>'.length)) {
                s += c
            }
            return s
        });

        temp = temp.replace(/<span([^>]*)>[^<]*<\/span>/g, (str, space) => {
            let s = '';
            for (const c of str.slice('<span'.length + space.length +1, str.length - '<\/span>'.length)) {
                s += c;
            }
            return s
        });

        temp = temp.replace(/<a href=([^>]*) rel=[^<]*<\/a>/g, (str, space) => {
            return " "+space+" ";
        });
        // if(temp.length > 24)
        //     temp = temp.substring(0,24) + "...";
        this.contentLabel.string = temp;
    }
}
