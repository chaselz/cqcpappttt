
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var ApiManager = require("ApiManager");

@ccclass
export default class MsgItem extends Iitem {

    @property(cc.Label)
    titleLable: cc.Label = null;

    private msgId:number = 0;
    private readed:number = 0;
    private data:any = null;

    start() {
        this.node.on("click",function () {
            if(!this.readed){
				ApiManager.User.get.ReadedMsg(this.msgId);
            }
			VM.setValue("msg.msgInfo",this.data);
			PageManager.MsgInfoPage();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.msgId = data.id;
        this.data = data;
        this.readed = data.readed;
        this.setTitle(data.title);
        this.readed ? this.setReadMode() : this.setNotReadMode();
    }

    setTitle(value){
        this.titleLable.string = value;
    }

    setReadMode(){
        this.titleLable.node.color = VM.getValue("color.graylabel");
    }
    
    setNotReadMode(){
        this.titleLable.node.color = VM.getValue("color.graylabel3");
    }

}
