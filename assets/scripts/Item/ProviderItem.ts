
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class ProviderItem extends Iitem {

    @property(cc.Sprite)
    gameSprite: cc.Sprite = null;

    @property(cc.Label)
    nameLable: cc.Label = null;

    private game_id:number = 0;
    private display:String = "";
    private platform_name:string = "";
    private code:string = "";
    start() {
        this.node.on("click",function () {
            let isReturn = this.onclik();
            if(isReturn){
                return;
            }
            VM.setValue("hall.platformCode",this.code);
            //是否為PT電子遊戲
            VM.setValue("hall.ptjackpot",this.platform_name == "Playtech");
			VM.setValue("hall.gameTitle",this.display);
			VM.setValue("hall.gameId",this.game_id);
			VM.setValue("hall.gameListLoading","Games");
			ApiManager.Game.get.GameList(this.game_id);
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.code = data.code;
        this.platform_name = data.name;
        this.game_id = data.id;
        this.display = data.display;
        this.nameLable.string = data.display;
        UI_manager.setSprite(this.gameSprite.node,data.imageName);
    }

}
