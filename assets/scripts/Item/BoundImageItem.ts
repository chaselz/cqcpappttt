
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class BoundImageItem extends Iitem {

    @property(cc.Sprite)
    boundSprite: cc.Sprite = null;

    private id:String = null;


    start() {
        this.node.on("click",function () {
            VM.setValue("bound.boundId",this.id);
            this.onclik();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {          //override(覆寫)
        super.setValue(data, onclickfunc, type);        //原本的方法
        this.id = data.id;
        this.setIndex(data.index);
        this.setActive(false);
        // this.setImage(data.mobile_title_img);    //舊版 api 參數
        this.setImage(data.mTitleImg);
    }

    setIndex(value){
        this.node.zIndex = value;
    }

    setActive(value){
        this.node.active = value;
    }

    setMaskHeight(value){
        this.node.height = value < this.node.height ? value : this.node.height;
    }

    setImage(url){
        let self = this;
        if(url == "" || url.length == 0 || url.toLowerCase().indexOf(".gif") != -1 || url.toLowerCase().indexOf(".svg") != -1){
            return;
        }
        cc.loader.load(url,function (err, texture) {
            if(err){
                return;
            }
            self.setActive(true);
            let spriteFrame  = new cc.SpriteFrame(texture);
            self.boundSprite.spriteFrame = spriteFrame;
            let size = spriteFrame.getOriginalSize();
            let image_height = self.node.width / size.width * size.height;      //圖片高度等比例放大縮小
            self.boundSprite.node.height = image_height;
            self.setMaskHeight(image_height);
        });
    }

}
