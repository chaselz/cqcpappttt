import {VM} from "../modelView/ViewModel";
import MobileExtension from "../Extension/Nactive/MobileExtension";
import Iitem from "./Iitem";
import BacOrder from "../AbstractClass/Order/BacOrder";
import KyOrder from "../AbstractClass/Order/KyOrder";
import BgOrder from "../AbstractClass/Order/BgOrder";
import VrOrder from "../AbstractClass/Order/VrOrder";
import BcOrder from "../AbstractClass/Order/BcOrder";
import YgOrder from "../AbstractClass/Order/YgOrder";
import ag15Order from "../AbstractClass/Order/ag15Order";
import mgbbinOrder from "../AbstractClass/Order/mgbbinOrder";
import Xj188Order from "../AbstractClass/Order/Xj188Order";
import aeOrder from "../AbstractClass/Order/aeOrder";
import rgOrder from "../AbstractClass/Order/rgOrder";

var UI_ctrl = require("UI_ctrl");
const {ccclass, property} = cc._decorator;
const UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");
var TitleAlertView = require("TitleAlertView");
var TitleAlertType = require("TitleAlertType");
var UIAssistant = require("UIAssistant");

@ccclass
export default class BetRecordNumItem extends Iitem {
    @property(cc.RichText)
    numlabel: cc.RichText = null;
    @property(cc.Label)
    amountlabel: cc.Label = null;
    @property(cc.Label)
    resultlabel: cc.Label = null;

    @property(cc.Button)
    copyBtn: cc.Button = null;
    @property(cc.Button)
    showBtn: cc.Button = null;
    @property(cc.Node)
    showBtnImg: cc.Node = null;
    @property(cc.Label)
    contentLabel:cc.Label = null;
    @property(cc.Node)
    labelparent: cc.Node = null;
    @property(cc.Node)
    line: cc.Node = null;
    @property(cc.Node)
    contentSpeace: cc.Node = null;
    private onclick : void = null;
    private detailDate = null;
    private code : string = "";
    private gameCat :number;
    protected onEnable(): void {
        this.contentLabel.node.parent.active = false;
        UI_manager.setSprite(this.showBtnImg,"own/icon-more");
    }

    onLoad () {
        if(CC_EDITOR)
            return;
    }

    start () {
        this.setCopyBtn();
        this.setShowBtn();
    }

    /***
     * 設定Item
     * @param data
     * @param onClickFunc
     */
    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.gameCat = data.detail.GameCat;
        this.detailDate = data.detail;
        this.amountlabel.string = data.bet;
        this.setResultLabel(data.profit);
        this.setOrderNum(data.order);
        this.updataLayout();
        this.setDetailInfo(data.detail,data.status);
        this.lineNormalState();
    }

    public lastItemInit(){
        this.line.width = 640;
    }

    private lineNormalState(){
        this.line.width = 576;
    }

    private updataLayout(){
        // this.numlabel._forceUpdateRenderData();
        this.amountlabel._forceUpdateRenderData();
        this.resultlabel._forceUpdateRenderData();
        let height = this.numlabel.node.height;
        if(this.amountlabel.node.height > height)
            height = this.amountlabel.node.height;

        if(this.resultlabel.node.height > height)
            height = this.resultlabel.node.height;

        this.labelparent.height = height;
    }


    private setDetailInfo(data:any,status:number) {
        if(this.checkDetailStatus()){
            this.showBtn.node.active = false;
            return;
        }
        //- 狀態，1:未結算、2:已結算、3:撤單
        if(status === 3){
            this.contentLabel.string = VM.getValue("lang.cancelorder");
            this.showBtn.node.active = true;
            return;
        }
        let order = null;
        switch (VM.getValue("betrecord.targetprovider")) {
            case "ky":
                order = new KyOrder();
                break;
            case "bg":
                order = new BgOrder();
                break;
            case "vr":
                order = new VrOrder();
                break;
            case "bc":
                order = new BcOrder();
                break;
            case "yg":
                order = new YgOrder();
                break;
            case "xj188":
                order = new Xj188Order();
                break;
            case "mg":
            case "bbin":
                order = new mgbbinOrder();
                break;
            case "ag":
                if(this.gameCat === 15 ) {
                    order = new ag15Order();
                    break;
                }
            case "ae":
                order = new aeOrder();
                break;
            case "rg":
                order = new rgOrder();
                break;
            default:
                order = new BacOrder();
                break;
        }

        if (order)
            order.setList(this.contentLabel, data);

        this.showBtn.node.active = order!=null;
    }

    protected drawGP(gp:cc.Graphics){
        gp.node.getComponent(cc.Layout).updateLayout();
        gp.clear();
        gp.roundRect(0-gp.node.width/2,0-gp.node.height/2,gp.node.width,gp.node.height,16);
        gp.fill();
    }


    /***
     * 設定注單號
     * @param str
     */
    private setOrderNum(str){
        //∂let Iswhitelist = VM.getValue("betrecord.whitelist").includes(VM.getValue("betrecord.targetprovider"));
        this.code = str;
        this.numlabel.string = "<on click='onClik'>" + str + "</on>";
        let text = this.numlabel.getComponent("RickTextClick");
        ///判斷是否能看細單白名單
        if(this.hasDetailUrl()) {
            text.setEvent(this.BetSplitCodeLabelClick.bind(this));
            this.numlabel.node.color = VM.getValue("color.gold");
        }else {
            text.setEvent(this.Copy.bind(this));
            this.numlabel.node.color = VM.getValue("color.graylabel3");
        }

    }

    /***
     * 設定輸贏Label
     * @param str
     */
    private setResultLabel(str){
        this.resultlabel.string= str;
        this.resultlabel.node.color = VM.getValue(parseFloat(str) < 0 ? "color.red" : "color.graylabel3");
    }

    private setShowBtn(){
        UI_manager.add_button_listen(this.showBtn.node,this,this.showAndrHideContent);
    }

    private showAndrHideContent(){
        var lastindex = this.node.getSiblingIndex() == this.node.parent.childrenCount -1;
        UI_manager.setSprite(this.showBtnImg,"own/icon-"+(this.contentLabel.node.parent.active ? "more":"close"));
        this.contentLabel.node.parent.active = !this.contentLabel.node.parent.active;
        this.line.active = !this.contentLabel.node.parent.active || lastindex;
        this.contentSpeace.active = this.contentLabel.node.parent.active && lastindex ;
        if(this.contentLabel.node.parent.active)
            this.drawGP(this.contentLabel.node.parent.getComponent(cc.Graphics));
    }
    /***
     * 設定CopyBtn
     */
    private setCopyBtn(){
        UI_manager.add_button_listen(this.copyBtn.node,this,this.Copy);
    }

    /***
     * Copy功能
     * @constructor
     */
    private Copy(){
        MobileExtension.Copy(this.code);
        TitleAlertView.main.show(TitleAlertType.Fulfill,VM.getValue("lang.betslipnum")+VM.getValue("lang.copysuccess"));
    }

    /***
     * 發送注單號明細Api
     */
    public sentOrderApi(){
        if(VM.getValue("core.lock"))
            return;
        cc.log("sentOrderApi");
        UIAssistant.main.SetPanelVisible("Load",true);
        VM.setValue("core.lock",true);
        let type = VM.getValue("betrecord.targetprovider");
        let json = null;
        switch (type) {
            case "vision":
                json = {
                    "order_id" : this.code, //注單單號
                    "lang" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
            case "majasport":
                json = {
                    "bet_id" : this.code, //注單單號
                };
                break;
            case "sb":
                json = {
                    "transaction_id" : this.code, //注單單號
                    "language" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
            case "bbin":
                json = {
                    "game_cat" : "18", //ACC遊戲類別，目前只開放電子17、真人18、捕魚21
                    "wager_id" : this.code, // 注單單號
                    "lang" : "zh-cn", // 語系，zh-cn、zh-tw、en-us
                    "game_code" : "winner", //遊戲代碼
                };
                break;
            case "eBET":
                json = {
                    // "round_code" : this.detailDate.RoundNo, //牌局号
                    "bet_id" : this.code, // 注單號 (eBET 下注纪录 ID, 唯一值)
                    "lang" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
            case "cq9":
                json = {
                    "round_id" : this.code, //局號
                    "game_hall" : VM.getValue("betrecord.targetprovider"), // 遊戲廠商
                    "lang" : "zh-cn", // 語系，zh-cn、zh-tw、en-us
                    "game_code" : VM.getValue("betrecord.targetgame"), //遊戲代碼
                };
                break;
            case "habanero":
                json = {
                    "game_instance_id" : this.code, //局號
                    "lang" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
            case "majaslot":
                json = {
                    "tran_id" : this.code, //局號
                    "lang" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
            case "pinnacle":
                json = {
                    "game_instance_id" : this.code, //局號
                    "lang" : "zh-cn" // 語系，zh-cn、zh-tw、en-us
                };
                break;
        }

        if(json)
            ApiManager.Game.get.GameReportLink(VM.getValue("betrecord.targetprovider"),json);
    }

    /***
     * 注單號RickLabel onClick 事件
     * @constructor
     */
    private BetSplitCodeLabelClick(){
        if(this.onclik)
            this.onclik();
        this.sentOrderApi();
    }

    private hasDetailUrl(){
        var gameProvider = VM.getValue("betrecord.targetprovider");
        switch (gameProvider) {
            case 'vision':
            case 'cq9':
            case 'sb':
            case 'eBET':
            case 'majasport':
            case 'majaslot':
            case 'habanero':
            case 'pinnacle':
                return true;
            case 'bbin':
                // bbin 目前僅開放真人、電子、捕魚
                switch (this.gameCat) {
                    case 17:
                    case 18:
                    case 21:
                        return true;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    private checkDetailStatus () {
        /*
          cq9、sb體育、bbin
            - 另開對外連結細單，不顯示下拉細單內容
          jdb
            - 不顯示下拉細單內容
          AG
            - AG電子(gamecat:10)不顯示下拉細單內容
            - AG捕魚機(gamecat:14)不顯示下拉細單內容
        */
        var gameProvider = VM.getValue("betrecord.targetprovider");
        switch (gameProvider) {
            case 'vision':
            case 'cq9':
            case 'sb':
            case 'bbin':
            case 'majasport':
            case 'majaslot':
            case 'habanero':
            case 'pinnacle':
            case 'jdb':
            case 'leg':
            case 'pt':
                return true;
            case 'ag':
                if (this.gameCat === 10 || this.gameCat === 14) {
                    return true
                } else {
                    return false
                }
            default:
                return false
        }
    }

    // 比對字典檔API
    private gameCodeListFormatter (id) {
        let target = this.gameCodeArray.find(node => node.key === id)
        if (target) return target.name
        else return id
    }


}
