
import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class GameItem extends Iitem {

    @property(cc.Sprite)
    gameSprite: cc.Sprite = null;

    @property(cc.Label)
    nameLable: cc.Label = null;

    private id:number = 0;
    private code:String = "";

    start() {
        this.node.on("click",function () {
            VM.setValue("hall.gameCode",this.code);
            this.onclik();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.id = data.id;
        this.code = data.code;
        this.setGameName(data.name);
        this.setIcon(data.image);
    }

    private setGameName(value){
        this.nameLable.string = value;
    }

    private setIcon(url){
        let self = this;
        UI_manager.setSprite(this.gameSprite.node,"universal/icon-default-logo");
        if(url == "")
            return;
        cc.loader.load(url, function (err, texture) {
            if(err) {
                UI_manager.setSprite(self.gameSprite.node,"universal/icon-default-logo");
                return;
            }
            let sp  = new cc.SpriteFrame(texture);
            self.gameSprite.spriteFrame = sp;
        });
    }

}
