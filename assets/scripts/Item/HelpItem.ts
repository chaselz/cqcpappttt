import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");

@ccclass
export default class HelpItem extends Iitem {

    @property(cc.Label)
    titleLable: cc.Label = null;

    private id:number = 0;

    start() {
        this.node.on("click",function () {
            VM.setValue("help.helpid",this.id);
            PageManager.HelpInfoPage();
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.id = data.id;
        this.setTitle(data.sub_title);
    }

    setTitle(value){
        this.titleLable.string = value;
    }

}