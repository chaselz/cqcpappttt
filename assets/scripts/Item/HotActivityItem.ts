
import Iitem from "./Iitem";
import BhvRollNumber from "../../Behavior/ui/BhvRollNumber";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
var ApiManager = require("ApiManager");

@ccclass
export default class HotActivityItem extends Iitem {

    @property(cc.Sprite)
    img: cc.Sprite = null;

    @property(BhvRollNumber)
    num: BhvRollNumber = null;

    private href : string = "";
    private id : number = -1;
    start() {
        this.node.on("click",function () {
            if(this.id < 0)
                return;

            let code = this.href.split('/')[2];
            VM.setValue("bound.boundId",code);
            PageManager.BoundDetailPage();
            ApiManager.Game.set.ClickHotActivity(this.id);
        },this);
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.href = data.href;
        this.num.targetValue = data.count;

        if(data.id != this.id) {
            this.id = data.id;
            UI_manager.setSpriteFormUrl(this.img.node, data.mImage, cc.Color.WHITE);
        }
    }

}
