
const {ccclass, property} = cc._decorator;

@ccclass
export default class Iitem extends cc.Component {


    protected onclik: void = null;
    /***
     * 設定Item
     * @param data
     * @param onclickfunc
     * @param type Item類型
     */
    public setValue(data:any,onclickfunc:void,type:any = null){
        if(onclickfunc != null) {
            this.onclik = onclickfunc;
        }
    }
}
