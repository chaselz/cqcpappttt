

import Iitem from "./Iitem";
import VMLabel from "../modelView/VMLabel";
import {VM} from "../modelView/ViewModel";
var UI_manager = require("UI_manager");

const {ccclass, property} = cc._decorator;

@ccclass
export default class RechargeBtn extends Iitem {

    @property(VMLabel)
    titlelabel: VMLabel = null;

    @property(cc.Label)
    lowernumlabel: cc.Label = null;

    @property(cc.Label)
    uppernumlabel: cc.Label = null;

    @property(cc.Sprite)
    icon: cc.Sprite = null;

    private mode: string = "";
    public get Mode(){
        return this.mode;
    }

    protected start(): void {
        UI_manager.add_button_listen(this.node,this,function () {
            VM.setValue("recharge.paytype",this.mode);

            if(this.onclik)
                this.onclik();
        });
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.mode = data.mode;
        this.setTitle(data.mode);
        this.setIcon(data.mode);
        this.setLower(data.deposit_minimum);
        this.setUpper(data.deposit_maximum);
        this.node.active = true;
    }

    Hide(){
        this.node.active = false;
    }

    private setTitle(key:string){
        this.titlelabel.setWatchArrValue(["lang."+key+"pay"]);
    }

    private setLower(num:string){
        this.lowernumlabel.string = num;
    }

    private setUpper(num:string){
        this.uppernumlabel.string = num;
    }

    private setIcon(img:string){
        UI_manager.setSprite(this.icon,"payway/icon-"+img);
    }
}
