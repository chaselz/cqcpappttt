import Iitem from "./Iitem";
import {VM} from "../modelView/ViewModel";
import PageManager from "../ManagerUI/PageManager";
const UI_manager = require("UI_manager");
const {ccclass, property} = cc._decorator;
var CtrlAssistant = require("CtrlAssistant");

@ccclass
export default class AdPageItem extends Iitem {

    @property(cc.Sprite)
    AdImg: cc.Sprite = null;

    private href : string = "";
    start () {
        UI_manager.add_button_listen(this.node,this,function () {
            if(this.href.includes("promotion"))
                this.ToBoundDetailView();
            else if(this.href.includes("http"))
                this.ToWebView();
            else if(this.href.isNull())
                return;

        });
    }

    public setValue(data: any, onclickfunc: void, type: any = null) {
        super.setValue(data, onclickfunc, type);
        this.href = data.href;
        if(!data.mobile_img.includes(".gif")&&!data.mobile_img.includes(".svg"))
            UI_manager.setSpriteFormUrl(this.AdImg.node,data.mobile_img,cc.Color.WHITE);

    }

    private ToBoundDetailView(){
        let code = this.href.split('/')[2];
        VM.setValue("bound.boundId",code);
        PageManager.BoundDetailPage();
    }

    private ToWebView(){
        let vc = CtrlAssistant.main.getCtrl("MainCtrl");
        vc.AllScreenWebUrl(this.href);
    }
}
