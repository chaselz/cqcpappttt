import Iitem from "./Iitem";
import SubCheckItem from "./SubCheckItem";
import {VM} from "../modelView/ViewModel";
import {ChecckStatus} from "../Enum/ChecckStatus";
import Awaiter from "../Extension/AwaitExtension";

const {ccclass, property} = cc._decorator;
var UI_manager = require("UI_manager");
@ccclass
export default class CheckItem extends Iitem {

    @property(cc.Label)
    titlelabel: cc.Label = null;

    @property(cc.Sprite)
    checkstateImg: cc.Sprite = null;

    @property(cc.Sprite)
    arrImg: cc.Sprite = null;

    @property(cc.Node)
    lineNode: cc.Node = null;

    @property(cc.Node)
    subNode: cc.Node = null;

    @property(cc.Node)
    nunmoreNode: cc.Node = null;

    @property(cc.Label)
    numLabel: cc.Label = null;

    @property(cc.Label)
    chooseLable: cc.Label = null;

    @property(cc.Node)
    BtnNode: cc.Node = null;

    @property(cc.Node)
    CheckBtn: cc.Node = null;

    protected key : number = 0;
    protected state : ChecckStatus = ChecckStatus.NO;
    protected sub_key = [];
    protected lock:boolean = false;
    public get Status(){
        return this.state;
    }

    protected onLoad(): void {
        this.subNode.getComponentInChildren(SubCheckItem).setValue(0,this.AllSubItemEvent.bind(this));
    }

    protected start(): void {
        this.BtnNode.on("click",function () {
            var stat = this.subNode.active;
            this.subNode.active = !stat;
            UI_manager.setSprite(this.arrImg,"own/"+(stat?"icon-extend":"icon-reduce"));
        },this);
        this.CheckBtn.on("click",async function () {
            if(this.state == ChecckStatus.All){
                await this.setAllCancel();
                this.onclik();
            }else{
                await this.setAll();
                this.onclik();
            }
        },this);
    }

    hideSub(){
        this.subNode.active = false;
    }

    setValue(data: any, onclickfunc: void, type: any = null) {
        this.lock = true;
        super.setValue(data, onclickfunc, type);
        this.CreatItems("SubCheckItem",data,this.SubItemCB.bind(this));
    }

    setKey(_key){
        this.key = _key;
        this.titlelabel.string = VM.getValue("fundsname.id"+_key);
    }

    getKey(){
        return this.key;
    }

    getItems(){
        return this.sub_key;
    }

    async setAll(){
        this.sub_key = await this.getAllSubKeys();
        this.state = ChecckStatus.All;
        await this.setsubAllItems();
        this.changeAllSubItem();
        this.updateChooseLabel();
    }

    async setAllCancel(){
        this.state = ChecckStatus.NO;
        this.sub_key.length = 0;
        await this.setsubAllItems();
        this.changeAllSubItem();
        this.updateChooseLabel();
    }

    private SubItemCB(key){
        if(this.sub_key.includes(key)){
            this.sub_key.remove2(key);
        }else{
            this.sub_key.push(key);
        }
        if(this.state == ChecckStatus.All){
            this.state = ChecckStatus.Section;
        }else{
            if(this.subNode.childrenCount -1 == this.sub_key.length)
                this.state = ChecckStatus.All;
            else if(this.sub_key.length == 0)
                this.state = ChecckStatus.NO;
            else
                this.state = ChecckStatus.Section;
        }
        this.changeAllSubItem();
        this.chanagecheckImg();
        this.updateChooseLabel();
        this.onclik();
    }

    /***
     *
     * @constructor
     */
    private AllSubItemEvent(key){
        switch (this.state) {
            case ChecckStatus.All:
                this.setAllCancel();
                this.onclik();
                break;
            case ChecckStatus.NO:
            case ChecckStatus.Section:
                this.setAll();
                this.onclik();
                break;
        }

    }

    private async getAllSubKeys(){
        await Awaiter.until(_=>!this.lock);
        var temp = [];
        for(var i = 1;i<this.subNode.childrenCount;i++){
            temp.push(this.subNode.children[i].getComponent(SubCheckItem).Key);
        }
        return temp;
    }

    private chanagecheckImg(){
        UI_manager.setSprite(this.checkstateImg,"detail/icon-selectbox-"+(this.state==ChecckStatus.All?"s": this.state == ChecckStatus.NO ?"n":"multi"));
    }

    private async setsubAllItems(){
        await Awaiter.until(_=>!this.lock);
        UI_manager.setSprite(this.checkstateImg,"detail/icon-selectbox-"+(this.state==ChecckStatus.All?"s":"n"));
        for(var i = 1 ;i<this.subNode.childrenCount;i++){
            if(this.state == ChecckStatus.All){
                this.subNode.children[i].getComponent(SubCheckItem).setClickState();
            }else{
                this.subNode.children[i].getComponent(SubCheckItem).setCancelState();
            }
        }
    }

    private changeAllSubItem() {
        this.subNode.children[0].getComponent(SubCheckItem).setValue((this.state == ChecckStatus.All? 9999: 0),null);
    }

    protected updateChooseLabel(){
        let str = "";
        if(this.state == ChecckStatus.All){
            str = VM.getValue("fundsname.id0");
        }else if(this.state == ChecckStatus.NO){
        }else{
            for(var i =0;i<this.sub_key.length;i++){
                if(i>2)
                    break;
                var temp = VM.getValue("fundsname.id"+this.sub_key[i]).split(" - ");
                str += temp[temp.length -1] +"，";
            }
            str = str.substring(0, str.length-1);
        }
        this.chooseLable.string = str;
        this.updateNumAlert(this.sub_key.length > 3 && this.state == ChecckStatus.Section);
    }

    protected updateNumAlert(show:boolean) {
        this.nunmoreNode.active = show;
        if (show) {
            this.numLabel.string = (this.sub_key.length - 3) + "+";
            //更新Label的數值;
            this.numLabel._forceUpdateRenderData();
            this.nunmoreNode.getComponent(cc.Layout).updateLayout();
            this.DrawGPNumNode();
        }
    }

    protected DrawGPNumNode(){
        var gp = this.nunmoreNode.getComponent(cc.Graphics);
        gp.clear();
        gp.roundRect(0 - this.nunmoreNode.width / 2, 0 - this.nunmoreNode.height /2,
            this.nunmoreNode.width,this.nunmoreNode.height,16);
        gp.fill();

    }

    /***
     * 製造及更新物件
     * @param path 父物件
     * @param itemname 物件名
     * @param data 資料
     * @param func 方法
     * @returns {Promise<void>}
     * @constructor
     */
    async CreatItems(itemname,data,func = null,post = false) {
        await data.asyncForEach(async (value,index) =>{
            await this.creatFactory(itemname, index, value, func,post);
        });

        for(let i = data.length+1;i<this.subNode.childrenCount;i++){
            this.subNode.children[i].active = false;
        }
        this.lock = false;
    }
    /***
     * 物件工廠
     * @param itemname	物件名
     * @param index	第幾個
     * @param data	資料
     * @param onClickFunc	點擊事件
     * @param type	物件類別
     * @returns {Promise<void>}
     */
    async creatFactory(itemname,index,data,onClickFunc,post) {
        let item = null;
        if(index + 1 >= this.subNode.childrenCount) {
            item = await UI_manager.create_item_Sync(this.subNode, itemname);
            item.getComponent("Iitem").setValue(data,onClickFunc);
        }else {
            item = this.subNode.children[index + 1];
            item.getComponent("Iitem").setValue(data);
        }
        item.active = true;
    }
}
