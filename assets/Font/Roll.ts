

const {ccclass, property} = cc._decorator;

@ccclass
export default class Roll extends cc.Component {
    @property(cc.Boolean)
    needRoll: Boolean =true;
    @property(cc.Prefab)
    numLayoutPrefab: cc.Prefab =null;
    @property(cc.Node)
    allLayoutNode: cc.Node = null;
    @property(cc.Integer)
    rollTime: number = 0.1;
    @property(cc.Node)
    symbolNodes: cc.Node[] = [];

    prefabPool:cc.NodePool;
    prefabArray = [];
    eachHeight:number = 0;
    private num:number = 0;
    private updateInterval = null;
    private addInterval = null;
    protected onEnable(): void {
        this.setUpdateInterval();
    }

    protected onLoad(): void {
        // 节点池
        this.prefabPool = new cc.NodePool();
        // 预制数组，方便管理
        this.prefabArray = [];
        // 获取每个数字长度，用于获取y坐标
        let numLayout = cc.instantiate(this.numLayoutPrefab);
        this.eachHeight = numLayout.height / 10;
        this.prefabPool.put(numLayout);

    }

    protected onDisable(): void {
        clearInterval(this.updateInterval);
        clearInterval(this.addInterval);
    }

    protected onDestroy(): void {
        clearInterval(this.updateInterval);
        clearInterval(this.addInterval);
    }

    /***
     * 設定取得JackPot資料的計時器
     */
    private setUpdateInterval(){
        this.getPTJackPotData();
        ///5分鐘更新一次;
        this.updateInterval = setInterval(()=>{
            if(this.updateInterval)
                clearInterval(this.updateInterval);
            this.getPTJackPotData();
            },300000);
    }
    /***
     * 取的PT JackPot資料
     */
    private getPTJackPotData(){
        let self = this;
        new Promise((resolve,reject)=>{

            let xhr = new XMLHttpRequest();
            xhr.open("GET", 'https://tickers.playtech.com/js?info=4&currency=CNY',true);

            xhr.onload = function(){
                if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                    var res = xhr.responseText;
                    var conversion = res
                        .slice(res.indexOf('<total local=\\"1\\"'), res.indexOf('</data>'))
                        .split('currency=\\"cny\\">')[1]
                        .split('</amount></total>')[0]

                    cc.log("conversion =",conversion);
                    self.startRollInterval(conversion.replace(".",""))
                }else{
                    resolve(false);
                }
            };
            
            xhr.send();
        });
    }

    /***
     * 開始累加計時器
     * @param num
     */
    private startRollInterval(num:string){
        if(this.addInterval)
            clearInterval(this.addInterval);
        this.num = Number(num);
        this.addInterval = setInterval(()=>{
            this.num++;
            this.setNumLabel(this.num.toString());
        },10);
    }

    /***
     * 設定數字Label
     * @param numStr
     */
    private setNumLabel(numStr:string){

        // 再判断是否可以转换为数字
        let num = Number(numStr);
        if (!isNaN(num)) {
            // 根据numStr长度生成相应数量的预制
            for(let i=0; i<numStr.length; i++) {
                // 将生成的预制放入数组中
                if(numStr.length > this.prefabArray.length )
                    this.prefabArray.push(this.getNewPrefab());
            }
            this.updataSymbolNodesStatus(numStr.length);
            // 执行滚动效果
            for(let i=0; i<numStr.length; i++) {
                this.roll(numStr[i], this.prefabArray[i])
            }
        }
        else {
            console.log('非数字！');
        }
    }

    /***
     * 更新符號狀態
     * @param num_length
     */
    private updataSymbolNodesStatus(num_length:number){
        this.symbolNodes[0].active = num_length > 2;
        this.symbolNodes[1].active = num_length > 5;
        this.symbolNodes[2].active = num_length > 8;
        this.symbolNodes[3].active = num_length > 11;
    }

    /***
     * 取的數字Label
     */
    private getNewPrefab () {
        // 生成预制
        let numLayout = null;
        // 根据节点池大小判断是从预制池中获取还是新生成一个
        if (this.prefabPool.size() > 0)
            numLayout = this.prefabPool.get();
        else
            numLayout = cc.instantiate(this.numLayoutPrefab);
        // 添加到总布局中
        this.allLayoutNode.addChild(numLayout);
        return numLayout;
    }

    /***
     * 滾動個別數字Label;
     * @param num
     * @param prefab
     */
    private roll (num, prefab) {
        // 关键是获取y坐标
        let y = null;

        y = (Number(num)-4.5)*this.eachHeight;
        if(this.needRoll) {
            let moveTo = cc.moveTo(this.rollTime, cc.v2(0, y)).easing(cc.easeCubicActionOut());
            prefab.runAction(moveTo);
        }else
            prefab.y = y;
    }
}
