cc.Class({

    extends: cc.Component,

    properties: {

        speed: 500,

        content: {

            type: cc.Node,

            default: null

        }

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

        if (this.is_scrolling) {

            return;

        }

        this.item_height = this.content.height / 20;

        this.start_pos = this.item_height * 0.5;

        this.now_value = 0;

        this.content.y = this.start_pos;

        this.set_value(2);

        this.rolling_to(1);

    },

    set_value(value) {

        if (value < 0 || value > 9) {

            return;

        }

        this.now_value = value;

        this.content.y = this.start_pos + value * this.item_height;

    },

    rolling_to(value) {

        if (value < 0 || value > 9) {

            return;

        }

        this.is_scrolling = true;

        value = this.now_value > value ? value + 10 : value;

        var move_s = (value - this.now_value) * this.item_height;

        var time = move_s / this.speed;

        var m = cc.moveTo(time, 0, this.content.y + move_s);

        m.easing(cc.easeCubicActionOut());

        var end_func = cc.callFunc(function() {

            this.now_value = (value >= 10) ? (value - 10) : value;

            if (value >= 10) {

                this.content.y -= 10 * this.item_height;

            }

            this.is_scrolling = false;

        }.bind(this));

        var seq = cc.sequence(m, end_func);

        this.content.runAction(seq);

    },

    // update (dt) {},

});